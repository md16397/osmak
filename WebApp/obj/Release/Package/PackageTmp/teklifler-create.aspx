﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Osmak.Master" AutoEventWireup="true" CodeBehind="teklifler-create.aspx.cs" Inherits="WebApp.teklifler_create" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="assets/js/teklifler-create.js"></script>
    <script type="text/javascript" src="assets/js/component/breadcrumb.js"></script>
    <script type="text/javascript" src="assets/js/component/page-header.js"></script>
    <script type="text/javascript" src="assets/js/component/hammadde-dropdown.js"></script>
    <script type="text/javascript" src="assets/js/component/cari-dropdown.js"></script>
    <script type="text/javascript" src="assets/js/component/teklifurunadi-dropdown.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-create-component.js"></script>
    <script type="text/javascript" src="assets/js/component/page-footer.js"></script>

    <script type="text/javascript" src="assets/js/component/boru-agirlik-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/mil-agirlik-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/flans-agirlik-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/kovan-agirlik-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-iptalet-modal.js"></script>

    <script type="text/javascript" src="assets/js/component/teklifcomponent-create-btn.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-hammaddeyarimamulcreate-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-create-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-modify-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/teklifcomponent-delete-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-excel-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/sabit6hamyarimamulgrubu-uyari-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/tamburcreateaspx-uyari-modal.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <page-header></page-header>

    <div class="content">
   
    <teklif-create-component ng-controller="teklifcreateComponentCtrl"></teklif-create-component>
    <teklifcomponent-create-btn ng-controller="teklifcomponentCreateBtnCtrl"></teklifcomponent-create-btn>

    <page-footer></page-footer>

    <boru-agirlik-modal></boru-agirlik-modal>
    <mil-agirlik-modal></mil-agirlik-modal>
    <flans-agirlik-modal></flans-agirlik-modal>
    <kovan-agirlik-modal></kovan-agirlik-modal>
    <teklif-iptalet-modal></teklif-iptalet-modal>
    <teklif-create-modal></teklif-create-modal>
        
    <teklif-modify-modal></teklif-modify-modal>  
    <maliyet-delete-modal></maliyet-delete-modal>
    <teklifcomponent-delete-modal></teklifcomponent-delete-modal>
    <teklif-excel-modal></teklif-excel-modal>
    <sabit6hamyarimamulgrubu-uyari-modal></sabit6hamyarimamulgrubu-uyari-modal>
    <tamburcreateaspx-uyari-modal></tamburcreateaspx-uyari-modal>
    <teklif-hammaddeyarimamulcreate-modal></teklif-hammaddeyarimamulcreate-modal>  
	</div>

</asp:Content>

       

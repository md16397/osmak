﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Osmak.Master" AutoEventWireup="true" CodeBehind="teklifler.aspx.cs" Inherits="WebApp.teklifler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="assets/js/teklif.js?v=0.0.1"></script>
    <!-- Angular Components -->
    <script type="text/javascript" src="assets/js/component/breadcrumb.js"></script>
    <script type="text/javascript" src="assets/js/component/page-header.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-list.js"></script>
    <script type="text/javascript" src="assets/js/component/page-footer.js"></script>
    <script type="text/javascript" src="assets/js/component/hammadde-dropdown.js"></script>
    <script type="text/javascript" src="assets/js/component/cari-dropdown.js"></script>
    <script type="text/javascript" src="assets/js/component/kullanici-dropdown.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-state-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-create-btn.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-report-btn.js"></script>

    <script type="text/javascript" src="assets/js/component/teklif-report-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-modify-component.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-delete-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-detail-component.js"></script>

    <!-- /angular Components -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <page-header></page-header>
    <!-- Content -->
    <div class="content">

    <teklif-list ng-controller="teklifListCtrl"></teklif-list>
    <teklif-create-btn ng-controller="teklifCreateBtnCtrl"></teklif-create-btn>
    <teklif-report-btn ng-controller="teklifReportBtnCtrl"></teklif-report-btn>
    <page-footer></page-footer>
    <teklif-report-modal></teklif-report-modal>
    <teklif-state-modal></teklif-state-modal>
    <teklif-delete-modal></teklif-delete-modal>

	</div>
    <!-- /content -->
</asp:Content>

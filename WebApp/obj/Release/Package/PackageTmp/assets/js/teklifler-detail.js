﻿var page = {
    data: {
        breadcrumbs: {
            home: {
                name: 'Anasayfa',
                href: '#'
            },
            links: [],
            active: {
                name: 'Teklif Detay Ekranı'
            }
        },
        header: {
            name: 'Teklifler',
            detail: 'Teklif Detay Ekranı'
        }
    }
};
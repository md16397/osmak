﻿var page = {
    data: {
        breadcrumbs: {
            home: {
                name: 'Anasayfa',
                href: '#'
            },
            links: [],
            active: {
                name: 'Teklif İşlemleri'
            }
        },
        header: {
            name: 'Teklifler',
            detail: 'Teklif İşlemleri'
        },
          karyuzdesi: [{
            name: '%0',
            value: 0
        }, {
            name: '%25',
            value: 20
        }, {
            name: '%50',
            value: 50
        }, {
            name: '%18',
            value: 1.18
        }
        ],
        //olcubirimi2: [{
        //    name: 'Metre',
        //    value: 'metre',
        //}, {
        //    name: 'Uzunluk',
        //    value: 'uzunluk',
        //    }, 
        //   {
        //        name: 'Kg',
        //        value: 'kg',
        //    },
        //    {
        //        name: 'Adet',
        //        value: 'adet',
        //    },
        //]
        onaydurumu: [
            {
                name: '',
                value: 2
            },
            {
                name: 'Onaylanmış',
                value: 1
            }, {
                name: 'Onaylanmamış',
                value: 0
            }
        ]
    }
};


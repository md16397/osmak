﻿app.angular
    .controller('ipDeleteModalCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
      
        $rootScope.showDeleteModal = function (ipData) {
            $scope.ip.set(ipData);
            $('.ip-delete-modal:first').modal({
                backdrop: 'static'
            });
        };
        $scope.ip = {
            rawdata: null,
            id: {
                value: 0,
                init: function () {
                    $scope.ip.id.set(0);
                },
                set: function (value) {
                    $scope.ip.id.value = value;
                }
            },
            address: {
                value: '',
                init: function () {
                    $scope.ip.address.value = '';
                    $scope.ip.address.set('');
                },
                set: function (value) {
                    $scope.ip.address.value = value;
                }
            },
            port: {
                value: '',
                init: function () {
                    $scope.ip.port.value = '';
                    $scope.ip.port.set('');
                },
                set: function (value) {
                    $scope.ip.port.value = value;
                }
            },
            protocol: {
                value: '',
                init: function () {
                    $scope.ip.protocol.value = '';
                    $scope.ip.protocol.set('');
                },
                set: function (value) {
                    $scope.ip.protocol.value = value;
                }
            },
            local: {
                name: 'Genel IP',
                value: false,
                init: function () {
                    $scope.ip.local.select($scope.ip.local.no);
                },
                select: function (local) {
                    $scope.ip.local.name = local.name;
                    $scope.ip.local.value = local.value;
                },
                set: function (value) {
                    $scope.ip.local.select(value ? $scope.ip.local.yes : $scope.ip.local.no);
                },
                yes: {
                    name: 'Lokal IP',
                    value: true,
                    anchor: {
                        cls: ''
                    }
                },
                no: {
                    name: 'Genel IP',
                    value: false,
                    anchor: {
                        cls: ''
                    }
                }
            },
            state: {
                name: 'Aktif',
                value: true,
                icon: {
                    cls: 'icon-checkmark-circle2 text-success'
                },
                init: function () {
                    $scope.ip.state.select($scope.ip.state.active);
                },
                select: function (state) {
                    $scope.ip.state.name = state.name;
                    $scope.ip.state.value = state.value;
                    $scope.ip.state.icon.cls = state.icon.cls.replace(/ pull-right/g, '');
                },
                set: function (value) {
                    $scope.ip.state.select(value ? $scope.ip.state.active : $scope.ip.state.passive);
                },
                active: {
                    name: 'Aktif',
                    value: true,
                    icon: {
                        cls: 'icon-checkmark-circle2 text-success'
                    },
                    anchor: {
                        cls: ''
                    }
                },
                passive: {
                    name: 'Pasif',
                    value: false,
                    icon: {
                        cls: 'icon-close2 text-danger'
                    },
                    anchor: {
                        cls: ''
                    }
                }
            },
            set: function (ipData) {
                $scope.ip.rawdata = ipData;
                $scope.ip.id.set(ipData.ID);
                $scope.ip.address.set(ipData.ADDRESS);
                $scope.ip.port.set(ipData.PORT);
                $scope.ip.protocol.set(ipData.PROTOCOL);
                $scope.ip.local.set(ipData.LOCALVAL);
                $scope.ip.state.set(ipData.ACTIVEVAL);
            },
            init: function () {
                $scope.ip.id.init();
                $scope.ip.address.init();
                $scope.ip.port.init();
                $scope.ip.protocol.init();
                $scope.ip.local.init();
                $scope.ip.state.init();
                $scope.ip.rawdata = null;
            },
            delet: function () {
                app.show.spinner($('ip-delete-modal > div:first > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/ip.ashx?&t=5&d=' + new Date(),
                    data: {
                        id: $scope.ip.id.value
                    }
                }).then(function (response) {
                    app.hide.spinner($('ip-delete-modal > div:first > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 1) {//success
                        $rootScope.deleteRow(response.data);
                        $scope.ip.init();
                        $('.ip-delete-modal:first').modal('hide');
                        app.show.success('Ip Silindi!');//'Ip Silindi!'
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to modify a user
                        window.location.href = 'login.aspx';
                        return;
                    }
                    //if running code come through this far then it means an unexpected error occurred
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'

                }, function (response) {
                    console.log(response);
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                    app.hide.spinner($('ip-delete-modal > div:first > div:first'));
                });
            }
        };
    }])
    .directive('ipDeleteModal', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/ip-delete-modal.html'
        };
    });
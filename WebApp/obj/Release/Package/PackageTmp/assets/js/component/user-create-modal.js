﻿app.angular
    .controller('userCreateModalCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
  
        $scope.user = {
            username: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.user.username.isValid = false;
                    $scope.user.username.value = '';
                    $scope.user.username.cls = '';
                    $scope.user.username.msg = '';
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.user.username.value);
                    if (applyCheck || $scope.user.username.isValid !== check) {
                        $scope.user.username.isValid = check;
                        if ($scope.user.username.isValid) {
                            $scope.user.username.success();
                        } else {
                            $scope.user.username.error('* Geçersiz Kullanıcı Adı');//'* Geçersiz Kullanıcı Adı'
                        }
                    }
                    return $scope.user.username.isValid;
                },
                change: function () {
                    $scope.user.username.check();
                },
                success: function () {
                    $scope.user.username.cls = '';
                    $scope.user.username.msg = '';
                },
                error: function (msg) {
                    $scope.user.username.cls = 'has-error';
                    $scope.user.username.msg = msg;
                }
            },
            type: {
                list: page.data.usertypes,
                isValid: false,
                selected: null,
                cls: '',
                msg: '',
                init: function () {
                    $scope.user.type.isValid = false;
                    $scope.user.type.selected = null;
                    $scope.user.type.cls = '';
                    $scope.user.type.msg = '';
                },
                check: function (applyCheck) {
                    var check = $scope.user.type.selected && $scope.user.type.selected.hasOwnProperty('value') && typeof $scope.user.type.selected.value !== 'undefined' && $scope.user.type.selected.value != null ? true : false;
                    if (applyCheck || $scope.user.type.isValid !== check) {
                        $scope.user.type.isValid = check;
                        if ($scope.user.type.isValid) {
                            $scope.user.type.success();
                        } else {
                            $scope.user.type.error('* Geçersiz Kullanıcı Tipi');//'* Geçersiz Kullanıcı Tipi'
                        }
                    }
                    return $scope.user.type.isValid;
                },
                change: function () {
                    $scope.user.type.check();
                    $scope.user.startpage.filter();
                },
                success: function () {
                    $scope.user.type.cls = '';
                    $scope.user.type.msg = '';
                },
                error: function (msg) {
                    $scope.user.type.cls = 'has-error';
                    $scope.user.type.msg = msg;
                }
            },
            name: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.user.name.isValid = false;
                    $scope.user.name.value = '';
                    $scope.user.name.cls = '';
                    $scope.user.name.msg = '';
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.user.name.value);
                    if (applyCheck || $scope.user.name.isValid !== check) {
                        $scope.user.name.isValid = check;
                        if ($scope.user.name.isValid) {
                            $scope.user.name.success();
                        } else {
                            $scope.user.name.error('* Geçersiz Ad');//'* Geçersiz Ad'
                        }
                    }
                    return $scope.user.name.isValid;
                },
                change: function () {
                    $scope.user.name.check();
                },
                success: function () {
                    $scope.user.name.cls = '';
                    $scope.user.name.msg = '';
                },
                error: function (msg) {
                    $scope.user.name.cls = 'has-error';
                    $scope.user.name.msg = msg;
                }
            },
            surname: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.user.surname.isValid = false;
                    $scope.user.surname.value = '';
                    $scope.user.surname.cls = '';
                    $scope.user.surname.msg = '';
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.user.surname.value);
                    if (applyCheck || $scope.user.surname.isValid !== check) {
                        $scope.user.surname.isValid = check;
                        if ($scope.user.surname.isValid) {
                            $scope.user.surname.success();
                        } else {
                            $scope.user.surname.error('* Geçersiz Soyad');//'* Geçersiz Soyad'
                        }
                    }
                    return $scope.user.surname.isValid;
                },
                change: function () {
                    $scope.user.surname.check();
                },
                success: function () {
                    $scope.user.surname.cls = '';
                    $scope.user.surname.msg = '';
                },
                error: function (msg) {
                    $scope.user.surname.cls = 'has-error';
                    $scope.user.surname.msg = msg;
                }
            },
            startpage: {
                filter: function () {
                    $scope.user.startpage.list = page.fn.startpage.filter($scope.user.type.selected);
                    $scope.user.startpage.init();
                },
                list: [],
                isValid: false,
                selected: null,
                cls: '',
                msg: '',
                init: function () {
                    $scope.user.startpage.selected = null;
                    $scope.user.startpage.isValid = false;
                    $scope.user.startpage.cls = '';
                    $scope.user.startpage.msg = '';
                },
                check: function (applyCheck) {
                    var check = $scope.user.startpage.selected && $scope.user.startpage.selected.hasOwnProperty('value') && typeof $scope.user.startpage.selected.value !== 'undefined' && $scope.user.startpage.selected.value != null ? true : false;
                    if (applyCheck || $scope.user.startpage.isValid !== check) {
                        $scope.user.startpage.isValid = check;
                        if ($scope.user.startpage.isValid) {
                            $scope.user.startpage.success();
                        } else {
                            $scope.user.startpage.error('* Geçersiz Başlangıç Sayfası');//'* Geçersiz Başlangıç Sayfası'
                        }
                    }
                    return $scope.user.startpage.isValid;
                },
                change: function () {
                    $scope.user.startpage.check();
                },
                success: function () {
                    $scope.user.startpage.cls = '';
                    $scope.user.startpage.msg = '';
                },
                error: function (msg) {
                    $scope.user.startpage.cls = 'has-error';
                    $scope.user.startpage.msg = msg;
                }
            },
            password: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.user.password.value = '';
                    $scope.user.password.isValid = false;
                    $scope.user.password.cls = '';
                    $scope.user.password.msg = '';
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.user.password.value);
                    if (applyCheck || $scope.user.password.isValid !== check) {
                        $scope.user.password.isValid = check;
                        if ($scope.user.password.isValid) {
                            $scope.user.password.success();
                        } else {
                            $scope.user.password.error('* Geçersiz Şifre');//'* Geçersiz Şifre'
                        }
                    }
                    if ($scope.user.password.isValid) {
                        if ($scope.user.password.again.value !== $scope.user.password.value) {
                            $scope.user.password.again.isValid = false;
                            $scope.user.password.again.error('* Şifre alanı ile uyuşmuyor.');//'* Şifre alanı ile uyuşmuyor.'
                        } else if (!$scope.user.password.again.isValid && $scope.user.password.again.value === $scope.user.password.value) {
                            $scope.user.password.again.success();
                        }
                    }
                    return $scope.user.password.isValid;
                },
                change: function () {
                    $scope.user.password.check();
                },
                success: function () {
                    $scope.user.password.cls = '';
                    $scope.user.password.msg = '';
                },
                error: function (msg) {
                    $scope.user.password.cls = 'has-error';
                    $scope.user.password.msg = msg;
                },
                again: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.user.password.again.value = '';
                        $scope.user.password.again.isValid = false;
                        $scope.user.password.again.cls = '';
                        $scope.user.password.again.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.user.password.again.value);
                        if (applyCheck || $scope.user.password.again.isValid !== check) {
                            $scope.user.password.again.isValid = check;
                            if ($scope.user.password.again.isValid) {
                                $scope.user.password.again.success();
                            } else {
                                $scope.user.password.again.error('* Geçersiz Şifre Tekrar');//'* Geçersiz Şifre Tekrar'
                            }
                        }
                        if ($scope.user.password.again.isValid) {
                            if ($scope.user.password.again.value !== $scope.user.password.value) {
                                $scope.user.password.isValid = false;
                                $scope.user.password.error('* Şifre Tekrar alanı ile uyuşmuyor.');//'* Şifre Tekrar alanı ile uyuşmuyor.'
                            } else if (!$scope.user.password.isValid && $scope.user.password.again.value === $scope.user.password.value) {
                                $scope.user.password.success();
                            }
                        }
                        return $scope.user.password.again.isValid;
                    },
                    change: function () {
                        $scope.user.password.again.check();
                    },
                    success: function () {
                        $scope.user.password.again.cls = '';
                        $scope.user.password.again.msg = '';
                    },
                    error: function (msg) {
                        $scope.user.password.again.cls = 'has-error';
                        $scope.user.password.again.msg = msg;
                    }
                }
            },
            state: {
                name: 'Aktif',
                value: true,
                icon: {
                    cls: 'icon-user-check text-success'
                },
                init: function () {
                    $scope.user.state.select($scope.user.state.active);
                },
                select: function (state) {
                    $scope.user.state.name = state.name;
                    $scope.user.state.value = state.value;
                    $scope.user.state.icon.cls = state.icon.cls.replace(/ pull-right/g, '');
                },
                active: {
                    name: 'Aktif',
                    value: true,
                    icon: {
                        cls: 'icon-user-check text-success'
                    },
                    anchor: {
                        cls: ''
                    }
                },
                passive: {
                    name: 'Pasif',
                    value: false,
                    icon: {
                        cls: 'icon-user-block text-danger'
                    },
                    anchor: {
                        cls: ''
                    }
                }
            },
            init: function () {
                $scope.user.username.init();
                $scope.user.type.init();
                $scope.user.name.init();
                $scope.user.surname.init();
                $scope.user.password.init();
                $scope.user.password.again.init();
                $scope.user.state.init();
            },
            check: function () {
                var applyCheck = true;
                var OK = {
                    username: $scope.user.username.check(applyCheck),
                    type: $scope.user.type.check(applyCheck),
                    name: $scope.user.name.check(applyCheck),
                    surname: $scope.user.surname.check(applyCheck),
                    password: $scope.user.password.check(applyCheck),
                    passwordAgain: $scope.user.password.again.check(applyCheck)
                };
                return OK.username && OK.type && OK.name && OK.surname && OK.password && OK.passwordAgain;
            },
            save: function () {
                if (!$scope.user.check()) {
                    return;
                }
                app.show.spinner($('user-create-modal > div:first > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/user.ashx?&t=3&d=' + new Date(),
                    data: {
                        username: $scope.user.username.value,
                        type: $scope.user.type.selected.value,
                        name: $scope.user.name.value,
                        surname: $scope.user.surname.value,
                      //  startpage: $scope.user.startpage.selected.value,
                        password: $scope.user.password.value,
                        passwordAgain: $scope.user.password.again.value,
                        state: $scope.user.state.value
                    }
                }).then(function (response) {
                    app.hide.spinner($('user-create-modal > div:first > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 1) {//success
                        $scope.user.init();
                        app.show.success('Yeni kullanıcı oluşturuldu!');//'Yeni kullanıcı oluşturuldu!'
                        $rootScope.loadList();
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6 || response.data['RESULT'] === 13) {//user is not exist or passive or user has no right to create a user
                        window.location.href = 'login.aspx';
                        return;
                    }
                    if (response.data['RESULT'] === 9) {//username is invalid
                        $scope.user.username.isValid = false;
                        $scope.user.username.error('* Geçersiz Kullanıcı Adı');//'* Geçersiz Kullanıcı Adı'
                        return;
                    }
                    if (response.data['RESULT'] === 10) {//name is invalid
                        $scope.user.name.isValid = false;
                        $scope.user.name.error('* Geçersiz Ad');//'* Geçersiz Ad'
                        return;
                    }
                    if (response.data['RESULT'] === 11) {//surname is invalid
                        $scope.user.surname.isValid = false;
                        $scope.user.surname.error('* Geçersiz Soyad');//'* Geçersiz Soyad'
                        return;
                    }
                    //if running code come through this far then it means an unexpected error occurred
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                    /*if (response.data['RESULT'] === 16 || response.data['RESULT'] === 2 || response.data['RESULT'] === 14 || response.data['RESULT'] === 12 || response.data['RESULT'] === 15) {//type or password or company or startpage or language is invalid
                        app.show.error(systemMessages[341]);//'Bir şeyler ters gitti...'
                        return;
                    }*/
                }, function (response) {
                    console.log(response);
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                    app.hide.spinner($('user-create-modal > div:first > div:first'));
                });
            }
        };
    }])
    .directive('userCreateModal', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/user-create-modal.html'
        };
    });
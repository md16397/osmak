﻿app.angular
    .component('teklifCreateModal', {
        templateUrl: 'assets/template/teklif-create-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', '$localStorage', function ($scope, $http, $timeout, $rootScope, $localStorage) {
            //timeLeft: number = 0;
            //var interval;
            //this.interval = setInterval(() => {
            //    if (this.timeLeft === 0) {
            //        this.timeLeft++;
            //    } else {
            //        this.timeLeft = 0;
            //    }
            //}, 10000)
            //$(document).ready(function () {
            //    $.ajax({
            //        type: "GET",
            //        url: "https://www.tcmb.gov.tr/kurlar/today.xml",
            //        'Access-Control-Allow-Credentials': true,
            //        dataType: "xml",
            //        success: function (xml) {
            //            var xmlText = new XMLSerializer().serializeToString(xml);
            //            // console.log("xmlText", xmlText);
            //            var x2js = new X2JS();
            //            var aftCnv = x2js.xml_str2json(xmlText);
            //            // return aftCnv;
            //            //console.log(aftCnv); // JSON DATA
            //            var dolar = aftCnv.Tarih_Date.Currency[0].ForexSelling;
            //            $rootScope.getdolar = dolar; // teklif-modify-modal için
            //            $scope.dolar = dolar;
            //            console.log(dolar); // DOLAR 
            //            var euro = aftCnv.Tarih_Date.Currency[3].ForexSelling;
            //            $rootScope.geteuro = euro;
            //            $scope.euro = euro;
            //            console.log(euro); // EURO 
            //            var xmlTextNode = document.createTextNode(xmlText);
            //            var parentDiv = document.getElementById('xml');
            //            // parentDiv.appendChild(xmlTextNode);
            //        }
            //    });
            //});  
            $http({ // dolar ve euro kurları
                method: "GET",
                url: 'hub/teklif.ashx?&t=13&d=' + new Date(), // dolar ve euro kurlarını çekmek için TYPE 13 gönderilir. Teklif.cs ye..
                data: "{}",
                contentType: "application/json",
                dataType: "json",
            }).then(function (response) {
                console.log(response.data);
                $scope.dolaralisfiyati = response.data.DOLARALISFIYATI;
                $scope.dolarsatisfiyati = response.data.DOLARSATISFIYATI;
                $scope.euroalisfiyati = response.data.EUROALISFIYATI;
                $scope.eurosatisfiyati = response.data.EUROSATISFIYATI;
                $scope.teklif.dolareurotarihi = "* Bu kurlar Merkez Bankasının " + response.data.Tarih + " tarihli verilerinden alınmıştır.";
                console.log($scope.euroalisfiyati);
            });
            $rootScope.showCreateTeklifModal = function () {
                $scope.teklif.selecthesaplabutonu = false;
                $scope.teklif.isExist = false; // rapor almak için isExist true olmalı.
                $scope.teklif.detail.inputs.hammadde.init();
                $scope.teklif.init();
                $scope.teklif.detail.inputs.init();
                //
                $scope.teklif.adidisabled = true;
                $scope.teklif.miktardisabled = true;
                localStorage.removeItem('boruagirlik');
                localStorage.removeItem('milagirlik');
                localStorage.removeItem('flansagirlik');
                localStorage.removeItem('kovanagirlik');
                $('.teklif-create-modal:first').modal({
                    backdrop: 'static'
                });
            };
            $rootScope.boruagirlik = function () { // boru\mil\flanş ağırlık modaldan gelen dataları Miktar inputuna set etmek için kullanılır.
                $scope.teklif.detail.inputs.miktar.change();
            },
                $rootScope.teklif_create_modal_hammaddeyarimamulgrubu = function (hammaddeyarimamulgrubu) {//teklif-hammaddeyarimamul-create-modal.js yeni eklenen bir grup var ise bu fonksiyon çalışmalı.Ve yeni eklenen hammade/yarimamul grubu set edilmeli.
                    var yenieklenenhammaddeyarimamulgrubu = hammaddeyarimamulgrubu;
                    console.log(yenieklenenhammaddeyarimamulgrubu);
                    if (yenieklenenhammaddeyarimamulgrubu !== null) {
                        $scope.teklif.detail.inputs.hammadde.isValid = true;
                        $scope.teklif.detail.inputs.hammadde.selected = yenieklenenhammaddeyarimamulgrubu;
                        $scope.teklif.detail.inputs.hammadde.cls = '';
                        $scope.teklif.detail.inputs.hammadde.msg = '';
                        $scope.teklif.detail.inputs.hammadde.inputValue = yenieklenenhammaddeyarimamulgrubu.ADI;
                        $scope.teklif.detail.inputs.hammadde.shouldLoadOptionsOnInputFocus = false;
                        $scope.teklif.detail.inputs.hammadde.select(yenieklenenhammaddeyarimamulgrubu);
                        return;
                    }
                },
                $rootScope.teklifidExist = 0;
            $scope.teklif = {
                count: 0,
                sendcreate: 'create-boru',
                rowisExist: false,
                selecthesaplabutonu: false,
                adidisabled: true,
                miktardisabled: true,
                controltable: false,
                teklifadi2: '',
                birimsatis: '',
                adet: '',
                karyuzdesi: '',
                onaydurumu: false,
                toplamsatis: '',
                toplamteklif: '',
                tables: [],
                getdata: [],
                aciklama: '', //create-componentten gelen aciklama null ise aciklama.value = '' olmalı.
                ilkeklenenkayit: '',
                teklifkodukontrolu: '',
                dolareurotarihi: '',
                //
                hesapla: function () {  // teklif-create-modal.html de yer alan Hesapla butonunun aktif olabilmesi için BORU, MİL yada FLANŞ seçilmelidir.
                    if ($scope.teklif.detail.inputs.hammadde.inputValue.match("BORU")) {
                        $rootScope.showBoruModal($scope.teklif.sendcreate, $scope.teklif.detail.inputs.olcubirimi.selected, $scope.teklif.detail.inputs.girilenfiyat.value);
                    }
                    if ($scope.teklif.detail.inputs.hammadde.inputValue.match("MİL")) {
                        $rootScope.showMilModal($scope.teklif.sendcreate);
                    }
                    if ($scope.teklif.detail.inputs.hammadde.inputValue.match("FLANŞ")) {
                        $rootScope.showFlansModal($scope.teklif.sendcreate);
                    }
                    if ($scope.teklif.detail.inputs.hammadde.inputValue.match("KOVAN")) { //mil için kullanılan ağırlık hesabı kullanılıyor.
                        $rootScope.showKovanModal($scope.teklif.sendcreate);
                    }
                },
                detail: { // teklife ait kalemlerdir.
                    isValid: false,
                    isExist: false,
                    cls: '',
                    msg: '',
                    rows: [],
                    //rows2: [], //teklif-create-component 
                    init: function () { // dolu inputlar boş olur.
                        $scope.teklif.detail.isValid = false;
                        $scope.teklif.detail.isExist = false;
                        $scope.teklif.detail.cls = '';
                        $scope.teklif.detail.msg = '';
                        $scope.teklif.detail.rows = [];
                        $scope.teklif.detail.inputs.init();
                    },
                    check: function (applyCheck) { // inputların dolu yada boş olmasını kontrol eder.
                        var check = $scope.teklif.detail.rows === null || typeof $scope.teklif.detail.rows === 'undefined' || !$scope.teklif.detail.rows.length ? false : true;
                        if (applyCheck || $scope.teklif.detail.isValid !== check) {
                            $scope.teklif.detail.isValid = check;
                            if ($scope.teklif.detail.isValid) {
                                $scope.teklif.detail.success();
                            } else {
                                $scope.teklif.detail.error('* En az bir satır girmelisiniz!');
                            }
                        }
                        return $scope.teklif.detail.isValid;
                    },
                    success: function () {
                        $scope.teklif.detail.cls = '';
                        $scope.teklif.detail.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.detail.cls = 'has-error';
                        $scope.teklif.detail.msg = msg;
                    },
                    remove: function (index, row, addToRemovedRows) { // teklif-create-modal.html.
                        localStorage.removeItem('boruagirlik');
                        localStorage.removeItem('milagirlik');
                        localStorage.removeItem('flansagirlik');
                        localStorage.removeItem('kovanagirlik');
                        //
                        $scope.teklif.detail.inputs.toplamtutar.value = $scope.teklif.detail.inputs.toplamtutar.value - $scope.teklif.detail.rows[index].TUTARVAL;
                        $scope.teklif.birimsatisfiyati.value = $scope.teklif.detail.inputs.toplamtutar.value;
                        $scope.teklif.birimsatisfiyati.text = $scope.teklif.detail.inputs.toplamtutar.value.formatMoney(2, ',', '.');
                        //
                        $scope.teklif.birimsatisveadet.value = $scope.teklif.birimsatisfiyati.value + (($scope.teklif.birimsatisfiyati.value / 100) * $scope.teklif.karyuzdesi.value);
                        //Birim Maliyet Fiyatının Kar yüzdesini kendisine ilave edip Birim Satış Fiyatına eşitle.
                        //Birim Satış Fiyatı ile Adet çarpımı Toplam Teklif Tutarı olacak.
                        $scope.teklif.toplamsatisfiyati.value = $scope.teklif.birimsatisveadet.value;
                        $scope.teklif.toplamsatisfiyati.text = $scope.teklif.toplamsatisfiyati.value.formatMoney(2, ',', '.');
                        $scope.teklif.toplamtekliftutari.value = $scope.teklif.toplamsatisfiyati.value * $scope.teklif.adet.value;
                        $scope.teklif.toplamtekliftutari.text = $scope.teklif.toplamtekliftutari.value.formatMoney(2, ',', '.');
                        //
                        $scope.teklif.detail.rows.splice(index, 1);
                        $scope.teklif.detail.isExist = $scope.teklif.detail.rows.length > 0;
                        if ($scope.teklif.detail.inputs.hammadde.selected.ADI === 'RULMAN' || $scope.teklif.detail.inputs.hammadde.selected.ADI === 'RULMAN YATAĞI') {
                            $scope.teklif.miktardisabled = false;
                            $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                            $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                            return;
                        }
                    },
                    modify: function (index, row) {
                        if (!$scope.teklif.detail.check()) {
                            return;
                        }
                        $http({ // ADGIRILSINMI ve MIKTARGIRILSINMI datalarını çekmek için.(AD YADA MIKTAR INPUTLARI DISABLED OLMASI İÇİN.)
                            method: 'POST',
                            url: 'hub/urun.ashx?&t=1&d=' + new Date(),
                            data: {
                                urunid: row.HAMMADDEYARIMAMULID,
                            }
                        }).then(function (response) {
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                                window.location.href = 'login.aspx';
                                return;
                            }
                            console.log(response.data);
                            $scope.urungelendata = response.data;

                            $scope.teklif.detail.inputs.set(index, row);
                            $scope.teklif.detail.remove(index, row);

                        }, function (response) {
                            app.show.error('Bir şeyler ters gitti...');
                        });
                    },
                    inputs: {
                        hammadde: '',
                        yarımamuladi: '',
                        olcubirimi: '',
                        miktar: '',
                        agirlik: '',
                        boruagirlik: '',
                        birimliste: '',
                        tutar: '',
                        isExistAdet: true, // Adet girildiyse Girilen Fiyat pasif olmalı ?
                        id: {
                            value: 0,
                            init: function () {
                                $scope.teklif.detail.inputs.id.value = '';
                            },
                            set: function (id) {
                                $scope.teklif.detail.inputs.id.value = id;
                            },
                        },
                        hammadde: {
                            isValid: false,
                            selected: '',
                            cls: '',
                            msg: '',
                            inputValue: '',
                            shouldLoadOptionsOnInputFocus: true,
                            init: function () {
                                $scope.teklif.detail.inputs.hammadde.isValid = false;
                                $scope.teklif.detail.inputs.hammadde.selected = '';
                                $scope.teklif.detail.inputs.hammadde.cls = '';
                                $scope.teklif.detail.inputs.hammadde.msg = '';
                                $scope.teklif.detail.inputs.hammadde.inputValue = '';
                                $scope.teklif.detail.inputs.hammadde.shouldLoadOptionsOnInputFocus = true;
                            },
                            set: function (hammadde) {
                                console.log(hammadde); //ADI
                                if ($scope.urungelendata.ADGIRILSINMI === false && $scope.urungelendata.MIKTARGIRILSINMI === false) {
                                    $scope.teklif.adidisabled = false;
                                    $scope.teklif.miktardisabled = false;
                                    $scope.teklif.detail.inputs.hammadde.isValid = true;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.cls = '';
                                    $scope.teklif.detail.inputs.hammadde.msg = '';
                                    $scope.teklif.detail.inputs.hammadde.inputValue = hammadde.ADI;
                                    $scope.teklif.detail.inputs.hammadde.shouldLoadOptionsOnInputFocus = false;
                                    return;
                                }
                                if ($scope.urungelendata.ADGIRILSINMI === false) {
                                    $scope.teklif.adidisabled = false;
                                    $scope.teklif.detail.inputs.hammadde.isValid = true;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.cls = '';
                                    $scope.teklif.detail.inputs.hammadde.msg = '';
                                    $scope.teklif.detail.inputs.hammadde.inputValue = hammadde.ADI;
                                    $scope.teklif.detail.inputs.hammadde.shouldLoadOptionsOnInputFocus = false;
                                    return;
                                }
                                if ($scope.urungelendata.MIKTARGIRILSINMI === false) {
                                    $scope.teklif.miktardisabled = false;
                                    $scope.teklif.detail.inputs.hammadde.isValid = true;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.cls = '';
                                    $scope.teklif.detail.inputs.hammadde.msg = '';
                                    $scope.teklif.detail.inputs.hammadde.inputValue = hammadde.ADI;
                                    $scope.teklif.detail.inputs.hammadde.shouldLoadOptionsOnInputFocus = false;
                                    return;
                                }
                                else {
                                    $scope.teklif.detail.inputs.hammadde.isValid = true;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.cls = '';
                                    $scope.teklif.detail.inputs.hammadde.msg = '';
                                    $scope.teklif.detail.inputs.hammadde.inputValue = hammadde.ADI;
                                    $scope.teklif.detail.inputs.hammadde.shouldLoadOptionsOnInputFocus = false;
                                }
                            },
                            check: function (applyCheck) {
                                var check = $scope.teklif.detail.inputs.hammadde.selected && $scope.teklif.detail.inputs.hammadde.selected.hasOwnProperty('HAMMADDEYARIMAMULLERID') && typeof $scope.teklif.detail.inputs.hammadde.selected.HAMMADDEYARIMAMULLERID !== 'undefined' && $scope.teklif.detail.inputs.hammadde.selected.HAMMADDEYARIMAMULLERID != null ? true : false;
                                if (applyCheck || $scope.teklif.detail.inputs.hammadde.isValid !== check) {
                                    $scope.teklif.detail.inputs.hammadde.isValid = check;
                                    if ($scope.teklif.detail.inputs.hammadde.isValid) {
                                        $scope.teklif.detail.inputs.hammadde.success();
                                    } else {
                                        $scope.teklif.detail.inputs.hammadde.error('* Geçersiz Ham Madde/Yarı Mamul');
                                    }
                                }
                                return $scope.teklif.detail.inputs.hammadde.isValid;
                            },
                            select: function (hammadde) {
                                //RULMAN VE RULMANYATAĞINDA MIKTAR, İŞÇİLİKTE ADI VE MIKTAR, SIKMA DA MIKTAR DISABLED OLMALI.
                                $scope.teklif.detail.inputs.olcubirimi.setolcubirimi(hammadde.ADI); // ölçü birimi BORU,MİL,FLANŞ,KOVAN dışında kalanlar için 'ADET' olmalı.
                                if (hammadde.ADGIRILSINMI === false && hammadde.MIKTARGIRILSINMI === false) {
                                    console.log(hammadde);
                                    $scope.teklif.adidisabled = false;
                                    $scope.teklif.miktardisabled = false;
                                    $scope.teklif.selecthesaplabutonu = false;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.check();
                                    return;
                                }
                                if (hammadde.ADGIRILSINMI === false) { // ad alanı boş olsun.
                                    console.log(hammadde);
                                    $scope.teklif.adidisabled = false;
                                    $scope.teklif.miktardisabled = true;
                                    $scope.teklif.selecthesaplabutonu = false;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.check();
                                    return;
                                }
                                if (hammadde.MIKTARGIRILSINMI === false) { // miktar alanı boş olsun.
                                    console.log(hammadde);
                                    $scope.teklif.adidisabled = true;
                                    $scope.teklif.miktardisabled = false;
                                    $scope.teklif.selecthesaplabutonu = false;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.check();
                                    return;
                                }
                                if (hammadde.ADI === 'DİĞER') { // yeni bir Ham Madde/Yarı Mamul Grubu eklenebilmeli.
                                    console.log("DİĞER");
                                    $scope.teklif.selecthesaplabutonu = false;
                                    $rootScope.showHammaddecreateModal();
                                    return;
                                }
                                if (hammadde.ADI.includes("KOVAN") === true) { //KOVAN kelimesini var mı yok mu kontrol eder. True/False döner. Ağırlık hesabı modalı çıkmalı.
                                    $scope.teklif.detail.inputs.olcubirimi.list = page.data.olcubirimi; //teklifler-create.js'de.
                                    $scope.teklif.selecthesaplabutonu = true;
                                    $scope.teklif.adidisabled = true;
                                    $scope.teklif.miktardisabled = true;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.check();
                                    return;
                                }
                                if (hammadde.ADI === 'MİL' || hammadde.ADI === 'FLANŞ') {
                                    $scope.teklif.selecthesaplabutonu = true;
                                    $scope.teklif.adidisabled = true;
                                    $scope.teklif.miktardisabled = true;
                                    console.log(hammadde.ADI);
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.check();
                                    return;
                                }
                                if (hammadde.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Kg') {
                                    $scope.teklif.selecthesaplabutonu = true;
                                    $scope.teklif.adidisabled = true;
                                    $scope.teklif.miktardisabled = true;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.check();
                                    return;
                                }
                                if (hammadde.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Metre' && $scope.teklif.detail.inputs.girilenfiyat.value !== 0 && (typeof $scope.teklif.detail.inputs.girilenfiyat.value !== 'undefined')) {
                                
                                    $scope.teklif.selecthesaplabutonu = true;
                                    $scope.teklif.miktardisabled = true;
                                    $scope.teklif.adidisabled = true;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.check();
                                    return;
                                }
                                else {
                                    $scope.teklif.selecthesaplabutonu = false;
                                    $scope.teklif.adidisabled = true;
                                    $scope.teklif.miktardisabled = true;
                                    $scope.teklif.detail.inputs.hammadde.selected = hammadde;
                                    $scope.teklif.detail.inputs.hammadde.check();
                                }
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.hammadde.cls = '';
                                $scope.teklif.detail.inputs.hammadde.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.hammadde.cls = 'has-error';
                                $scope.teklif.detail.inputs.hammadde.msg = msg;
                            }
                        },
                        hammaddeadi: {
                            isValid: false,
                            value: '',
                            cls: '',
                            msg: '',
                            init: function () {
                                $scope.teklif.detail.inputs.hammaddeadi.isValid = false;
                                $scope.teklif.detail.inputs.hammaddeadi.value = '';
                                $scope.teklif.detail.inputs.hammaddeadi.cls = '';
                                $scope.teklif.detail.inputs.hammaddeadi.msg = '';
                                $scope.teklif.detail.inputs.hammaddeadi.set('');
                            },
                            set: function (hammaddeadi) {
                                $scope.teklif.detail.inputs.hammaddeadi.value = hammaddeadi;
                                $scope.teklif.detail.inputs.hammaddeadi.isValid = true;
                                $scope.teklif.detail.inputs.hammaddeadi.cls = '';
                                $scope.teklif.detail.inputs.hammaddeadi.msg = '';
                            },
                            check: function (applyCheck) {
                                console.log(applyCheck);
                                console.log($scope.teklif.detail.inputs.hammaddeadi.value);
                                var check = app.valid.text($scope.teklif.detail.inputs.hammaddeadi.value);
                                console.log(check);
                                if (applyCheck || $scope.teklif.detail.inputs.hammaddeadi.isValid !== check) {
                                    $scope.teklif.detail.inputs.hammaddeadi.isValid = check;
                                    if ($scope.teklif.detail.inputs.hammaddeadi.isValid) {
                                        $scope.teklif.detail.inputs.hammaddeadi.success();
                                    } else {
                                        $scope.teklif.detail.inputs.hammaddeadi.error('* Geçersiz Ham Madde / Yarı Mamul!');
                                    }
                                }
                                return $scope.teklif.detail.inputs.hammaddeadi.isValid;
                            },
                            change: function () {
                                $scope.teklif.detail.inputs.hammaddeadi.check($scope.teklif.detail.inputs.hammaddeadi.value);
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.hammaddeadi.cls = '';
                                $scope.teklif.detail.inputs.hammaddeadi.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.hammaddeadi.cls = 'has-error';
                                $scope.teklif.detail.inputs.hammaddeadi.msg = msg;
                            }
                        },
                        olcubirimi: { // boru, mil, flanş ve kovan dışındaki hammade/yarı mamul grubu için Ölçü Birimi sadece 'Adet' listelenmelidir.
                            list: '',
                            isValid: false,
                            selected: '',
                            cls: '',
                            msg: '',
                            inputValue: '',
                            shouldLoadOptionsOnInputFocus: true,
                            setolcubirimi: function (hammadde) { // BORU,MİL,FLANŞ yada KOVAN dışındakilerden biri seçilirse ölçü birimi sadece 'ADET' listelenmelidir.
                                if (hammadde === 'BORU' || hammadde === 'MİL' || hammadde === 'FLANŞ' || hammadde.includes("KOVAN") === true) {
                                    console.log("if setolcubirimi");
                                    $scope.teklif.detail.inputs.olcubirimi.list = page.data.olcubirimi;
                                } else {
                                    console.log("else setolcubirimi");
                                    $scope.teklif.detail.inputs.olcubirimi.list = page.data.olcubirimi2;
                                }
                            },
                            init: function () {
                                console.log("ölçü init");
                                $scope.teklif.detail.inputs.olcubirimi.isValid = false;
                                $scope.teklif.detail.inputs.olcubirimi.selected = '';
                                $scope.teklif.detail.inputs.olcubirimi.cls = '';
                                $scope.teklif.detail.inputs.olcubirimi.msg = '';
                            },
                            check: function (applyCheck) {
                                console.log("ölçü check");
                                var check = $scope.teklif.detail.inputs.olcubirimi.selected && $scope.teklif.detail.inputs.olcubirimi.selected.hasOwnProperty('value') && typeof $scope.teklif.detail.inputs.olcubirimi.selected.value !== 'undefined' && $scope.teklif.detail.inputs.olcubirimi.selected.value != null ? true : false;
                                if (applyCheck || $scope.teklif.detail.inputs.olcubirimi.isValid !== check) {
                                    $scope.teklif.detail.inputs.olcubirimi.isValid = check;
                                    if ($scope.teklif.detail.inputs.olcubirimi.isValid) {
                                        $scope.teklif.detail.inputs.olcubirimi.success();
                                    } else {
                                        $scope.teklif.detail.inputs.olcubirimi.error('* Geçersiz Ölçü Birimi');
                                    }
                                }
                                return $scope.teklif.detail.inputs.olcubirimi.isValid;
                            },
                            change: function () {
                                console.log("ölçü change");
                                $scope.teklif.detail.inputs.olcubirimi.check();
                                if ($scope.teklif.adidisabled === false && $scope.teklif.miktardisabled === false) {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                if ($scope.teklif.adidisabled === false) {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                if ($scope.teklif.miktardisabled === false) {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                if ($scope.teklif.detail.inputs.hammadde.selected.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Kg') {
                                    $scope.teklif.detail.inputs.girilenfiyat.value = 0;
                                    $scope.teklif.detail.inputs.girilenfiyat.text = '';
                                    $scope.teklif.selecthesaplabutonu = true;
                                    $scope.teklif.miktardisabled = true;
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                if ($scope.teklif.detail.inputs.hammadde.selected.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Metre') {
                                    $scope.teklif.detail.inputs.girilenfiyat.value = 0;
                                    $scope.teklif.detail.inputs.girilenfiyat.text = '';
                                    $scope.teklif.selecthesaplabutonu = false;
                                    $scope.teklif.miktardisabled = true;
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                else {
                                    $scope.teklif.detail.inputs.girilenfiyat.value = 0;
                                    $scope.teklif.detail.inputs.girilenfiyat.text = '';
                                    $scope.teklif.miktardisabled = true;
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                }
                            },
                            select: function (olcubirimi) {
                                console.log("ölçü select");
                                    $scope.teklif.detail.inputs.olcubirimi.isValid = true;
                                    $scope.teklif.detail.inputs.olcubirimi.selected = olcubirimi;
                                    $scope.teklif.detail.inputs.olcubirimi.cls = '';
                                    $scope.teklif.detail.inputs.olcubirimi.msg = '';
                            },
                            set: function (value) {
                                console.log("ölçü set");
                                var goon = true;
                                var i = 0;
                                while (goon && i < $scope.teklif.detail.inputs.olcubirimi.list.length) {
                                    if ($scope.teklif.detail.inputs.olcubirimi.list[i].value === value) {
                                        $scope.teklif.detail.inputs.olcubirimi.select($scope.teklif.detail.inputs.olcubirimi.list[i]);
                                        goon = false;
                                    }
                                    i++;
                                }
                                if (goon) {
                                    $scope.teklif.detail.inputs.olcubirimi.init();
                                }
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.olcubirimi.cls = '';
                                $scope.teklif.detail.inputs.olcubirimi.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.olcubirimi.cls = 'has-error';
                                $scope.teklif.detail.inputs.olcubirimi.msg = msg;
                            }
                        },
                        miktar: {
                            isValid: false,
                            value: 0,
                            text: '',
                            cls: '',
                            msg: '',
                            init: function () {
                                $scope.teklif.detail.inputs.miktar.isValid = false;
                                $scope.teklif.detail.inputs.miktar.value = '';
                                $scope.teklif.detail.inputs.miktar.text = '';
                                $scope.teklif.detail.inputs.miktar.cls = '';
                                $scope.teklif.detail.inputs.miktar.msg = '';
                                $scope.teklif.detail.inputs.miktar.set('');
                            },
                            set: function (miktar) {
                                $scope.teklif.detail.inputs.miktar.value = miktar.value;
                                $scope.teklif.detail.inputs.miktar.text = miktar.text;
                                $scope.teklif.detail.inputs.miktar.isValid = true;
                                $scope.teklif.detail.inputs.miktar.cls = '';
                                $scope.teklif.detail.inputs.miktar.msg = '';
                            },
                            check: function (applyCheck) {
                                if (typeof $scope.teklif.detail.inputs.miktar.text === 'undefined') {
                                    $scope.teklif.detail.inputs.miktar.text = '';
                                }
                                $scope.teklif.detail.inputs.miktar.value = $scope.teklif.detail.inputs.miktar.text.tryParseMoneyToFloat();
                                var check = app.valid.positiveNumber($scope.teklif.detail.inputs.miktar.value);
                                if (JSON.parse(localStorage.getItem("boruagirlik")) !== null) {
                                    $scope.teklif.detail.inputs.miktar.text = JSON.parse(localStorage.getItem("boruagirlik"));
                                    $scope.teklif.detail.inputs.miktar.value = $scope.teklif.detail.inputs.miktar.text.tryParseMoneyToFloat();
                                    var check = app.valid.positiveNumber($scope.teklif.detail.inputs.miktar.value);
                                    if (applyCheck || $scope.teklif.detail.inputs.miktar.isValid !== check) {
                                        $scope.teklif.detail.inputs.miktar.isValid = check;
                                        if ($scope.teklif.detail.inputs.miktar.isValid) {
                                            $scope.teklif.detail.inputs.miktar.success();
                                        } else {
                                            $scope.teklif.detail.inputs.miktar.error('* Geçersiz Miktar !');
                                        }
                                    }
                                    return $scope.teklif.detail.inputs.miktar.isValid;
                                }
                                else if (JSON.parse(localStorage.getItem("milagirlik")) !== null) {
                                    $scope.teklif.detail.inputs.miktar.text = JSON.parse(localStorage.getItem("milagirlik"));
                                    $scope.teklif.detail.inputs.miktar.value = $scope.teklif.detail.inputs.miktar.text.tryParseMoneyToFloat();

                                    var check = app.valid.positiveNumber($scope.teklif.detail.inputs.miktar.value);
                                    if (applyCheck || $scope.teklif.detail.inputs.miktar.isValid !== check) {
                                        $scope.teklif.detail.inputs.miktar.isValid = check;
                                        if ($scope.teklif.detail.inputs.miktar.isValid) {
                                            $scope.teklif.detail.inputs.miktar.success();
                                        } else {
                                            $scope.teklif.detail.inputs.miktar.error('* Geçersiz Miktar !');
                                        }
                                    }
                                    return $scope.teklif.detail.inputs.miktar.isValid;
                                }
                                else if (JSON.parse(localStorage.getItem("flansagirlik")) !== null) {
                                    $scope.teklif.detail.inputs.miktar.text = JSON.parse(localStorage.getItem("flansagirlik"));
                                    $scope.teklif.detail.inputs.miktar.value = $scope.teklif.detail.inputs.miktar.text.tryParseMoneyToFloat();

                                    var check = app.valid.positiveNumber($scope.teklif.detail.inputs.miktar.value);
                                     if (applyCheck || $scope.teklif.detail.inputs.miktar.isValid !== check) {
                                        $scope.teklif.detail.inputs.miktar.isValid = check;
                                        if ($scope.teklif.detail.inputs.miktar.isValid) {
                                            $scope.teklif.detail.inputs.miktar.success();
                                        } else {
                                            $scope.teklif.detail.inputs.miktar.error('* Geçersiz Miktar !');
                                        }
                                      }
                                    return $scope.teklif.detail.inputs.miktar.isValid;
                                }
                                else if (JSON.parse(localStorage.getItem("kovanagirlik")) !== null) {
                                    $scope.teklif.detail.inputs.miktar.text = JSON.parse(localStorage.getItem("kovanagirlik"));
                                    $scope.teklif.detail.inputs.miktar.value = $scope.teklif.detail.inputs.miktar.text.tryParseMoneyToFloat();

                                    var check = app.valid.positiveNumber($scope.teklif.detail.inputs.miktar.value);
                                    if (applyCheck || $scope.teklif.detail.inputs.miktar.isValid !== check) {
                                        $scope.teklif.detail.inputs.miktar.isValid = check;
                                        if ($scope.teklif.detail.inputs.miktar.isValid) {
                                            $scope.teklif.detail.inputs.miktar.success();
                                        } else {
                                            $scope.teklif.detail.inputs.miktar.error('* Geçersiz Miktar !');
                                        }
                                    }
                                    return $scope.teklif.detail.inputs.miktar.isValid;
                                }
                                else {
                                    $scope.teklif.detail.inputs.miktar.value = $scope.teklif.detail.inputs.miktar.text.tryParseMoneyToFloat();
                                    var check = app.valid.positiveNumber($scope.teklif.detail.inputs.miktar.value);
                                     if (applyCheck || $scope.teklif.detail.inputs.miktar.isValid !== check) {
                                        $scope.teklif.detail.inputs.miktar.isValid = check;
                                        if ($scope.teklif.detail.inputs.miktar.isValid) {
                                            $scope.teklif.detail.inputs.miktar.success();
                                        } else {
                                            $scope.teklif.detail.inputs.miktar.error('* Geçersiz Miktar !');
                                        }
                                    }
                                    return $scope.teklif.detail.inputs.miktar.isValid;
                                }
                            },
                            change: function () {
                                $scope.teklif.detail.inputs.miktar.check($scope.teklif.detail.inputs.miktar.value);
                                if (JSON.parse(localStorage.getItem("boruagirlik")) !== null) {
                                    console.log($scope.teklif.detail.inputs.miktar.value);
                                    localStorage.removeItem('milagirlik');
                                    localStorage.removeItem('flansagirlik');
                                    localStorage.removeItem('kovanagirlik');
                                    $scope.teklif.detail.inputs.miktar.text = JSON.parse(localStorage.getItem("boruagirlik"));
                                    $scope.teklif.detail.inputs.miktar.check($scope.teklif.detail.inputs.miktar.value);
                                    $scope.teklif.detail.inputs.miktar.value = Number($scope.teklif.detail.inputs.miktar.value).toFixed(2);
                                    $scope.teklif.detail.inputs.miktar.text = $scope.teklif.detail.inputs.miktar.value;
                                    if ($scope.teklif.detail.inputs.hammadde.selected.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Metre') {
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                        return;
                                    }
                                    if (typeof $scope.teklif.detail.inputs.adet.value === 'undefined' || (typeof $scope.teklif.detail.inputs.adet.text === 'undefined')) {
                                        $scope.teklif.detail.inputs.adet.value = 1;
                                        $scope.teklif.detail.inputs.adet.text = '1';
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    }
                                    else {
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    }
                                }
                                else if (JSON.parse(localStorage.getItem("milagirlik")) !== null) {
                                    localStorage.removeItem('boruagirlik');
                                    localStorage.removeItem('flansagirlik');
                                    localStorage.removeItem('kovanagirlik');
                                    $scope.teklif.detail.inputs.miktar.text = JSON.parse(localStorage.getItem("milagirlik"));
                                    $scope.teklif.detail.inputs.miktar.check($scope.teklif.detail.inputs.miktar.value);
                                    $scope.teklif.detail.inputs.miktar.value = Number($scope.teklif.detail.inputs.miktar.value).toFixed(2);
                                    $scope.teklif.detail.inputs.miktar.text = $scope.teklif.detail.inputs.miktar.value;
                                    if (typeof $scope.teklif.detail.inputs.adet.value === 'undefined' || (typeof $scope.teklif.detail.inputs.adet.text === 'undefined')) {
                                        $scope.teklif.detail.inputs.adet.value = 1;
                                        $scope.teklif.detail.inputs.adet.text = '1';
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    }
                                    else {
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    }
                                }
                                else if (JSON.parse(localStorage.getItem("flansagirlik")) !== null) {
                                    localStorage.removeItem('boruagirlik');
                                    localStorage.removeItem('milagirlik');
                                    localStorage.removeItem('kovanagirlik');
                                    $scope.teklif.detail.inputs.miktar.text = JSON.parse(localStorage.getItem("flansagirlik"));
                                    $scope.teklif.detail.inputs.miktar.check($scope.teklif.detail.inputs.miktar.value);
                                    $scope.teklif.detail.inputs.miktar.value = Number($scope.teklif.detail.inputs.miktar.value).toFixed(2);
                                    $scope.teklif.detail.inputs.miktar.text = $scope.teklif.detail.inputs.miktar.value;

                                    if (typeof $scope.teklif.detail.inputs.adet.value === 'undefined' || (typeof $scope.teklif.detail.inputs.adet.text === 'undefined')) {
                                        $scope.teklif.detail.inputs.adet.value = 1;
                                        $scope.teklif.detail.inputs.adet.text = '1';
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    }
                                    else {
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    }
                                }
                                else if (JSON.parse(localStorage.getItem("kovanagirlik")) !== null) {
                                    localStorage.removeItem('boruagirlik');
                                    localStorage.removeItem('flansagirlik');
                                    localStorage.removeItem('milagirlik');
                                    $scope.teklif.detail.inputs.miktar.text = JSON.parse(localStorage.getItem("kovanagirlik"));
                                    $scope.teklif.detail.inputs.miktar.check($scope.teklif.detail.inputs.miktar.value);
                                    $scope.teklif.detail.inputs.miktar.value = Number($scope.teklif.detail.inputs.miktar.value).toFixed(2);
                                    $scope.teklif.detail.inputs.miktar.text = $scope.teklif.detail.inputs.miktar.value;
                                    if (typeof $scope.teklif.detail.inputs.adet.value === 'undefined' || (typeof $scope.teklif.detail.inputs.adet.text === 'undefined')) {
                                        $scope.teklif.detail.inputs.adet.value = 1;
                                        $scope.teklif.detail.inputs.adet.text = '1';
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    }
                                    else {
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    }
                                }
                                else if ($scope.teklif.detail.inputs.hammadde.selected.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Metre' && $scope.teklif.detail.inputs.girilenfiyat.value !== 0 && typeof $scope.teklif.detail.inputs.girilenfiyat.value !== 'undefined') {
                                    localStorage.removeItem('boruagirlik');
                                    localStorage.removeItem('milagirlik');
                                    localStorage.removeItem('flansagirlik');
                                    localStorage.removeItem('kovanagirlik');
                                    $scope.teklif.detail.inputs.miktar.check($scope.teklif.detail.inputs.miktar.value);
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                }
                                else {
                                    localStorage.removeItem('boruagirlik');
                                    localStorage.removeItem('milagirlik');
                                    localStorage.removeItem('flansagirlik');
                                    localStorage.removeItem('kovanagirlik');
                                    console.log($scope.teklif.detail.inputs.miktar.text);
                                    console.log($scope.teklif.detail.inputs.miktar.value);
                                    $scope.teklif.detail.inputs.miktar.check($scope.teklif.detail.inputs.miktar.value);
                                     if (typeof $scope.teklif.detail.inputs.adet.value === 'undefined' || (typeof $scope.teklif.detail.inputs.adet.text === 'undefined')) {
                                        $scope.teklif.detail.inputs.adet.value = 1;
                                        $scope.teklif.detail.inputs.adet.text = '1';
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    }
                                    else {
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    }
                                }
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.miktar.cls = '';
                                $scope.teklif.detail.inputs.miktar.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.miktar.cls = 'has-error';
                                $scope.teklif.detail.inputs.miktar.msg = msg;
                            }
                        },
                        adet: {  
                            isValid: false,
                            value: 1,
                            text: '1',
                            cls: '',
                            msg: '',
                            init: function () {
                                $scope.teklif.detail.inputs.adet.isValid = false;
                                $scope.teklif.detail.inputs.adet.value = 1;
                                $scope.teklif.detail.inputs.adet.text = '1';
                                $scope.teklif.detail.inputs.adet.cls = '';
                                $scope.teklif.detail.inputs.adet.msg = '';
                                $scope.teklif.detail.inputs.adet.set(0);
                            },
                            set: function (adet) {
                                $scope.teklif.detail.inputs.adet.value = adet.value;
                                $scope.teklif.detail.inputs.adet.text = adet.text;
                                $scope.teklif.detail.inputs.adet.isValid = true;
                                $scope.teklif.detail.inputs.adet.cls = '';
                                $scope.teklif.detail.inputs.adet.msg = '';
                            },
                            change: function () {
                                $scope.teklif.detail.inputs.adet.value = $scope.teklif.detail.inputs.adet.text.tryParseMoneyToFloat();
                                if ($scope.teklif.adidisabled === false &&  $scope.teklif.miktardisabled === false) {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                if ($scope.teklif.adidisabled === false ) {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                if ($scope.teklif.miktardisabled === false) {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                console.log("typeof girilen fiyat ->", typeof $scope.teklif.detail.inputs.girilenfiyat.value);
                                if ($scope.teklif.detail.inputs.hammadde.selected.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Metre' && $scope.teklif.detail.inputs.girilenfiyat.value != undefined) {
                                    console.log("girilen fiyat ->", $scope.teklif.detail.inputs.girilenfiyat.value);
                                    $scope.teklif.selecthesaplabutonu = true;
                                    $scope.teklif.miktardisabled = true;
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                else {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.adet.cls = '';
                                $scope.teklif.detail.inputs.adet.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.adet.cls = 'has-error';
                                $scope.teklif.detail.inputs.adet.msg = msg;
                            }
                        },
                        birimlistefiyati: {
                            isValid: false,
                            value: 0,
                            text: '',
                            cls: '',
                            msg: '',
                            init: function () {
                                $scope.teklif.detail.inputs.birimlistefiyati.isValid = false;
                                $scope.teklif.detail.inputs.birimlistefiyati.value = 0;
                                $scope.teklif.detail.inputs.birimlistefiyati.text = '';
                                $scope.teklif.detail.inputs.birimlistefiyati.cls = '';
                                $scope.teklif.detail.inputs.birimlistefiyati.msg = '';
                                $scope.teklif.detail.inputs.birimlistefiyati.set('');
                            },
                            set: function (birimlistefiyati) {
                                $scope.teklif.detail.inputs.birimlistefiyati.value = birimlistefiyati.value;
                                $scope.teklif.detail.inputs.birimlistefiyati.text = birimlistefiyati.text;
                                $scope.teklif.detail.inputs.birimlistefiyati.isValid = true;
                                $scope.teklif.detail.inputs.birimlistefiyati.cls = '';
                                $scope.teklif.detail.inputs.birimlistefiyati.msg = '';
                            },
                            check: function (applyCheck) {
                                if (typeof $scope.teklif.detail.inputs.birimlistefiyati.text === 'undefined') {
                                    $scope.teklif.detail.inputs.birimlistefiyati.text = '';
                                }
                                $scope.teklif.detail.inputs.birimlistefiyati.value = $scope.teklif.detail.inputs.birimlistefiyati.text.tryParseMoneyToFloat();
                                var check = app.valid.positiveNumber($scope.teklif.detail.inputs.birimlistefiyati.value);
                                if (applyCheck || $scope.teklif.detail.inputs.birimlistefiyati.isValid !== check) {
                                    $scope.teklif.detail.inputs.birimlistefiyati.isValid = check;
                                    if ($scope.teklif.detail.inputs.birimlistefiyati.isValid) {
                                        console.log($scope.teklif.detail.inputs.birimlistefiyati.value);
                                        $scope.teklif.detail.inputs.birimlistefiyati.success();
                                    } else {
                                        $scope.teklif.detail.inputs.birimlistefiyati.error('* Geçersiz Birim Liste Fiyatı !');
                                    }
                                }
                                return $scope.teklif.detail.inputs.birimlistefiyati.isValid;
                            },
                            change: function () {
                                $scope.teklif.detail.inputs.birimlistefiyati.check($scope.teklif.detail.inputs.birimlistefiyati.value);
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.birimlistefiyati.cls = '';
                                $scope.teklif.detail.inputs.birimlistefiyati.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.birimlistefiyati.cls = 'has-error';
                                $scope.teklif.detail.inputs.birimlistefiyati.msg = msg;
                            }
                        },
                        girilenfiyat: {
                            isValid: false,
                            value: 0,
                            text: '',
                            cls: '',
                            msg: '',
                            init: function () {
                                $scope.teklif.detail.inputs.girilenfiyat.isValid = false;
                                $scope.teklif.detail.inputs.girilenfiyat.value = 0;
                                $scope.teklif.detail.inputs.girilenfiyat.text = '';
                                $scope.teklif.detail.inputs.girilenfiyat.cls = '';
                                $scope.teklif.detail.inputs.girilenfiyat.msg = '';
                                $scope.teklif.detail.inputs.girilenfiyat.set('');
                            },
                            set: function (girilenfiyat) {
                                $scope.teklif.detail.inputs.girilenfiyat.value = girilenfiyat.value;
                                $scope.teklif.detail.inputs.girilenfiyat.text = girilenfiyat.text;
                                $scope.teklif.detail.inputs.girilenfiyat.isValid = true;
                                $scope.teklif.detail.inputs.girilenfiyat.cls = '';
                                $scope.teklif.detail.inputs.girilenfiyat.msg = '';
                                if (($scope.teklif.detail.inputs.hammadde.selected.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Metre') ||
                                    ($scope.teklif.detail.inputs.hammadde.selected.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Kg') ||
                                    ($scope.teklif.detail.inputs.hammadde.selected.ADI === 'MİL' || $scope.teklif.detail.inputs.hammadde.selected.ADI === 'FLANŞ')) {
                                    // boru ve metre , boru ve kg, mil yada flanş seçili ise 'Hesapla' butonu aktif olmalı.
                                    $scope.teklif.selecthesaplabutonu = true;
                                }
                            },
                            check: function (applyCheck) {
                                if (typeof $scope.teklif.detail.inputs.girilenfiyat.text === 'undefined') {
                                    $scope.teklif.detail.inputs.girilenfiyat.text = '';
                                }
                                $scope.teklif.detail.inputs.girilenfiyat.value = $scope.teklif.detail.inputs.girilenfiyat.text.tryParseMoneyToFloat();
                                var check = app.valid.positiveNumber($scope.teklif.detail.inputs.girilenfiyat.value);
                                if (applyCheck || $scope.teklif.detail.inputs.girilenfiyat.isValid !== check) {
                                    $scope.teklif.detail.inputs.girilenfiyat.isValid = check;
                                    if ($scope.teklif.detail.inputs.girilenfiyat.isValid) {
                                        $scope.teklif.detail.inputs.girilenfiyat.success();
                                    } else {
                                        $scope.teklif.detail.inputs.girilenfiyat.error('* Geçersiz Girilen Fiyat !');
                                    }
                                }
                                return $scope.teklif.detail.inputs.girilenfiyat.isValid;
                            },
                            change: function () {
                                if (typeof $scope.teklif.detail.inputs.adet.text === 'undefined') {
                                    $scope.teklif.detail.inputs.adet.text = '1';
                                    $scope.teklif.detail.inputs.adet.value = 1;
                                }
                                $scope.teklif.detail.inputs.girilenfiyat.check($scope.teklif.detail.inputs.girilenfiyat.value);
                                if ($scope.teklif.adidisabled === false && $scope.teklif.miktardisabled === false) {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                if ($scope.teklif.adidisabled === false) {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                if ($scope.teklif.miktardisabled === false) {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                    return;
                                }
                                if ($scope.teklif.detail.inputs.hammadde.selected.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Kg') {
                                        $scope.teklif.selecthesaplabutonu = true;
                                        $scope.teklif.miktardisabled = true;
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                }
                                if ($scope.teklif.detail.inputs.hammadde.selected.ADI === 'BORU' && $scope.teklif.detail.inputs.olcubirimi.selected.value === 'Metre') {
                                    console.log("GIRILEN FIYAT METRE-->");
                                    if ($scope.teklif.detail.inputs.girilenfiyat.text === '') {
                                        $scope.teklif.selecthesaplabutonu = false;
                                    }
                                    else {
                                        console.log("GIRILEN FIYAT METRE-->");
                                        $scope.teklif.selecthesaplabutonu = true;
                                        $scope.teklif.miktardisabled = true;
                                        $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                        $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                        return;
                                    }
                                }
                                // RULMAN YADA RULMAN YATAĞI SEÇİLİRSE MİKTAR INPUTU DISABLED OLMALI.
                                else {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                }
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.girilenfiyat.cls = '';
                                $scope.teklif.detail.inputs.girilenfiyat.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.girilenfiyat.cls = 'has-error';
                                $scope.teklif.detail.inputs.girilenfiyat.msg = msg;
                            }
                        },
                        tutar: {
                            value: 0,
                            text: '',
                            init: function () {
                                $scope.teklif.detail.inputs.tutar.value = 0;
                                $scope.teklif.detail.inputs.tutar.text = '';
                                $scope.teklif.detail.inputs.tutar.set('');
                            },
                            set: function (tutar) {
                                $scope.teklif.detail.inputs.tutar.value = tutar.value;
                                $scope.teklif.detail.inputs.tutar.text = tutar.text;
                            }
                        },
                        toplamtutar: { // kalemlerdeki tutarların toplamı
                            value: 0,
                            uyari: false,
                            init: function () {
                                $scope.teklif.detail.inputs.toplamtutar.value = 0;
                                $scope.teklif.detail.inputs.tutar.set(0);
                                $scope.teklif.detail.inputs.toplamtutar.set(0);
                            },
                            set: function (value) {
                                // $scope.teklif.detail.inputs.toplamtutar.value = value + $scope.teklif.detail.tutar.value;
                            }
                        },
                        set: function (index, row) {
                            $scope.teklif.detail.inputs.hammadde.set({ HAMMADDEYARIMAMULLERID: row.HAMMADDEYARIMAMULID, ADI: row.HAMMADDEYARIMAMULADI });
                            $scope.teklif.detail.inputs.hammaddeadi.set(row.HAMMADDEYARIMAMUL);
                            $scope.teklif.detail.inputs.olcubirimi.set(row.OLCUBIRIMI);
                            $scope.teklif.detail.inputs.miktar.set({ value: row.MIKTARVAL, text: row.MIKTARTXT });
                            $scope.teklif.detail.inputs.adet.set({ value: row.KALEMADETVAL, text: row.KALEMADETTXT });
                            $scope.teklif.detail.inputs.birimlistefiyati.set({ value: row.BIRIMLISTEFIYATIVAL, text: row.BIRIMLISTEFIYATITXT });
                            $scope.teklif.detail.inputs.girilenfiyat.set({ value: row.GIRILENFIYATVAL, text: row.GIRILENFIYATTXT });
                            $scope.teklif.detail.inputs.tutar.set({ value: row.TUTARVAL, text: row.TUTARTXT });
                        },
                        init: function () {
                            $scope.teklif.detail.inputs.hammadde.init();
                            $scope.teklif.detail.inputs.hammaddeadi.init();
                            $scope.teklif.detail.inputs.olcubirimi.init();
                            $scope.teklif.detail.inputs.miktar.init();
                            $scope.teklif.detail.inputs.adet.init();
                            $scope.teklif.detail.inputs.birimlistefiyati.init();
                            $scope.teklif.detail.inputs.girilenfiyat.init();
                            $scope.teklif.detail.inputs.tutar.init();
                            $scope.teklif.detail.inputs.toplamtutar.init();
                        },
                        clear: function () {
                            $scope.teklif.detail.inputs.hammadde.init();
                            $scope.teklif.detail.inputs.hammaddeadi.init();
                            $scope.teklif.detail.inputs.olcubirimi.init();
                            $scope.teklif.detail.inputs.miktar.init();
                            $scope.teklif.detail.inputs.adet.init();
                            $scope.teklif.detail.inputs.birimlistefiyati.init();
                            $scope.teklif.detail.inputs.girilenfiyat.init();
                            $scope.teklif.detail.inputs.tutar.init();
                        },
                        check: function () {
                            var applyCheck = true;
                            if ($scope.teklif.adidisabled === false && $scope.teklif.miktardisabled === false) {
                                var OK = {
                                    hammadde: $scope.teklif.detail.inputs.hammadde.check(applyCheck),
                                    olcubirimi: $scope.teklif.detail.inputs.olcubirimi.check(applyCheck),
                                    birimlistefiyati: $scope.teklif.detail.inputs.birimlistefiyati.check(applyCheck),
                                    girilenfiyat: $scope.teklif.detail.inputs.girilenfiyat.check(applyCheck)
                                };
                                return OK.hammadde && OK.olcubirimi && OK.birimlistefiyati && OK.girilenfiyat;
                            }
                            if ($scope.teklif.adidisabled === false) {
                                var OK = {
                                    hammadde: $scope.teklif.detail.inputs.hammadde.check(applyCheck),
                                    olcubirimi: $scope.teklif.detail.inputs.olcubirimi.check(applyCheck),
                                    miktar: $scope.teklif.detail.inputs.miktar.check(applyCheck),
                                    birimlistefiyati: $scope.teklif.detail.inputs.birimlistefiyati.check(applyCheck),
                                    girilenfiyat: $scope.teklif.detail.inputs.girilenfiyat.check(applyCheck)
                                };
                                return OK.hammadde && OK.olcubirimi && OK.miktar && OK.birimlistefiyati && OK.girilenfiyat;
                            }
                            if ($scope.teklif.miktardisabled === false) {
                                var OK = {
                                    hammadde: $scope.teklif.detail.inputs.hammadde.check(applyCheck),
                                    hammaddeadi: $scope.teklif.detail.inputs.hammaddeadi.check(applyCheck),
                                    olcubirimi: $scope.teklif.detail.inputs.olcubirimi.check(applyCheck),
                                    birimlistefiyati: $scope.teklif.detail.inputs.birimlistefiyati.check(applyCheck),
                                    girilenfiyat: $scope.teklif.detail.inputs.girilenfiyat.check(applyCheck)
                                };
                                return OK.hammadde && OK.hammaddeadi && OK.olcubirimi && OK.birimlistefiyati && OK.girilenfiyat;
                            }
                            else {
                                var OK = {
                                    hammadde: $scope.teklif.detail.inputs.hammadde.check(applyCheck),
                                    hammaddeadi: $scope.teklif.detail.inputs.hammaddeadi.check(applyCheck),
                                    olcubirimi: $scope.teklif.detail.inputs.olcubirimi.check(applyCheck),
                                    miktar: $scope.teklif.detail.inputs.miktar.check(applyCheck),
                                    birimlistefiyati: $scope.teklif.detail.inputs.birimlistefiyati.check(applyCheck),
                                    girilenfiyat: $scope.teklif.detail.inputs.girilenfiyat.check(applyCheck)
                                };
                                return OK.hammadde && OK.hammaddeadi && OK.olcubirimi && OK.miktar && OK.birimlistefiyati && OK.girilenfiyat;
                            }
                        },
                        add: function () {
                            if (!$scope.teklif.detail.inputs.check()) {
                                return;
                            }
                            if ($scope.teklif.detail.inputs.tutar.text === '0,00') {
                                return;
                            } 
                            if ($scope.teklif.miktardisabled === false) {
                                console.log($scope.teklif.detail.inputs.miktar.value);
                                console.log($scope.teklif.detail.inputs.miktar.text);
                                $scope.teklif.detail.inputs.miktar.value = 1;
                                $scope.teklif.detail.inputs.miktar.text = "";
                                console.log($scope.teklif.detail.inputs.miktar.value);
                                console.log($scope.teklif.detail.inputs.miktar.text);
                            }
                            else if ($scope.teklif.adidisabled === false) {
                                console.log($scope.teklif.detail.inputs.hammaddeadi.value);
                                $scope.teklif.detail.inputs.hammaddeadi.value = "";
                                console.log($scope.teklif.detail.inputs.hammaddeadi.value);
                            }
                            else if ($scope.teklif.adidisabled === false && $scope.teklif.miktardisabled === false) {
                                console.log($scope.teklif.detail.inputs.miktar.value);
                                console.log($scope.teklif.detail.inputs.miktar.text);
                                console.log($scope.teklif.detail.inputs.hammaddeadi.value);
                                $scope.teklif.detail.inputs.hammaddeadi.value = "";
                                $scope.teklif.detail.inputs.miktar.value = 1;
                                $scope.teklif.detail.inputs.miktar.text = '';
                                console.log($scope.teklif.detail.inputs.miktar.value);
                                console.log($scope.teklif.detail.inputs.miktar.text);
                                console.log($scope.teklif.detail.inputs.hammaddeadi.value);
                            }
                            var row = {
                                ID: 0,
                                STATE: 'NEW',
                                HAMMADDEYARIMAMULID: $scope.teklif.detail.inputs.hammadde.selected.HAMMADDEYARIMAMULLERID,
                                HAMMADDEYARIMAMULADI: $scope.teklif.detail.inputs.hammadde.selected.ADI,
                                HAMMADDEYARIMAMUL: $scope.teklif.detail.inputs.hammaddeadi.value,
                                OLCUBIRIMI: $scope.teklif.detail.inputs.olcubirimi.selected.value,
                                MIKTARVAL: $scope.teklif.detail.inputs.miktar.value,
                                MIKTARTXT: $scope.teklif.detail.inputs.miktar.text,
                                KALEMADETVAL: $scope.teklif.detail.inputs.adet.value,
                                KALEMADETTXT: $scope.teklif.detail.inputs.adet.text,
                                BIRIMLISTEFIYATIVAL: $scope.teklif.detail.inputs.birimlistefiyati.value,
                                BIRIMLISTEFIYATITXT: $scope.teklif.detail.inputs.birimlistefiyati.text,
                                GIRILENFIYATVAL: $scope.teklif.detail.inputs.girilenfiyat.value,
                                GIRILENFIYATTXT: $scope.teklif.detail.inputs.girilenfiyat.text,
                                TUTARVAL: $scope.teklif.detail.inputs.tutar.value,
                                TUTARTXT: $scope.teklif.detail.inputs.tutar.text,
                            };
                            if ($scope.teklif.detail.isExist) {
                                $scope.teklif.detail.rows.unshift(row);
                            } else {
                                $scope.teklif.detail.rows = [row];
                                $scope.teklif.detail.isExist = true;
                            }
                            console.log([row]);
                            localStorage.setItem("teklif-create-modal-detail", JSON.stringify($scope.teklif.detail.rows));
                            $scope.teklif.miktardisabled = true;
                            $scope.teklif.adidisabled = true;
                            $scope.teklif.selecthesaplabutonu = false;
                            $scope.teklif.detail.inputs.toplamtutar.value = $scope.teklif.detail.inputs.toplamtutar.value + $scope.teklif.detail.inputs.tutar.value;
                            $scope.teklif.birimsatisfiyati.value = $scope.teklif.detail.inputs.toplamtutar.value;
                            $scope.teklif.birimsatisfiyati.text = $scope.teklif.detail.inputs.toplamtutar.value.formatMoney(2, ',', '.');
                            localStorage.setItem("teklif-create-modal-birimsatisfiyati", JSON.stringify($scope.teklif.birimsatisfiyati.value)); 
                            //
                            $scope.teklif.birimsatisveadet.value = $scope.teklif.birimsatisfiyati.value + (($scope.teklif.birimsatisfiyati.value / 100) * $scope.teklif.karyuzdesi.value);
                            //Birim Maliyet Fiyatının Kar yüzdesini kendisine ilave edip Birim Satış Fiyatına eşitle.
                            //Birim Satış Fiyatı ile Adet çarpımı Toplam Teklif Tutarı olacak.
                            $scope.teklif.toplamsatisfiyati.value = $scope.teklif.birimsatisveadet.value;
                            $scope.teklif.toplamsatisfiyati.text = $scope.teklif.toplamsatisfiyati.value.formatMoney(2, ',', '.');
                            localStorage.setItem("teklif-create-modal-toplamsatisfiyati", JSON.stringify($scope.teklif.toplamsatisfiyati.value));
                            $scope.teklif.toplamtekliftutari.value = $scope.teklif.toplamsatisfiyati.value * $scope.teklif.adet.value;
                            $scope.teklif.toplamtekliftutari.text = $scope.teklif.toplamtekliftutari.value.formatMoney(2, ',', '.');
                            localStorage.setItem("teklif-create-modal-toplamtekliftutari", JSON.stringify($scope.teklif.toplamtekliftutari.value));
                            //
                            localStorage.removeItem('boruagirlik');
                            localStorage.removeItem('milagirlik');
                            localStorage.removeItem('flansagirlik');
                            localStorage.removeItem('kovanagirlik');
                            $scope.teklif.detail.inputs.clear();
                            $scope.teklif.detail.check();
                        },
                    }
                },
                id: {
                    value: 0,
                    init: function () {
                        $scope.teklif.id.value = 0;
                    },
                    set: function (value) {
                        $scope.teklif.id.value = value;
                    }
                },
                teklifurunadi: {
                    isValid: false,
                    selected: null,
                    cls: '',
                    msg: '',
                    inputValue: '',
                    shouldLoadOptionsOnInputFocus: true,
                    init: function () {
                        $scope.teklif.teklifurunadi.isValid = false;
                        $scope.teklif.teklifurunadi.selected = '';
                        $scope.teklif.teklifurunadi.cls = '';
                        $scope.teklif.teklifurunadi.msg = '';
                        $scope.teklif.teklifurunadi.inputValue = '';
                        $scope.teklif.teklifurunadi.shouldLoadOptionsOnInputFocus = true;
                    },
                    set: function (teklifurunadi) {
                            $scope.teklif.teklifurunadi.isValid = true;
                            $scope.teklif.teklifurunadi.selected = teklifurunadi;
                            $scope.teklif.teklifurunadi.cls = '';
                            $scope.teklif.teklifurunadi.msg = '';
                            $scope.teklif.teklifurunadi.inputValue = teklifurunadi.TEKLIFURUNADI;
                            $scope.teklif.teklifurunadi.shouldLoadOptionsOnInputFocus = false;
                    },
                    check: function (applyCheck) {
                        var check = $scope.teklif.teklifurunadi.selected && $scope.teklif.teklifurunadi.selected.hasOwnProperty('TEKLIFURUNLERID') && typeof $scope.teklif.teklifurunadi.selected.TEKLIFURUNLERID !== 'undefined' && $scope.teklif.teklifurunadi.selected.TEKLIFURUNLERID != null ? true : false;
                        if (applyCheck || $scope.teklif.teklifurunadi.isValid !== check) {
                            $scope.teklif.teklifurunadi.isValid = check;
                            if ($scope.teklif.teklifurunadi.isValid) {
                                $scope.teklif.teklifurunadi.success();
                                localStorage.setItem("teklif-create-modal-teklifurunadi-TEKLIFURUNLERID", JSON.stringify($scope.teklif.teklifurunadi.selected.TEKLIFURUNLERID));
                            } else {
                                $scope.teklif.teklifurunadi.error('* Geçersiz Teklif Ürün Adı');
                            }
                        }
                        return $scope.teklif.teklifurunadi.isValid;
                    },
                    select: function (teklifurunadi) {
                        console.log(teklifurunadi);
                        //RULMAN VE RULMANYATAĞINDA MIKTAR, İŞÇİLİKTE ADI VE MIKTAR, SIKMA DA MIKTAR DISABLED OLMALI
                        $scope.teklif.teklifurunadi.selected = teklifurunadi;
                        console.log($scope.teklif.teklifurunadi.selected);
                        $scope.teklif.teklifurunadi.check();
                    },
                    success: function () {
                        $scope.teklif.teklifurunadi.cls = '';
                        $scope.teklif.teklifurunadi.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.teklifurunadi.cls = 'has-error';
                        $scope.teklif.teklifurunadi.msg = msg;
                    }
                },
                olcu: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.olcu.isValid = false;
                        $scope.teklif.olcu.value = '';
                        $scope.teklif.olcu.cls = '';
                        $scope.teklif.olcu.msg = '';
                        $scope.teklif.olcu.set('');
                    },
                    set: function (value) {
                        $scope.teklif.olcu.value = value;
                        $scope.teklif.olcu.isValid = true;
                        $scope.teklif.olcu.cls = '';
                        $scope.teklif.olcu.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.olcu.value);
                        if (applyCheck || $scope.teklif.olcu.isValid !== check) {
                            $scope.teklif.olcu.isValid = check;
                            if ($scope.teklif.olcu.isValid) {
                                $scope.teklif.olcu.success();
                                localStorage.setItem("teklif-create-modal-teklifadiolcusu", JSON.stringify($scope.teklif.olcu.value));
                            } else {
                                $scope.teklif.olcu.error('* Geçersiz Ölçü');
                            }
                        }
                        return $scope.teklif.olcu.isValid;
                    },
                    change: function () {
                        $scope.teklif.olcu.check($scope.teklif.olcu.value);
                    },
                    success: function () {
                        $scope.teklif.olcu.cls = '';
                        $scope.teklif.olcu.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.olcu.cls = 'has-error';
                        $scope.teklif.olcu.msg = msg;
                    }
                },
                birimsatisfiyati: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.birimsatisfiyati.isValid = false;
                        $scope.teklif.birimsatisfiyati.value = '';
                        $scope.teklif.birimsatisfiyati.text = '';
                        $scope.teklif.birimsatisfiyati.cls = '';
                        $scope.teklif.birimsatisfiyati.msg = '';
                        $scope.teklif.birimsatisfiyati.set('');
                    },
                    set: function (value) {
                        $scope.teklif.birimsatisfiyati.value = value;
                        $scope.teklif.birimsatisfiyati.text = value;
                        $scope.teklif.birimsatisfiyati.isValid = true;
                        $scope.teklif.birimsatisfiyati.cls = '';
                        $scope.teklif.birimsatisfiyati.msg = '';
                    },
                    check: function (applyCheck) {
                        $scope.teklif.birimsatisfiyati.value = $scope.teklif.birimsatisfiyati.text.tryParseMoneyToFloat();
                        var check = app.valid.positiveNumber($scope.teklif.birimsatisfiyati.value);
                        if (applyCheck || $scope.teklif.birimsatisfiyati.isValid !== check) {
                            $scope.teklif.birimsatisfiyati.isValid = check;
                            if ($scope.teklif.birimsatisfiyati.isValid) {
                                $scope.teklif.birimsatisfiyati.success();
                                localStorage.setItem("teklif-create-modal-birimsatisfiyati", JSON.stringify($scope.teklif.birimsatisfiyati.value)); 
                            } else {
                                $scope.teklif.birimsatisfiyati.error('* Geçersiz Birim Satış Fiyatı');
                            }
                        }
                        return $scope.teklif.birimsatisfiyati.isValid;
                    },
                    change: function () {
                        $scope.teklif.birimsatisfiyati.check($scope.teklif.birimsatisfiyati.value);
                        $scope.teklif.birimsatisveadet.value = $scope.teklif.birimsatisfiyati.value + (($scope.teklif.birimsatisfiyati.value / 100) * $scope.teklif.karyuzdesi.value);
                        //Birim Maliyet Fiyatının Kar yüzdesini kendisine ilave edip Birim Satış Fiyatına eşitle.
                        //Birim Satış Fiyatı ile Adet çarpımı Toplam Teklif Tutarı olacak.
                        $scope.teklif.toplamsatisfiyati.value = $scope.teklif.birimsatisveadet.value;
                        $scope.teklif.toplamsatisfiyati.text = $scope.teklif.toplamsatisfiyati.value.formatMoney(2, ',', '.');
                        $scope.teklif.toplamtekliftutari.value = $scope.teklif.toplamsatisfiyati.value * $scope.teklif.adet.value;
                        $scope.teklif.toplamtekliftutari.text = $scope.teklif.toplamtekliftutari.value.formatMoney(2, ',', '.');
                        localStorage.setItem("teklif-create-modal-birimsatisfiyati", JSON.stringify($scope.teklif.birimsatisfiyati.value));
                    },
                    success: function () {
                        $scope.teklif.birimsatisfiyati.cls = '';
                        $scope.teklif.birimsatisfiyati.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.birimsatisfiyati.cls = 'has-error';
                        $scope.teklif.birimsatisfiyati.msg = msg;
                    }
                },
                adet: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.adet.isValid = false;
                        $scope.teklif.adet.value = '';
                        $scope.teklif.adet.text = '';
                        $scope.teklif.adet.cls = '';
                        $scope.teklif.adet.msg = '';
                        $scope.teklif.adet.set('');
                    },
                    set: function (value) {
                        $scope.teklif.adet.value = value;
                        $scope.teklif.adet.text = value;
                        $scope.teklif.adet.isValid = true;
                        $scope.teklif.adet.cls = '';
                        $scope.teklif.adet.msg = '';
                    },
                    check: function (applyCheck) {
                        $scope.teklif.adet.value = $scope.teklif.adet.text.tryParseMoneyToFloat();
                        var check = app.valid.positiveNumber($scope.teklif.adet.value);
                        if (applyCheck || $scope.teklif.adet.isValid !== check) {
                            $scope.teklif.adet.isValid = check;
                            if ($scope.teklif.adet.isValid) {
                                $scope.teklif.adet.success();
                            } else {
                                $scope.teklif.adet.error('* Geçersiz Adet');
                            }
                        }
                        return $scope.teklif.adet.isValid;
                    },
                    change: function () {
                        $scope.teklif.adet.check($scope.teklif.adet.value);
                        $scope.teklif.birimsatisfiyati.check($scope.teklif.birimsatisfiyati.value);
                        $scope.teklif.birimsatisveadet.value = $scope.teklif.birimsatisfiyati.value + (($scope.teklif.birimsatisfiyati.value / 100) * $scope.teklif.karyuzdesi.value);
                        //Birim Maliyet Fiyatının Kar yüzdesini kendisine ilave edip Birim Satış Fiyatına eşitle.
                        //Birim Satış Fiyatı ile Adet çarpımı Toplam Teklif Tutarı olacak.
                        $scope.teklif.toplamsatisfiyati.value = $scope.teklif.birimsatisveadet.value;
                        $scope.teklif.toplamsatisfiyati.text = $scope.teklif.toplamsatisfiyati.value.formatMoney(2, ',', '.');
                        $scope.teklif.toplamtekliftutari.value = $scope.teklif.toplamsatisfiyati.value * $scope.teklif.adet.value;
                        $scope.teklif.toplamtekliftutari.text = $scope.teklif.toplamtekliftutari.value.formatMoney(2, ',', '.');
                        localStorage.setItem("teklif-create-modal-adet", JSON.stringify($scope.teklif.adet.value));
                        localStorage.setItem("teklif-create-modal-toplamtekliftutari", JSON.stringify($scope.teklif.toplamtekliftutari.value));
                    },
                    success: function () {
                        $scope.teklif.adet.cls = '';
                        $scope.teklif.adet.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.adet.cls = 'has-error';
                        $scope.teklif.adet.msg = msg;
                    }
                },
                karyuzdesi: { // integer sadece
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.karyuzdesi.isValid = false;
                        $scope.teklif.karyuzdesi.value = '';
                        $scope.teklif.karyuzdesi.text = '';
                        $scope.teklif.karyuzdesi.cls = '';
                        $scope.teklif.karyuzdesi.msg = '';
                        $scope.teklif.karyuzdesi.set('');
                    },
                    set: function (value) {
                        $scope.teklif.karyuzdesi.value = value;
                        $scope.teklif.karyuzdesi.text = value;
                        $scope.teklif.karyuzdesi.isValid = true;
                        $scope.teklif.karyuzdesi.cls = '';
                        $scope.teklif.karyuzdesi.msg = '';
                    },
                    check: function (applyCheck) {
                        $scope.teklif.karyuzdesi.value = $scope.teklif.karyuzdesi.text.tryParseMoneyToFloat();
                        var check = app.valid.positiveNumber($scope.teklif.karyuzdesi.value);
                        if (applyCheck || $scope.teklif.karyuzdesi.isValid !== check) {
                            $scope.teklif.karyuzdesi.isValid = check;
                            if ($scope.teklif.karyuzdesi.isValid) {
                                $scope.teklif.karyuzdesi.success();
                            } else {
                                $scope.teklif.karyuzdesi.error('* Geçersiz Kar Yüzdesi');
                            }
                        }
                        return $scope.teklif.karyuzdesi.isValid;
                    },
                    change: function () {
                        $scope.teklif.karyuzdesi.check($scope.teklif.karyuzdesi.value);
                        $scope.teklif.birimsatisfiyati.check($scope.teklif.birimsatisfiyati.value);
                        $scope.teklif.birimsatisveadet.value = $scope.teklif.birimsatisfiyati.value + (($scope.teklif.birimsatisfiyati.value / 100) * $scope.teklif.karyuzdesi.value);
                        //Birim Maliyet Fiyatının Kar yüzdesini kendisine ilave edip Birim Satış Fiyatına eşitle.
                        //Birim Satış Fiyatı ile Adet çarpımı Toplam Teklif Tutarı olacak.
                        $scope.teklif.toplamsatisfiyati.value = $scope.teklif.birimsatisveadet.value;
                        $scope.teklif.toplamsatisfiyati.text = $scope.teklif.toplamsatisfiyati.value.formatMoney(2, ',', '.');
                        $scope.teklif.toplamtekliftutari.value = $scope.teklif.toplamsatisfiyati.value * $scope.teklif.adet.value;
                        $scope.teklif.toplamtekliftutari.text = $scope.teklif.toplamtekliftutari.value.formatMoney(2, ',', '.');
                        localStorage.setItem("teklif-create-modal-karyuzdesi", JSON.stringify($scope.teklif.karyuzdesi.value));
                        localStorage.setItem("teklif-create-modal-toplamsatisfiyati", JSON.stringify($scope.teklif.toplamsatisfiyati.value));
                    },
                    success: function () {
                        $scope.teklif.karyuzdesi.cls = '';
                        $scope.teklif.karyuzdesi.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.karyuzdesi.cls = 'has-error';
                        $scope.teklif.karyuzdesi.msg = msg;
                    }
                },
                toplamsatisfiyati: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.toplamsatisfiyati.isValid = false;
                        $scope.teklif.toplamsatisfiyati.value = 0;
                        $scope.teklif.toplamsatisfiyati.text = '';
                        $scope.teklif.toplamsatisfiyati.set('');
                    },
                    set: function (toplamsatisfiyati) {
                        $scope.teklif.toplamsatisfiyati.value = toplamsatisfiyati.value;
                        $scope.teklif.toplamsatisfiyati.text = toplamsatisfiyati.value;
                        $scope.teklif.toplamsatisfiyati.isValid = true;
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.toplamsatisfiyati.value);
                        if (applyCheck || $scope.teklif.toplamsatisfiyati.isValid !== check) {
                            $scope.teklif.toplamsatisfiyati.isValid = check;
                            if ($scope.teklif.toplamsatisfiyati.isValid) {
                                $scope.teklif.toplamsatisfiyati.success();
                            } else {
                                $scope.teklif.toplamsatisfiyati.error('* Geçersiz Toplam Satış Fiyatı');
                            }
                        }
                        return $scope.teklif.toplamsatisfiyati.isValid;
                    },
                    change: function () {
                        $scope.teklif.toplamsatisfiyati.check($scope.teklif.toplamsatisfiyati.value);

                    },
                },
                toplamtekliftutari: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.toplamtekliftutari.isValid = false;
                        $scope.teklif.toplamtekliftutari.value = 0;
                        $scope.teklif.toplamtekliftutari.cls = '';
                        $scope.teklif.toplamtekliftutari.msg = '';
                        $scope.teklif.toplamtekliftutari.set('');
                    },
                    set: function (toplamtekliftutari) {
                        $scope.teklif.toplamtekliftutari.value = toplamtekliftutari.value;
                        $scope.teklif.toplamtekliftutari.text = toplamtekliftutari.value;
                        $scope.teklif.toplamtekliftutari.isValid = true;
                        $scope.teklif.toplamtekliftutari.cls = '';
                        $scope.teklif.toplamtekliftutari.msg = '';
                    },
                },
                birimsatisveadet: {
                    value: 0,
                    uyari: false,
                    init: function () {
                        $scope.teklif.birimsatisveadet.value = 0;
                        $scope.teklif.birimsatisveadet.set(0);
                    },
                    set: function (value) {
                        // $scope.teklif.detail.inputs.toplamtutar.value = value + $scope.teklif.detail.tutar.value;
                    },
                },
                toplamsatis: {
                    value: 0,
                    uyari: false,
                    init: function () {
                        $scope.teklif.toplamsatis.value = 0;
                        $scope.teklif.toplamsatis.set(0);
                    },
                    set: function (value) {
                        $scope.teklif.toplamsatis.value = value;
                    },
                },
                toplamtutar: {
                    value: 0,
                    uyari: false,
                    init: function () {
                        $scope.teklif.toplamtutar.value = 0;
                        $scope.teklif.toplamtutar.set(0);
                    },
                    set: function (value) {
                        // $scope.teklif.detail.inputs.toplamtutar.value = value + $scope.teklif.detail.tutar.value;
                    },
                },
                toplamsatisvetekliftutari: {
                    value: 0,
                    init: function () {
                        $scope.teklif.toplamsatisvetekliftutari.value = 0;
                    },
                    set: function (value) {
                        $scope.teklif.toplamsatisvetekliftutari.value = value;
                    }
                },
                init: function () {
                    $scope.teklif.toplamtekliftutari.init();
                    $scope.teklif.teklifurunadi.init();
                    $scope.teklif.olcu.init();
                    $scope.teklif.birimsatisfiyati.init();
                    $scope.teklif.adet.init();
                    $scope.teklif.karyuzdesi.init();
                    $scope.teklif.toplamsatisfiyati.init();
                    $scope.teklif.toplamtekliftutari.init();
                    $scope.teklif.detail.init();
                },
                check: function () {
                    var applyCheck = true;
                    var OK = {
                        toplamtekliftutari: $scope.teklif.toplamtekliftutari.value,
                        teklifurunadi: $scope.teklif.teklifurunadi.check(applyCheck),
                        olcu: $scope.teklif.olcu.check(applyCheck),
                        birimsatisfiyati: $scope.teklif.birimsatisfiyati.check(applyCheck),
                        adet: $scope.teklif.adet.check(applyCheck),
                        karyuzdesi: $scope.teklif.karyuzdesi.check(applyCheck),
                        details: $scope.teklif.detail.check(applyCheck)
                    };
                    return OK.toplamtekliftutari && OK.teklifurunadi && OK.olcu && OK.birimsatisfiyati && OK.adet && OK.karyuzdesi && OK.details;
                },
                isExist: false,
                save: function () { // ilk kayıt ekleniyorsa... 
                    // miktar ve adı boş geçilmemeli. Ama disabled olduğu durumda boş gönderilmeli.
                    if (JSON.parse(localStorage.getItem("aciklama")) === null) { // teklif-create-component.js den gelir.
                        $scope.teklif.aciklama = '';
                        localStorage.setItem("aciklama", JSON.stringify($scope.teklif.aciklama));
                    }
                    if (JSON.parse(localStorage.getItem("teklifkodu")) === null) { // teklif-create-component.js den gelir.
                        $scope.teklif.teklifkodukontrolu = '';
                        localStorage.setItem("teklifkodu", JSON.stringify($scope.teklif.teklifkodukontrolu));
                    }
                    if (JSON.parse(localStorage.getItem("tekliftarihi")) === null) { // teklif-create-component.js den gelir.
                        localStorage.setItem("tekliftarihi", JSON.stringify(new Date()));
                    }
                    if (!$scope.teklif.check() || $scope.teklif.detail.rows === null) { 
                        return;
                    }
                    if ($rootScope.teklifidExist !== 0) { 
                        console.log("EĞER TEKLIF ID VARSA MODIFY ÇALIŞACAK");
                        $scope.teklif.modify();
                        return;
                    }
                    console.log($scope.teklif.teklifurunadi.selected.TEKLIFURUNLERID);
                    if ($scope.teklif.teklifurunadi.selected.TEKLIFURUNLERID == 1) {
                        console.log($scope.teklif.detail.rows);
                        // RULO seçilirse
                        //boru - mil - rulman - rulman yatağı - işçilik - boya & paketleme grupları girimediyse kullanıcıya uyarı verilmeli.
                        var boru = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('BORU');
                        var mil = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('MİL');
                        var rulman = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('RULMAN');
                        var rulmanyatagi = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('RULMAN YATAĞI');
                        var iscilik = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('İŞÇİLİK');
                        var boyapaketleme = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('BOYA&PAKETLEME');
                 
                        if (boru == -1 || mil == -1 || rulman == -1 || rulmanyatagi == -1 || iscilik == -1 || boyapaketleme == -1) {
                            $rootScope.showSabit6hammaddeyarimamulgrubuUyariModal('teklif-create-modal-save', ' ');
                            return;
                        }
                    }       
                    if ($scope.teklif.teklifurunadi.selected.TEKLIFURUNLERID == 2) {
                        console.log($scope.teklif.detail.rows);
                        // TAMBUR seçilirse
                        //boru - mil - işçilik - pens - kaplama - flanş grupları girimediyse kullanıcıya uyarı verilmeli.
                        var boru = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('BORU');
                        var mil = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('MİL');
                        var pens = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('PENS/CTL');
                        var kaplama = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('KAPLAMA');
                        var iscilik = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('İŞÇİLİK');
                        var flans = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('FLANŞ');
                        console.log("PENS -> ", pens);
                        if (boru == -1 || mil == -1 || pens == -1 || kaplama == -1 || iscilik == -1 || flans == -1) {
                            $rootScope.showTamburcreateaspxUyariModal('teklif-create-modal-save', ' ');
                            return;
                        }
                    }  
                    app.show.spinner($('teklif-create-modal > div:first > div:first'));
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=3&d=' + new Date(), // TYPE 3
                        data: {
                            cari: JSON.parse(localStorage.getItem("cari")), 
                            tekliftarihi: JSON.parse(localStorage.getItem("tekliftarihi")),
                            teklifkodu: JSON.parse(localStorage.getItem("teklifkodu")),
                            toplamtekliftutari: $scope.teklif.toplamtekliftutari.value,//birim satış fiyatı * adet = toplam teklif tutarı
                            aciklama: JSON.parse(localStorage.getItem("aciklama")),
                            teklifadi: $scope.teklif.teklifurunadi.selected.TEKLIFURUNLERID,
                            teklifadiolcusu: $scope.teklif.olcu.value,
                            birimsatisfiyati: $scope.teklif.birimsatisfiyati.value,
                            adet: $scope.teklif.adet.value,
                            karyuzdesi: $scope.teklif.karyuzdesi.value,
                            toplamsatisfiyati: $scope.teklif.toplamsatisfiyati.value,
                            maliyettekliftutari: $scope.teklif.toplamtekliftutari.value,
                            detail: $scope.teklif.detail.rows
                        }
                    }).then(function (response) {
                        app.hide.spinner($('teklif-create-modal > div:first > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 1) { // veritabanından gelen RESULT değeri 1 ise kaydedilmiştir.
                            $rootScope.teklifcreate = true;
                            console.log(response.data);
                            $scope.teklif.ilkeklenenkayit = response.data.TEKLIFKODU;
                            app.show.success('Yeni Teklif Oluşturuldu!'); // success modalı.
                           //localStorage.removeItem('teklifdata');
                            $rootScope.control = true;
                            localStorage.setItem("teklifdata", JSON.stringify(response.data));
                            Runopen3 = $rootScope.run3; // teklif-create-component.js de ki function burada çalıştırılır. Yeni eklenen kaydı teklif-create-componentte listelemek için.
                            Runopen3();
                            $rootScope.teklifidExist = response.data.TEKLIFLERID;
                            $scope.teklif.id.value = response.data.TEKLIFLERID;
                            $rootScope.teklifcreatemodalId = $scope.teklif.id.value;
                            console.log($rootScope.teklifcreatemodalId);
                            $rootScope.index = $scope.index;
                            $('.teklif-create-modal:first').modal('hide'); // modal kapanır.
                            //
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                            window.location.href = 'login.aspx';
                            return;
                        }
                        if (response.data['RESULT'] === 124) {
                            app.show.warning('UYARI', 'Bu Teklif daha önce oluşturuldu.');
                            return;
                        }
                        app.show.error('Bir şeyler ters gitti...');
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                        app.hide.spinner($('teklif-create-modal > div:first > div:first'));
                    });
                },
                modify: function () { // eğer ilk kayıttan sonraki kayıtlar ekleniyorsa... TEKLIFLER ID aynı kalmalı. sadece maliyet ve kalemleri ekleniyor.
                    //if (JSON.parse(localStorage.getItem("aciklama")) === null) {
                    //    $scope.teklif.aciklama = '';
                    //    localStorage.setItem("aciklama", JSON.stringify($scope.teklif.aciklama));
                    //}
                    console.log($scope.teklif.ilkeklenenkayit);
                    if ($scope.teklif.ilkeklenenkayit == '') {
                        $scope.teklif.ilkeklenenkayit = JSON.parse(localStorage.getItem("sabit6hamyarimamul-uyari-modal-teklifkodu"));
                    }
                    if (JSON.parse(localStorage.getItem("teklifkodu")) === null) {
                        $scope.teklif.teklifkodukontrolu = '';
                        localStorage.setItem("teklifkodu", JSON.stringify($scope.teklif.teklifkodukontrolu));
                    }
                    if (JSON.parse(localStorage.getItem("tekliftarihi")) === null) {
                        localStorage.setItem("tekliftarihi", JSON.stringify(new Date()));
                    }
                    console.log($scope.teklif.id.value);
                    if ($scope.teklif.id.value == 0) {
                        if ($rootScope.teklifmodifymodalId != undefined) {
                            $scope.teklif.id.value = $rootScope.teklifmodifymodalId;
                            console.log($scope.teklif.id.value);
                        }
                        console.log($scope.teklif.id.value);
                        $scope.teklif.id.value = JSON.parse(localStorage.getItem("sabit6hamyarimamul-uyari-modal-id")); // sabit6hammadde-uyari-modal dan da gelebilir.
                        console.log($scope.teklif.id.value);
                    }
                    if (!$scope.teklif.check()) {
                        return;
                    }
                    if ($scope.teklif.teklifurunadi.selected.TEKLIFURUNLERID == 1) {
                        console.log($scope.teklif.detail.rows);
                        // RULO seçilirse
                        //boru - mil - rulman - rulman yatağı - işçilik - boya & paketleme grupları girimediyse kullanıcıya uyarı verilmeli.
                        var boru = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('BORU');
                        var mil = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('MİL');
                        var rulman = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('RULMAN');
                        var rulmanyatagi = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('RULMAN YATAĞI');
                        var iscilik = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('İŞÇİLİK');
                        var boyapaketleme = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('BOYA&PAKETLEME');
           
                        if (boru == -1 || mil == -1 || rulman == -1 || rulmanyatagi == -1 || iscilik == -1 || boyapaketleme == -1) {
                            $rootScope.showSabit6hammaddeyarimamulgrubuUyariModal('teklif-create-modal-modify', ' ');
                            return;
                        }
                    }
                    if ($scope.teklif.teklifurunadi.selected.TEKLIFURUNLERID == 2) {
                        console.log($scope.teklif.detail.rows);
                        // TAMBUR seçilirse
                        //boru - mil - işçilik - pens - kaplama - flanş grupları girimediyse kullanıcıya uyarı verilmeli.
                        var boru = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('BORU');
                        var mil = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('MİL');
                        var pens = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('PENS/CTL');
                        var kaplama = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('KAPLAMA');
                        var iscilik = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('İŞÇİLİK');
                        var flans = $scope.teklif.detail.rows.map(function (ad) { return ad.HAMMADDEYARIMAMULADI; }).indexOf('FLANŞ');
                        console.log("PENS -> ", pens);
                        if (boru == -1 || mil == -1 || pens == -1 || kaplama == -1 || iscilik == -1 || flans == -1) {
                            $rootScope.showTamburcreateaspxUyariModal('teklif-create-modal-modify', ' ');
                            return;
                        }
                    } 
                    app.show.spinner($('teklif-create-modal > div:first > div:first'));
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=8&d=' + new Date(), // TYPE 8
                        data: {
                            teklifid: $scope.teklif.id.value,
                            cari: JSON.parse(localStorage.getItem("cari")),
                            tekliftarihi: JSON.parse(localStorage.getItem("tekliftarihi")),
                            teklifkodu: $scope.teklif.ilkeklenenkayit,
                            toplamtekliftutari: $scope.teklif.toplamtekliftutari.value,//satış fiyatlarının toplamı 'Toplam Teklif Tutarı'nı verir.
                            aciklama: JSON.parse(localStorage.getItem("aciklama")),
                            teklifadi: $scope.teklif.teklifurunadi.selected.TEKLIFURUNLERID,
                            teklifadiolcusu: $scope.teklif.olcu.value,
                            birimsatisfiyati: $scope.teklif.birimsatisfiyati.value,
                            adet: $scope.teklif.adet.value,
                            karyuzdesi: $scope.teklif.karyuzdesi.value,
                            onaydurumu: $scope.teklif.onaydurumu,
                            toplamsatisfiyati: $scope.teklif.toplamsatisfiyati.value,
                            maliyettekliftutari: $scope.teklif.toplamtekliftutari.value,
                            detail: $scope.teklif.detail.rows
                        }
                    }).then(function (response) {
                        app.hide.spinner($('teklif-create-modal > div:first > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 1) {
                            $rootScope.control = true;
                            localStorage.setItem("teklifdata", JSON.stringify(response.data));
                            console.log(response.data);
                            app.show.success('Teklif-Maliyet Bilgileri Eklendi!');
                            Runopen3 = $rootScope.run3;
                            Runopen3();
                            $rootScope.teklifidExist = response.data.TEKLIFLERID;
                            $scope.teklif.id.value = response.data.TEKLIFLERID;
                            $rootScope.teklifcreatemodalId = $scope.teklif.id.value;
                            console.log($rootScope.teklifcreatemodalId);
                            $('.teklif-create-modal:first').modal('hide');
                            //
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                            window.location.href = 'login.aspx';
                            return;
                        }
                        app.show.error('Bir şeyler ters gitti...');
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                        app.hide.spinner($('teklif-create-modal > div:first > div:first'));
                    });
                },
      
            };
        }]
    })


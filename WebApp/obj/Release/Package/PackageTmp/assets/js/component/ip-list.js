﻿app.angular
    .component('ipList', {
        templateUrl: 'assets/template/ip-list.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
            $rootScope.modifyRow = function (ip) {
                var i = 0;
                var goon = true;
                while (goon && $scope.ip.list.length > i) {
                    if (ip.ID === $scope.ip.list[i].ID) {
                        goon = false;
                        $scope.ip.list[i] = ip;
                    }
                    i++;
                }
            };
            $rootScope.deleteRow = function (ip) {
                var i = 0;
                var goon = true;
                while (goon && $scope.ip.list.length > i) {
                    if (ip.ID === $scope.ip.list[i].ID) {
                        goon = false;

                        $scope.ip.load();
                    }
                    i++;
                }
            };
            $rootScope.loadList = function () {
                if ($scope.ip.filter.allowAll.value) {
                    return;
                }
                $scope.ip.filter.init();
                $scope.ip.page.index = 1;
                $scope.ip.load();
            };
            $scope.ip = {
                page: {
                    index: 1
                },
                filter: {
                    value: '',
                    allowAll: {
                        ID: 1,
                        value: systemConfig[1]['BOOLEAN'],
                        change: function () {
                            app.show.spinner($('ip-list > div:first'));
                            $http({
                                method: 'POST',
                                url: 'hub/config.ashx?&t=4&d=' + new Date(),
                                data: {
                                    id: $scope.ip.filter.allowAll.ID,
                                    type: systemConfig[$scope.ip.filter.allowAll.ID]['TYPEVAL'],
                                    code: systemConfig[$scope.ip.filter.allowAll.ID]['CODE'],
                                    name: systemConfig[$scope.ip.filter.allowAll.ID]['NAME'],
                                    boolean: $scope.ip.filter.allowAll.value,
                                    date: systemConfig[$scope.ip.filter.allowAll.ID]['DATEVAL'],
                                    integer: systemConfig[$scope.ip.filter.allowAll.ID]['INT'],
                                    deciml: systemConfig[$scope.ip.filter.allowAll.ID]['DECIMAL'],
                                    string128: systemConfig[$scope.ip.filter.allowAll.ID]['STRING128'],
                                    string256: systemConfig[$scope.ip.filter.allowAll.ID]['STRING256']
                                }
                            }).then(function (response) {
                                app.hide.spinner($('ip-list > div:first'));
                                if (!response.data.hasOwnProperty('RESULT')) {
                                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                                    return;
                                }
                                if (response.data['RESULT'] === 1) {//success
                                    $rootScope.setCreateBtnVisibility(!$scope.ip.filter.allowAll.value);
                                    return;
                                }
                                if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive
                                    window.location.href = 'login.aspx';
                                    return;
                                }
                                if (response.data['RESULT'] === 70) {//user has no right to modify configs
                                    app.show.error('Sistem konfigürasyon ayarlarını değiştirme yetkiniz bulunmuyor!');//'Sistem konfigürasyon ayarlarını değiştirme yetkiniz bulunmuyor!'
                                    return;
                                }
                                if (response.data['RESULT'] === 72) {//can not reached config record with that id, type, code, name
                                    app.show.error('IP adresi ile alakalı sistem konfigürasyon ayarları bulunamadı!');//'IP adresi ile alakalı sistem konfigürasyon ayarları bulunamadı!'
                                    return;
                                }
                                //if running code come through this far then it means an unexpected error occurred
                                app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                                /*if (response.data['RESULT'] === 71) {//invalid config type
                                    app.show.error(systemMessages[624]);//'Bir şeyler ters gitti...'
                                    return;
                                }*/
                            }, function (response) {
                                $scope.ip.isLoading = false;
                                console.log(response);
                                app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                                app.hide.spinner($('ip-list > div:first'));
                            });
                        }
                    },
                    init: function () {
                        $scope.ip.filter.value = '';
                    },
                    change: function () {
                        $scope.ip.filter.timer.set();
                    },
                    timer: {
                        value: null,
                        interval: 500,
                        set: function () {
                            if ($scope.ip.filter.timer.value) {
                                $timeout.cancel($scope.ip.filter.timer.value);
                                $scope.ip.filter.timer.value = null;
                            }
                            $scope.ip.filter.timer.value = $timeout(function () {
                                if ($scope.ip.isLoading) {
                                    return;
                                }
                                $scope.ip.page.index = 1;
                                $scope.ip.load();
                            }, $scope.ip.filter.timer.interval);
                        }
                    }
                },
                totalRow: {
                    value: 0,
                    set: function (value) {
                        var paginate = $scope.ip.totalRow.value !== value;
                        $scope.ip.totalRow.value = value;
                        if (paginate) {
                            $scope.ip.paginate();
                        }
                    }
                },
                maxVisibleRow: {
                    value: app.pagination.maxVisible.row,
                    set: function (value) {
                        var paginate = $scope.ip.maxVisibleRow.value !== value;
                        $scope.ip.maxVisibleRow.value = value;
                        if (paginate) {
                            $scope.ip.paginate();
                        }
                    }
                },
                list: [],
                isExist: false,
                load: function () {
                    $scope.ip.isLoading = true;
                    app.show.spinner($('ip-list > div:first'));
                    $http({
                        method: 'POST',
                        url: 'hub/ip.ashx?&t=2&d=' + new Date(),
                        data: {
                            pageIndex: $scope.ip.page.index,
                            filter: $scope.ip.filter.value,
                            pageRowCount: $scope.ip.maxVisibleRow.value

                        }
                    }).then(function (response) {
                        $scope.ip.isLoading = false;
                        app.hide.spinner($('ip-list > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...4');//'Bir şeyler ters gitti...'
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive
                            window.location.href = 'login.aspx';
                            return;
                        }
                        $scope.ip.list = response.data.LIST;
                        $scope.ip.isExist = $scope.ip.list.length > 0;
                        $scope.ip.totalRow.set(response.data.TOTALROW);
                    }, function (response) {
                        $scope.ip.isLoading = false;
                        console.log(response);
                        app.show.error('Bir şeyler ters gitti...5');//'Bir şeyler ters gitti...'
                        app.hide.spinner($('ip-list > div:first'));
                    });
                },
                show: {
                    update: function (ip) {
                        $rootScope.showModifyModal(ip);

                    },
                    delet: function (ip) {
                        $rootScope.showDeleteModal(ip);
                    }
                },
                paginate: function () {
                    var pageCount = $scope.ip.totalRow.value !== 0 ? Math.ceil($scope.ip.totalRow.value / $scope.ip.maxVisibleRow.value) : 1;
                    $('.bootpag-flat').bootpag({
                        total: pageCount,
                        maxVisible: app.pagination.maxVisible.pageIndex,
                        page: $scope.ip.page.index,
                        leaps: false
                    }).on("page", function (event, num) {
                        $scope.ip.page.index = num;
                        $scope.ip.load();
                    }).children('.pagination').addClass('pagination-flat pagination-sm');
                },
                switchery: function () {
                    $timeout(function () {
                        var warning = document.querySelector('.switchery-warning');
                        $(warning).next().text('Hepsine izin ver');//'Hepsine izin ver'
                        var switchery = new Switchery(warning, { color: '#FFA726' });
                    });
                }
            };
            $scope.ip.load();
            $scope.ip.switchery();
        }]
    });
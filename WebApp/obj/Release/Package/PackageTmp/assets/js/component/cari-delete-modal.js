﻿app.angular
    .component('cariDeleteModal', {
        templateUrl: 'assets/template/cari-delete-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
        
            $rootScope.showDeleteModal = function (_cari) {
                $scope.cari.set(_cari);
                $('.cari-delete-modal:first').modal({
                    backdrop: 'static'
                });
            };
            $scope.cari = {
                rawdata: null,
                id: {
                    value: 0,
                    init: function () {
                        $scope.cari.id.value = 0;
                    },
                    set: function (value) {
                        $scope.cari.id.value = value;
                    }
                },
                cari: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.cari.cari.isValid = false;
                        $scope.cari.cari.value = '';
                        $scope.cari.cari.cls = '';
                        $scope.cari.cari.msg = '';
                        $scope.cari.cari.set('');
                    },
                    set: function (value) {
                        $scope.cari.cari.value = value;
                        $scope.cari.cari.isValid = true;
                        $scope.cari.cari.cls = '';
                        $scope.cari.cari.msg = '';
                    },
                },
                state: {
                    name: 'Aktif',
                    value: true,
                    icon: {
                        cls: 'icon-check text-success'
                    },
                    init: function () {
                        $scope.cari.state.select($scope.cari.state.active);
                    },
                    select: function (state) {
                        $scope.cari.state.name = state.name;
                        $scope.cari.state.value = state.value;
                        $scope.cari.state.icon.cls = state.icon.cls.replace(/ pull-right/g, '');
                    },
                    set: function (value) {
                        $scope.cari.state.select(value ? $scope.cari.state.active : $scope.cari.state.passive);
                    },
                    active: {
                        name: 'Aktif',
                        value: true,
                        icon: {
                            cls: 'icon-check text-success'
                        },
                        anchor: {
                            cls: ''
                        }
                    },
                    passive: {
                        name: 'Pasif',
                        value: false,
                        icon: {
                            cls: 'icon-blocked text-danger'
                        },
                        anchor: {
                            cls: ''
                        }
                    }
                },
                aciklama: {
                    value: '',
                    set: function (aciklama) {
                        $scope.cari.aciklama.value = aciklama;
                    },
                    init: function () {
                        $scope.cari.aciklama.value = '';
                    }
                },
                init: function () {
                    $scope.cari.cari.init();
                    $scope.cari.state.init();
                    $scope.cari.aciklama.init();
                },
                set: function (_cari) {
                    $scope.cari.rawdata = _cari;
                    $scope.cari.id.set(_cari.CARILERID);
                    $scope.cari.cari.set(_cari.ADI);
                    $scope.cari.state.set(_cari.AKTIF);
                    $scope.cari.aciklama.set(_cari.ACIKLAMA);
                },
                delete: function () {
                    app.show.spinner($('cari-delete-modal > div:first > div:first'));

                    $http({
                        method: 'POST',
                        url: 'hub/cari.ashx?&t=5&d=' + new Date(),
                        data: {
                            id: $scope.cari.id.value,
                            cariadi: $scope.cari.cari.value,
                            aktif: $scope.cari.state.value,
                            aciklama: $scope.cari.aciklama.value
                        }
                    }).then(function (response) {
                        app.hide.spinner($('cari-delete-modal > div:first > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti.');
                            return;
                        }
                        if (response.data['RESULT'] === 1) {
                            $scope.cari.init();
                            $rootScope.loadList();
                            app.show.success('Cari Bilgisi Silindi !');
                            $('.cari-delete-modal:first').modal('hide');
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                            window.location.href = 'login.aspx';
                            return;
                        }
                        if (response.data['RESULT'] === 124) {
                            app.show.warning('UYARI', 'Bu Cari bilgisi daha önce oluşturuldu.');
                            console.log(1);
                            return;
                        }
                        if (response.data['RESULT'] === 18) {
                            $scope.cari.cari.isValid = false;
                            $scope.cari.cari.error('*Geçersiz Cari');
                            return;
                        }
                        app.show.error('Bir şeyler ters gitti.');
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti.');//systemMessages[1934]
                        app.hide.spinner($('cari-delete-modal > div:first > div:first'));
                    });
                }
            };
        }],
        bindings: {
            dateDisable: '<'
        }
    });

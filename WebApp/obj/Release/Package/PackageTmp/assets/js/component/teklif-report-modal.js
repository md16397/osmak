﻿app.angular
    .component('teklifReportModal', {
        templateUrl: 'assets/template/teklif-report-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
 
            $rootScope.showReportModal = function () {
                localStorage.removeItem('dolareuro');
                localStorage.removeItem('DOLARALISFIYATI');
                localStorage.removeItem('DOLARSATISFIYATI');
                localStorage.removeItem('EUROALISFIYATI');
                localStorage.removeItem('EUROSATISFIYATI');
                localStorage.removeItem('Tarih');
                $scope.teklif.init();
                $http({
                    method: "GET", //DOLAR ve EURO
                    url: 'hub/teklif.ashx?&t=13&d=' + new Date(), // dolar ve euro kurlarını excel'de göstermek için.
                    data: "{}",
                    contentType: "application/json",
                    dataType: "json",
                }).then(function (response) {
                    localStorage.setItem("dolareuro", JSON.stringify(response.data));
                    localStorage.setItem("DOLARALISFIYATI", JSON.stringify(response.data.DOLARALISFIYATI));
                    localStorage.setItem("DOLARSATISFIYATI", JSON.stringify(response.data.DOLARSATISFIYATI));
                    localStorage.setItem("EUROALISFIYATI", JSON.stringify(response.data.EUROALISFIYATI));
                    localStorage.setItem("EUROSATISFIYATI", JSON.stringify(response.data.EUROSATISFIYATI));
                    localStorage.setItem("Tarih", JSON.stringify(response.data.Tarih));
                }, function (response) {
                    app.show.error('Bir şeyler ters gitti...');
                });
                //https://stackoverflow.com/questions/51506138/angularjs-group-objects-in-an-array-with-a-same-value-into-a-new-array
          
                $('.teklif-report-modal:first').modal({
                    backdrop: 'static'
                });
            };
            $scope.teklif = {
                list: [],
                detailslist: [],
                detailslist2: [],
                teklifdata: [],
                teklifkod: null,
                init: function () {
                    $scope.teklif.cari.init();
                    $scope.teklif.kullanici.init();
                    $scope.teklif.onaydurumu.init();
                    $scope.teklif.baslangictarihi.init();
                    $scope.teklif.bitistarihi.init();
                },
                cari: {
                    isValid: false,
                    selected: null,
                    cls: '',
                    msg: '',
                    inputValue: '',
                    shouldLoadOptionsOnInputFocus: true,
                    init: function () {
                        $scope.teklif.cari.isValid = false;
                        $scope.teklif.cari.selected = null;
                        $scope.teklif.cari.cls = '';
                        $scope.teklif.cari.msg = '';
                        $scope.teklif.cari.inputValue = '';
                        $scope.teklif.cari.shouldLoadOptionsOnInputFocus = true;
                    },
                    select: function (cari) {
                        $scope.teklif.cari.selected = cari;
                    },
                    set: function (cari) {
                        $scope.teklif.cari.isValid = true;
                        $scope.teklif.cari.selected = cari;
                        $scope.teklif.cari.cls = '';
                        $scope.teklif.cari.msg = '';
                        $scope.teklif.cari.inputValue = cari.CARI; //select içerisinde console.log(cari); yaz gelen değerlere bak. ADI ve PARAMETRELERID geliyor
                        $scope.teklif.cari.shouldLoadOptionsOnInputFocus = true;
                    }
                },
                kullanici: {
                    isValid: false,
                    selected: null,
                    cls: '',
                    msg: '',
                    inputValue: '',
                    shouldLoadOptionsOnInputFocus: true,
                    init: function () {
                        $scope.teklif.kullanici.isValid = false;
                        $scope.teklif.kullanici.selected = null;
                        $scope.teklif.kullanici.cls = '';
                        $scope.teklif.kullanici.msg = '';
                        $scope.teklif.kullanici.inputValue = '';
                        $scope.teklif.kullanici.shouldLoadOptionsOnInputFocus = true;
                    },
                    select: function (kullanici) {
                        $scope.teklif.kullanici.selected = kullanici;
                    },
                    set: function (kullanici) {
                        $scope.teklif.kullanici.isValid = true;
                        $scope.teklif.kullanici.selected = kullanici;
                        $scope.teklif.kullanici.cls = '';
                        $scope.teklif.kullanici.msg = '';
                        $scope.teklif.kullanici.inputValue = kullanici.KULLANICI; //select içerisinde console.log(kullanici); yaz gelen değerlere bak. ADI ve PARAMETRELERID geliyor
                        $scope.teklif.kullanici.shouldLoadOptionsOnInputFocus = true;
                    }
                },
                onaydurumu: {
                    list: page.data.onaydurumu,
                    isValid: false,
                    selected: { name: '', value: 2 },
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.onaydurumu.isValid = false;
                        $scope.teklif.onaydurumu.selected = { name: '', value: 2 };
                        $scope.teklif.onaydurumu.cls = '';
                        $scope.teklif.onaydurumu.msg = '';
                    }
                },
                baslangictarihi: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.baslangictarihi.isValid = false;
                        $scope.teklif.baslangictarihi.value = '';
                        $scope.teklif.baslangictarihi.cls = '';
                        $scope.teklif.baslangictarihi.msg = '';
                    }
                },
                bitistarihi: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.bitistarihi.isValid = false;
                        $scope.teklif.bitistarihi.value = '';
                        $scope.teklif.bitistarihi.cls = '';
                        $scope.teklif.bitistarihi.msg = '';
                    }
                },
                raporal: function () {  
                    console.log($scope.teklif.onaydurumu.selected.value);
                    if ($scope.teklif.cari.selected == null && $scope.teklif.kullanici.selected == null &&
                        $scope.teklif.baslangictarihi.value == '' && $scope.teklif.bitistarihi.value == '' &&
                        $scope.teklif.onaydurumu.selected.value == 2) { // eğer hiçbir filtre seçilmediyse uyarı modalı olmalı.
                        app.show.warning("Lütfen seçim yapınız.");
                        return;
                    }
                    app.show.spinner($('teklif-list > div:first'));
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=16&d=' + new Date(),  // Rapor alırken bütün teklife ait data gelmeli.
                        data: {
                            cariid: $scope.teklif.cari.selected === null ? 0 : $scope.teklif.cari.selected.CARILERID,
                            kullaniciid: $scope.teklif.kullanici.selected === null ? 0 : $scope.teklif.kullanici.selected.ID,
                            filtreonaydurumu: $scope.teklif.onaydurumu.selected.value,
                            onaydurumu: $scope.teklif.onaydurumu.selected.value === 2 ? null : $scope.teklif.onaydurumu.selected.value,
                            baslangictarihi: $scope.teklif.baslangictarihi.value === '' ? '' : new Date($scope.teklif.baslangictarihi.value).toLocaleDateString().split(".").reverse().join("-"),
                            bitistarihi: $scope.teklif.bitistarihi.value === '' ? '' : new Date($scope.teklif.bitistarihi.value).toLocaleDateString().split(".").reverse().join("-"),
                        }
                    }).then(function (response) {
                        app.hide.spinner($('teklif-list > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or user is passive
                            window.location.href = 'login.aspx';
                            return;
                        }
                        if (response.data.LIST == 0) {
                            app.show.warning("Seçtiğiniz aralıklarda Teklif bulunamadı.");
                        }
                        else {
                            $scope.teklif.detailslist = response.data.LIST;
                            $scope.teklif.isExist = $scope.teklif.detailslist.length > 0;
                            console.log($scope.teklif.detailslist);
                            var eventArray = $scope.teklif.detailslist,
                                result = Array.from(
                                    eventArray.reduce((m, o) => m.set(o.TEKLIFID, (m.get(o.TEKLIFID) || []).concat(o)), new Map),
                                    ([TEKLIFID, events]) => ({ TEKLIFID, events })
                                );
                            console.log(result);
                            $scope.teklif.detailslist = result; 
                            console.log($scope.teklif.detailslist);

                            $scope.teklif.isExist = $scope.teklif.detailslist.length > 0;
                            var teklifDate = new Date();
                            var yearTxt = teklifDate.getFullYear();
                            var montTxt = (teklifDate.getMonth() < 9 ? '0' : '') + (teklifDate.getMonth() + 1);
                            var dayTxt = (teklifDate.getDate() < 10 ? '0' : '') + teklifDate.getDate();
                            var filename = 'TeklifMaliyetReport_' + dayTxt + '-' + montTxt + '-' + yearTxt + '.xls';
                            $scope.teklif.tableToExcel('ReportTable', 'ExcelReportPage', filename);
                            $('.teklif-report-modal:first').modal('hide');
                            app.show.success('Tekliflere Ait Rapor Oluşturuldu!');
                        }
                    }, function (response) {
                            app.show.error('Bir şeyler ters gitti...');
                            console.log(response);
                        app.hide.spinner($('teklif-list > div:first'));
                    });
                },
                tableToExcel: (function () {
                    var uri = 'data:application/vnd.ms-excel;charset=UTF-8;base64,'
                        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

                    return function (table, name, filename) {
                        var ctx = { worksheet: name || 'Worksheet', table: $scope.teklif.dataToInnerTable() };//table.innerHTML } //değiştirdim
                        var blobData = new Blob([format(template, ctx)], { type: 'application/vnd.ms-excel' });//ekledim
                        //var blobdata = new Blob([table], { type: 'text/csv' });
                        document.getElementById("dlink").href = uri + base64(format(template, ctx));// uri + base64(format(template, ctx));//değiştirdim
                        document.getElementById("dlink").download = filename;
                        document.getElementById("dlink").click();
                    }
                })(),
                dataToInnerTable: function () {
                    var thead =
                        "<thead><tr><td><th></th></td></tr></thead>";
                    var tbody = "<tbody>";
                     $scope.Osmaklogo = '<img src="http://osmaksistem.com/assets/images/logo_entry.png" width="190" height="75"';  
                    var tr =
                        "<tr><td>" + $scope.Osmaklogo +  "</td></tr>";
                    tbody += tr;
                    
                    for (var i = 0; i < $scope.teklif.detailslist.length; i++) {
                        var tr =
                        "<tr><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 180px; background-color: #474866; color: white;\"><b>" + "MÜŞTERİ ADI" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 170px; background-color: #474866; color: white;\"><b>" + "TOPLAM TEKLİF TUTARI" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 150px; background-color: #474866; color: white;\"><b>" + "TEKLİF KODU" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 110px; background-color: #474866; color: white;\"><b>" + "TEKLİF TARİHİ" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 110px; background-color: #474866; color: white;\"><b>" + "AÇIKLAMA" +
                        "</b></td></td></td></tr>";
                        tbody += tr;
                        console.log($scope.teklif.detailslist.length); 
                            var tr =
                            "<tr><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 18px; color: #00173C;\"><b>" + $scope.teklif.detailslist[i].events[0].CARI +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.detailslist[i].events[0].TOPLAMTEKLIFTUTARITXT + " ₺" +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.detailslist[i].events[0].TEKLIFKODU.replace(/,/g, "','") + "'" + //EXCEL teklif kodunu biçimlendiriyor. Bunu önlemek için ' kullanıldı.
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.detailslist[i].events[0].TEKLIFTARIHITXT +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.detailslist[i].events[0].TEKLIFACIKLAMA +
                            "</b></td></td></td></tr>";
                        tbody += tr; 
                        console.log($scope.teklif.detailslist.length); 
                        $scope.Tarih = "* Bu kurlar Merkez Bankasının " + localStorage.getItem("Tarih") + " tarihli verilerinden alınmıştır.";
                        var tr =
                            "<tr><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "DOLAR ALIŞ" +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "DOLAR SATIŞ" +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "EURO ALIŞ" +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "EURO SATIŞ" +
                            "</b></td><td style=\"font-size: 9px;\" rowspan='2'><b>" + $scope.Tarih +
                            "</b></td></td></td></tr>";
                        tbody += tr;

                        $scope.dolareuro = localStorage.getItem("dolareuro");
                        $scope.DOLARALISFIYATI = parseFloat(localStorage.getItem("DOLARALISFIYATI")).toFixed(3);
                        $scope.DOLARSATISFIYATI = parseFloat(localStorage.getItem("DOLARSATISFIYATI")).toFixed(3);
                        $scope.EUROALISFIYATI = parseFloat(localStorage.getItem("EUROALISFIYATI")).toFixed(3);
                        $scope.EUROSATISFIYATI = parseFloat(localStorage.getItem("EUROSATISFIYATI")).toFixed(3);
                        $scope.Tarih = "* Bu kurlar Merkez Bankasının " + localStorage.getItem("Tarih") + " tarihli verilerinden alınmıştır.";

                        if (typeof $scope.dolareuro !== 'undefined') {
                            var tr =
                                "<tr><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.DOLARALISFIYATI + " ₺" +
                                "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.DOLARSATISFIYATI + " ₺" +
                                "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.EUROALISFIYATI + " ₺" +
                                "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.EUROSATISFIYATI + " ₺" +
                                "</b></td></td></td></tr>";
                            tbody += tr;
                        }
                        var tr = "<tr></tr>"
                        tbody += tr;

                        do {
                            if ($scope.teklif.detailslist.length > 0) { // KALEMLERI GRUPLAMAK ICIN.
                                console.log($scope.teklif.detailslist[i]); // 7 data
                                var eventArray = $scope.teklif.detailslist[i].events,
                                    result = Array.from(
                                        eventArray.reduce((m, o) => m.set(o.TEKLIFMALIYETID, (m.get(o.TEKLIFMALIYETID) || []).concat(o)), new Map),
                                        ([TEKLIFMALIYETID, events]) => ({ TEKLIFMALIYETID, events })
                                    );
                                console.log(result);
                                $scope.teklif.detailslist2 = result;
                            for (var y = 0; (typeof $scope.teklif.detailslist2[y] !== 'undefined'); y++) {
                                console.log($scope.teklif.detailslist2[y]); 
                                var tr =
                                    "<tr><td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 180px; background-color: #9d9eab;\"><b>" + "ÜRÜN ADI" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 150px; background-color: #9d9eab;\"><b>" + "ÜRÜN ÖLÇÜSÜ" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "BİRİM MALİYET FİYATI" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "BİRİM MALİYET ADETİ" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "KAR YÜZDESİ" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "BİRİM SATIŞ FİYATI" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 160px; background-color: #9d9eab;\" colspan='2'><b>" + "TOPLAM TEKLİF TUTARI" +
                                    "</b></td></td></tr>";
                                tbody += tr;
                                if (typeof $scope.teklif.detailslist2[y].events[0] !== 'undefined') {
                                    console.log($scope.teklif.detailslist2[y].events[0]);
                                    var tr =
                                        "<tr><td><td style=\"border: 1px solid black; text-align: center; font-size: 19px; color: #00173C;\"><b>" + $scope.teklif.detailslist2[y].events[0].TEKLIFMALIYETADI +
                                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 16px; color: #00173C;\"><b>" + $scope.teklif.detailslist2[y].events[0].TEKLIFADIOLCUSU +
                                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist2[y].events[0].BIRIMSATISFIYATITXT + " ₺" +
                                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist2[y].events[0].ADETTXT +
                                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist2[y].events[0].KARYUZDESI + " %" +
                                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist2[y].events[0].TOPLAMSATISFIYATITXT + " ₺" +
                                        "</b></td></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\" colspan='2'><b>" + $scope.teklif.detailslist2[y].events[0].MALIYETTEKLIFTUTARITXT + " ₺" + "</b></td></tr>"
                                    tbody += tr;
                                }
                                var tr =
                                    "<tr><td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "HAM M./YARI M. GRUBU" +
                                    "</b></td><td style=\"border: 1px solid black; text-align: center; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "ADI" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "MİKTAR" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "ADET" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "ÖLÇÜ BİRİMİ" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "LİSTE BİRİM FİYATI" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; width: 130px; background-color: #e6e6eb;\"><b>" + "GİRİLEN BİRİM FİYAT" +
                                    "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "TUTAR" +
                                    "</b></td></td></tr>";
                                tbody += tr;
                                for (var z = 0; (typeof $scope.teklif.detailslist2[y].events[z] !== 'undefined'); z++) {
                                    console.log($scope.teklif.detailslist2[y].events[z]);
                                        var tr =
                                            "<tr><td><td style=\"border: 1px solid black; text-align: center; font-size: 12px; width: 95px;\">" + $scope.teklif.detailslist2[y].events[z].HAMMADDEYARIMAMULADI +
                                            "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist2[y].events[z].HAMMADDEYARIMAMUL +
                                            "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist2[y].events[z].MIKTARTXT +
                                            "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist2[y].events[z].KALEMADETTXT +
                                            "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist2[y].events[z].OLCUBIRIMI +
                                            "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist2[y].events[z].BIRIMLISTEFIYATITXT + " ₺" +
                                            "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist2[y].events[z].GIRILENFIYATTXT + " ₺" +
                                            "</td></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist2[y].events[z].TUTARTXT + " ₺" + "</td>"
                                        tbody += tr;
                                    } "</tr>"
                                }
                                var tr = "<tr></tr>"
                                tbody += tr;
                                console.log($scope.teklif.detailslist2[y]); 
                                console.log(typeof $scope.teklif.detailslist2[y]);  
                            }
                        } while (typeof $scope.teklif.detailslist2[y] !== 'undefined');
                        console.log($scope.teklif.detailslist[i]);
                     
                        var tr =
                            "<tr><td><td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "OLUŞTURAN KULLANICI" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "OLUŞTURULMA TARİHİ" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "DÜZENLEYEN KULLANICI" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "DÜZENLENME TARİHİ" +
                            "</b></td></td></td></tr>";
                        tbody += tr;
                        var tr =
                            "<tr><td><td><td style=\"border: 1px solid black; position: absolute; bottom: 0; text-align: center; font-size: 13px;\"><b>" + $scope.teklif.detailslist[i].events[0].CREATEUSER +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 10px;\"><b>" + $scope.teklif.detailslist[i].events[0].CREATEDATETXT +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 13px;\"><b>" + $scope.teklif.detailslist[i].events[0].MODIFYUSER +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 10px;\"><b>" + $scope.teklif.detailslist[i].events[0].MODIFYDATETXT +
                            "</b></td></td></td></tr>"
                        tbody += tr;
                        var tr = "<tr></tr>"
                        tbody += tr;
                        var tr = "<tr></tr>"
                        tbody += tr;
                        var tr = "<tr></tr>"
                        tbody += tr;
                        tbody += "</tbody>";   
                    }
                    var tfoot = "<tfoot>";
                    tfoot += tr;
                    var tr =
                        "<tr><td><td><td><td><td><td><td style=\"position: absolute; bottom: 0; right: 0; font-size: 12px; text-decoration: underline; text-align: center; width: 120px;\"><b>" + "YETKİLİ KİŞİNİN İMZASI" +
                        "</b></td></td></td></td></td></td></td></tr>"
             
                    tfoot += tr;
                    tfoot += "</tfoot>";   
                  
                    return thead + tbody + tfoot; // return thead + tbody + tfoot;
                },
            }
        }]
     })
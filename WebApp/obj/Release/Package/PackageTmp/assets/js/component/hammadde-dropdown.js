﻿app.angular
    .component('hammaddeDropdown', {
        templateUrl: 'assets/template/hammadde-dropdown.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
            var ctrl = this;
            $rootScope.disabled = function () {
                ctrl.hammaddedisabled = true;
            };
            $scope.hammadde = {
                shouldInitOnInputChange: false,
                onfocus: false,
                focus: function ($event) {
                    $scope.hammadde.onfocus = true;
                    var length = typeof ctrl.inputValue === 'string' ? ctrl.inputValue.length : 0;
                    $event.target.setSelectionRange(0, length);
                    var width = angular.element($event.target).prop('offsetWidth');
                    $scope.hammadde.options.container.style.width = width + 'px';
                    $scope.hammadde.options.show();
                    if (ctrl.shouldLoadOptionsOnInputFocus) {
                        $scope.hammadde.options.load();
                    }
                },
                blur: function () {
                    $scope.hammadde.onfocus = false;
                    $scope.hammadde.options.hide();
                },
                filter: {
                    type: 0, 
                    //value: '',
                    init: function () {
                        ctrl.inputValue = '';
                    },
                    change: function () {
                        $scope.hammadde.filter.timer.set();
                        if ($scope.hammadde.shouldInitOnInputChange && typeof ctrl.init === 'function') {
                            $scope.hammadde.shouldInitOnInputChange = false;
                            ctrl.init();
                        }
                    },
                    timer: {
                        value: null,
                        interval: 500,
                        set: function () {
                            if ($scope.hammadde.filter.timer.value) {
                                $timeout.cancel($scope.hammadde.filter.timer.value);
                                $scope.hammadde.filter.timer.value = null;
                            }
                            $scope.hammadde.filter.timer.value = $timeout(function () {
                                if ($scope.hammadde.options.isLoading) {
                                    return;
                                }
                                $scope.hammadde.options.page.index = 1;
                                $scope.hammadde.options.load();
                            }, $scope.hammadde.filter.timer.interval);
                        }
                    }
                },
                options: {
                    page: {
                        index: 1,
                        count: 1,
                        isLastPage: true
                    },
                    totalRow: {
                        value: 0,
                        set: function (value) {
                            $scope.hammadde.options.totalRow.value = value;
                        }
                    },
                    maxVisibleRow: {
                        value: app.pagination.maxVisible.row,
                        set: function (value) {
                            $scope.hammadde.options.maxVisibleRow.value = value;
                        }
                    },
                    isExist: false,
                    isLoading: false,
                    list: [],
                    container: {
                        style: {
                            width: '0px'
                        },
                        cls: ''
                    },
                    show: function () {
                        $scope.hammadde.options.container.cls = 'open';
                    },
                    hide: function () {
                        $timeout(function () {
                            if ($scope.hammadde.onfocus || $scope.hammadde.options.more.onfocus) {
                                return;
                            }
                            $scope.hammadde.options.container.cls = '';
                        }, 250);
                    },
                    load: function (extendList) {
                        $scope.hammadde.options.isLoading = true;
                        app.show.spinner($('hammadde-dropdown > .option-container'));
                        $http({
                            method: 'POST',
                            url: 'hub/urun.ashx?&t=2&d=' + new Date(),
                            data: {
                                pageIndex: $scope.hammadde.options.page.index,
                                filterType: $scope.hammadde.filter.type,
                                filter: ctrl.inputValue,
                                pageRowCount: $scope.hammadde.options.maxVisibleRow.value,
                            }
                        }).then(function (response) {
                            $scope.hammadde.options.isLoading = false;
                            app.hide.spinner($('hammadde-dropdown > .option-container'));
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive
                                window.location.href = 'login.aspx';
                                return;
                            }
                            if (!extendList) {
                                $scope.hammadde.options.list = response.data.LIST;
                            } else {
                                $scope.hammadde.options.list.push.apply($scope.hammadde.options.list, response.data.LIST);
                            }
                            $scope.hammadde.options.isExist = $scope.hammadde.options.list.length > 0;
                            ctrl.shouldLoadOptionsOnInputFocus = !$scope.hammadde.options.isExist;
                            $scope.hammadde.options.totalRow.set(response.data.TOTALROW);
                            $scope.hammadde.options.paginate();
                        }, function (response) {
                            $scope.hammadde.options.isLoading = false;
                            console.log(response);
                            app.show.error('Bir şeyler ters gitti...');
                            app.hide.spinner($('hammadde-dropdown > .option-container'));
                        });
                    },
                    paginate: function () {
                        $scope.hammadde.options.page.count = $scope.hammadde.options.totalRow.value !== 0 ? Math.ceil($scope.hammadde.options.totalRow.value / $scope.hammadde.options.maxVisibleRow.value) : 1;
                        $scope.hammadde.options.page.isLastPage = $scope.hammadde.options.page.count <= $scope.hammadde.options.page.index;
                    },
                    click: function (hammadde) {
                        $scope.hammadde.shouldInitOnInputChange = true;
                        ctrl.shouldLoadOptionsOnInputFocus = true;
                        ctrl.inputValue = hammadde.ADI; // database kolon adı = ADI
                        if (typeof ctrl.onSelect === 'function') {
                            ctrl.onSelect({ hammadde: hammadde });
                        }
                    },
                    more: {
                        load: function () {
                            if ($scope.hammadde.options.page.isLastPage) {
                                return;
                            }
                            $scope.hammadde.options.page.index++;
                            var extendList = true;
                            $scope.hammadde.options.load(extendList);
                        },
                        onfocus: false,
                        focus: function () {
                            $scope.hammadde.options.more.onfocus = true;
                        },
                        blur: function () {
                            $scope.hammadde.options.more.onfocus = false;
                            $scope.hammadde.options.hide();
                        }
                    }
                }
            };
        }],
        bindings: {
            hammaddedisabled: '=',
            inputLabel: '@',
            inputMessage: '@',
            componentClass: '@',
            onSelect: '&',
            init: '&',
            inputValue: '=',
            shouldLoadOptionsOnInputFocus: '=',
        }
    });
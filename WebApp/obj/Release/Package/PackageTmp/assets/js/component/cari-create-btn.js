﻿app.angular
    .controller('cariCreateBtnCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {
        window.addEventListener("keydown", function (event) {
            if (event.defaultPrevented) {
                console.log(0);
                return;
            }
            var handled = false;
            if (event.key !== undefined) {
                if (event.keyCode == 45) {
                    $rootScope.showCreateModal();
                }
            } else if (event.keyCode !== undefined) {
                console.log(2);
            }
            if (handled) {
                console.log(3);
                event.preventDefault();
            }
        }, true);
        $scope.cls = 'fab-menu-bottom-right';
        $scope.show = {
            modal: function () {
                $rootScope.showCreateModal();
            }
        };
    }])
    .directive('cariCreateBtn', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/cari-create-btn.html'
        };
    });
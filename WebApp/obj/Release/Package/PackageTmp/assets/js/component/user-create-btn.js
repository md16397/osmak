﻿app.angular
    .controller('userCreateBtnCtrl', ['$scope', function ($scope) {
        $scope.cls = 'fab-menu-bottom-right';
        $scope.show = {
            modal: function () {
                $('.user-create-modal:first').modal({
                    backdrop: 'static'
                });
            }
        };
    }])
    .directive('userCreateBtn', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/user-create-btn.html'
        };
    });
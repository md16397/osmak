﻿app.angular
    .component('flansAgirlikModal', {
        templateUrl: 'assets/template/flans-agirlik-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', '$localStorage', function ($scope, $http, $timeout, $rootScope, $localStorage) {

            $rootScope.showFlansModal = function (data) {
                $scope.teklif.get(data);
                localStorage.removeItem('boruagirlik');
                localStorage.removeItem('milagirlik');
                localStorage.removeItem('flansagirlik');
                $('.flans-agirlik-modal:first').appendTo("body");   // modalı öne getirir.
                $scope.teklif.init();
                $('.flans-agirlik-modal:first').modal({
                    backdrop: 'static'
                });
            };

            $scope.teklif = {
                flansagirlik: '',
                en: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.en.isValid = false;
                        $scope.teklif.en.value = '';
                        $scope.teklif.en.text = '';
                        $scope.teklif.en.cls = '';
                        $scope.teklif.en.msg = '';
                        $scope.teklif.en.set('');
                    },
                    set: function (en) {
                        $scope.teklif.en.value = en.value;
                        $scope.teklif.en.text = en.text;
                        $scope.teklif.en.isValid = true;
                        $scope.teklif.en.cls = '';
                        $scope.teklif.en.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.en.value);
                        if (applyCheck || $scope.teklif.en.isValid !== check) {
                            $scope.teklif.en.isValid = check;
                            if ($scope.teklif.en.isValid) {
                                $scope.teklif.en.success();
                            } else {
                                $scope.teklif.en.error('* Geçersiz En !');
                            }
                        }
                        return $scope.teklif.en.isValid;
                    },
                    change: function () {
                        $scope.teklif.en.check($scope.teklif.en.value);
                    },
                    success: function () {
                        $scope.teklif.en.cls = '';
                        $scope.teklif.en.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.en.cls = 'has-error';
                        $scope.teklif.en.msg = msg;
                    }
                },
                boy: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.boy.isValid = false;
                        $scope.teklif.boy.value = '';
                        $scope.teklif.boy.text = '';
                        $scope.teklif.boy.cls = '';
                        $scope.teklif.boy.msg = '';
                        $scope.teklif.boy.set('');
                    },
                    set: function (boy) {
                        $scope.teklif.boy.value = boy.value;
                        $scope.teklif.boy.text = boy.text;
                        $scope.teklif.boy.isValid = true;
                        $scope.teklif.boy.cls = '';
                        $scope.teklif.boy.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.boy.value);
                        if (applyCheck || $scope.teklif.boy.isValid !== check) {
                            $scope.teklif.boy.isValid = check;
                            if ($scope.teklif.boy.isValid) {
                                $scope.teklif.boy.success();
                            } else {
                                $scope.teklif.boy.error('* Geçersiz Boy !');
                            }
                        }
                        return $scope.teklif.boy.isValid;
                    },
                    change: function () {
                        $scope.teklif.boy.check($scope.teklif.boy.value);
                    },
                    success: function () {
                        $scope.teklif.boy.cls = '';
                        $scope.teklif.boy.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.boy.cls = 'has-error';
                        $scope.teklif.boy.msg = msg;
                    }
                },
                etkalinligi: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.etkalinligi.isValid = false;
                        $scope.teklif.etkalinligi.value = '';
                        $scope.teklif.etkalinligi.text = '';
                        $scope.teklif.etkalinligi.cls = '';
                        $scope.teklif.etkalinligi.msg = '';
                        $scope.teklif.etkalinligi.set('');
                    },
                    set: function (etkalinligi) {
                        $scope.teklif.etkalinligi.value = etkalinligi.value;
                        $scope.teklif.etkalinligi.text = etkalinligi.text;
                        $scope.teklif.etkalinligi.isValid = true;
                        $scope.teklif.etkalinligi.cls = '';
                        $scope.teklif.etkalinligi.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.etkalinligi.value);
                        if (applyCheck || $scope.teklif.etkalinligi.isValid !== check) {
                            $scope.teklif.etkalinligi.isValid = check;
                            if ($scope.teklif.etkalinligi.isValid) {
                                $scope.teklif.etkalinligi.success();
                            } else {
                                $scope.teklif.etkalinligi.error('* Geçersiz Et Kalınlığı !');
                            }
                        }
                        return $scope.teklif.etkalinligi.isValid;
                    },
                    change: function () {
                        $scope.teklif.etkalinligi.check($scope.teklif.etkalinligi.value);
                    },
                    success: function () {
                        $scope.teklif.etkalinligi.cls = '';
                        $scope.teklif.etkalinligi.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.etkalinligi.cls = 'has-error';
                        $scope.teklif.etkalinligi.msg = msg;
                    }
                },
                materyal: {
                    list: page.data.materyal,
                    isValid: false,
                    selected: { name: 'Demir', value: '7.86' },
                    cls: '',
                    msg: '',
                    inputValue: '',
                    shouldLoadOptionsOnInputFocus: true,
                    init: function () {
                        $scope.teklif.materyal.isValid = false;
                        $scope.teklif.materyal.selected = { name: 'Demir', value: '7.86' };
                        $scope.teklif.materyal.cls = '';
                        $scope.teklif.materyal.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = $scope.teklif.materyal.selected && $scope.teklif.materyal.selected.hasOwnProperty('value') && typeof $scope.teklif.materyal.selected.value !== 'undefined' && $scope.teklif.materyal.selected.value != null ? true : false;
                        if (applyCheck || $scope.teklif.materyal.isValid !== check) {
                            $scope.teklif.materyal.isValid = check;
                            if ($scope.teklif.materyal.isValid) {
                                $scope.teklif.materyal.success();
                            } else {
                                $scope.teklif.materyal.error('* Geçersiz Materyal');
                            }
                        }
                        return $scope.teklif.materyal.isValid;
                    },
                    change: function () {
                        $scope.teklif.materyal.check();
                    },
                    select: function (materyal) {
                        $scope.teklif.materyal.isValid = true;
                        $scope.teklif.materyal.selected = materyal;
                        $scope.teklif.materyal.cls = '';
                        $scope.teklif.materyal.msg = '';
                    },
                    set: function (value) {
                        var goon = true;
                        var i = 0;
                        while (goon && i < $scope.teklif.materyal.list.lcapgth) {
                            if ($scope.teklif.materyal.list[i].value === value) {
                                $scope.teklif.materyal.select($scope.teklif.materyal.list[i]);
                                goon = false;
                            }
                            i++;
                        }
                        if (goon) {
                            $scope.teklif.materyal.init();
                        }
                    },
                    success: function () {
                        $scope.teklif.materyal.cls = '';
                        $scope.teklif.materyal.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.materyal.cls = 'has-error';
                        $scope.teklif.materyal.msg = msg;
                    }
                },
                init: function () {
                    $scope.teklif.en.init();
                    $scope.teklif.boy.init();
                    $scope.teklif.etkalinligi.init();
                    $scope.teklif.materyal.init();
                },
                get: function (data) {
                    $scope.teklif.getdata = data;
                },
                check: function () {
                    var applyCheck = true;
                    var OK = {
                        en: $scope.teklif.en.check(applyCheck),
                        boy: $scope.teklif.boy.check(applyCheck),
                        etkalinligi: $scope.teklif.etkalinligi.check(applyCheck)
                    };
                    return OK.en && OK.boy && OK.etkalinligi;
                },
                save: function () {
                    if (!$scope.teklif.check()) {
                        return;
                    }
                    app.show.spinner($('flans-agirlik-modal > div:first > div:first'));
                    // Ağırlık -> (En * Boy * Kalınlık * 0.8) / 100.000
                    // Toplam Ağırlık -> (Adet  * 1 Adet malzemenin Ağırlığı)
                    // Fiyat = Kg fiyatı * Toplam Ağırlık

                    $scope.teklif.flansagirlik = ($scope.teklif.en.value * $scope.teklif.boy.value * $scope.teklif.etkalinligi.value * (0.8)) / 100000;
                    var num = $scope.teklif.flansagirlik;
                    var with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0] // ondalıklı sayıyı yuvarlamadan eşitle.
                    $scope.teklif.flansagirlik = with2Decimals.replace(".", ",");

                    localStorage.setItem("flansagirlik", JSON.stringify($scope.teklif.flansagirlik));
                    console.log(" $scope.teklif.flansagirlik -->", $scope.teklif.flansagirlik);
                    $('.flans-agirlik-modal:first').modal('hide');

                    $rootScope.control = true;
                    if ($scope.teklif.getdata === 'create-boru') { //create-modal dan gelen data
                        $rootScope.boruagirlik();
                    }
                    else {
                        $rootScope.boruagirlik2();
                    }
                    $scope.teklif.init();
                },
            };
        }]
    })

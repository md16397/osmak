﻿app.angular
    .component('tamburmodifyaspxUyariModal', {
        templateUrl: 'assets/template/tamburmodifyaspx-uyari-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {

            $rootScope.showTamburmodifyaspxUyariModal = function (teklifcreatemodal, teklifmodifymodal) {
                $scope.savemimodifymi = teklifcreatemodal;
                $scope.teklifmodifymodal = teklifmodifymodal;
                console.log($scope.savemimodifymi);
                console.log($scope.teklifmodifymodal);
                $('.tamburmodifyaspx-uyari-modal:first').modal({
                    backdrop: 'static',
                    keyboard: true
                });
            };
            $scope.teklif = {
                save: function () {
                    if ($scope.teklifmodifymodal == 'modifycomponent-modify-modal-modifyfunction') {
                        console.log("modifycomponent-modify-modaldan gelen modify function ise :");
                        $http({
                            method: 'POST',
                            url: 'hub/teklif.ashx?&t=4&d=' + new Date(),
                            data: {
                                teklifid: JSON.parse(localStorage.getItem("modifycomponentmodifymodal-id")),
                                teklifmaliyetid: JSON.parse(localStorage.getItem("modifycomponentmodifymodal-maliyetid")),
                                cari: JSON.parse(localStorage.getItem("modifycomponentmodifymodal-cari")),
                                teklifkodu: JSON.parse(localStorage.getItem("modifycomponentmodifymodal-teklifkodu")),
                                toplamtekliftutari: JSON.parse(localStorage.getItem("modifycomponent-modify-modal-toplamtekliftutari")),
                                onaydurumu: JSON.parse(localStorage.getItem("modifycomponent-modify-modal-onaydurumu")),
                                aciklama: JSON.parse(localStorage.getItem("modifycomponent-modify-modal-aciklama")),
                                teklifadi: JSON.parse(localStorage.getItem("modifycomponent-modify-modal-teklifurunadi")),
                                teklifadiolcusu: JSON.parse(localStorage.getItem("modifycomponent-modify-modal-olcu")),
                                birimsatisfiyati: JSON.parse(localStorage.getItem("modifycomponent-modify-modal-birimsatisfiyati")),
                                adet: JSON.parse(localStorage.getItem("modifycomponent-modify-modal-adet")),
                                karyuzdesi: JSON.parse(localStorage.getItem("modifycomponent-modify-modal-karyuzdesi")),
                                toplamsatisfiyati: JSON.parse(localStorage.getItem("modifycomponentmodifymodal-toplamsatisfiyati")),
                                maliyettekliftutari: JSON.parse(localStorage.getItem("modifycomponent-modify-modal-toplamtekliftutari")),
                                detail: JSON.parse(localStorage.getItem("modifycomponent-modify-modal-detail"))
                            }
                        }).then(function (response) {
                            app.hide.spinner($('modifycomponent-modify-modal > div:first > div:first'));
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');
                                return;
                            }
                            if (response.data['RESULT'] === 1) {//success
                                app.show.success('Teklif-Maliyet bilgileri Güncellendi!');
                                console.log(response.data);
                                localStorage.setItem("teklifdata", JSON.stringify(response.data));
                                Runopenmodify = $rootScope.runmodifydata;
                                Runopenmodify();
                                $scope.id = response.data.TEKLIFLERID;
                                localStorage.removeItem('boruagirlik');  //
                                $('.tamburmodifyaspx-uyari-modal:first').modal('hide');// modal kapanır.
                                $('.modifycomponent-modify-modal:first').modal('hide'); // modal kapanır.
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                                window.location.href = 'login.aspx';
                                return;
                            }
                            //if running code come through this far then it means an unexpected error occurred
                            app.show.error('Bir şeyler ters gitti...');
                        }, function (response) {
                            app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                            app.hide.spinner($('modifycomponent-modify-modal > div:first > div:first'));
                        });
                        return;
                    }
                    if ($scope.savemimodifymi == 'modifycomponent-create-modal-save') { //modifycomponent-create-modaldan gelen save function ise :
                        console.log("modifycomponent-create-modaldan gelen save function ise :");
                        $http({
                            method: 'POST',
                            url: 'hub/teklif.ashx?&t=3&d=' + new Date(), // TYPE 3
                            data: {
                                cari: JSON.parse(localStorage.getItem("cari")),
                                tekliftarihi: JSON.parse(localStorage.getItem("tekliftarihi")),
                                teklifkodu: JSON.parse(localStorage.getItem("teklifkodu")),
                                toplamtekliftutari: JSON.parse(localStorage.getItem("modifycomponent-create-modal-toplamtekliftutari")),
                                aciklama: JSON.parse(localStorage.getItem("aciklama")),
                                teklifadi: JSON.parse(localStorage.getItem("modifycomponent-create-modal-teklifurunadi-TEKLIFURUNLERID")),
                                teklifadiolcusu: JSON.parse(localStorage.getItem("modifycomponent-create-modal-teklifadiolcusu")),
                                birimsatisfiyati: JSON.parse(localStorage.getItem("modifycomponent-create-modal-birimsatisfiyati")),
                                adet: JSON.parse(localStorage.getItem("modifycomponent-create-modal-adet")),
                                karyuzdesi: JSON.parse(localStorage.getItem("modifycomponent-create-modal-karyuzdesi")),
                                toplamsatisfiyati: JSON.parse(localStorage.getItem("modifycomponent-create-modal-toplamsatisfiyati")),
                                maliyettekliftutari: JSON.parse(localStorage.getItem("modifycomponent-create-modal-toplamtekliftutari")),
                                detail: JSON.parse(localStorage.getItem("modifycomponent-create-modal-detail"))
                            }
                        }).then(function (response) {
                            app.hide.spinner($('tamburmodifyaspx-uyari-modal > div:first > div:first'));
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');
                                return;
                            }
                            if (response.data['RESULT'] === 1) { // veritabanından gelen RESULT değeri 1 ise kaydedilmiştir.
                                $rootScope.teklifcreate = true;
                                console.log(response.data);
                                localStorage.setItem("sabit6hamyarimamul-uyari-modal-teklifkodu", JSON.stringify(response.data.TEKLIFKODU));
                                app.show.success('Yeni Teklif Oluşturuldu!'); // success modalı.
                                //localStorage.removeItem('teklifdata');
                                $rootScope.control = true;
                                localStorage.setItem("teklifdata", JSON.stringify(response.data));
                                Runopenmodify = $rootScope.runmodifydata;
                                Runopenmodify();
                                $rootScope.teklifidExist = response.data.TEKLIFLERID;
                                $scope.id = response.data.TEKLIFLERID;
                                $rootScope.teklifcreatemodalId = $scope.id;
                                console.log($rootScope.teklifcreatemodalId);
                                localStorage.setItem("tamburmodifyaspx-uyari-modal-id", JSON.stringify($scope.id));
                                $rootScope.index = $scope.index;
                                $('.tamburmodifyaspx-uyari-modal:first').modal('hide'); // modal kapanır.
                                $('.modifycomponent-create-modal:first').modal('hide'); // modal kapanır.
                                //
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                                window.location.href = 'login.aspx';
                                return;
                            }
                            if (response.data['RESULT'] === 124) {
                                app.show.warning('UYARI', 'Bu Teklif daha önce oluşturuldu.');
                                return;
                            }
                            app.show.error('Bir şeyler ters gitti...');
                        }, function (response) {
                            app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                            app.hide.spinner($('modifycomponent-modify-modal > div:first > div:first'));
                        });
                        return;
                    }
                    else { //teklif-create-modaldan gelen modify function ise :
                        console.log("modifycomponent-create-modaldan gelen modify function ise :");
                        console.log($scope.id);
                        if ($scope.id == undefined) {
                            $scope.id = JSON.parse(localStorage.getItem("teklif-modify-component-teklifid"));
                            console.log("$scope.id ->", $scope.id);
                        }
                        $http({
                            method: 'POST',
                            url: 'hub/teklif.ashx?&t=8&d=' + new Date(), // TYPE 8
                            data: {
                                teklifid: $scope.id,
                                cari: JSON.parse(localStorage.getItem("cari")),
                                tekliftarihi: JSON.parse(localStorage.getItem("tekliftarihi")),
                                teklifkodu: JSON.parse(localStorage.getItem("teklif-modify-component-teklifkodu")),
                                toplamtekliftutari: JSON.parse(localStorage.getItem("modifycomponent-create-modal-toplamtekliftutari")),
                                aciklama: JSON.parse(localStorage.getItem("aciklama")),
                                teklifadi: JSON.parse(localStorage.getItem("modifycomponent-create-modal-teklifurunadi-TEKLIFURUNLERID")),
                                teklifadiolcusu: JSON.parse(localStorage.getItem("modifycomponent-create-modal-teklifadiolcusu")),
                                birimsatisfiyati: JSON.parse(localStorage.getItem("modifycomponent-create-modal-birimsatisfiyati")),
                                adet: JSON.parse(localStorage.getItem("modifycomponent-create-modal-adet")),
                                karyuzdesi: JSON.parse(localStorage.getItem("modifycomponent-create-modal-karyuzdesi")),
                                onaydurumu: false,
                                toplamsatisfiyati: JSON.parse(localStorage.getItem("modifycomponent-create-modal-toplamsatisfiyati")),
                                maliyettekliftutari: JSON.parse(localStorage.getItem("modifycomponent-create-modal-toplamtekliftutari")),
                                detail: JSON.parse(localStorage.getItem("modifycomponent-create-modal-detail"))
                            }
                        }).then(function (response) {
                            app.hide.spinner($('tamburmodifyaspx-uyari-modal > div:first > div:first'));
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');
                                return;
                            }
                            if (response.data['RESULT'] === 1) {
                                $rootScope.control = true;
                                localStorage.setItem("teklifdata", JSON.stringify(response.data));
                                console.log(response.data);
                                localStorage.setItem("sabit6hamyarimamul-uyari-modal-teklifkodu", JSON.stringify(response.data.TEKLIFKODU));
                                app.show.success('Teklif-Maliyet Bilgileri Eklendi!');
                                Runopenmodify = $rootScope.runmodifydata;
                                Runopenmodify();
                                $rootScope.teklifidExist = response.data.TEKLIFLERID;
                                $scope.id = response.data.TEKLIFLERID;
                                localStorage.setItem("tamburmodifyaspx-uyari-modal-id", JSON.stringify($scope.id));
                                $('.tamburmodifyaspx-uyari-modal:first').modal('hide');
                                $('.modifycomponent-create-modal:first').modal('hide'); // modal kapanır.
                                //
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                                window.location.href = 'login.aspx';
                                return;
                            }
                            app.show.error('Bir şeyler ters gitti...');
                        }, function (response) {
                            app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                            app.hide.spinner($('modifycomponent-modify-modal > div:first > div:first'));
                        });
                        return;
                    }
                }
            }
        }]
    })
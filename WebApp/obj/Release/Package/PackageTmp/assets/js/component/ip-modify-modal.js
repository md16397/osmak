﻿app.angular
    .controller('ipModifyModalCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
      
        $rootScope.showModifyModal = function (ipData) {
            $scope.ip.set(ipData);
            $('.ip-modify-modal:first').modal({
                backdrop: 'static'
            });
        };
        $scope.ip = {
            rawdata: null,
            id: {
                value: 0,
                init: function () {
                    $scope.ip.id.set(0);
                },
                set: function (value) {
                    $scope.ip.id.value = value;
                }
            },
            address: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.ip.address.isValid = false;
                    $scope.ip.address.cls = '';
                    $scope.ip.address.msg = '';
                    $scope.ip.address.set('');
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.ip.address.value);
                    if (applyCheck || $scope.ip.address.isValid !== check) {
                        $scope.ip.address.isValid = check;
                        if ($scope.ip.address.isValid) {
                            $scope.ip.address.success();
                        } else {
                            $scope.ip.address.error('* Geçersiz IP Adres');//'* Geçersiz IP Adres'
                        }
                    }
                    return $scope.ip.address.isValid;
                },
                change: function () {
                    $scope.ip.address.check();
                },
                success: function () {
                    $scope.ip.address.cls = '';
                    $scope.ip.address.msg = '';
                },
                error: function (msg) {
                    $scope.ip.address.cls = 'has-error';
                    $scope.ip.address.msg = msg;
                },
                set: function (value) {
                    $scope.ip.address.value = value;
                    $scope.ip.address.isValid = true;
                    $scope.ip.address.cls = '';
                    $scope.ip.address.msg = '';
                }
            },
            port: {
                value: '',
                init: function () {
                    $scope.ip.port.value = '';
                    $scope.ip.port.set('');
                },
                set: function (value) {
                    $scope.ip.port.value = value;
                }
            },
            protocol: {
                value: '',
                init: function () {
                    $scope.ip.protocol.value = '';
                    $scope.ip.protocol.set('');
                },
                set: function (value) {
                    $scope.ip.protocol.value = value;
                }
            },
            local: {
                name: 'Genel IP',
                value: false,
                init: function () {
                    $scope.ip.local.select($scope.ip.local.no);
                },
                select: function (local) {
                    $scope.ip.local.name = local.name;
                    $scope.ip.local.value = local.value;
                },
                set: function (value) {
                    $scope.ip.local.select(value ? $scope.ip.local.yes : $scope.ip.local.no);
                },
                yes: {
                    name: 'Lokal IP',
                    value: true,
                    anchor: {
                        cls: ''
                    }
                },
                no: {
                    name: 'Genel IP',
                    value: false,
                    anchor: {
                        cls: ''
                    }
                }
            },
            state: {
                name: 'Aktif',
                value: true,
                icon: {
                    cls: 'icon-checkmark-circle2 text-success'
                },
                init: function () {
                    $scope.ip.state.select($scope.ip.state.active);
                },
                select: function (state) {
                    $scope.ip.state.name = state.name;
                    $scope.ip.state.value = state.value;
                    $scope.ip.state.icon.cls = state.icon.cls.replace(/ pull-right/g, '');
                },
                set: function (value) {
                    $scope.ip.state.select(value ? $scope.ip.state.active : $scope.ip.state.passive);
                },
                active: {
                    name: 'Aktif',
                    value: true,
                    icon: {
                        cls: 'icon-checkmark-circle2 text-success'
                    },
                    anchor: {
                        cls: ''
                    }
                },
                passive: {
                    name: 'Pasif',
                    value: false,
                    icon: {
                        cls: 'icon-close2 text-danger'
                    },
                    anchor: {
                        cls: ''
                    }
                }
            },
            set: function (ipData) {
                $scope.ip.rawdata = ipData;
                $scope.ip.id.set(ipData.ID);
                $scope.ip.address.set(ipData.ADDRESS);
                $scope.ip.port.set(ipData.PORT);
                $scope.ip.protocol.set(ipData.PROTOCOL);
                $scope.ip.local.set(ipData.LOCALVAL);
                $scope.ip.state.set(ipData.ACTIVEVAL);
            },
            init: function () {
                $scope.ip.id.init();
                $scope.ip.address.init();
                $scope.ip.port.init();
                $scope.ip.protocol.init();
                $scope.ip.local.init();
                $scope.ip.state.init();
                $scope.ip.rawdata = null;
            },
            check: function () {
                var applyCheck = true;
                var OK = {
                    address: $scope.ip.address.check(applyCheck)
                };
                return OK.address;
            },
            modify: function () {
                if (!$scope.ip.check()) {
                    return;
                }
                app.show.spinner($('ip-modify-modal > div:first > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/ip.ashx?&t=4&d=' + new Date(),
                    data: {
                        id: $scope.ip.id.value,
                        address: $scope.ip.address.value,
                        port: $scope.ip.port.value,
                        protocol: $scope.ip.protocol.value,
                        local: $scope.ip.local.value,
                        state: $scope.ip.state.value
                    }
                }).then(function (response) {
                    app.hide.spinner($('ip-modify-modal > div:first > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 1) {//success
                        $rootScope.modifyRow(response.data);
                        $scope.ip.init();
                        $('.ip-modify-modal:first').modal('hide');
                        app.show.success('Ip güncellendi!');//'Ip güncellendi!'
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to modify a user
                        window.location.href = 'login.aspx';
                        return;
                    }
                    if (response.data['RESULT'] === 41) {//ipaddress is invalid
                        $scope.ip.address.isValid = false;
                        $scope.ip.address.error('* Geçersiz IP Adres');//'* Geçersiz IP Adres'
                        return;
                    }
                    //if running code come through this far then it means an unexpected error occurred
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'

                }, function (response) {
                    console.log(response);
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                    app.hide.spinner($('ip-modify-modal > div:first > div:first'));
                });
            }
        };
    }])
    .directive('ipModifyModal', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/ip-modify-modal.html'
        };
    });
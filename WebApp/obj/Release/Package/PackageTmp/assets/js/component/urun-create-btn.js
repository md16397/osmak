﻿app.angular
    .controller('urunCreateBtnCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {
        window.addEventListener("keydown", function (event) {
            if (event.defaultPrevented) {
                console.log(0);
                return; // Should do nothing if the default action has been cancelled
            }
            var handled = false;
            if (event.key !== undefined) {
                // Handle the event with KeyboardEvent.key and set handled true.
                if (event.keyCode == 45) {
                    $rootScope.showCreateModal();
                }
            } else if (event.keyCode !== undefined) {
                console.log(2);
                // Handle the event with KeyboardEvent.keyCode and set handled true.
            }
            if (handled) {
                console.log(3);
                // Suppress "double action" if event handled
                event.preventDefault();
            }
        }, true);
        $scope.cls = 'fab-menu-bottom-right';
        $scope.show = {
            modal: function () {
                $rootScope.showCreateModal();
            }
        };
    }])
    .directive('urunCreateBtn', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/urun-create-btn.html'
        };
    });
﻿app.angular
    .controller('teklifmodifyComponentCtrl', ['$scope', '$http', '$rootScope', '$rootScope', '$localStorage', function ($scope, $http, $rootScope, $localStorage) {

        var Runopenmodify;
        $rootScope.runmodifydata = function () {
            localStorage.removeItem('MODIFYFUNCTIONTEKLIFLERID'); // modifycomponent-create-modal Modify Function()
            var _teklifcreate = JSON.parse(localStorage.getItem("tekliflist-modify")); 
            console.log(_teklifcreate);
            if (JSON.parse(localStorage.getItem("tekliflistesindengelenToplamTeklif")) !== null) {
                $scope.teklif.satisfiyatlaritoplami.value = JSON.parse(localStorage.getItem("tekliflistesindengelenToplamTeklif"));
                console.log($scope.teklif.satisfiyatlaritoplami.value);
            }
            if (typeof _teklifcreate.TEKLIFLERID !== 'undefined') {
                $http({
                    method: 'POST',
                    url: 'hub/teklif.ashx?&t=6&d=' + new Date(),
                    data: {
                        tekliflerid: _teklifcreate.TEKLIFLERID
                    }
                }).then(function (response) {
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error('Bir şeyler ters gitti...');
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                        window.location.href = 'login.aspx';
                        return;
                    }
                    $scope.teklif.teklifdata = response.data;
                    console.log($scope.teklif.teklifdata);
                    $scope.teklif.rows = response.data.DETAILS; // IPTAL ET VE ÇIK BUTONU İLE TÜM TEKLIF MALIYET VE KALEMLERI SILINMELI (kalemleri silinmiyor)
                    console.log($scope.teklif.rows);
                    //  
                    localStorage.setItem("teklif-modify-component-teklifid", JSON.stringify($scope.teklif.teklifdata.TEKLIFLERID)); 
                    localStorage.setItem("teklif-modify-component-teklifkodu", JSON.stringify($scope.teklif.teklifdata.TEKLIFKODU)); 
                    localStorage.setItem("modify-componentten-modala-teklifkodu-gonderiliyor", JSON.stringify($scope.teklif.teklifdata.TEKLIFKODU)); 
                    if ($scope.teklif.teklifdata.TEKLIFLERID > 0) { // modifycomponent-create-modal'da eğer ilk kayıt ekleniyorsa Save(), 2. yada sonraki kayıtlar ekleniyorsa Modify() çalışacak.
                        localStorage.setItem("MODIFYFUNCTIONTEKLIFLERID", JSON.stringify($scope.teklif.teklifdata.TEKLIFLERID));
                    }
                    if ($scope.teklif.teklifdata.RESULT === 78) { // Tüm işlemler silindiyse
                        console.log("TEKLIF VE KALEMLERI YOK");
                        $scope.teklif.teklifinit();
                    }
                    if ($scope.teklif.teklifdata.RESULT !== 78) { // Teklifler varsa set etmeli.
                        $scope.teklif.set(response.data);
                    }
                    //if ($scope.teklif.rows.length === 0) {
                    //    $scope.teklif.satisfiyatlaritoplami.value = 0;
                    //}
                    var i = 0;
                    $scope.teklif.tekliftutarlarinitopla.value = 0; // component her açıldığında toplam teklif tutarı sıfırlanmalı. Maliyete ait bütün teklif tutarlarını toplamak için.
                    var eventArray = $scope.teklif.rows, // gelen datayı TEKLIFMALIYETID ye göre gruplar.
                        result = Array.from(
                            eventArray.reduce((m, o) => m.set(o.TEKLIFMALIYETID, (m.get(o.TEKLIFMALIYETID) || []).concat(o)), new Map),
                            ([TEKLIFMALIYETID, events]) => ({ TEKLIFMALIYETID, events })
                        );
                    console.log(result);
                    var i = 0;
                    if (result.length > 0) {
                        do {
                            if (result[i] !== null) {
                                $scope.teklif.tekliftutarlarinitopla.value = $scope.teklif.tekliftutarlarinitopla.value + result[i].events[0].MALIYETTEKLIFTUTARIVAL;
                                $scope.teklif.toplamtekliftutari.set($scope.teklif.tekliftutarlarinitopla.value);
                                console.log($scope.teklif.tekliftutarlarinitopla.value);
                            }
                            i++;
                            console.log($scope.teklif.rows[i]);
                            console.log($scope.teklif.tekliftutarlarinitopla.value);
                        } while (typeof result[i] !== 'undefined');
                        $scope.teklif.toplamtekliftutari.set($scope.teklif.tekliftutarlarinitopla.value);
                        console.log($scope.teklif.tekliftutarlarinitopla.value);
                    }
                }, function (response) {
                    app.show.error('Bir şeyler ters gitti...');
                });
            }
        };
        $rootScope.modifykontrol = { // cari ve teklif kodu yokken 'TEKLIF KALEMI EKLE' butonuna basarsa
            check: function () {
                var applyCheck = true;
                var OK = {
                    cari: $scope.teklif.cari.check(applyCheck)
                };
                return OK.cari;
            },
        },
            $scope.teklif = {
                error: function (msg) {// kalem yokken 'teklifi kaydet ve kapat' butonuna tıklanırsa uyarı vermeli.
                    $scope.teklif.msg = msg;
                },
            iptaletvecik: function () { // Teklifler-liste ekranına yönlendirir.
                window.location.pathname = 'teklifler.aspx';
            },
                show: {
                    update: function (key) {
                        $http({
                            method: 'POST',
                            url: 'hub/teklif.ashx?&t=9&d=' + new Date(),
                            data: {
                                tekliflerid: key
                            }
                        }).then(function (response) {
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                                window.location.href = 'login.aspx';
                                return;
                            }
                            $scope.teklif.modifydata = response.data;
                            $rootScope.showModifyModal(key);
                        }, function (response) {
                            app.show.error('Bir şeyler ters gitti...');
                        });
                    },
                    delete: function (key, teklifid) {
                        $rootScope.showDeleteModal(key, teklifid);
                    },
                },
                detail: {
                    isValid: false,
                    isExist: false,
                    cls: '',
                    msg: '',
                    rows: [],
                    kalem: true,
                    init: function () {
                        $scope.teklif.detail.isValid = false;
                        $scope.teklif.detail.isExist = false;
                        $scope.teklif.detail.cls = '';
                        $scope.teklif.detail.msg = '';
                        $scope.teklif.detail.rows = [];
                        $scope.teklif.detail.inputs.init();
                    },
                    set: function (rows) {
                        $scope.teklif.detail.isValid = true;
                        $scope.teklif.detail.isExist = rows.length > 0;
                        $scope.teklif.detail.cls = '';
                        $scope.teklif.detail.msg = '';
                        $scope.teklif.detail.rows = rows;
                        $scope.teklif.detail.removedRows = [];
                        $scope.teklif.detail.inputs.init();
                    },
                    check: function (applyCheck) {
                        var check = $scope.teklif.detail.rows === null || typeof $scope.teklif.detail.rows === 'undefined' || !$scope.teklif.detail.rows.length ? false : true;
                        if (applyCheck || $scope.teklif.detail.isValid !== check) {
                            $scope.teklif.detail.isValid = check;
                            if ($scope.teklif.detail.isValid) {
                                $scope.teklif.detail.success();
                            } else {
                                $scope.teklif.detail.error('* En az bir satır girmelisiniz.');
                            }
                        }
                        return $scope.teklif.detail.isValid;
                    },
                    success: function () {
                        $scope.teklif.detail.cls = '';
                        $scope.teklif.detail.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.detail.cls = 'has-error';
                        $scope.teklif.detail.msg = ''
                    },
                    remove: function (index, row, addToRemovedRows) {
                        if (addToRemovedRows && (row.STATE === 'OLD' || row.STATE === 'UPD')) {
                            row.STATE = 'DEL';
                            $scope.teklif.detail.removedRows.unshift(row);
                        }
                        $scope.teklif.detail.rows.splice(index, 1);
                        $scope.teklif.detail.isExist = $scope.teklif.detail.rows.length > 0;
                    },
                    modify: function (index, row) {
                        if (!$scope.teklif.detail.check()) {
                            return;
                        }
                        $scope.teklif.detail.inputs.set(index, row);
                        $scope.teklif.detail.remove(index, row, false);
                    },
                    inputs: {
                        rawdata: {
                            value: null,
                            isExist: false,
                            init: function () {
                                $scope.teklif.detail.inputs.rawdata.value = null;
                                $scope.teklif.detail.inputs.rawdata.isExist = false;
                            },
                            set: function (row) {
                                $scope.teklif.detail.inputs.rawdata.value = row;
                                $scope.teklif.detail.inputs.rawdata.isExist = true;
                            },
                        },
                        id: {
                            value: 0,
                            init: function () {
                                $scope.teklif.detail.inputs.id.value = 0;
                            },
                            set: function (value) {
                                $scope.teklif.detail.inputs.id.value = value;
                            }
                        },
                        index: {
                            value: 0,
                            init: function () {
                                $scope.teklif.detail.inputs.index.value = 0;
                            },
                            set: function (value) {
                                $scope.teklif.detail.inputs.index.value = value;
                            }
                        },
                        state: {
                            value: 'NEW',
                            init: function () {
                                $scope.teklif.detail.inputs.state.value = 'NEW';
                            },
                            set: function (value) {
                                $scope.teklif.detail.inputs.state.value = value;
                            }
                        },
                        hammadde: {
                            isValid: false,
                            value: '',
                            cls: '',
                            msg: '',
                            init: function () {
                                $scope.teklif.detail.inputs.hammadde.isValid = false;
                                $scope.teklif.detail.inputs.hammadde.value = '';
                                $scope.teklif.detail.inputs.hammadde.cls = '';
                                $scope.teklif.detail.inputs.hammadde.msg = '';
                                $scope.teklif.detail.inputs.hammadde.set('');
                            },
                            set: function (value) {
                                $scope.teklif.detail.inputs.hammadde.value = value;
                                $scope.teklif.detail.inputs.hammadde.isValid = true;
                                $scope.teklif.detail.inputs.hammadde.cls = '';
                                $scope.teklif.detail.inputs.hammadde.msg = '';
                            },
                            check: function (applyCheck) {
                                var check = app.valid.text($scope.teklif.detail.inputs.hammadde.value);
                                if (applyCheck || $scope.teklif.detail.inputs.hammadde.isValid !== check) {
                                    $scope.teklif.detail.inputs.hammadde.isValid = check;
                                    if ($scope.teklif.detail.inputs.hammadde.isValid) {
                                        $scope.teklif.detail.inputs.hammadde.success();
                                    } else {
                                        $scope.teklif.detail.inputs.hammadde.error('* Geçersiz Ham Madde / Yarı Mamul!');
                                    }
                                }
                                return $scope.teklif.detail.inputs.isValid;
                            },
                            change: function () {
                                $scope.teklif.detail.inputs.hammadde.check();
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.hammadde.cls = '';
                                $scope.teklif.detail.inputs.hammadde.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.hammadde.cls = 'has-error';
                                $scope.teklif.detail.inputs.hammadde.msg = msg;
                            }
                        },
                        olcubirimi: {
                            list: page.data.olcubirimi,
                            isValid: false,
                            selected: { name: 'Metre', value: 'metre' },
                            cls: '',
                            msg: '',
                            inputValue: '',
                            shouldLoadOptionsOnInputFocus: true,
                            init: function () {
                                $scope.teklif.detail.inputs.olcubirimi.isValid = false;
                                $scope.teklif.detail.inputs.olcubirimi.selected = { name: 'Metre', value: 'metre' };
                                $scope.teklif.detail.inputs.olcubirimi.cls = '';
                                $scope.teklif.detail.inputs.olcubirimi.msg = '';
                            },
                            check: function (applyCheck) {
                                var check = $scope.teklif.detail.inputs.olcubirimi.selected && $scope.teklif.detail.inputs.olcubirimi.selected.hasOwnProperty('value') && typeof $scope.teklif.detail.inputs.olcubirimi.selected.value !== 'undefined' && $scope.teklif.detail.inputs.olcubirimi.selected.value != null ? true : false;
                                if (applyCheck || $scope.teklif.detail.inputs.olcubirimi.isValid !== check) {
                                    $scope.teklif.detail.inputs.olcubirimi.isValid = check;
                                    if ($scope.teklif.detail.inputs.olcubirimi.isValid) {
                                        $scope.teklif.detail.inputs.olcubirimi.success();
                                    } else {
                                        $scope.teklif.detail.inputs.olcubirimi.error('* Geçersiz Ölçü Birimi');
                                    }
                                }
                                return $scope.teklif.detail.inputs.olcubirimi.isValid;
                            },
                            change: function () {
                                $scope.teklif.detail.inputs.olcubirimi.check();
                            },
                            select: function (olcubirimi) {
                                $scope.teklif.detail.inputs.olcubirimi.isValid = true;
                                $scope.teklif.detail.inputs.olcubirimi.selected = olcubirimi;
                                $scope.teklif.detail.inputs.olcubirimi.cls = '';
                                $scope.teklif.detail.inputs.olcubirimi.msg = '';
                            },
                            set: function (value) {
                                var goon = true;
                                var i = 0;
                                while (goon && i < $scope.teklif.detail.inputs.olcubirimi.list.length) {
                                    if ($scope.teklif.detail.inputs.olcubirimi.list[i].value === value) {
                                        $scope.teklif.detail.inputs.olcubirimi.select($scope.teklif.detail.inputs.olcubirimi.list[i]);
                                        goon = false;
                                    }
                                    i++;
                                }
                                if (goon) {
                                    $scope.teklif.detail.inputs.olcubirimi.init();
                                }
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.olcubirimi.cls = '';
                                $scope.teklif.detail.inputs.olcubirimi.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.olcubirimi.cls = 'has-error';
                                $scope.teklif.detail.inputs.olcubirimi.msg = msg;
                            }
                        },
                        miktar: {
                            isValid: false,
                            value: 0,
                            text: '',
                            cls: '',
                            msg: '',
                            init: function () {
                                $scope.teklif.detail.inputs.miktar.isValid = false;
                                $scope.teklif.detail.inputs.miktar.value = '';
                                $scope.teklif.detail.inputs.miktar.text = '';
                                $scope.teklif.detail.inputs.miktar.cls = '';
                                $scope.teklif.detail.inputs.miktar.msg = '';
                                $scope.teklif.detail.inputs.miktar.set('');
                            },
                            set: function (miktar) {
                                $scope.teklif.detail.inputs.miktar.value = miktar.value;
                                $scope.teklif.detail.inputs.miktar.text = miktar.text;
                                $scope.teklif.detail.inputs.miktar.isValid = true;
                                $scope.teklif.detail.inputs.miktar.cls = '';
                                $scope.teklif.detail.inputs.miktar.msg = '';
                            },
                            check: function (applyCheck) {
                                $scope.teklif.detail.inputs.miktar.value = $scope.teklif.detail.inputs.miktar.text.tryParseMoneyToFloat();
                                var check = app.valid.positiveNumber($scope.teklif.detail.inputs.miktar.value);
                                if (applyCheck || $scope.teklif.detail.inputs.miktar.isValid !== check) {
                                    $scope.teklif.detail.inputs.miktar.isValid = check;
                                    if ($scope.teklif.detail.inputs.miktar.isValid) {
                                        $scope.teklif.detail.inputs.miktar.success();
                                    } else {
                                        $scope.teklif.detail.inputs.miktar.error('* Geçersiz Miktar !');
                                    }
                                }
                                return $scope.teklif.detail.inputs.miktar.isValid;
                            },
                            change: function () {
                                $scope.teklif.detail.inputs.miktar.check($scope.teklif.detail.inputs.miktar.value);
                                $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.hesaplananfiyat.value;
                                $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');//virgülden sonra 2basamak
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.miktar.cls = '';
                                $scope.teklif.detail.inputs.miktar.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.miktar.cls = 'has-error';
                                $scope.teklif.detail.inputs.miktar.msg = msg;
                            }
                        },
                        adet: {
                            isValid: false,
                            value: 0,
                            text: '',
                            cls: '',
                            msg: '',
                            init: function () {
                                $scope.teklif.detail.inputs.adet.isValid = false;
                                $scope.teklif.detail.inputs.adet.value = 0;
                                $scope.teklif.detail.inputs.adet.text = '';
                                $scope.teklif.detail.inputs.adet.cls = '';
                                $scope.teklif.detail.inputs.adet.msg = '';
                                $scope.teklif.detail.inputs.adet.set('');
                            },
                            set: function (adet) {
                                $scope.teklif.detail.inputs.adet.value = adet.value;
                                $scope.teklif.detail.inputs.adet.text = adet.text;
                                $scope.teklif.detail.inputs.adet.isValid = true;
                                $scope.teklif.detail.inputs.adet.cls = '';
                                $scope.teklif.detail.inputs.adet.msg = '';
                            },
                            check: function (applyCheck) {
                                $scope.teklif.detail.inputs.adet.value = $scope.teklif.detail.inputs.adet.text.tryParseMoneyToFloat();
                                var check = app.valid.positiveNumber($scope.teklif.detail.inputs.adet.value);
                                if (applyCheck || $scope.teklif.detail.inputs.adet.isValid !== check) {
                                    $scope.teklif.detail.inputs.adet.isValid = check;
                                    if ($scope.teklif.detail.inputs.adet.isValid) {
                                        $scope.teklif.detail.inputs.adet.success();
                                    } else {
                                        $scope.teklif.detail.inputs.adet.error('* Geçersiz Adet !');
                                    }
                                }
                                return $scope.teklif.detail.inputs.adet.isValid;
                            },
                            change: function () {
                                $scope.teklif.detail.inputs.adet.check($scope.teklif.detail.inputs.adet.value);
                                console.log("$scope.teklif.detail.inputs.adet.value --->", $scope.teklif.detail.inputs.adet.value);
                                if ($scope.teklif.detail.inputs.adet.value !== null) {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.adet.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                }
                                else {
                                    $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                    $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                                }
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.adet.cls = '';
                                $scope.teklif.detail.inputs.adet.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.adet.cls = 'has-error';
                                $scope.teklif.detail.inputs.adet.msg = msg;
                            }
                        },
                        birimlistefiyati: {
                            isValid: false,
                            value: 0,
                            text: '',
                            cls: '',
                            msg: '',
                            init: function () {
                                $scope.teklif.detail.inputs.birimlistefiyati.isValid = false;
                                $scope.teklif.detail.inputs.birimlistefiyati.value = 0;
                                $scope.teklif.detail.inputs.birimlistefiyati.text = '';
                                $scope.teklif.detail.inputs.birimlistefiyati.cls = '';
                                $scope.teklif.detail.inputs.birimlistefiyati.msg = '';
                                $scope.teklif.detail.inputs.birimlistefiyati.set('');
                            },
                            set: function (birimlistefiyati) {
                                $scope.teklif.detail.inputs.birimlistefiyati.value = birimlistefiyati.value;
                                $scope.teklif.detail.inputs.birimlistefiyati.text = birimlistefiyati.text;
                                $scope.teklif.detail.inputs.birimlistefiyati.isValid = true;
                                $scope.teklif.detail.inputs.birimlistefiyati.cls = '';
                                $scope.teklif.detail.inputs.birimlistefiyati.msg = '';
                            },
                            check: function (applyCheck) {
                                $scope.teklif.detail.inputs.birimlistefiyati.value = $scope.teklif.detail.inputs.birimlistefiyati.text.tryParseMoneyToFloat();
                                var check = app.valid.positiveNumber($scope.teklif.detail.inputs.birimlistefiyati.value);
                                if (applyCheck || $scope.teklif.detail.inputs.birimlistefiyati.isValid !== check) {
                                    $scope.teklif.detail.inputs.birimlistefiyati.isValid = check;
                                    if ($scope.teklif.detail.inputs.birimlistefiyati.isValid) {
                                        $scope.teklif.detail.inputs.birimlistefiyati.success();
                                    } else {
                                        $scope.teklif.detail.inputs.birimlistefiyati.error('* Geçersiz Birim Liste Fiyatı !');
                                    }
                                }
                                return $scope.teklif.detail.inputs.birimlistefiyati.isValid;
                            },
                            change: function () {
                                $scope.teklif.detail.inputs.birimlistefiyati.check($scope.teklif.detail.inputs.birimlistefiyati.value);
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.birimlistefiyati.cls = '';
                                $scope.teklif.detail.inputs.birimlistefiyati.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.birimlistefiyati.cls = 'has-error';
                                $scope.teklif.detail.inputs.birimlistefiyati.msg = msg;
                            }
                        },
                        girilenfiyat: {
                            isValid: false,
                            value: 0,
                            text: '',
                            cls: '',
                            msg: '',
                            init: function () {
                                $scope.teklif.detail.inputs.girilenfiyat.isValid = false;
                                $scope.teklif.detail.inputs.girilenfiyat.value = 0;
                                $scope.teklif.detail.inputs.girilenfiyat.text = '';
                                $scope.teklif.detail.inputs.girilenfiyat.cls = '';
                                $scope.teklif.detail.inputs.girilenfiyat.msg = '';
                                $scope.teklif.detail.inputs.girilenfiyat.set('');
                            },
                            set: function (girilenfiyat) {
                                $scope.teklif.detail.inputs.girilenfiyat.value = girilenfiyat.value;
                                $scope.teklif.detail.inputs.girilenfiyat.text = girilenfiyat.text;
                                $scope.teklif.detail.inputs.girilenfiyat.isValid = true;
                                $scope.teklif.detail.inputs.girilenfiyat.cls = '';
                                $scope.teklif.detail.inputs.girilenfiyat.msg = '';
                            },
                            check: function (applyCheck) {
                                $scope.teklif.detail.inputs.girilenfiyat.value = $scope.teklif.detail.inputs.girilenfiyat.text.tryParseMoneyToFloat();
                                var check = app.valid.positiveNumber($scope.teklif.detail.inputs.girilenfiyat.value);
                                if (applyCheck || $scope.teklif.detail.inputs.girilenfiyat.isValid !== check) {
                                    $scope.teklif.detail.inputs.girilenfiyat.isValid = check;
                                    if ($scope.teklif.detail.inputs.girilenfiyat.isValid) {
                                        $scope.teklif.detail.inputs.girilenfiyat.success();
                                    } else {
                                        $scope.teklif.detail.inputs.girilenfiyat.error('* Geçersiz Girilen Fiyat !');
                                    }
                                }
                                return $scope.teklif.detail.inputs.girilenfiyat.isValid;
                            },
                            change: function () {
                                $scope.teklif.detail.inputs.girilenfiyat.check($scope.teklif.detail.inputs.girilenfiyat.value);
                                $scope.teklif.detail.inputs.tutar.value = $scope.teklif.detail.inputs.miktar.value * $scope.teklif.detail.inputs.girilenfiyat.value;
                                $scope.teklif.detail.inputs.tutar.text = $scope.teklif.detail.inputs.tutar.value.formatMoney(2, ',', '.');
                            },
                            success: function () {
                                $scope.teklif.detail.inputs.girilenfiyat.cls = '';
                                $scope.teklif.detail.inputs.girilenfiyat.msg = '';
                            },
                            error: function (msg) {
                                $scope.teklif.detail.inputs.girilenfiyat.cls = 'has-error';
                                $scope.teklif.detail.inputs.girilenfiyat.msg = msg;
                            }
                        },
                        tutar: {
                            value: 0,
                            text: '',
                            init: function () {
                                $scope.teklif.detail.inputs.tutar.value = 0;
                                $scope.teklif.detail.inputs.tutar.text = '';
                                $scope.teklif.detail.inputs.tutar.set('');
                            },
                            set: function (tutar) {
                                $scope.teklif.detail.inputs.tutar.value = tutar.value;
                                $scope.teklif.detail.inputs.tutar.text = tutar.text;
                            }
                        },
                        toplamtutar: { // kalemlerdeki tutarların toplamı
                            value: 0,
                            uyari: false,
                            init: function () {
                                $scope.teklif.detail.inputs.toplamtutar.value = 0;
                                $scope.teklif.detail.inputs.tutar.set(0);
                                $scope.teklif.detail.inputs.toplamtutar.set(0);
                            },
                            set: function (value) {
                                // $scope.teklif.detail.inputs.toplamtutar.value = value + $scope.teklif.detail.tutar.value;
                            }
                        },
                        set: function (index, row) {
                            $scope.teklif.detail.inputs.rawdata.set(row);
                            $scope.teklif.detail.inputs.id.set(row.TEKLIFMALIYETKALEMLERIID);
                            $scope.teklif.detail.inputs.index.set(index);
                            $scope.teklif.detail.inputs.state.set(row.STATE);
                            $scope.teklif.detail.inputs.hammadde.set(row.HAMMADDEYARIMAMUL);
                            $scope.teklif.detail.inputs.olcubirimi.set(row.OLCUBIRIMI);
                            $scope.teklif.detail.inputs.miktar.set({ value: row.MIKTARVAL, text: row.MIKTARTXT });
                            $scope.teklif.detail.inputs.adet.set({ value: row.KALEMADETVAL, text: row.KALEMADETTXT });
                            $scope.teklif.detail.inputs.birimlistefiyati.set({ value: row.BIRIMLISTEFIYATIVAL, text: row.BIRIMLISTEFIYATITXT });
                            $scope.teklif.detail.inputs.girilenfiyat.set({ value: row.GIRILENFIYATVAL, text: row.GIRILENFIYATTXT });
                            $scope.teklif.detail.inputs.tutar.set({ value: row.TUTARVAL, text: row.TUTARTXT });
                            $scope.teklif.detail.inputs.toplamtutar.set({ value: row.TOPLAMTEKLIFTUTARIVAL, text: row.TOPLAMTEKLIFTUTARITXT });
                        },
                        init: function (index, row) {
                            $scope.teklif.detail.inputs.hammadde.init();
                            $scope.teklif.detail.inputs.olcubirimi.init();
                            $scope.teklif.detail.inputs.miktar.init();
                            $scope.teklif.detail.inputs.adet.init();
                            $scope.teklif.detail.inputs.birimlistefiyati.init();
                            $scope.teklif.detail.inputs.tutar.init();
                            $scope.teklif.detail.inputs.toplamtutar.init();
                        },
                        undo: function () {
                            if (!$scope.teklif.detail.inputs.rawdata.isExist) {
                                return;
                            }
                            if ($scope.teklif.detail.inputs.rawdata.isExist) {
                                $scope.teklif.detail.rows.unshift($scope.teklif.detail.inputs.rawdata.value);
                            } else {
                                $scope.teklif.detail.row = [$scope.teklif.detail.inputs.rawdata.value];
                            }
                        },
                        clear: function () {
                            $scope.teklif.detail.inputs.undo();
                            $scope.teklif.detail.inputs.init();
                        },
                        check: function () {
                            var applyCheck = true;
                            var OK = {
                                hammadde: $scope.teklif.detail.inputs.hammadde.check(applyCheck),
                                olcubirimi: $scope.teklif.detail.inputs.olcubirimi.check(applyCheck),
                                miktar: $scope.teklif.detail.inputs.miktar.check(applyCheck),
                                adet: $scope.teklif.detail.inputs.adet.check(applyCheck),
                                birimlistefiyati: $scope.teklif.detail.inputs.birimlistefiyati.check(applyCheck)
                            };
                            return OK.hammadde && OK.olcubirimi && OK.miktar && OK.adet && OK.birimlistefiyati;
                        },
                        add: function () {
                            if (!$scope.teklif.detail.inputs.check()) {
                                return;
                            }
                            var row = {
                                ID: $scope.teklif.detail.inputs.id.value,
                                STATE: $scope.teklif.detail.inputs.state.value === 'OLD' ? 'UPD' : $scope.teklif.detail.inputs.state.value,
                                TEKLIFMALIYETID: 0,
                                HAMMADDEYARIMAMULID: $scope.teklif.detail.inputs.hammadde.selected.HAMMADDEYARIMAMULLERID,
                                HAMMADDEYARIMAMULADI: $scope.teklif.detail.inputs.hammadde.selected.ADI,
                                OLCUBIRIMI: $scope.teklif.detail.inputs.olcubirimi.selected.value,
                                MIKTARVAL: $scope.teklif.detail.inputs.miktar.value,
                                MIKTARTXT: $scope.teklif.detail.inputs.miktar.text,
                                KALEMADETVAL: $scope.teklif.detail.inputs.adet.value,
                                KALEMADETTXT: $scope.teklif.detail.inputs.adet.text,
                                BIRIMLISTEFIYATIVAL: $scope.teklif.detail.inputs.birimlistefiyati.value,
                                BIRIMLISTEFIYATITXT: $scope.teklif.detail.inputs.birimlistefiyati.text,
                                GIRILENFIYATVAL: $scope.teklif.detail.inputs.girilen.value,
                                GIRILENFIYATTXT: $scope.teklif.detail.inputs.girilen.text,
                                TUTARVAL: $scope.teklif.detail.inputs.tutar.value,
                                TUTARTXT: $scope.teklif.detail.inputs.tutar.text,
                            };
                            if ($scope.teklif.detail.isExist) {
                                $scope.teklif.detail.rows.unshift(row);
                            } else {
                                $scope.teklif.detail.rows = [row];
                                $scope.teklif.detail.isExist = true;
                            }
                            $scope.teklif.detail.inputs.init();
                        },
                    },
                },
                rawdata: null,
                id: {
                    value: 0,
                    init: function () {
                        $scope.teklif.id.value = 0;
                    },
                    set: function (value) {
                        $scope.teklif.id.value = value;
                    }
                },
                maliyetid: {
                    value: 0,
                    init: function () {
                        $scope.teklif.maliyetid.value = 0;
                    },
                    set: function (value) {
                        $scope.teklif.maliyetid.value = value;
                    }
                },
                //
                cari: {
                    isValid: false,
                    selected: null,
                    cls: '',
                    msg: '',
                    inputValue: '',
                    shouldLoadOptionsOnInputFocus: true,
                    init: function () {
                        $scope.teklif.cari.isValid = false;
                        $scope.teklif.cari.selected = null;
                        $scope.teklif.cari.cls = '';
                        $scope.teklif.cari.msg = '';
                        $scope.teklif.cari.inputValue = '';
                        $scope.teklif.cari.shouldLoadOptionsOnInputFocus = true;
                    },
                    set: function (cari) {
                        $scope.teklif.cari.isValid = true;
                        $scope.teklif.cari.selected = cari;
                        $scope.teklif.cari.cls = '';
                        $scope.teklif.cari.msg = '';
                        $scope.teklif.cari.inputValue = cari.ADI;
                        $scope.teklif.cari.shouldLoadOptionsOnInputFocus = true;
                        localStorage.setItem("cari", JSON.stringify($scope.teklif.cari.selected.CARILERID));
                    },
                    check: function (applyCheck) {
                        var check = $scope.teklif.cari.selected && $scope.teklif.cari.selected.hasOwnProperty('CARILERID') && typeof $scope.teklif.cari.selected.CARILERID !== 'undefined' && $scope.teklif.cari.selected.CARILERID != null ? true : false;
                        if (applyCheck || $scope.teklif.cari.isValid !== check) {
                            $scope.teklif.cari.isValid = check;
                            if ($scope.teklif.cari.isValid) {
                                $scope.teklif.cari.success();
                            } else {
                                $scope.teklif.cari.error('* Geçersiz Cari Adı');
                            }
                        }
                        return $scope.teklif.cari.isValid;
                    },
                    select: function (cari) {
                        $scope.teklif.cari.selected = cari;
                        localStorage.setItem("cari", JSON.stringify($scope.teklif.cari.selected.CARILERID));
                        $scope.teklif.cari.check();
                    },
                    success: function () {
                        $scope.teklif.cari.cls = '';
                        $scope.teklif.cari.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.cari.cls = 'has-error';
                        $scope.teklif.cari.msg = msg;
                    }
                },
                teklifkodu: {
                    isValid: false,
                    value: '',
                    index: [],
                    init: function () {
                        $scope.teklif.teklifkodu.isValid = false;
                        $scope.teklif.teklifkodu.value = '';
                        $scope.teklif.teklifkodu.set('');
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.teklifkodu.value);
                        if (applyCheck || $scope.teklif.teklifkodu.isValid !== check) {
                            $scope.teklif.teklifkodu.isValid = check;
                            if ($scope.teklif.teklifkodu.isValid) {
                                $scope.teklif.teklifkodu.success();
                            } else {
                                $scope.teklif.teklifkodu.error('* Geçersiz Teklif Kodu !');
                            }
                        }
                        return $scope.teklif.teklifkodu.isValid;
                    },
                    change: function () {
                        $scope.teklif.teklifkodu.check($scope.teklif.teklifkodu.value);
                        $rootScope.teklifkodu = $scope.teklif.teklifkodu.value;
                        localStorage.setItem("modify-component-teklifkodu", JSON.stringify($scope.teklif.teklifkodu.value));
                    },
                    set: function (value) {
                        localStorage.setItem("modify-component-teklif-kodu", JSON.stringify(value));
                        $scope.teklif.teklifkodu.value = value;
                        $scope.teklif.teklifkodu.text = value;
                        $scope.teklif.teklifkodu.isValid = true;
                        $scope.teklif.teklifkodu.cls = '';
                        $scope.teklif.teklifkodu.msg = '';
                    },
                    success: function () {
                        $scope.teklif.teklifkodu.cls = '';
                        $scope.teklif.teklifkodu.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.teklifkodu.cls = 'has-error';
                        $scope.teklif.teklifkodu.msg = msg;
                    }
                },
                tekliftarihi: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.tekliftarihi.isValid = false;
                        $scope.teklif.tekliftarihi.value = new Date();
                        $scope.teklif.tekliftarihi.cls = '';
                        $scope.teklif.tekliftarihi.msg = '';
                        $scope.teklif.tekliftarihi.set('');
                    },
                    set: function (value) {
                        $scope.teklif.tekliftarihi.value = new Date(value);
                        $scope.teklif.tekliftarihi.isValid = true;
                        $scope.teklif.tekliftarihi.cls = '';
                        $scope.teklif.tekliftarihi.msg = '';
                        localStorage.setItem("modify-tekliftarihi", JSON.stringify(value));
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.tekliftarihi.value);
                        if (applyCheck || $scope.teklif.tekliftarihi.isValid !== check) {
                            $scope.teklif.tekliftarihi.isValid = check;
                            if ($scope.teklif.tekliftarihi.isValid) {
                                $scope.teklif.tekliftarihi.success();
                            } else {
                                $scope.teklif.tekliftarihi.error('* Geçersiz Teklif Tarihi!');
                            }
                        }
                        return $scope.teklif.tekliftarihi.isValid;
                    },
                    change: function () {
                        $scope.teklif.tekliftarihi.check();
                        localStorage.setItem("modify-tekliftarihi", JSON.stringify($scope.teklif.tekliftarihi.value));
                    },
                    success: function () {
                        $scope.teklif.tekliftarihi.cls = '';
                        $scope.teklif.tekliftarihi.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.tekliftarihi.cls = 'has-error';
                        $scope.teklif.tekliftarihi.msg = msg;
                    }
                },
                toplamtekliftutari: {
                    isValid: false,
                    value: '',
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.toplamtekliftutari.isValid = false;
                        $scope.teklif.toplamtekliftutari.value = '';
                        $scope.teklif.toplamtekliftutari.cls = '';
                        $scope.teklif.toplamtekliftutari.msg = '';
                        $scope.teklif.toplamtekliftutari.set('');
                    },
                    set: function (toplamtekliftutari) {
                        $scope.teklif.toplamtekliftutari.value = toplamtekliftutari;
                        $scope.teklif.toplamtekliftutari.text = toplamtekliftutari;
                        $scope.teklif.toplamtekliftutari.isValid = true;
                        $scope.teklif.toplamtekliftutari.cls = '';
                        $scope.teklif.toplamtekliftutari.msg = '';
                    },
                },
                aciklama: {
                    value: '',
                    init: function () {
                        $scope.teklif.aciklama.value = '';
                    },
                    set: function (value) {
                        $scope.teklif.aciklama.value = value;
                        localStorage.setItem("aciklama", JSON.stringify($scope.teklif.aciklama.value));
                    },
                    check: function (applyCheck) {
                        $scope.teklif.aciklama.value = applyCheck;
                        localStorage.setItem("aciklama", JSON.stringify($scope.teklif.aciklama.value));
                    },
                    change: function () {
                        $scope.teklif.aciklama.check($scope.teklif.aciklama.value);

                    },
                },
                //
                teklifadi: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.teklifadi.isValid = false;
                        $scope.teklif.teklifadi.value = '';
                        $scope.teklif.teklifadi.cls = '';
                        $scope.teklif.teklifadi.msg = '';
                        $scope.teklif.teklifadi.set('');
                    },
                    set: function (value) {
                        $scope.teklif.teklifadi.value = $scope.teklif.rows.TEKLIFMALIYETADI;
                        $scope.teklif.teklifadi.isValid = true;
                        $scope.teklif.teklifadi.cls = '';
                        $scope.teklif.teklifadi.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.teklifadi.value);
                        if (applyCheck || $scope.teklif.teklifadi.isValid !== check) {
                            $scope.teklif.teklifadi.isValid = check;
                            if ($scope.teklif.teklifadi.isValid) {
                                $scope.teklif.teklifadi.success();
                            } else {
                                $scope.teklif.teklifadi.error('* Geçersiz Teklif Adı !');
                            }
                        }
                        return $scope.teklif.teklifadi.isValid;
                    },
                    change: function () {
                        $scope.teklif.teklifadi.check($scope.teklif.teklifadi.value);
                    },
                    success: function () {
                        $scope.teklif.teklifadi.cls = '';
                        $scope.teklif.teklifadi.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.teklifadi.cls = 'has-error';
                        $scope.teklif.teklifadi.msg = msg;
                    }
                },
                birimsatisfiyati: {
                    isValid: false,
                    value: '',
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.birimsatisfiyati.isValid = false;
                        $scope.teklif.birimsatisfiyati.value = '';
                        $scope.teklif.birimsatisfiyati.text = '';
                        $scope.teklif.birimsatisfiyati.cls = '';
                        $scope.teklif.birimsatisfiyati.msg = '';
                        $scope.teklif.birimsatisfiyati.set('');
                    },
                    set: function (birimsatisfiyati) {
                        $scope.teklif.birimsatisfiyati.value = birimsatisfiyati.value;
                        $scope.teklif.birimsatisfiyati.text = birimsatisfiyati.text;
                        $scope.teklif.birimsatisfiyati.isValid = true;
                        $scope.teklif.birimsatisfiyati.cls = '';
                        $scope.teklif.birimsatisfiyati.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.birimsatisfiyati.value);
                        if (applyCheck || $scope.teklif.birimsatisfiyati.isValid !== check) {
                            $scope.teklif.birimsatisfiyati.isValid = check;
                            if ($scope.teklif.birimsatisfiyati.isValid) {
                                $scope.teklif.birimsatisfiyati.success();
                            } else {
                                $scope.teklif.birimsatisfiyati.error('* Geçersiz Birim Satış Fiyatı !');
                            }
                        }
                        return $scope.teklif.birimsatisfiyati.isValid;
                    },
                    change: function () {
                        $scope.teklif.birimsatisfiyati.check($scope.teklif.birimsatisfiyati.value);
                        $scope.teklif.birimsatisveadet.value = $scope.teklif.birimsatisfiyati.value * $scope.teklif.adet.value;//12 * 10
                        $scope.teklif.toplamsatis.value = $scope.teklif.birimsatisveadet.value * $scope.teklif.karyuzdesi.value;// 120 * 10
                        $scope.teklif.toplamsatisfiyati.value = $scope.teklif.birimsatisveadet.value + ($scope.teklif.toplamsatis.value / 100);// 120 + 12
                        $scope.teklif.toplamsatisfiyati.text = $scope.teklif.toplamsatisfiyati.value.formatMoney(2, ',', '.');//virgülden sonra 2basamak
                    },
                    success: function () {
                        $scope.teklif.birimsatisfiyati.cls = '';
                        $scope.teklif.birimsatisfiyati.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.birimsatisfiyati.cls = 'has-error';
                        $scope.teklif.birimsatisfiyati.msg = msg;
                    }
                },
                adet: {
                    isValid: false,
                    value: '',
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.adet.isValid = false;
                        $scope.teklif.adet.value = '';
                        $scope.teklif.adet.text = '';
                        $scope.teklif.adet.cls = '';
                        $scope.teklif.adet.msg = '';
                        $scope.teklif.adet.set('');
                    },
                    set: function (adet) {
                        $scope.teklif.adet.value = adet.value;
                        $scope.teklif.adet.text = adet.text;
                        $scope.teklif.adet.isValid = true;
                        $scope.teklif.adet.cls = '';
                        $scope.teklif.adet.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.adet.value);
                        if (applyCheck || $scope.teklif.adet.isValid !== check) {
                            $scope.teklif.adet.isValid = check;
                            if ($scope.teklif.adet.isValid) {
                                $scope.teklif.adet.success();
                            } else {
                                $scope.teklif.adet.error('* Geçersiz Adet !');
                            }
                        }
                        return $scope.teklif.adet.isValid;
                    },
                    change: function () {
                        $scope.teklif.adet.check($scope.teklif.adet.value);
                        $scope.teklif.birimsatisveadet.value = $scope.teklif.birimsatisfiyati.value * $scope.teklif.adet.value;
                        $scope.teklif.toplamsatis.value = $scope.teklif.birimsatisveadet.value * $scope.teklif.karyuzdesi.value;
                        $scope.teklif.toplamsatisfiyati.value = $scope.teklif.birimsatisveadet.value + ($scope.teklif.toplamsatis.value / 100);
                        $scope.teklif.toplamsatisfiyati.text = $scope.teklif.toplamsatisfiyati.value.formatMoney(2, ',', '.');//virgülden sonra 2basamak
                    },
                    success: function () {
                        $scope.teklif.adet.cls = '';
                        $scope.teklif.adet.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.adet.cls = 'has-error';
                        $scope.teklif.adet.msg = msg;
                    }
                },
                karyuzdesi: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.karyuzdesi.isValid = false;
                        $scope.teklif.karyuzdesi.value = '';
                        $scope.teklif.karyuzdesi.cls = '';
                        $scope.teklif.karyuzdesi.msg = '';
                        $scope.teklif.karyuzdesi.set('');
                    },
                    set: function (karyuzdesi) {
                        $scope.teklif.karyuzdesi.value = karyuzdesi;
                        $scope.teklif.karyuzdesi.isValid = true;
                        $scope.teklif.karyuzdesi.cls = '';
                        $scope.teklif.karyuzdesi.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.karyuzdesi.value);
                        if (applyCheck || $scope.teklif.karyuzdesi.isValid !== check) {
                            $scope.teklif.karyuzdesi.isValid = check;
                            if ($scope.teklif.karyuzdesi.isValid) {
                                $scope.teklif.karyuzdesi.success();
                            } else {
                                $scope.teklif.karyuzdesi.error('* Geçersiz Kar Yüzdesi !');
                            }
                        }
                        return $scope.teklif.karyuzdesi.isValid;
                    },
                    change: function () {
                        $scope.teklif.karyuzdesi.check($scope.teklif.karyuzdesi.value);
                        $scope.teklif.birimsatisveadet.value = $scope.teklif.birimsatisfiyati.value * $scope.teklif.adet.value;
                        $scope.teklif.toplamsatis.value = $scope.teklif.birimsatisveadet.value * $scope.teklif.karyuzdesi.value;
                        $scope.teklif.toplamsatisfiyati.value = $scope.teklif.birimsatisveadet.value + ($scope.teklif.toplamsatis.value / 100);
                        $scope.teklif.toplamsatisfiyati.text = $scope.teklif.toplamsatisfiyati.value.formatMoney(2, ',', '.');//virgülden sonra 2basamak
                    },
                    success: function () {
                        $scope.teklif.karyuzdesi.cls = '';
                        $scope.teklif.karyuzdesi.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.karyuzdesi.cls = 'has-error';
                        $scope.teklif.karyuzdesi.msg = msg;
                    }
                },
                toplamsatisfiyati: {
                    isValid: false,
                    value: '',
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.toplamsatisfiyati.isValid = false;
                        $scope.teklif.toplamsatisfiyati.value = '';
                        $scope.teklif.toplamsatisfiyati.text = '';
                        $scope.teklif.toplamsatisfiyati.set('');
                    },
                    set: function (toplamsatisfiyati) {
                        $scope.teklif.toplamsatisfiyati.value = toplamsatisfiyati.value;
                        $scope.teklif.toplamsatisfiyati.text = toplamsatisfiyati.text;
                        $scope.teklif.toplamsatisfiyati.isValid = true;
                    },
                },
                birimsatisveadet: {
                    value: 0,
                    uyari: false,
                    init: function () {
                        $scope.teklif.birimsatisveadet.value = 0;
                        $scope.teklif.birimsatisveadet.set(0);
                    },
                    set: function (value) {
                        $scope.teklif.detail.inputs.toplamtutar.value = value + $scope.teklif.detail.tutar.value;
                    },
                },
                toplamsatis: {
                    value: 0,
                    uyari: false,
                    init: function () {
                        $scope.teklif.toplamsatis.value = 0;
                        $scope.teklif.toplamsatis.set(0);
                    },
                    set: function (value) {
                        // $scope.teklif.detail.inputs.toplamtutar.value = value + $scope.teklif.detail.tutar.value;
                    },
                },
                satisfiyatlaritoplami: {
                    value: 0,
                    text: '',
                    init: function () {
                        $scope.teklif.satisfiyatlaritoplami.value = 0;
                        $scope.teklif.satisfiyatlaritoplami.text = '';
                    },
                    set: function (value) {
                        $scope.teklif.satisfiyatlaritoplami.value = value;
                        $scope.teklif.satisfiyatlaritoplami.text = value;
                    }
                },
                tekliftutarlarinitopla: {
                    value: 0,
                    text: '',
                    init: function () {
                        $scope.teklif.tekliftutarlarinitopla.value = 0;
                        $scope.teklif.tekliftutarlarinitopla.text = '';
                    },
                    set: function (value) {
                        $scope.teklif.tekliftutarlarinitopla.value = value;
                        $scope.teklif.tekliftutarlarinitopla.text = value;
                    }
                },
                set: function (_teklif) {
                    $scope.teklif.rawdata = _teklif;
                    $scope.teklif.id.set(_teklif.TEKLIFLERID);
                    $scope.teklif.maliyetid.set(_teklif.TEKLIFMALIYETLERID);
                    $scope.teklif.cari.set({ CARILERID: _teklif.CARIID, ADI: _teklif.CARI });
                    $scope.teklif.teklifkodu.set(_teklif.TEKLIFKODU);
                    $scope.teklif.tekliftarihi.set(_teklif.TEKLIFTARIHIVAL);
                    $scope.teklif.toplamtekliftutari.set({ value: _teklif.TOPLAMTEKLIFTUTARIVAL, text: _teklif.TOPLAMTEKLIFTUTARITXT });
                    $scope.teklif.aciklama.set(_teklif.TEKLIFACIKLAMA);
                    $scope.teklif.detail.set(_teklif.DETAILS);
                },
                teklifinit: function () { // Iptal et ve çık butonuna tıklanılırsa tüm teklif inputları sıfırlanmalı
                    $scope.teklif.cari.init();
                    $scope.teklif.teklifkodu.init();
                    $scope.teklif.aciklama.init();
                },
                init: function () {
                    $scope.teklif.cari.init();
                    $scope.teklif.teklifkodu.init();
                    $scope.teklif.toplamtekliftutari.init();
                    $scope.teklif.teklifadi.init();
                    $scope.teklif.birimsatisfiyati.init();
                    $scope.teklif.adet.init();
                    $scope.teklif.karyuzdesi.init();
                    $scope.teklif.toplamsatisfiyati.init();
                    $scope.teklif.aciklama.init();
                    $scope.teklif.detail.init();
                },
                check: function () {
                    var applyCheck = true;
                    var OK = {
                        cari: $scope.teklif.cari.check(applyCheck),
                        teklifkodu: $scope.teklif.teklifkodu.check(applyCheck),
                        toplamtekliftutari: $scope.teklif.toplamtekliftutari.value,
                        birimsatisfiyati: $scope.teklif.birimsatisfiyati.check(applyCheck),
                        adet: $scope.teklif.adet.check(applyCheck),
                        karyuzdesi: $scope.teklif.karyuzdesi.check(applyCheck)
                    };
                    return OK.cari && OK.teklifkodu && OK.toplamtekliftutari && OK.birimsatisfiyati &&
                        OK.adet && OK.karyuzdesi;
                },
                saveandclose: function () {
                var teklifexcel = $scope.teklif.teklifdata;
                var teklifdetail = $scope.teklif.rows;
                    if (typeof $scope.teklif.rows === 'undefined' || $scope.teklif.rows.length === 0) {
                        //  $scope.teklif.error('* Lütfen Teklif kalemleri ekleyiniz.');
                        return;
                    } if ($scope.teklif.cari.selected === null) {
                        $scope.teklif.error('* Lütfen Cari Adı seçiniz.');
                        return;
                    } if ($scope.teklif.teklifkodu.value === '') {
                        return;
                    }
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=12&d=' + new Date(),
                        data: {
                            teklifid: $scope.teklif.teklifdata.TEKLIFLERID,
                            cari: $scope.teklif.cari.selected.CARILERID,
                            toplamtekliftutari: $scope.teklif.toplamtekliftutari.value,
                            tekliftarihi: $scope.teklif.tekliftarihi.value,
                            teklifkodu: $scope.teklif.teklifkodu.value,
                            tekliftarihi: $scope.teklif.tekliftarihi.value,
                            onaydurumu: $scope.teklif.teklifdata.TEKLIFONAYDURUMU,
                            aciklama: $scope.teklif.aciklama.value
                        }
                    }).then(function (response) {
                        app.hide.spinner($('teklif-modify-modal > div:first > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 1) {
                            console.log(response.data);
                            $rootScope.showExcelModal(response.data, teklifdetail, response.data.TEKLIFKODU);// excel çıktısı almak için Modal açılmalı.
                       
                            // app.show.success('Teklif-Maliyet Bilgileri Eklendi!');
                            //window.location.pathname = 'teklifler.aspx';
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                            window.location.href = 'login.aspx';
                            return;
                        }
                        app.show.error('Bir şeyler ters gitti...');
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                        app.hide.spinner($('teklif-modify-modal > div:first > div:first'));
                    });
                },
        };
        Runopenmodify = $rootScope.runmodifydata;
        Runopenmodify();
  
    }])
    .directive('teklifModifyComponent', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/teklif-modify-component.html'
        };
    });
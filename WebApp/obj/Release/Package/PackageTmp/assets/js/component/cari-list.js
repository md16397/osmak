﻿app.angular
    .controller('cariListCtrl', ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
  
        $rootScope.modifyRow = function (_cari) {
            var i = 0;
            var goon = true;
            while (goon && $scope.cari.list.length > i) {
                if (_cari.CARIID === $scope.cari.list[i].CARIID) {
                    goon = false;
                    $scope.cari.list[i] = _cari;
                }
                i++;
            }
        };
        $rootScope.loadList = function () {
            $scope.cari.filter.init();
            $scope.cari.page.index = 1;
            $scope.cari.load();
        };
        $scope.cari = {
            page: {
                index: 1
            },
            filter: {
                type: 1, //this filter type is for page selecting = GET ALL LIST
                value: '',
                init: function () {
                    $scope.cari.filter.value = '';
                },
                change: function () {
                    $scope.cari.filter.timer.set();
                },
                timer: {
                    value: null,
                    interval: 500,
                    set: function () {
                        if ($scope.cari.filter.timer.value) {
                            $timeout.cancel($scope.cari.filter.timer.value);
                            $scope.cari.filter.timer.value = null;
                        }
                        $scope.cari.filter.timer.value = $timeout(function () {
                            if ($scope.cari.isLoading) {
                                return;
                            }
                            $scope.cari.page.index = 1;
                            $scope.cari.load();
                        }, $scope.cari.filter.timer.interval);
                    }
                }
            },
            totalRow: {
                value: 0,
                set: function (value) {
                    var paginate = $scope.cari.totalRow.value !== value;
                    $scope.cari.totalRow.value = value;
                    if (paginate) {
                        $scope.cari.paginate();
                    }
                }
            },
            maxVisibleRow: {
                value: app.pagination.maxVisible.row,
                set: function (value) {
                    var paginate = $scope.cari.maxVisibleRow.value !== value;
                    $scope.cari.maxVisibleRow.value = value;
                    if (paginate) {
                        $scope.cari.paginate();
                    }
                }
            },
            ngshow: {
                value: 0,
                set: function (value) {
                    $scope.cari.ngshow.value = value;
                }
            },
            list: [],
            isExist: false,
            load: function () {
                $scope.cari.isLoading = true;
                app.show.spinner($('cari-list > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/cari.ashx?&t=2&d=' + new Date(),
                    data: {
                        pageIndex: $scope.cari.page.index,
                        filterType: $scope.cari.filter.type,
                        filter: $scope.cari.filter.value,
                        pageRowCount: $scope.cari.maxVisibleRow.value

                    }
                }).then(function (response) {
                    $scope.cari.isLoading = false;
                    app.hide.spinner($('cari-list > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or user is passive
                        window.location.href = 'login.aspx';
                        return;
                    }
                    $scope.cari.list = response.data.LIST;
                    $scope.cari.isExist = $scope.cari.list.length > 0;
                    $scope.cari.totalRow.set(response.data.TOTALROW);
                }, function (response) {
                    $scope.cari.isLoading = false;
                    console.log(response);
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                    app.hide.spinner($('cari-list > div:first'));
                });
            },
            show: {
                update: function (_cari) {
                    $rootScope.showModifyModal(_cari);
                },
                delete: function (_cari) {
                    $rootScope.showDeleteModal(_cari);
                }
            },
            paginate: function () {
                var pageCount = $scope.cari.totalRow.value !== 0 ? Math.ceil($scope.cari.totalRow.value / $scope.cari.maxVisibleRow.value) : 1;
                $('.bootpag-flat').bootpag({
                    total: pageCount,
                    maxVisible: app.pagination.maxVisible.pageIndex,
                    page: $scope.cari.page.index,
                    leaps: false
                }).on("page", function (event, num) {
                    $scope.cari.page.index = num;
                    $scope.cari.load();
                }).children('.pagination').addClass('pagination-flat pagination-sm');
            }
        };
        $scope.cari.load();
    }])
    .directive('cariList', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/cari-list.html'
        };
    });
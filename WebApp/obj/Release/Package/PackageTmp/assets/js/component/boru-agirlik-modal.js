﻿app.angular
    .component('boruAgirlikModal', {
        templateUrl: 'assets/template/boru-agirlik-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', '$localStorage', function ($scope, $http, $timeout, $rootScope, $localStorage) {
     
            $rootScope.showBoruModal = function (data, olcubirimi, girilenfiyat) {
                $scope.teklif.get(data, olcubirimi, girilenfiyat);
                localStorage.removeItem('boruagirlik');
                localStorage.removeItem('milagirlik');
                localStorage.removeItem('flansagirlik');
                console.log(olcubirimi);
                if (olcubirimi.value === 'Metre') {// ölçü birimi metre ise Çap ve Et Kalınlığı kullanıcıya gösterilmemeli.Ağırlık = (Boy * Girilen fiyat)/ 1000 olduğu için.
                    console.log(olcubirimi.value);
                    $scope.teklif.showetkalinligi = false;
                    $scope.teklif.showcap = false;
                }
                else {
                    console.log(olcubirimi.value);
                    $scope.teklif.showetkalinligi = true;
                    $scope.teklif.showcap = true;
                }
                $('.boru-agirlik-modal:first').appendTo("body"); // modalı öne getirir.
                $scope.teklif.init();
                $('.boru-agirlik-modal:first').modal({
                    backdrop: 'static'
                });
            };
            $scope.teklif = {
                getdata: '',
                olcubirimi: '',
                boruacilimi: 0,
                carpim: 0,
                boruagirlik: '',
                girilenfiyat: 0,
                showetkalinligi: true,
                showcap: true,
                metresecildiyse: true, // ölçü birimi metre seçildiyse modal da sadece Boy inputu görünmeli.
                cap: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.cap.isValid = false;
                        $scope.teklif.cap.value = '';
                        $scope.teklif.cap.text = '';
                        $scope.teklif.cap.cls = '';
                        $scope.teklif.cap.msg = '';
                        $scope.teklif.cap.set('');
                    },
                    set: function (cap) {
                        $scope.teklif.cap.value = cap.value;
                        $scope.teklif.cap.text = cap.text;
                        $scope.teklif.cap.isValid = true;
                        $scope.teklif.cap.cls = '';
                        $scope.teklif.cap.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.cap.value);
                        if (applyCheck || $scope.teklif.cap.isValid !== check) {
                            $scope.teklif.cap.isValid = check;
                            if ($scope.teklif.cap.isValid) {
                                $scope.teklif.cap.success();
                            } else {
                                $scope.teklif.cap.error('* Geçersiz Çap !');
                            }
                        }
                        return $scope.teklif.cap.isValid;
                    },
                    change: function () {
                        $scope.teklif.cap.check($scope.teklif.cap.value);
                    },
                    success: function () {
                        $scope.teklif.cap.cls = '';
                        $scope.teklif.cap.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.cap.cls = 'has-error';
                        $scope.teklif.cap.msg = msg;
                    }
                },
                boy: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.boy.isValid = false;
                        $scope.teklif.boy.value = '';
                        $scope.teklif.boy.text = '';
                        $scope.teklif.boy.cls = '';
                        $scope.teklif.boy.msg = '';
                        $scope.teklif.boy.set('');
                    },
                    set: function (boy) {
                        $scope.teklif.boy.value = boy.value;
                        $scope.teklif.boy.text = boy.text;
                        $scope.teklif.boy.isValid = true;
                        $scope.teklif.boy.cls = '';
                        $scope.teklif.boy.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.boy.value);
                        if (applyCheck || $scope.teklif.boy.isValid !== check) {
                            $scope.teklif.boy.isValid = check;
                            if ($scope.teklif.boy.isValid) {
                                $scope.teklif.boy.success();
                            } else {
                                $scope.teklif.boy.error('* Geçersiz Boy !');
                            }
                        }
                        return $scope.teklif.boy.isValid;
                    },
                    change: function () {
                        $scope.teklif.boy.check($scope.teklif.boy.value);
                    },
                    success: function () {
                        $scope.teklif.boy.cls = '';
                        $scope.teklif.boy.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.boy.cls = 'has-error';
                        $scope.teklif.boy.msg = msg;
                    }
                },
                etkalinligi: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.etkalinligi.isValid = false;
                        $scope.teklif.etkalinligi.value = '';
                        $scope.teklif.etkalinligi.text = '';
                        $scope.teklif.etkalinligi.cls = '';
                        $scope.teklif.etkalinligi.msg = '';
                        $scope.teklif.etkalinligi.set('');
                    },
                    set: function (etkalinligi) {
                        $scope.teklif.etkalinligi.value = etkalinligi.value;
                        $scope.teklif.etkalinligi.text = etkalinligi.text;
                        $scope.teklif.etkalinligi.isValid = true;
                        $scope.teklif.etkalinligi.cls = '';
                        $scope.teklif.etkalinligi.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.etkalinligi.value);
                        if (applyCheck || $scope.teklif.etkalinligi.isValid !== check) {
                            $scope.teklif.etkalinligi.isValid = check;
                            if ($scope.teklif.etkalinligi.isValid) {
                                $scope.teklif.etkalinligi.success();
                            } else {
                                $scope.teklif.etkalinligi.error('* Geçersiz Et Kalınlığı !');
                            }
                        }
                        return $scope.teklif.etkalinligi.isValid;
                    },
                    change: function () {
                        $scope.teklif.etkalinligi.check($scope.teklif.etkalinligi.value);
                    },
                    success: function () {
                        $scope.teklif.etkalinligi.cls = '';
                        $scope.teklif.etkalinligi.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.etkalinligi.cls = 'has-error';
                        $scope.teklif.etkalinligi.msg = msg;
                    }
                },
                get: function (data, olcubirimi, girilenfiyat) {
                    $scope.teklif.getdata = data;
                    $scope.teklif.olcubirimi = olcubirimi.value;
                    $scope.teklif.girilenfiyat = girilenfiyat;
                },
                init: function () {
                    $scope.teklif.cap.init();
                    $scope.teklif.boy.init();
                    $scope.teklif.etkalinligi.init();
                },
                check: function () {
                    var applyCheck = true;
                    var OK = {
                        cap: $scope.teklif.cap.check(applyCheck),
                        boy: $scope.teklif.boy.check(applyCheck),
                        etkalinligi: $scope.teklif.etkalinligi.check(applyCheck),
                    };
                    return OK.cap && OK.boy && OK.etkalinligi;
                },
                borucheck: function () { // ölçü birimi metre seçiliyse sadece Boru check edilmeli.
                    var applyCheck = true;
                    var OK = {
                        boy: $scope.teklif.boy.check(applyCheck)
                    };
                    return OK.boy;
                },
                save: function () {
                    app.show.spinner($('boru-agirlik-modal > div:first > div:first'));
                    // Boru Ağırlık = (Boru Çapı - Et Kalınlığı) * (Et Kalınlığı * Boyu) / 40.45 / 1000
                    if ($scope.teklif.olcubirimi !== 'Metre') {
                        if (!$scope.teklif.check()) {
                            return;
                        }
                        $scope.teklif.boruagirlik = ($scope.teklif.cap.value - $scope.teklif.etkalinligi.value) * $scope.teklif.etkalinligi.value * $scope.teklif.boy.value / 40.45 / 1000;
                        if ($scope.teklif.boruagirlik < 0) {
                            $scope.teklif.boruagirlik = $scope.teklif.boruagirlik * -1;
                        }
                        var num = $scope.teklif.boruagirlik;
                        var with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0] // ondalıklı sayıyı yuvarlamadan eşitle.
                        $scope.teklif.boruagirlik = with2Decimals.replace(".", ",");
                        console.log($scope.teklif.boruagirlik); 

                            localStorage.setItem("boruagirlik", JSON.stringify($scope.teklif.boruagirlik));
                            console.log("$scope.teklif.boruagirlik --->", $scope.teklif.boruagirlik);
                            $('.boru-agirlik-modal:first').modal('hide');

                            $rootScope.control = true;
                            if ($scope.teklif.getdata === 'create-boru') { //create-modal dan gelen data
                                $rootScope.boruagirlik();
                            }
                            else {
                                $rootScope.boruagirlik2();
                            }
                            $scope.teklif.init();
                            return;
                    }
                    else { // ölçü birimi metre seçildiyse
                        if (!$scope.teklif.borucheck()) {
                            return;
                        }
                        $scope.teklif.boruagirlik = ($scope.teklif.boy.value * $scope.teklif.girilenfiyat) / 1000;
                        console.log($scope.teklif.boruagirlik);
                        var num = $scope.teklif.boruagirlik;
                        var with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0] // ondalıklı sayıyı yuvarlamadan eşitle.
                        $scope.teklif.boruagirlik = with2Decimals.replace(".", ",");
                        console.log($scope.teklif.boruagirlik);
                        localStorage.setItem("boruagirlik", JSON.stringify($scope.teklif.boruagirlik));
                        console.log("$scope.teklif.boruagirlik --->", $scope.teklif.boruagirlik);
                        $('.boru-agirlik-modal:first').modal('hide');

                        $rootScope.control = true;
                        if ($scope.teklif.getdata === 'create-boru') { //create-modal dan gelen data
                           $rootScope.boruagirlik();
                        }
                        else {
                           $rootScope.boruagirlik2();
                        }
                        $scope.teklif.init();
                        return;
                    }
                },

            };
        }]
    })

﻿app.angular
    .component('modifycomponentDeleteModal', {
        templateUrl: 'assets/template/modifycomponent-delete-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
        
            $rootScope.showDeleteModal = function (key, teklifid) {
                $scope.teklif.tekliflerid = teklifid;
                $scope.teklif.maliyetid = key;
                $('.modifycomponent-delete-modal:first').modal({
                    backdrop: 'static',
                    keyboard: true
                });
            };
            $scope.teklif = {
                tekliflerid: 0,
                maliyetid: 0,
                delete: function () {
                    app.show.spinner($('modifycomponent-delete-modal > div:first > div:first'));
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=11&d=' + new Date(), // DELETE MALIYET BY TEKLIFLERID
                        data: {
                            teklifid: $scope.teklif.tekliflerid,
                            teklifmaliyetid: $scope.teklif.maliyetid
                        }
                    }).then(function (response) {
                        app.hide.spinner($('modifycomponent-delete-modal > div:first > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 1) {
                            $('.modifycomponent-delete-modal:first').modal('hide');
                            app.show.success('Teklif-Maliyet Bilgileri Silindi !');
                           // localStorage.removeItem('teklifdata');
                            localStorage.setItem("tekliflist-modify", JSON.stringify(response.data)); //create-component
                            console.log(response.data);
                            Runopenmodify = $rootScope.runmodifydata;
                            Runopenmodify();
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                            window.location.href = 'login.aspx';
                            return;
                        }
                        //if running code come through this far then it means an unexpected error occurred
                        app.show.error('Bir şeyler ters gitti...');
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                        app.hide.spinner($('modifycomponent-delete-modal > div:first > div:first'));
                    });
                }
            }
        }]
    })
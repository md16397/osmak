﻿app.angular
    .controller('teklifCreateBtnCtrl', ['$scope', function ($scope) {
        window.addEventListener("keydown", function (event) {
            if (event.defaultPrevented) {
                console.log(0);
                return; 
            }
            var handled = false;
            if (event.key !== undefined) {
                if (event.keyCode == 45) {
                    window.location.pathname = 'teklifler-create.aspx';
                }
            } else if (event.keyCode !== undefined) {
                console.log(2);
            }
            if (handled) {
                console.log(3);
                event.preventDefault();
            }
        }, true);
        $scope.cls = 'fab-menu-bottom-right';
        $scope.show = {
            modal: function () {
                localStorage.removeItem('tekliftarihi');
                window.location.pathname = 'teklifler-create.aspx';
            }
        };
    }])
    .directive('teklifCreateBtn', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/teklif-create-btn.html'
        };
    });
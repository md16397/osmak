﻿var page = {
    data: {
        breadcrumbs: {
            home: {
                name: 'Anasayfa',
                href: '#'
            },
            links: [],
            active: {
                name: 'Profil'
            }
        },
        header: {
            name: 'Profil',
            detail: 'Profil Ayarları'
        }
    }
};
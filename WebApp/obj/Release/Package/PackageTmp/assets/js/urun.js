﻿var page = {
    data: {
        breadcrumbs: {
            home: {
                name: 'Anasayfa',
                href: '#'
            },
            links: [],
            active: {
                name: 'Ham Madde/Yarı Mamul İşlemleri'
            }
        },
        header: {
            name: 'Ham Madde/Yarı Mamul',
            detail: 'Ham Madde/Yarı Mamul İşlemleri'
        }
    }
};
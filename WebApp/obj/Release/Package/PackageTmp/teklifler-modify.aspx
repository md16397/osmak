﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Osmak.Master" AutoEventWireup="true" CodeBehind="teklifler-modify.aspx.cs" Inherits="WebApp.teklifler_modify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="assets/js/teklifler-modify.js"></script>
    <script type="text/javascript" src="assets/js/component/breadcrumb.js"></script>
    <script type="text/javascript" src="assets/js/component/page-header.js"></script>
    <script type="text/javascript" src="assets/js/component/cari-dropdown.js"></script>
    <script type="text/javascript" src="assets/js/component/hammadde-dropdown.js"></script>
    <script type="text/javascript" src="assets/js/component/teklifurunadi-dropdown.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-modify-component.js"></script>
    <script type="text/javascript" src="assets/js/component/page-footer.js"></script>

    <script type="text/javascript" src="assets/js/component/modifycomponent-boru-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/modifycomponent-mil-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/modifycomponent-flans-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/modifycomponent-kovan-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/modifycomponent-iptalet-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-list.js"></script>

    <script type="text/javascript" src="assets/js/component/modifycomponent-create-btn.js"></script>
    <script type="text/javascript" src="assets/js/component/teklifmodifycomponent-hammaddeyarimamulcreate-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/modifycomponent-create-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/modifycomponent-modify-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/modifycomponent-delete-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-excel-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/modifyaspxsabit6hamyarimamulgrubu-uyari-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/tamburmodifyaspx-uyari-modal.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <page-header></page-header>
    <!-- Content -->
    <div class="content">
    <teklif-modify-component ng-controller="teklifmodifyComponentCtrl"></teklif-modify-component>
    <modifycomponent-create-btn ng-controller="modifycomponentCreateBtnCtrl"></modifycomponent-create-btn>
    <page-footer></page-footer>

    <teklif-list ng-hide="!teklifcreate2" ng-controller="teklifListCtrl"></teklif-list>

    <modifycomponent-boru-modal></modifycomponent-boru-modal>
    <modifycomponent-mil-modal></modifycomponent-mil-modal>
    <modifycomponent-flans-modal></modifycomponent-flans-modal>
    <modifycomponent-kovan-modal></modifycomponent-kovan-modal>
    <modifycomponent-iptalet-modal></modifycomponent-iptalet-modal>
    <modifycomponent-agirlik-modal></modifycomponent-agirlik-modal>
    <modifycomponent-create-modal></modifycomponent-create-modal>
    <modifycomponent-modify-modal></modifycomponent-modify-modal>
    <modifycomponent-delete-modal></modifycomponent-delete-modal>
    <teklif-excel-modal></teklif-excel-modal>
    <modifyaspxsabit6hamyarimamulgrubu-uyari-modal></modifyaspxsabit6hamyarimamulgrubu-uyari-modal>
    <tamburmodifyaspx-uyari-modal></tamburmodifyaspx-uyari-modal>
    <teklifmodifycomponent-hammaddeyarimamulcreate-modal></teklifmodifycomponent-hammaddeyarimamulcreate-modal>
	</div>
    <!-- /content -->
</asp:Content>

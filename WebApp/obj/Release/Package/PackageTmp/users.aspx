﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Osmak.Master" AutoEventWireup="true" CodeBehind="users.aspx.cs" Inherits="WebApp.users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script type="text/javascript" src="assets/js/locales/users.<%=((WebApp.Osmak)Master).user.Language.ToString() %>.js?v=<%=UsersJSVersion.Version %>"></script>
    <script type="text/javascript" src="assets/js/users.js"></script>
    <!-- Angular Components -->
    <script type="text/javascript" src="assets/js/component/breadcrumb.js"></script>
    <script type="text/javascript" src="assets/js/component/page-header.js"></script>
    <script type="text/javascript" src="assets/js/component/page-footer.js"></script>

    <script type="text/javascript" src="assets/js/component/user-list.js"></script>
    <script type="text/javascript" src="assets/js/component/user-create-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/user-create-btn.js"></script>
    <script type="text/javascript" src="assets/js/component/user-modify-modal.js?v=0.0.1"></script>
    <!-- /angular Components -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <page-header></page-header>
    <!-- Content -->
    <div class="content">
        <user-create-btn ng-controller="userCreateBtnCtrl"></user-create-btn>
        <user-list ng-controller="userListCtrl"></user-list>
		<page-footer></page-footer>
        <user-create-modal ng-controller="userCreateModalCtrl"></user-create-modal>
        <user-modify-modal ng-controller="userModifyModalCtrl"></user-modify-modal>
	</div>
    <!-- /content -->
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="WebApp.login" %>

<!DOCTYPE html>

<html lang="" dir="auto">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Kullanıcı Girişi</title>

    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
  <%--  <!-- Icon -->
	<link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="assets/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="assets/icon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    <!-- /icon -->--%>
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <%--<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">--%>
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
	<link href="assets/css/core.css" rel="stylesheet" />
	<link href="assets/css/components.css" rel="stylesheet" />
	<link href="assets/css/colors.css" rel="stylesheet" />
	<!-- /global stylesheets -->

    <!-- Osmak stylesheets -->
    <link href="assets/css/login.css" rel="stylesheet" />
    <!-- /Osmak stylesheets -->

	<!-- Core JS files -->
    <script src="assets/js/plugins/loaders/pace.min.js"></script>
    <script src="assets/js/core/libraries/jquery.min.js"></script>
	<script src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->
	<!-- Theme JS files -->
    <script type="text/javascript" src="assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/cookie/cookie.min.js"></script>
	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<!-- /theme JS files -->
    <!-- Osmak JS files -->
    <script type="text/javascript" src="assets/js/login.js?v=0.0.1"></script>
    <!-- /Osmak JS files -->
 
</head>
<body class="login-container">
   
    <!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<%--<a class="navbar-brand logo" href="#" style="background-color:#FFF400;"><img class="sys175 logo img-rounded" src="assets/images/logo.png" alt=""></a>--%>
			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Content area -->
				<div class="content">
					<!-- Simple login form -->
					<form id="login-form">
						<div class="panel panel-body login-form">
                            <div class="logo_image">
                                <img src="assets/images/logo_entry.png" style="width:280px" />
                            </div>
							<div class="text-center">
								<h5 class="content-group"><span class="sys176">Hesabınıza giriş yapın</span> <small class="display-block sys177">Kullanıcı bilgilerinizi girin</small></h5>
							</div>

							<div class="form-group has-feedback <%=FeedbackHtmlCls %>">
								<input name="username" type="text" class="form-control sys178" placeholder="Kullanıcı Adı" autocomplete="off"><!--Kullanıcı Adı-->
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
                                <span class="help-block"></span>
							</div>

							<div class="form-group has-feedback <%=FeedbackHtmlCls %>">
								<input name="password" type="password" class="form-control sys179" placeholder="Şifre"><!--Şifre-->
								<div class="form-control-feedback">
									<i class="icon-key text-muted"></i>
								</div>
                                <span class="help-block"></span>
							</div>

                            <div class="form-group text-center">
                                <img class="img-captcha" src="hub/user.ashx?t=8" />
                                <button class="btn btn-icon btn-default" type="button" onclick="login.captcha.reload()"><i class="icon-reset"></i></button>
                            </div>

                            <div class="form-group has-feedback <%=FeedbackHtmlCls %>">
								<input name="captcha" type="text" class="form-control sys179" placeholder="Güvenlik Kodu" autocomplete="off"><!--Güvenlik Kodu-->
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
                                <span class="help-block"></span>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block"><span class="sys180">Giriş</span> <i class="<%=IconHtmlCls %>"></i></button>
							</div>
                            
						</div>
					</form>
					<!-- /simple login form -->


					<!-- Footer -->
					<div class="footer text-muted text-center">
						&copy; 2021. Osmak Maliyet Hazırlama Sistemi
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

</body>
</html>

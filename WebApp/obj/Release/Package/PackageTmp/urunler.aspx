﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Osmak.Master" AutoEventWireup="true" CodeBehind="urunler.aspx.cs" Inherits="WebApp.urunler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

  <script type="text/javascript" src="assets/js/urun.js?v=0.0.1"></script>
    <!-- Angular Components -->
    <script type="text/javascript" src="assets/js/component/breadcrumb.js"></script>
    <script type="text/javascript" src="assets/js/component/page-header.js"></script>
    <script type="text/javascript" src="assets/js/component/page-footer.js"></script>
    <script type="text/javascript" src="assets/js/component/urun-list.js"></script>
    <script type="text/javascript" src="assets/js/component/urun-create-modal.js?v=0.0.1"></script>    
    <script type="text/javascript" src="assets/js/component/urun-create-btn.js?v=0.0.1"></script>
    <script type="text/javascript" src="assets/js/component/urun-modify-modal.js?v=0.0.1"></script>
    <script type="text/javascript" src="assets/js/component/urun-delete-modal.js?v=0.0.1"></script>
    <!-- /angular Components -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <page-header></page-header>
    <!-- Content -->
    <div class="content">
    <urun-create-btn ng-controller="urunCreateBtnCtrl"></urun-create-btn>
    <urun-list ng-controller="urunListCtrl"></urun-list>
    <page-footer></page-footer>
        
    <urun-create-modal></urun-create-modal>
    <urun-modify-modal></urun-modify-modal>
    <urun-delete-modal></urun-delete-modal>	
	</div>
    <!-- /content -->

</asp:Content>

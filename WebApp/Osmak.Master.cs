﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.controls;
using WebApp.models;

namespace WebApp
{
    public partial class Osmak : System.Web.UI.MasterPage
    {
        public User user { get; private set; }
        public WebApp.models.Menu menu { get; private set; }
        public Dictionary<int, string> SystemMessages { get; private set; }
        public JSVersion MasterJSVersion { get { return controls.SystemCredentials.Versions.Master[user.Language]; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new User(Context);
            if (!user.IsLoggedIn)
            {
                Response.Redirect("login.aspx", true);
                return;
            }
            SystemMessages = controls.SystemCredentials.GetPage(user, Request.Url.AbsolutePath);
            menu = new models.Menu(SystemMessages);
            menu.SetVisibilities(user);
            MenuLink selectedPage = menu.Links.Select(Request.Url.AbsolutePath);
            if (!selectedPage.Visible)
                Response.Redirect(user.StartPage, true);
        }
    }
}
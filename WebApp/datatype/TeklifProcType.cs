﻿namespace WebApp.datatype
{
    public enum TeklifProcType
    {
        GetById = 1,
        GetAll = 2,
        Create = 3,
        Modify = 4,
        Delete = 5,
        GetByAll = 6,
        GetDataforExcel = 7,
        ModifyTeklif = 8,
        GetAllByTeklifMaliyetId = 9,
        TeklifState = 10,
        DeleteMaliyetKalemleri = 11,
        SaveAllByTekliflerId = 12, // TEKLIFI KAYDET VE KAPAT
        Dovizkuru = 13, 
        TeklifUrunAdi = 14,
        GetAllTeklifiOlusturanKullanicilar = 15, // TEKLIFI OLUŞTURAN KULLANICILAR DROPDOWN İÇERİSİNDE LİSTELENMELİDİR. (TEKLIF LISTELEME EKRANI)
        GetAllTekliflerByFilter = 16
    }
}
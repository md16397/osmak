﻿
namespace WebApp.datatype
{
    public enum IpProcType
    {
        GetAll = 2,
        Create = 3,
        Modify = 4,
        Delete = 5
    }
}
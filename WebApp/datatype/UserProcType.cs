﻿
namespace WebApp.datatype
{
    public enum UserProcType
    {
        Login = 0,
        GetById = 1,
        GetAll = 2,
        Create = 3,
        Modify = 4,
        Logout = 5,
        ProfileModify = 6,
        Captcha = 8
    }
}
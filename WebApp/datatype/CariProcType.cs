﻿namespace WebApp.datatype
{
    public enum CariProcType
    {
        GetById = 1,
        GetAll = 2,
        Create = 3,
        Modify = 4,
        Delete = 5
    }
}
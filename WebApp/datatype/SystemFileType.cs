﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.datatype
{
    public enum SystemFileType
    {
        import,
        export,
        checkin,
        checkout,
        passlist,
        product,
        permissions,
        document,
        firm,
        stock,
        currencylist,
        users,
        nationality,
        language,
        ip,
        unit,
        profile,
        login,
        master,
        config
    }
}
﻿namespace WebApp.datatype
{
    public enum LangProcType
    {
        GetAll = 2,
        Create = 3,
        Modify = 4,
        GetByPageURL = 6,
        GetPageJSData = 7
    }
}
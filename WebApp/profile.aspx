﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Osmak.Master" AutoEventWireup="true" CodeBehind="profile.aspx.cs" Inherits="WebApp.profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Theme JS files -->
    <script type="text/javascript" src="assets/js/plugins/cookie/cookie.min.js"></script>
    <!-- /theme JS files -->

    <script type="text/javascript" src="assets/js/locales/profile.js?v=<%=ProfileJSVersion.Version %>"></script>
    <script type="text/javascript" src="assets/js/profile.js"></script>

    <!-- Angular Components -->
    <script type="text/javascript" src="assets/js/component/breadcrumb.js"></script>
    <script type="text/javascript" src="assets/js/component/page-header.js"></script>
    <script type="text/javascript" src="assets/js/component/profile-component.js"></script>
    <script type="text/javascript" src="assets/js/component/page-footer.js"></script>
    <script type="text/javascript" src="assets/js/component/profile-passwordmodify-modal.js"></script>
    <!-- /angular Components -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <page-header></page-header>
    <div class="content">
    <profile-component ng-controller="profileComponentCtrl"></profile-component>
    <page-footer></page-footer>
    <profile-passwordmodify-modal ng-controller="profilePasswordModalCtrl"></profile-passwordmodify-modal>
    
    </div>
</asp:Content>

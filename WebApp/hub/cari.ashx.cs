﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.controls;
using WebApp.models;
using WebApp.datatype;

namespace WebApp.hub
{
    /// <summary>
    /// Summary description for cari
    /// </summary>
    public class cari : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            int type = -1;
            int.TryParse(Converter.QueryStringToTurkishQueryString("t"), out type);
            string result;
            Cari cari;
            User user = new User(context);
            if (!Valid.User(user, out result))
            {
                response(context, result);
                return;
            }
            switch (type)
            {
                case (int)CariProcType.GetById:
                    cari = new Cari(user);
                    result = cari.GetById();
                    break;
                case (int)CariProcType.GetAll:
                    cari = new Cari(user);
                    result = cari.GetAll();
                    break;
                case (int)CariProcType.Create:
                    cari = new Cari(user);
                    result = cari.Create();
                    break;
                case (int)CariProcType.Modify:
                    cari = new Cari(user);
                    result = cari.Modify();
                    break;
                case (int)CariProcType.Delete:
                    cari = new Cari(user);
                    result = cari.Delete();
                    break;
                default:
                    result = string.Empty;
                    break;
            }
            response(context, result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private void response(HttpContext context, string result)
        {
            context.Response.Clear();
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(result);
            context.Response.End();
        }
    }
}
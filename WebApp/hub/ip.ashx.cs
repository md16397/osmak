﻿using System;
using System.Net;
using System.Web;
using WebApp.controls;
using WebApp.datatype;
using WebApp.models;

namespace WebApp.hub
{
    /// <summary>
    /// ip için özet açıklama
    /// </summary>
    public class ip : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            int type = -1;
            int.TryParse(Converter.QueryStringToTurkishQueryString("t"), out type);
            string result;
            Ip ip;
            User user = new User(context);
            if (!Valid.User(user, out result))
            {
                response(context, result);
                return;
            }
            switch (type)
            {
                case (int)IpProcType.GetAll:
                    ip = new Ip(user);
                    result = ip.GetAll();
                    break;
                case (int)IpProcType.Create:
                    ip = new Ip(user);
                    result = ip.Create();
                    break;
                case (int)IpProcType.Modify:
                    ip = new Ip(user);
                    result = ip.Modify();
                    break;
                case (int)IpProcType.Delete:
                    ip = new Ip(user);
                    result = ip.Delete();
                    break;
                default:
                    result = string.Empty;
                    break;
            }
            response(context, result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private void response(HttpContext context, string result)
        {
            context.Response.Clear();
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(result);
            context.Response.End();
        }
    }
}
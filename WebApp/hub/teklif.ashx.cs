﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.controls;
using WebApp.models;
using WebApp.datatype;
namespace WebApp.hub
{
    /// <summary>
    /// Summary description for teklif
    /// </summary>
    public class teklif : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            int type = -1;
            int.TryParse(Converter.QueryStringToTurkishQueryString("t"), out type);
            string result;
            Teklif teklif;
            User user = new User(context);
            if (!Valid.User(user, out result))
            {
                response(context, result);
                return;
            }
            switch (type)
            {
                case (int)TeklifProcType.GetById:
                    teklif = new Teklif(user);
                    result = teklif.GetById();
                    break;
                case (int)TeklifProcType.GetAll:
                    teklif = new Teklif(user);
                    result = teklif.GetAll();
                    break;
                case (int)TeklifProcType.Create:
                    teklif = new Teklif(user);
                    result = teklif.Create();
                    break;
                case (int)TeklifProcType.Modify: // 4
                    teklif = new Teklif(user);
                    result = teklif.Modify(); // 
                    break;
                case (int)TeklifProcType.Delete:
                    teklif = new Teklif(user);
                    result = teklif.Delete();
                    break;
                case (int)TeklifProcType.GetByAll:
                    teklif = new Teklif(user);
                    result = teklif.GetByAll();
                    break;
                case (int)TeklifProcType.GetDataforExcel: // TYPE 7
                    teklif = new Teklif(user);
                    result = teklif.GetDataforExcel();
                    break;
                case (int)TeklifProcType.ModifyTeklif: // 8
                    teklif = new Teklif(user);
                    result = teklif.ModifyTeklif();
                    break;
                case (int)TeklifProcType.GetAllByTeklifMaliyetId:
                    teklif = new Teklif(user);
                    result = teklif.GetAllByTeklifMaliyetId(); // create component içerisinde maliyet modal
                    break;
                case (int)TeklifProcType.TeklifState:
                    teklif = new Teklif(user);
                    result = teklif.TeklifOnayDurumu(); 
                    break; 
                case (int)TeklifProcType.DeleteMaliyetKalemleri:
                    teklif = new Teklif(user);
                    result = teklif.DeleteMaliyetKalemleri();
                    break; 
                case (int)TeklifProcType.SaveAllByTekliflerId:
                    teklif = new Teklif(user);
                    result = teklif.SaveAllByTekliflerId();
                    break; 
                case (int)TeklifProcType.Dovizkuru:
                   teklif = new Teklif(user);
                    result = teklif.DovizKuru();
                    break; 
                case (int)TeklifProcType.TeklifUrunAdi:
                    teklif = new Teklif(user);
                    result = teklif.TeklifUrunAdiGetAll(); // [TEKLIFURUNADI_PROC] type = 14 olmalı. (DB'de TEKLIFURUNADI_PROC)
                    break;
                case (int)TeklifProcType.GetAllTekliflerByFilter:
                    teklif = new Teklif(user);
                    result = teklif.GetAllTekliflerByFilter(); // TEKLIF LISTE EKRANINDA RAPOR ALMA
                    break;
                default:
                    result = string.Empty;
                    break;
            }
            response(context, result);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private void response(HttpContext context, string result)
        {
            context.Response.Clear();
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(result);
            context.Response.End();
        }
    }
}
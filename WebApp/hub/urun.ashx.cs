﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.datatype;
using WebApp.controls;
using WebApp.models;

namespace WebApp.hub
{
    /// <summary>
    /// Summary description for urun
    /// </summary>
    public class urun : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            int type = -1;
            int.TryParse(Converter.QueryStringToTurkishQueryString("t"), out type);
            string result;
            Urun urun;
            User user = new User(context);
            if (!Valid.User(user, out result))
            {
                response(context, result);
                return;
            }
            switch (type)
            {
                case (int)UrunProcType.GetById:
                    urun = new Urun(user);
                    result = urun.GetById();
                    break;
                case (int)UrunProcType.GetAll:
                    urun = new Urun(user);
                    result = urun.GetAll();
                    break;
                case (int)UrunProcType.Create:
                    urun = new Urun(user);
                    result = urun.Create();
                    break;
                case (int)UrunProcType.Modify:
                    urun = new Urun(user);
                    result = urun.Modify();
                    break;
                case (int)UrunProcType.Delete:
                    urun = new Urun(user);
                    result = urun.Delete();
                    break;
                default:
                    result = string.Empty;
                    break;
            }
            response(context, result);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private void response(HttpContext context, string result)
        {
            context.Response.Clear();
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(result);
            context.Response.End();
        }
    }
}
﻿using System;
using System.Net;
using System.Web;
using WebApp.controls;
using WebApp.datatype;
using WebApp.models;

namespace WebApp.hub
{
    /// <summary>
    /// Summary description for user
    /// </summary>
    public class user : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            int type = -1;
            int.TryParse(Converter.QueryStringToTurkishQueryString("t"), out type);
            string result;
            User user = new User(context);
            switch (type)
            {
                case (int)UserProcType.Captcha:
                    System.Drawing.Bitmap captcha = user.Captcha();
                    captcha.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
                    captcha.Dispose();
                    return;
                case (int)UserProcType.Login:
                    Tuple<string, bool> ipData = GetClientIP(context.Request);
                    result = user.Login(context.Request.Form["username"], context.Request.Form["password"], context.Request.Form["captcha"], ipData.Item1, ipData.Item2);
                    break;
                case (int)UserProcType.GetById:
                    result = user.RawData == null || user.RawData.Rows.Count <= 0 ? "{}" : Converter.DataRowToJSON(user.RawData.Rows[0]);
                    break;
                case (int)UserProcType.ProfileModify:
                    result = user.ProfileModify();
                    break;
                case (int)UserProcType.GetAll:
                    result = user.GetAll();
                    break;
                case (int)UserProcType.Create:
                    result = user.Create();
                    break;
                case (int)UserProcType.Modify:
                    result = user.Modify();
                    break;
                case (int)UserProcType.Logout:
                    result = user.Logout();
                    break;
                default: throw new ArgumentException();
            }
            context.Response.Clear();
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.Write(result);
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private Tuple<string, bool> GetClientIP(HttpRequest request)
        {
            string ip = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            bool isLocal = false;
            if (Valid.IP(ip))//if (!string.IsNullOrEmpty(ip) && ip.Trim() != "::1")
                return Tuple.Create(ip, isLocal);
            ip = request.ServerVariables["REMOTE_ADDR"];
            if (Valid.IP(ip))
                return Tuple.Create(ip, isLocal);
            ip = request.UserHostAddress;
            if (Valid.IP(ip))
                return Tuple.Create(ip, isLocal);

            isLocal = true;
            string hostName = Dns.GetHostName();//Local(LAN) Connected ID Address
            IPHostEntry ipHostEntries = Dns.GetHostEntry(hostName);
            IPAddress[] IpArray = ipHostEntries.AddressList;
            int i = 0;
            bool goon = true;
            while (i < IpArray.Length && goon)
            {
                if (Valid.IP(IpArray[i].ToString()))
                {
                    goon = false;
                    ip = IpArray[i].ToString();
                }
                i++;
            }
            if (goon)
                try
                {
                    IpArray = Dns.GetHostAddresses(hostName);
                    ip = IpArray[0].ToString();
                }
                catch { ip = "127.0.0.1"; }
            return Tuple.Create(ip, isLocal);
        }
    }
}
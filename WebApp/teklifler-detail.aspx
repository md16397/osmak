﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Osmak.Master" AutoEventWireup="true" CodeBehind="teklifler-detail.aspx.cs" Inherits="WebApp.teklifler_detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="assets/js/teklifler-detail.js"></script>
    <script type="text/javascript" src="assets/js/component/breadcrumb.js"></script>
    <script type="text/javascript" src="assets/js/component/page-header.js"></script>
    <script type="text/javascript" src="assets/js/component/cari-dropdown.js"></script>
    <script type="text/javascript" src="assets/js/component/teklif-detail-component.js"></script>
    <script type="text/javascript" src="assets/js/component/page-footer.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <page-header></page-header>

    <div class="content">
    <teklif-detail-component ng-controller="teklifdetailComponentCtrl"></teklif-detail-component>
    <page-footer></page-footer>
	</div>
</asp:Content>

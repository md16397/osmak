﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.controls;
using WebApp.datatype;
using WebApp.models;

namespace WebApp
{
    public partial class login : System.Web.UI.Page
    {
        private LangType langType;
        public string Language { get { return langType.ToString(); } }
        public string FlagPath { get; private set; }
        public string LanguageText { get; private set; }
        public string FeedbackHtmlCls { get; private set; }
        public string IconHtmlCls { get; private set; }
        public string PullRightHtmlCls { get; private set; }
        public string TextRightHtmlCls { get; private set; }
        public JSVersion LoginJSVersion { get { return controls.SystemCredentials.Versions.Login; } }
        public Dictionary<int, string> SystemMessages { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            User user = new User(HttpContext.Current);
            if (user.IsLoggedIn) // aktif yada  mevcut ise
            {
                Response.Redirect(user.StartPage);
                return;
            }
            Cookie cookie = new Cookie(Context);
            DateTime expirationTime = DateTime.Now.AddMonths(1);
            if (!cookie.IsExist("lang"))
            {
                langType = LangType.tr;
                cookie.AddOrUpdate("lang", Language, expirationTime);
                FeedbackHtmlCls = "has-feedback-left";
                IconHtmlCls = "icon-circle-right2 position-right";
                FlagPath = "assets/images/flags/tr.png";
                LanguageText = "Türkçe";
            }
            else if (LangType.tr.ToString() == cookie.Value("lang"))
            {
                langType = LangType.tr;
                cookie.AddOrUpdate("lang", Language, expirationTime);
                FeedbackHtmlCls = "has-feedback-left";
                IconHtmlCls = "icon-circle-right2 position-right";
                FlagPath = "assets/images/flags/tr.png";
                LanguageText = "Türkçe";
            }
            else
                throw new ArgumentException();
            this.SystemMessages = controls.SystemCredentials.GetPage(user, Request.Url.AbsolutePath, langType);
        }
    }
}
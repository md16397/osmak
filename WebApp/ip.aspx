﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Osmak.Master" AutoEventWireup="true" CodeBehind="ip.aspx.cs" Inherits="WebApp.ip" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Theme JS files -->
    <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
    <!-- /theme JS files -->
    <script type="text/javascript" src="assets/js/config.js?v=<%=ConfigJSVersion.Version %>"></script>
    <script type="text/javascript" src="assets/js/locales/ip.<%=((WebApp.Osmak)Master).user.Language.ToString() %>.js?v=<%=IPJSVersion.Version %>"></script>
    <script type="text/javascript" src="assets/js/ip.js"></script>
    <!-- Angular Components -->
    <script type="text/javascript" src="assets/js/component/breadcrumb.js"></script>
    <script type="text/javascript" src="assets/js/component/page-header.js"></script>
    <script type="text/javascript" src="assets/js/component/page-footer.js"></script>
    <script type="text/javascript" src="assets/js/component/ip-list.js"></script>
    <script type="text/javascript" src="assets/js/component/ip-create-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/ip-create-btn.js"></script>
    <script type="text/javascript" src="assets/js/component/ip-modify-modal.js"></script>
    <script type="text/javascript" src="assets/js/component/ip-delete-modal.js"></script>
    <!-- /angular Components -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <page-header></page-header>
    <div class="content">
        <ip-create-btn ng-controller="ipCreateBtnCtrl"></ip-create-btn>
        <ip-list></ip-list>
	    <page-footer></page-footer>
        <ip-create-modal ng-controller="ipCreateModalCtrl"></ip-create-modal>
        <ip-modify-modal ng-controller="ipModifyModalCtrl"></ip-modify-modal>
        <ip-delete-modal ng-controller="ipDeleteModalCtrl"></ip-delete-modal>
    </div>
</asp:Content>

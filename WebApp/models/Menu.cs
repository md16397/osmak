﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.models
{
    public class Menu
    {
        public MenuHeaders Headers { get; private set; }
        public MenuLinks Links { get; private set; }

        public Menu(Dictionary<int, string> systemMessages)
        {
            Headers = new MenuHeaders(systemMessages);
            Links = new MenuLinks(systemMessages);
        }
        public void SetVisibilities(User user)
        {
            Headers.SetVisibilities(user.StartPage);
            Links.SetVisibilities(user.StartPage, user.Type);
        }
    }
}
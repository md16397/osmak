﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web;
using WebApp.controls;
using WebApp.controls.database;
using WebApp.datatype;

namespace WebApp.models
{
    public class User
    {
        #region Properties
        public bool IsLoggedIn { get { return this.IsExist && this.IsActive; } }
        public int ID { get; private set; }
        public bool IsExist { get; private set; }
        public bool IsActive { get; private set; }
        public string Username { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public PasswordData Password { get; private set; }
        public LangType Language { get; private set; }
        public UserType Type { get; private set; }
        public IPData IP { get; private set; }
        public string StartPage { get; private set; }
        public DateTime LoginDate { get; private set; }
        public DataTable RawData { get; private set; }
        public HttpContext Context { get; private set; }

        private Cookie cookie;
        #endregion Properties

        #region Constructors
        public User(HttpContext context)
        {
            this.Context = context;
            cookie = new Cookie(context);
            if (!cookie.IsExist("token"))
            {
                this.IsExist = false;
                this.ID = 0;
                return;
            }
            string token = Crypto.Decrypt(cookie.Value("token"));
            Dictionary<string, object> tokenData = Converter.JSONToDictionary(token);
            this.Get(Convert.ToInt32(tokenData["ID"]));
            if (!this.IP.AllowAll && !this.IP.IsActive)
            {
                this.Logout();
                return;
            }

            //if a user simultaneously logs in with different IPs then older login attempt should end
            if ((this.IP.IsLocal != Convert.ToBoolean(tokenData["IPLOCAL"]) || this.IP.Address != tokenData["IP"].ToString()) && this.LoginDate > Convert.ToDateTime(tokenData["LOGINDATE"]))
                this.Logout();
        }
        #endregion Constructors

        #region Methods
        public string Login(string username, string password, string captcha, string ip, bool ipIsLocal)
        {
            if (!Valid.Password(password))
                return "{\"RESULT\":2}";
            if (!Valid.Username(username))
                return "{\"RESULT\":0}";
            if (!Valid.Captcha(this, captcha))
                return "{\"RESULT\":75}";
            LangType language;
            if (!Valid.Language(cookie.Value("lang"), out language))
                return "{\"RESULT\":-1}";
            PasswordData passwordData = new PasswordData
            {
                Encrypted = Crypto.Encrypt(password),
                Decrypted = password
            };
            DataTable dt = Database.FillTable(new UserProc(UserProcType.Login, username, passwordData.Encrypted, language, ip, ipIsLocal));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            int type = (int)dt.Rows[0]["RESULT"];
            string result = Converter.DataRowToJSON(dt.Rows[0]);
            if (type == 1 || type == 3 || type == 7)//success | local ip | new ip
            {
                string token = Crypto.Encrypt(result);
                cookie.AddOrUpdate("token", token);
            }
            return result;
        }
        public string Logout()
        {
            cookie.Remove("token");
            this.IsExist = false;
            return "{\"RESULT\":1}";
        }
        public User Get(int id)  // teklifler.aspx yüklenirken buraya giriyor
        {
            this.RawData = this.GetRawData(id);
            if (Convert.ToInt16(this.RawData.Rows[0]["RESULT"]) != 1)//not success
            {
                this.IsExist = false;
                return this;
            }
            this.ID = id;
            this.IsExist = true;
            this.IsActive = Convert.ToBoolean(this.RawData.Rows[0]["ACTIVEVAL"]);
            this.Username = this.RawData.Rows[0]["USERNAME"].ToString();
            this.Name = this.RawData.Rows[0]["NAME"].ToString();
            this.Surname = this.RawData.Rows[0]["SURNAME"].ToString();
            this.Password = new PasswordData
            {
                Encrypted = this.RawData.Rows[0]["PASSWORD"].ToString(),
                Decrypted = Crypto.Decrypt(this.RawData.Rows[0]["PASSWORD"].ToString())
            };
            this.Language = LangType.tr;
            switch (Convert.ToInt16(this.RawData.Rows[0]["TYPEVAL"]))
            {
                case (int)UserType.Admin:
                    this.Type = UserType.Admin;
                    break;
                case (int)UserType.User:
                    this.Type = UserType.User;
                    break;
            }
            this.IP = new IPData
            {
                IsActive = Convert.ToBoolean(this.RawData.Rows[0]["IPACTIVE"]),
                IsLocal = Convert.ToBoolean(this.RawData.Rows[0]["IPLOCAL"]),
                Address = this.RawData.Rows[0]["IP"].ToString(),
                AllowAll = Convert.ToBoolean(this.RawData.Rows[0]["IPALLOWALL"])
            };
            this.StartPage = this.RawData.Rows[0]["STARTPAGEVAL"].ToString();
            this.LoginDate = Convert.ToDateTime(this.RawData.Rows[0]["LOGINDATEVAL"]);
            if (RawData.Columns.Contains("PASSWORD"))
                foreach (DataRow user in RawData.Rows)
                    user["PASSWORD"] = Crypto.Decrypt(user["PASSWORD"].ToString());
            return this;
        }
        public string GetAll()
        {
            string result, filter;
            int pageIndex, pageRowCount;
            Dictionary<string, object> data = Converter.JSONToDictionary(Context.Request.InputStream);
            if (!Valid.UserGetAll(this, data, out result, out filter, out pageIndex, out pageRowCount))
                return result;
            DataSet ds = Database.FillSet(new UserProc(UserProcType.GetAll, this.ID, filter, pageIndex, pageRowCount));
            if (ds.Tables.Count == 2)
            {
                if (ds.Tables[1].Columns.Contains("PASSWORD"))
                    foreach (DataRow user in ds.Tables[1].Rows)
                        user["PASSWORD"] = Crypto.Decrypt(user["PASSWORD"].ToString());
                result = "{\"RESULT\":1,\"TOTALROW\":" + ds.Tables[0].Rows[0]["TOTALROW"].ToString() + ",\"PAGEINDEX\":" + ds.Tables[0].Rows[0]["PAGEINDEX"].ToString() + ",";
                ds.Tables[1].TableName = "LIST";
                result += Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
                return result;
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }
        public string Create()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(Context.Request.InputStream);
            string result, username, name, surname, password;
            UserType type;
            LangType language;
            bool state;
            if (!Valid.UserCreate(this, data, out result, out username, out type, out name, out surname, out password, out language, out state))
                return result;
            password = Crypto.Encrypt(password);
            DataTable dt = Database.FillTable(new UserProc(UserProcType.Create, this.ID, state, username, name, surname, password, language, type));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            if (dt.Columns.Contains("PASSWORD"))
                foreach (DataRow user in dt.Rows)
                    user["PASSWORD"] = Crypto.Decrypt(user["PASSWORD"].ToString());
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string Modify()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(Context.Request.InputStream);
            string result, username, name, surname, startPage, password;
            int id, failLoginCount;
            UserType type;
            LangType language;
            bool state;
            int subeid;
            if (!Valid.UserModify(this, data, out result, out id, out failLoginCount, out username, out type, out name, out surname, out password, out language, out state))
                return result;
            password = Crypto.Encrypt(password);
            DataTable dt = Database.FillTable(new UserProc(UserProcType.Modify, this.ID, id, state, failLoginCount, username, name, surname, password, language, type));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            if (dt.Columns.Contains("PASSWORD"))
                foreach (DataRow user in dt.Rows)
                    user["PASSWORD"] = Crypto.Decrypt(user["PASSWORD"].ToString());
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string ProfileModify()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(Context.Request.InputStream);
            string result, username, name, surname, password;
            int id;
            LangType language;
            if (!Valid.UserProfileModify(this, data, out result, out id, out username, out name, out surname, out password, out language))
                return result;
            password = Crypto.Encrypt(password);
            DataTable dt = Database.FillTable(new UserProc(UserProcType.ProfileModify, this.ID, id, username, name, surname, password, language));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            if (dt.Columns.Contains("PASSWORD"))
                foreach (DataRow user in dt.Rows)
                    user["PASSWORD"] = Crypto.Decrypt(user["PASSWORD"].ToString());
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public Bitmap Captcha()
        {
            string encryptedCode;
            Bitmap capthcaImg = controls.Captcha.Create(out encryptedCode);
            Cookie cookie = new Cookie(this.Context);
            cookie.AddOrUpdate("captcha", encryptedCode);
            return capthcaImg;
        }
        private DataTable GetRawData(int id) 
        {
            DataTable dt = Database.FillTable(new UserProc(UserProcType.GetById, id));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return dt;
        }
        #endregion Methods

        #region Classes
        public class IPData
        {
            public bool IsActive { get; set; }
            public bool IsLocal { get; set; }
            public string Address { get; set; }
            public bool AllowAll { get; set; }
        }
        public class PasswordData
        {
            public string Encrypted { get; set; }
            public string Decrypted { get; set; }
        }
        #endregion Classes


    }
}
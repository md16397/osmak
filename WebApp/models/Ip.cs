﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using WebApp.controls;
using WebApp.controls.database;
using WebApp.datatype;

namespace WebApp.models
{
    public class Ip
    {
        #region Properties
        private User user { get; set; }
        #endregion Properties

        #region Constructors
        public Ip(User user)
        {
            this.user = user;
        }
        #endregion Constructors

        #region Methods

        public Ip Get(int id)
        {
            return this;
        }
        public string GetAll()
        {
            string result, filter;
            int pageIndex, pageRowCount;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.IpGetAll(user, data, out result, out filter, out pageIndex, out pageRowCount))
                return result;
            DataSet ds = Database.FillSet(new IpProc(IpProcType.GetAll, user.ID, filter, pageIndex, pageRowCount));
            if (ds.Tables.Count == 2)
            {
                result = "{\"RESULT\":1,\"TOTALROW\":" + ds.Tables[0].Rows[0]["TOTALROW"].ToString() + ",\"PAGEINDEX\":" + ds.Tables[0].Rows[0]["PAGEINDEX"].ToString() + ",";
                ds.Tables[1].TableName = "LIST";
                result += Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
                return result;
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";

        }
        public string Create()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            string result, address, port, protocol;
            bool state, local;
            if (!Valid.IpCreate(user, data, out result, out address, out port, out protocol, out local, out state))
                return result;
            DataTable dt = Database.FillTable(new IpProc(IpProcType.Create, user.ID, address, port, protocol, local, state));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string Modify()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            string result, address, port, protocol;
            int id;
            bool state, local;
            if (!Valid.IpModify(user, data, out result, out id, out address, out port, out protocol, out local, out state))
                return result;
            DataTable dt = Database.FillTable(new IpProc(IpProcType.Modify, user.ID, id, address, port, protocol, local, state));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string Delete()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            string result;
            int id;
            if (!Valid.IpDelete(data, out result, out id))
                return result;
            DataTable dt = Database.FillTable(new IpProc(IpProcType.Delete, user.ID, id));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        private DataTable GetRawData(int id)
        {
            DataTable dt = Database.FillTable(new UserProc(UserProcType.GetById, id));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return dt;
        }
        #endregion Methods

        #region Classes

        #endregion Classes
    }
}
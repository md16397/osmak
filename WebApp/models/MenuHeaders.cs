﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.datatype;

namespace WebApp.models
{
    public class MenuHeaders
    {
        private MenuHeader teklif;
        public MenuHeader Teklif { get { return teklif; } set { teklif = value; } }
        private MenuHeader urun;
        public MenuHeader Urun { get { return urun; } set { urun = value; } }
        private MenuHeader cari;
        public MenuHeader Cari { get { return cari; } set { cari = value; } }
        private MenuHeader settings;
        public MenuHeader Settings { get { return settings; } set { settings = value; } }

        public MenuHeaders(Dictionary<int, string> systemMessages)
        {
            urun = new MenuHeader("");
            teklif = new MenuHeader("");
            cari = new MenuHeader("");
            settings = new MenuHeader("AYARLAR");
        }
        public void SetVisibilities(string start)
        {
            if (start != null)
            {
                urun.Visible = true;
                teklif.Visible = true;
                cari.Visible = true;
                settings.Visible = true;
            }
        }
    }
}
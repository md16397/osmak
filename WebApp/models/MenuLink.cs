﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.models
{
    public class MenuLink
    {
        public string Name { get; private set; }
        public string Path { get; private set; }
        public string Title { get; private set; }
        public bool Selected { get; set; }
        public bool Visible { get; set; }
        public string HtmlClass { get { return Visible ? (Selected ? "active" : string.Empty) : "hidden"; } }

        public MenuLink(string name, string title, string path, bool selected = false, bool visible = false)
        {
            Name = name;
            Title = title;
            Path = path;
            Visible = visible;
            Selected = selected;
        }
    }
}
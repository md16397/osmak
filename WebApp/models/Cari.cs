﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.controls;
using WebApp.controls.database;
using WebApp.datatype;
using System.Data;

namespace WebApp.models
{
    public class Cari
    {
        #region Properties
        private User user { get; set; }
        #endregion Properties
        #region Constructors
        public Cari(User user)
        {
            this.user = user;
        }
        #endregion Constructors
        #region Methods
        public Cari Get(int id)
        {
            return this;
        }
        public string GetById()
        {
            string result;
            int cariid;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.CariGetById(user, data, out result, out cariid))
                throw new HttpRequestValidationException();
            DataTable dt = Database.FillTable(new CariProc(CariProcType.GetById, user.ID, cariid));
            if (dt.Rows.Count >= 1 && dt.Columns.Contains("RESULT"))
                return "{\"RESULT\":" + dt.Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }
        public string GetAll()
        {
            string result, filter;
            int pageIndex, pageRowCount;
            FilterType filterType;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.CariGetAll(user, data, out result, out filterType, out filter, out pageIndex, out pageRowCount))
                return result;
            DataSet ds = Database.FillSet(new CariProc(CariProcType.GetAll, user.ID, filterType, filter, pageIndex, pageRowCount));
            if (ds.Tables.Count == 2)
            {
                result = "{\"RESULT\":1,\"TOTALROW\":" + ds.Tables[0].Rows[0]["TOTALROW"].ToString() + ",\"PAGEINDEX\":" + ds.Tables[0].Rows[0]["PAGEINDEX"].ToString() + ",\"KULTYPE\":\"" + ds.Tables[0].Rows[0]["KULTYPE"].ToString() + "\",";
                ds.Tables[1].TableName = "LIST";
                result += Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
                return result;
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";

        }
        public string Create()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            string result, cariadi, aciklama;
            Boolean state;
            if (!Valid.CariCreate(user, data, out result, out cariadi, out state, out aciklama))
                return result;
            DataTable dt = Database.FillTable(new CariProc(CariProcType.Create, user.ID, cariadi, state, aciklama));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string Modify()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            string result, cariadi, aciklama;
            int cariid;
            Boolean aktif;
            if (!Valid.CariModify(user, data, out result, out cariid, out cariadi, out aktif, out aciklama))
                return result;

            DataTable dt = Database.FillTable(new CariProc(CariProcType.Modify, user.ID, cariid, cariadi, aktif, aciklama));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string Delete()
        {
            string result;
            int Cariid;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.CariDelete(user, data, out result, out Cariid))
                throw new HttpRequestValidationException();
            DataTable dt = Database.FillTable(new CariProc(CariProcType.Delete, user.ID, Cariid));
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        private DataTable GetRawData(int id)
        {
            DataTable dt = Database.FillTable(new UserProc(UserProcType.GetById, id));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return dt;
        }
        #endregion Methods

        #region Classes

        #endregion Classes

    }
}
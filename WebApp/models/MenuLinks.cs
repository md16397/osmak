﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.datatype;

namespace WebApp.models
{
    public class MenuLinks
    {
        #region Properties
        private MenuLink teklifler;
        public MenuLink Teklifler { get { return teklifler; } set { teklifler = value; } }
        private MenuLink urunler;
        public MenuLink Urunler { get { return urunler; } set { urunler = value; } }
        private MenuLink cariler;
        public MenuLink Cariler { get { return cariler; } set { cariler = value; } }
        private MenuLink users;
        public MenuLink Users { get { return users; } set { users = value; } }
        private MenuLink ip;
        public MenuLink IP { get { return ip; } set { ip = value; } }
        private MenuLink profile;
        public MenuLink Profile { get { return profile; } set { profile = value; } }
        private MenuLink selected;
        public MenuLink Selected { get { return selected; } set { selected = value; } }

        private const string tekliflerPath = "/teklifler.aspx";
        private const string tekliflerCreatePath = "/teklifler-create.aspx";
        private const string tekliflerModifyPath = "/teklifler-modify.aspx";
        private const string tekliflerDetailPath = "/teklifler-detail.aspx";
        private const string urunlerPath = "/urunler.aspx";
        private const string carilerPath = "/cariler.aspx";
        private const string usersPath = "/users.aspx";
        private const string ipPath = "/ip.aspx";
        private const string profilePath = "/profile.aspx";
        #endregion Properties

        #region Constructors
        public MenuLinks(Dictionary<int, string> systemMessages)
        {
            teklifler = new MenuLink("TEKLİFLER", "Osmak | Teklifler", tekliflerPath);
            urunler = new MenuLink("HAM MADDE/YARI MAMUL", "Osmak | Ürünler", urunlerPath);
            cariler = new MenuLink("CARİLER", "Osmak | Cariler", carilerPath);
            users = new MenuLink("KULLANICILAR", "Osmak | Kullanıcılar", usersPath);
            ip = new MenuLink("ERİŞİM (IP)", "Osmak | IP İşlemleri", ipPath);
            profile = new MenuLink("PROFİL", "Osmak | Profil", profilePath);
        }
        #endregion Constructors

        #region Methods
        public MenuLink Select(string path)
        {
            if (selected != null)
            {
                if (selected.Path == path)
                    return Selected;
                switch (selected.Path)
                {
                    case tekliflerPath:
                        teklifler.Selected = false;
                        break;
                    case urunlerPath:
                        urunler.Selected = false;
                        break;
                    case carilerPath:
                        cariler.Selected = false;
                        break;
                    case usersPath:
                        users.Selected = false;
                        break;
                    case ipPath:
                        ip.Selected = false;
                        break;
                    case profilePath:
                        profile.Selected = false;
                        break;
                    case tekliflerCreatePath:
                        teklifler.Selected = false;
                        break;
                    case tekliflerModifyPath:
                        teklifler.Selected = false;
                        break;
                    case tekliflerDetailPath:
                        teklifler.Selected = false;
                        break;
                    default:
                        throw new ArgumentException();

                }
                selected = null;
            }
            switch (path)
            {
                case tekliflerPath:
                    teklifler.Selected = true;
                    selected = teklifler;
                    break;
                case urunlerPath:
                    urunler.Selected = true;
                    selected = urunler;
                    break;
                case carilerPath:
                    cariler.Selected = true;
                    selected = cariler;
                    break;
                case usersPath:
                    users.Selected = true;
                    selected = users;
                    break;
                case ipPath:
                    ip.Selected = true;
                    selected = ip;
                    break;
                case profilePath:
                    profile.Selected = true;
                    selected = profile;
                    break;
                case tekliflerCreatePath:
                    teklifler.Selected = true;
                    selected = teklifler;
                    break;
                case tekliflerModifyPath:
                    teklifler.Selected = true;
                    selected = teklifler;
                    break;
                case tekliflerDetailPath:
                    teklifler.Selected = true;
                    selected = teklifler;
                    break;
                default:
                    throw new ArgumentException();
            }
            return Selected;
        }

        public void SetVisibilities(string start, UserType usertype)
        {
            users.Visible = ip.Visible = profile.Visible = false;

            if (start != null)
            {
                switch (usertype)
                {
                    case UserType.Admin:
                        teklifler.Visible = true;
                        urunler.Visible = true;
                        cariler.Visible = true;
                        users.Visible = true;
                        ip.Visible = true;
                        profile.Visible = true;
                        break;
                    case UserType.User:
                        teklifler.Visible = true;
                        urunler.Visible = true;
                        cariler.Visible = true;
                        users.Visible = false;
                        ip.Visible = false;
                        profile.Visible = true;
                        break;
                }
            }
        }
      #endregion Methods
    }
}
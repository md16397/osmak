﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.models
{
    public class MenuHeader
    {
        public string Name { get; private set; }
        public bool Visible { get; set; }
        public string HtmlClass { get { return Visible ? string.Empty : "hidden"; } }

        public MenuHeader(string name)
        {
            Name = name;
        }
    }
}
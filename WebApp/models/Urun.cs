﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using WebApp.controls;
using WebApp.controls.database;
using WebApp.datatype;

namespace WebApp.models
{
    public class Urun
    {
        #region Properties
        private User user { get; set; }
        #endregion Properties
        #region Constructors
        public Urun(User user)
        {
            this.user = user;
        }
        #endregion Constructors
        #region Methods
        public Urun Get(int id)
        {
            return this;
        }
        public string GetAll()
        {
            string result, filter;
            int pageIndex, pageRowCount;
            FilterType filterType;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.UrunGetAll(user, data, out result, out filterType, out filter, out pageIndex, out pageRowCount))
                return result;
            DataSet ds = Database.FillSet(new UrunProc(UrunProcType.GetAll, user.ID, filterType, filter, pageIndex, pageRowCount));
            if (ds.Tables.Count == 2)
            {
                result = "{\"RESULT\":1,\"TOTALROW\":" + ds.Tables[0].Rows[0]["TOTALROW"].ToString() + ",\"PAGEINDEX\":" + ds.Tables[0].Rows[0]["PAGEINDEX"].ToString() + ",\"KULTYPE\":\"" + ds.Tables[0].Rows[0]["KULTYPE"].ToString() + "\",";
                ds.Tables[1].TableName = "LIST";
                result += Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
                return result;
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";

        }
        public string GetById()
        {
            string result;
            int urunid;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.UrunGetById(user, data, out result, out urunid))
                throw new HttpRequestValidationException();
            DataTable dt = Database.FillTable(new UrunProc(UrunProcType.GetById, user.ID, urunid));
            if (dt.Rows.Count >= 1 && dt.Columns.Contains("RESULT"))
                return Converter.DataRowToJSON(dt.Rows[0]);
            return "{\"RESULT\":-1}";
        }
        public string Create()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            string result, urun, urunkodu, aciklama;
            Boolean adgirilsinmi, miktargirilsinmi;
            if (!Valid.UrunCreate(user, data, out result, out urun, out urunkodu, out adgirilsinmi, out miktargirilsinmi, out aciklama))
                return result;

            DataTable dt = Database.FillTable(new UrunProc(UrunProcType.Create, user.ID, urun, urunkodu, adgirilsinmi, miktargirilsinmi, aciklama));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return Converter.DataRowToJSON(dt.Rows[0]);
        }  
        public string Modify()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            string result, urunadi, urunkodu, aciklama;
            Boolean adgirilsinmi, miktargirilsinmi;
            int urunid;
            if (!Valid.UrunModify(user, data, out result, out urunid, out urunadi, out urunkodu, out adgirilsinmi, out miktargirilsinmi, out aciklama))
                return result;

            DataTable dt = Database.FillTable(new UrunProc(UrunProcType.Modify, user.ID, urunid, urunadi, urunkodu, adgirilsinmi, miktargirilsinmi, aciklama));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string Delete()
        {
            string result;
            int id;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.UrunDelete(user, data, out result, out id))
                throw new HttpRequestValidationException();
            DataTable dt = Database.FillTable(new UrunProc(UrunProcType.Delete, user.ID, id));
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        private DataTable GetRawData(int id)
        {
            DataTable dt = Database.FillTable(new UserProc(UserProcType.GetById, id));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return dt;
        }
        #endregion Methods

        #region Classes

        #endregion Classes
    }
}
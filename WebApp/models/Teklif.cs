﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.controls;
using WebApp.controls.database;
using WebApp.datatype;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Text;

namespace WebApp.models
{
    public class Teklif
    {
        #region Properties
        private User user { get; set; }
        #endregion Properties
        #region Constructors
        public Teklif(User user)
        {
            this.user = user;
        }
        #endregion Constructors
        #region Methods
        public Teklif Get(int id)
        {
            return this;
        }
        public string GetAll()
        {
            string result, baslangictarihi, bitistarihi;
            int pageIndex, pageRowCount, cariid, kullaniciid, filtreonaydurumu;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.TeklifGetAll(user, data, out result, out pageIndex, out pageRowCount, out cariid, out kullaniciid, out filtreonaydurumu, out baslangictarihi, out bitistarihi))
                return result;
            DataSet ds = Database.FillSet(new TeklifProc(TeklifProcType.GetAll, user.ID, pageIndex, pageRowCount, cariid, kullaniciid, filtreonaydurumu, baslangictarihi, bitistarihi));
            if (ds.Tables.Count == 2)
            {
                result = "{\"RESULT\":1,\"TOTALROW\":" + ds.Tables[0].Rows[0]["TOTALROW"].ToString() + ",\"PAGEINDEX\":" + ds.Tables[0].Rows[0]["PAGEINDEX"].ToString() + ",\"KULTYPE\":\"" + ds.Tables[0].Rows[0]["KULTYPE"].ToString() + "\",";
                ds.Tables[1].TableName = "LIST";
                result += Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
                return result;
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }
      
        public string GetById()
        {
            string result;
            int teklifid;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.TeklifGetById(user, data, out result, out teklifid))
                throw new HttpRequestValidationException();
            DataSet ds = Database.FillSet(new TeklifProc(TeklifProcType.GetById, user.ID, teklifid));
            if (ds.Tables.Count == 2)
            {
                ds.Tables[1].TableName = "DETAILS";
                return "{" + Converter.DataRowToJSON(ds.Tables[0].Rows[0], false) + "," + Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }
        public string GetByAll()  // 6
        {
            string result;
            int tekliflerid;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.TeklifGetByAll(user, data, out result, out tekliflerid))
                return result;
            DataSet ds = Database.FillSet(new TeklifProc(TeklifProcType.GetByAll, user.ID, tekliflerid));
            if (ds.Tables.Count == 2)
            {
                ds.Tables[1].TableName = "DETAILS";
                return "{" + Converter.DataRowToJSON(ds.Tables[0].Rows[0], false) + "," + Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }
        public string Create()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            string result, teklifkodu, teklifadiolcusu, aciklama;
            DateTime tekliftarihi;
            int cari,karyuzdesi, teklifadi;// teklifadi : dropdown.
            decimal toplamtekliftutari, birimsatisfiyati, adet, toplamsatisfiyati, maliyettekliftutari;
            DataTable detail;
            if (!Valid.TeklifCreate(user, data, out result, out cari, out tekliftarihi, out teklifkodu, out toplamtekliftutari, out aciklama, out teklifadi, out teklifadiolcusu,
                out birimsatisfiyati, out adet, out karyuzdesi, out toplamsatisfiyati, out maliyettekliftutari, out detail))
                return result;
            DataSet ds = Database.FillSet(new TeklifProc(TeklifProcType.Create, user.ID, cari, tekliftarihi, teklifkodu, toplamtekliftutari, aciklama, teklifadi, teklifadiolcusu,
                birimsatisfiyati, adet, karyuzdesi, toplamsatisfiyati, maliyettekliftutari, detail));
            if (ds.Tables.Count == 2)
            {
                ds.Tables[1].TableName = "DETAILS";
                return "{" + Converter.DataRowToJSON(ds.Tables[0].Rows[0], false) + "," + Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }
        public string Modify()
        { // 4
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);

            string result, teklifkodu, teklifadiolcusu, aciklama;
            Boolean onaydurumu;
            int teklifid, teklifmaliyetid, cari, karyuzdesi, teklifadi;
            decimal toplamtekliftutari, birimsatisfiyati, adet, toplamsatisfiyati, maliyettekliftutari;
            DataTable detail;
            if (!Valid.TeklifModify(user, data, out result, out teklifid, out teklifmaliyetid, out cari, out teklifkodu, out toplamtekliftutari, out onaydurumu, out aciklama, out teklifadi, out teklifadiolcusu,
                out birimsatisfiyati, out adet, out karyuzdesi, out toplamsatisfiyati, out maliyettekliftutari, out detail))
                return result;
            DataSet ds = Database.FillSet(new TeklifProc(TeklifProcType.Modify, user.ID, teklifid, teklifmaliyetid, cari, teklifkodu, toplamtekliftutari, onaydurumu, aciklama,
            teklifadi, teklifadiolcusu, birimsatisfiyati, adet, karyuzdesi, toplamsatisfiyati, maliyettekliftutari, detail));
            if (ds.Tables.Count == 2)
            {
                ds.Tables[1].TableName = "DETAILS";
                return "{" + Converter.DataRowToJSON(ds.Tables[0].Rows[0], false) + "," + Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }
        public string Delete()
        {
            string result;
            int teklifid;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.TeklifDelete(user, data, out result, out teklifid))
                throw new HttpRequestValidationException();
            DataTable dt = Database.FillTable(new TeklifProc(TeklifProcType.Delete, user.ID, teklifid));
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string DeleteMaliyetKalemleri()
        {
            string result;
            int teklifid, teklifmaliyetid;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.MaliyetDelete(user, data, out result, out teklifid, out teklifmaliyetid))
                throw new HttpRequestValidationException();
            DataTable dt = Database.FillTable(new TeklifProc(TeklifProcType.DeleteMaliyetKalemleri, user.ID, teklifid, teklifmaliyetid));
            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string GetDataforExcel()  // TYPE 7
        {
            string result;
            int tekliflerid;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.TeklifGetByAll(user, data, out result, out tekliflerid))
                return result;
            DataSet ds = Database.FillSet(new TeklifProc(TeklifProcType.GetDataforExcel, user.ID, tekliflerid));
            if (ds.Tables.Count == 2)
            {
                ds.Tables[1].TableName = "DETAILS";
                return "{" + Converter.DataTableToJSONArray(ds.Tables[0]) + "," + Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }
        public string ModifyTeklif() // 8
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            string result, teklifkodu, teklifadiolcusu, aciklama;
            int teklifid, cari, karyuzdesi, teklifadi;
            Boolean onaydurumu;
            decimal toplamtekliftutari, birimsatisfiyati, adet, toplamsatisfiyati, maliyettekliftutari;
            DataTable detail;
            if (!Valid.ModifyTeklif(user, data, out result, out teklifid, out cari, out teklifkodu, out toplamtekliftutari, out onaydurumu, out aciklama, out teklifadi, out teklifadiolcusu,
                out birimsatisfiyati, out adet, out karyuzdesi, out toplamsatisfiyati, out maliyettekliftutari, out detail))
                return result; 

            DataSet ds = Database.FillSet(new TeklifProc(TeklifProcType.ModifyTeklif, user.ID, teklifid, cari, teklifkodu, toplamtekliftutari, onaydurumu, aciklama,
                teklifadi, teklifadiolcusu, birimsatisfiyati, adet, karyuzdesi, toplamsatisfiyati, maliyettekliftutari, detail));
            if (ds.Tables.Count == 2)
            {
                ds.Tables[1].TableName = "DETAILS";
                return "{" + Converter.DataRowToJSON(ds.Tables[0].Rows[0], false) + "," + Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }
        public string GetAllByTeklifMaliyetId()  // 9
        {
            string result;
            int tekliflerid;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.TeklifGetByAll(user, data, out result, out tekliflerid))
                return result;
            DataSet ds = Database.FillSet(new TeklifProc(TeklifProcType.GetAllByTeklifMaliyetId, user.ID, tekliflerid));
            if (ds.Tables.Count == 2)
            {
                ds.Tables[1].TableName = "DETAILS";
                return "{" + Converter.DataRowToJSON(ds.Tables[0].Rows[0], false) + "," + Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }
        public string TeklifOnayDurumu()
        {
            string result;
            int tekliflerid;
            Boolean onaydurumu;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.TeklifOnayDurumu(user, data, out result, out tekliflerid, out onaydurumu))
                return result;
            DataSet ds = Database.FillSet(new TeklifProc(TeklifProcType.TeklifState, user.ID, tekliflerid, onaydurumu));
            if (ds.Tables.Count == 2)
            {
                ds.Tables[1].TableName = "DETAILS";
                return "{" + Converter.DataRowToJSON(ds.Tables[0].Rows[0], false) + "," + Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }    
        public string SaveAllByTekliflerId()
        {
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            string result, teklifkodu, aciklama;
            DateTime tekliftarihi;
            Boolean onaydurumu;
            int teklifid, cari;
            decimal toplamtekliftutari;
            if (!Valid.SaveAllByTekliflerId(user, data, out result, out teklifid, out cari, out toplamtekliftutari, out teklifkodu, out tekliftarihi, out onaydurumu, out aciklama))
                return result;
            DataTable dt = Database.FillTable(new TeklifProc(TeklifProcType.SaveAllByTekliflerId, user.ID, teklifid, cari, toplamtekliftutari, teklifkodu, tekliftarihi, onaydurumu, aciklama));

            return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string DovizKuru()
        {
            String bugun = "https://www.tcmb.gov.tr/kurlar/today.xml";
            var xmldosya = new XmlDocument();
            xmldosya.Load(bugun);

            // string dt = xmldosya.SelectSingleNode("/Tarih_Date[@Date]").InnerXml;
            string tarih = xmldosya.SelectSingleNode("//Tarih_Date").Attributes["Tarih"].Value;

            //string tarih = xmldosya.SelectSingleNode("//Tarih_Date").InnerXml;
            string DolarSatisFiyati = xmldosya.SelectSingleNode("Tarih_Date /Currency[@Kod= 'USD']/ForexSelling").InnerXml;// dolar satış fiyati
            string EuroSatisFiyati = xmldosya.SelectSingleNode("Tarih_Date /Currency[@Kod= 'EUR']/ForexSelling").InnerXml;// euro satış fiyati
            string DolarAlisFiyati = xmldosya.SelectSingleNode("Tarih_Date /Currency[@Kod= 'USD']/ForexBuying").InnerXml;// dolar alış fiyati
            string EuroAlisFiyati = xmldosya.SelectSingleNode("Tarih_Date /Currency[@Kod= 'EUR']/ForexBuying").InnerXml;// euro alış fiyati

            return "{\"DOLARALISFIYATI\":" + DolarAlisFiyati + ",\"DOLARSATISFIYATI\":" + DolarSatisFiyati + ",\"EUROALISFIYATI\":" + EuroAlisFiyati + ",\"EUROSATISFIYATI\":" + EuroSatisFiyati +
                ",\"Tarih\":\"" + tarih + "\""+"}";
            //   return Converter.DataRowToJSON(dt.Rows[0]);
        }
        public string TeklifUrunAdiGetAll()
        {
            string result, filter;
            int pageIndex, pageRowCount;
            FilterType filterType;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.TeklifUrunAdiGetAll(user, data, out result, out filterType, out filter, out pageIndex, out pageRowCount))
                return result;
            DataSet ds = Database.FillSet(new TeklifUrunAdiProc(TeklifProcType.TeklifUrunAdi, user.ID, filterType, filter, pageIndex, pageRowCount));
            if (ds.Tables.Count == 2)
            {
                result = "{\"RESULT\":1,\"TOTALROW\":" + ds.Tables[0].Rows[0]["TOTALROW"].ToString() + ",\"PAGEINDEX\":" + ds.Tables[0].Rows[0]["PAGEINDEX"].ToString() + ",\"KULTYPE\":\"" + ds.Tables[0].Rows[0]["KULTYPE"].ToString() + "\",";
                ds.Tables[1].TableName = "LIST";
                result += Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
                return result;
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }  
        public string GetAllTekliflerByFilter() // TEKLİF LİSTE EKRANI RAPOR ALMA
        {
            string result, baslangictarihi, bitistarihi;
            int cariid, kullaniciid, filtreonaydurumu;
            Dictionary<string, object> data = Converter.JSONToDictionary(user.Context.Request.InputStream);
            if (!Valid.GetAllTekliflerByFilter(user, data, out result, out cariid, out kullaniciid, out filtreonaydurumu, out baslangictarihi, out bitistarihi))
                return result;
            DataSet ds = Database.FillSet(new TeklifProc(TeklifProcType.GetAllTekliflerByFilter, user.ID, cariid, kullaniciid, filtreonaydurumu, baslangictarihi, bitistarihi));
            if (ds.Tables.Count == 2)
            {
                result = "{\"RESULT\":1,";
                ds.Tables[1].TableName = "LIST";
                result += Converter.DataTableToJSONArray(ds.Tables[1]) + "}";
                return result;
            }
            if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Columns.Contains("RESULT"))
                return "{\"RESULT\":" + ds.Tables[0].Rows[0]["RESULT"].ToString() + "}";
            return "{\"RESULT\":-1}";
        }

        private DataTable GetRawData(int id)
        {
            DataTable dt = Database.FillTable(new UserProc(UserProcType.GetById, id));
            if (dt == null || dt.Rows.Count != 1 || dt.Columns[0].ColumnName != "RESULT")
                throw new ArgumentNullException();
            return dt;
        }

        #endregion Methods

        #region Classes

        #endregion Classes
    }
}
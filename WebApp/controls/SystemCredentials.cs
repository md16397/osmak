﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebApp.controls.database;
using WebApp.datatype;
using WebApp.models;

namespace WebApp.controls
{
    public class SystemCredentials
    {
        public const int MaxFailLoginCount = 5;

        public static JSVersions Versions = new JSVersions();

        public static Dictionary<int, string> GetPage(User user, string pageURL, LangType? langType = null)
        {
            DataTable dt = Database.FillTable(new LangProc(LangProcType.GetByPageURL, user.ID, pageURL));
            return Converter.DataTableToDictionary(dt, "ID", langType != null ? langType.ToString().ToUpper() : "MSG");
        }

        public static void CreateUpdateJS(User user, SystemFileType pageType, LangType? langType = null)
        {
            string pageURL = "/" + pageType.ToString() + ".aspx";
            DataTable dt = Database.FillTable(new LangProc(LangProcType.GetPageJSData, user.ID, pageURL));
            string data, path;
            string variableName = pageType != SystemFileType.master ? "systemMessages" : "systemMasterMessages";
            if (langType != null)
            {
                data = Converter.DataTableToJSON(dt, "ID", langType.ToString().ToUpper(), variableName);
                path = "~/assets/js/locales/" + pageType + "." + langType.ToString() + ".js";
            }
            else
            {
                data = Converter.DataTableToJSON(dt, "ID", variableName);
                path = "~/assets/js/locales/" + pageType + ".js";
            }
            System.IO.File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath(path), data, System.Text.Encoding.UTF8);
            SystemCredentials.Versions.Upgrade(pageType, langType);
        }
    }
}
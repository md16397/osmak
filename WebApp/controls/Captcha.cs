﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
namespace WebApp.controls
{
    public class Captcha
    {
        public const string EncryptionKey = "FHEAA!GB48cvgQoLDA0ODa==";

        public static Bitmap Create(out string encryptedCode)
        {
            string decryptedCode = createRandomText();
            encryptedCode = Crypto.Encrypt(decryptedCode, EncryptionKey);

            Bitmap bitmap = new Bitmap(150, 40, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(bitmap);

            try
            {
                Pen pen = new Pen(Color.Yellow);
                Rectangle rect = new Rectangle(0, 0, 150, 40);
                SolidBrush blue = new SolidBrush(Color.CornflowerBlue);
                SolidBrush black = new SolidBrush(Color.Black);
                int counter = 0;
                g.DrawRectangle(pen, rect);
                g.FillRectangle(blue, rect);

                Random rand = new Random();
                for (int i = 0; i < decryptedCode.Length; i++)
                {
                    g.DrawString(decryptedCode[i].ToString(), new Font("Tahoma", 5 + rand.Next(15, 20), FontStyle.Italic), black, new PointF(10 + counter, -5));
                    counter += 28;
                }
                drawRandomLines(g);
                g.Dispose();
            }
            catch
            {
                g.Dispose();
            }
            return bitmap;
        }

        private static string createRandomText()
        {
            StringBuilder randomText = new StringBuilder();
            string alphabets = "012345679abcdefghijkhlmnopqrstuvwxyz";
            Random r = new Random();
            for (int j = 0; j <= 3; j++)
            { randomText.Append(alphabets[r.Next(alphabets.Length)]); }
            return randomText.ToString();
        }

        private static void drawRandomLines(Graphics g)
        {
            SolidBrush yellow = new SolidBrush(Color.Yellow);
            for (int i = 0; i < 20; i++)
            { g.DrawLines(new Pen(yellow, 1), createRandomPoints()); }
        }

        private static Point[] createRandomPoints()
        {
            Random rand = new Random();
            Point[] points = { new Point(rand.Next(0, 100), rand.Next(1, 100)), new Point(rand.Next(0, 150), rand.Next(1, 140)) };
            return points;
        }
    }
}
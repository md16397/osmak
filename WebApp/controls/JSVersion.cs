﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.datatype;

namespace WebApp.controls
{
    public class JSVersion
    {
        private SystemFileType systemFileType;
        public SystemFileType SystemFileType
        {
            get { return systemFileType; }
        }

        private string filePath;
        public string FilePath
        {
            get { return filePath; }
        }

        private DateTime version;
        public DateTime Version
        {
            get { return version; }
            set { version = value; }
        }

        public JSVersion(SystemFileType syetmFileType, string filePath)
        {
            this.systemFileType = syetmFileType;
            this.filePath = filePath;
            this.SetToFileTime();
        }

        /// <summary>
        /// sets version to last modified time of file
        /// </summary>
        /// <returns>Version</returns>
        public DateTime SetToFileTime()
        {
            version = System.IO.File.GetLastWriteTime(System.Web.HttpContext.Current.Server.MapPath(filePath));
            return Version;
        }

        public DateTime SetToCurrentTime()
        {
            version = DateTime.Now;
            return Version;
        }
    }
}
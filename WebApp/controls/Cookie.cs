﻿using System;
using System.Web;

namespace WebApp.controls
{
    public class Cookie
    {
        public DateTime ExpirationTime
        {
            get { return DateTime.Now.AddDays(1); }
        }

        public int Count
        {
            get { return this.context.Request.Cookies.Count; }
        }

        private HttpContext context { get; set; }

        public Cookie(HttpContext context)
        {
            this.context = context;
        }

        public bool IsExist(string Name)
        {
            return this.context.Request.Cookies[Name] != null;
        }

        public string Value(string Name)
        {
            return this.context.Request.Cookies[Name].Value;
        }

        public void AddOrUpdate(string Name, string Value)
        {
            if (this.context.Request.Cookies[Name] != null)
            {
                this.context.Response.Cookies[Name].Value = Value;
                this.context.Response.Cookies[Name].Expires = this.ExpirationTime;
            }
            else
            {
                HttpCookie httpCookie = new HttpCookie(Name);
                httpCookie.Value = Value;
                httpCookie.Expires = this.ExpirationTime;
                this.context.Response.Cookies.Add(httpCookie);
            }
        }

        public void AddOrUpdate(string Name, string Value, DateTime expirationTime)
        {
            if (this.context.Request.Cookies[Name] != null)
            {
                this.context.Response.Cookies[Name].Value = Value;
                this.context.Response.Cookies[Name].Expires = expirationTime;
            }
            else
            {
                HttpCookie httpCookie = new HttpCookie(Name);
                httpCookie.Value = Value;
                httpCookie.Expires = expirationTime;
                this.context.Response.Cookies.Add(httpCookie);
            }
        }

        public void Remove(string Name)
        {
            if (this.context.Request.Cookies[Name] != null)
                this.context.Response.Cookies[Name].Expires = DateTime.Now.AddMonths(-1);
        }

        public void RemoveAll()
        {
            DateTime stoneAge = new DateTime(1900, 1, 1);
            string[] keys = this.context.Request.Cookies.AllKeys;
            foreach (string key in this.context.Request.Cookies.AllKeys)
                this.context.Response.Cookies[key].Expires = stoneAge;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.datatype;

namespace WebApp.controls.database
{
    public class CariProc : Proc
    {
        /// <summary>
        /// Use this constructor with CariProc.GetById & Use this constructor with CariProc.Delete
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="urunid"></param>
        public CariProc(CariProcType type, int userid, int urunid)
        {
            FilterType filterType = FilterType.Dropdown;
            init(type, userid, filterType, string.Empty, 0, 0, urunid, string.Empty, false, string.Empty);
        }
        /// <summary>
        /// Use this constructor with CariProc.GetAll
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="filterType"></param>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageRowCount"></param>
        public CariProc(CariProcType type, int userid, FilterType filterType, string filter, int pageIndex, int pageRowCount)
        {
            init(type, userid, filterType, filter, pageRowCount, pageIndex, 0, string.Empty, false, string.Empty);
        }
        /// <summary>
        /// Use this constructor with CariProc.Create
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="cariadi"></param>
        /// <param name="aktif"></param>
        /// <param name="aciklama"></param>
        public CariProc(CariProcType type, int userid, string cariadi, Boolean state, string aciklama)
        {
            FilterType filterType = FilterType.Dropdown;
            init(type, userid, filterType, string.Empty, 0, 0, 0, cariadi, state, aciklama);
        }
        /// <summary>
        /// Use this constructor with CariProc.Modify       
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="cariid"></param>
        /// <param name="cariadi"></param>
        /// <param name="aktif"></param>
        /// <param name="aciklama"></param>
        public CariProc(CariProcType type, int userid, int cariid, string cariadi, Boolean aktif, string aciklama)
        {
            FilterType filterType = FilterType.Dropdown;
            init(type, userid, filterType, string.Empty, 0, 0, cariid, cariadi, aktif, aciklama);
        }
        private void init(CariProcType type, int userid, FilterType filterType, string filter, int pagerowcount, int pageindex, int cariid, string cariadi, Boolean aktif, string aciklama)
        {
            Name = "CARI_PROC";
            Params = new Dictionary<string, object>
            {
                { "@TYPE", (int)type },
                { "@ID", userid },
                { "@FILTERTYPE", (int)filterType },
                { "@FILTER", filter },
                { "@PAGEROWCOUNT", pagerowcount },
                { "@PAGEINDEX", pageindex },
                { "@CARIID", cariid },
                { "@ADI", cariadi },
                { "@AKTIF", aktif },
                { "@ACIKLAMA", aciklama },
            };
        }
    }
}
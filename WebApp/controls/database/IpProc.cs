﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.datatype;

namespace WebApp.controls.database
{
    public class IpProc : Proc
    {

        /// <summary>
        /// Use this constructor with IpProcType.GetAll
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageRowCount"></param>
        public IpProc(IpProcType type, int userid, string filter, int pageIndex, int pageRowCount)
        {
            init(type, userid, filter, pageRowCount, pageIndex, 0, string.Empty, string.Empty, string.Empty, false, false);
        }

        /// <summary>
        /// Use this constructor with IpProcType.Create
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="address"></param>
        /// <param name="port"></param>
        /// <param name="protocol"></param>
        /// <param name="local"></param>
        /// <param name="active"></param>
        public IpProc(IpProcType type, int userid, string address, string port, string protocol, bool local, bool active)
        {
            init(type, userid, string.Empty, 0, 0, 0, address, port, protocol, local, active);
        }

        /// <summary>
        /// Use this constructor with IpProcType.Modify
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="ipid"></param>
        /// <param name="address"></param>
        /// <param name="port"></param>
        /// <param name="protocol"></param>
        /// <param name="local"></param>
        /// <param name="active"></param>
        public IpProc(IpProcType type, int userid, int ipid, string address, string port, string protocol, bool local, bool active)
        {
            init(type, userid, string.Empty, 0, 0, ipid, address, port, protocol, local, active);
        }
        /// <summary>
        /// Use this constructor with IpProcType.Delete
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="ipid"></param>
        public IpProc(IpProcType type, int userid, int ipid)
        {
            init(type, userid, string.Empty, 0, 0, ipid, string.Empty, string.Empty, string.Empty, false, false);
        }

        private void init(IpProcType type, int userid, string filter, int pagerowcount, int pageindex, int ipid, string address, string port, string protocol, bool local, bool active)
        {
            Name = "IP_PROC";
            Params = new Dictionary<string, object>
            {
                { "@TYPE", (int)type },
                { "@ID", userid },
                { "@FILTER", filter },
                { "@PAGEROWCOUNT", pagerowcount },
                { "@PAGEINDEX", pageindex },
                { "@IPID", ipid },
                { "@ADDRESS", address },
                { "@PORT", port },
                { "@PROTOCOL", protocol },
                { "@LOCAL", local },
                { "@ACTIVE", active }
            };
        }
    }
}
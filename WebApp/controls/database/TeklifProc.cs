﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebApp.datatype;

namespace WebApp.controls.database
{
    public class TeklifProc : Proc
    {
        /// <summary>
        /// Use this constructor with TeklifProc.GetById & TeklifProc.Delete & TeklifProc.GetByAll
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="teklifid"></param>        
        public TeklifProc(TeklifProcType type, int userid, int teklifid)
        {
            DataTable teklifmaliyetkalemleri = new TeklifMaliyetKalemleriTableType().Table;
            init(type, userid, 0, 0, teklifid, 0, 0, DateTime.Now, string.Empty, false, 0, string.Empty, 0, 0, 0, string.Empty, 0, 0, 0, 0, 0,
            false, string.Empty, string.Empty, 0, DateTime.Now, string.Empty, string.Empty, 0, teklifmaliyetkalemleri);
        }

        /// <summary>
        /// Use this constructor with TeklifProc.Delete Maliyet Kalemleri & Get By Id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="teklifid"></param>
        public TeklifProc(TeklifProcType type, int userid, int teklifid, int teklifmaliyetkalemleriid)
        {
            DataTable teklifmaliyetkalemleri = new TeklifMaliyetKalemleriTableType().Table;
            init(type, userid, 0, 0, teklifid, 0, 0, DateTime.Now, string.Empty, false, 0, string.Empty, teklifmaliyetkalemleriid, 0, 0, string.Empty, 0, 0, 0, 0, 0,
                false, string.Empty, string.Empty, 0, DateTime.Now, string.Empty, string.Empty, 0, teklifmaliyetkalemleri);
        }
        /// <summary>
        /// Use this constructor with TeklifProc.GetAll
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageRowCount"></param> 
        public TeklifProc(TeklifProcType type, int userid, int pageIndex, int pageRowCount, int cariid, int kullaniciid, int filtreonaydurumu, string teklifbaslangictarihi, string teklifbitistarihi)
        {   
            DataTable teklifmaliyetkalemleri = new TeklifMaliyetKalemleriTableType().Table;
            init(type, userid, pageRowCount, pageIndex, 0, cariid, 0, DateTime.Now, string.Empty, false, filtreonaydurumu, string.Empty, 0, 0, 0, string.Empty,
            0, 0, 0, 0, 0, false, string.Empty, string.Empty, 0, DateTime.Now, teklifbaslangictarihi, teklifbitistarihi, kullaniciid, teklifmaliyetkalemleri);
        }
        /// <summary>
        /// Use this constructor with TeklifProc.Create
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="hat"></param>
        /// <param name="hatsaati"></param>
        /// <param name="hatsuresi"></param>
        /// <param name="hatkm"></param>
        /// <param name="hesaplasmagunsuresi"></param>
        public TeklifProc(TeklifProcType type, int userid, int cari, DateTime tekliftarihi, string teklifkodu, decimal toplamtekliftutari, string aciklama, int teklifadi, string teklifadiolcusu,
            decimal birimsatisfiyati, decimal adet, int karyuzdesi, decimal toplamsatisfiyati, decimal maliyettekliftutari, DataTable teklifmaliyetkalemleri)
        {
            init(type, userid, 0, 0, 0, cari, toplamtekliftutari, tekliftarihi, teklifkodu, false, 0, aciklama, 0, 0,
            teklifadi, teklifadiolcusu, birimsatisfiyati, adet, karyuzdesi, toplamsatisfiyati, maliyettekliftutari, false, string.Empty, string.Empty, 0, DateTime.Now, string.Empty, string.Empty, 0, teklifmaliyetkalemleri);
        }
        /// <summary>
        /// Use this constructor with TeklifProc.ModifyTeklif  (4)
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        ///// <param name="hatkm"></param> 
        public TeklifProc(TeklifProcType type, int userid, int teklifid, int teklifmaliyetid, int cari, string teklifkodu, decimal toplamtekliftutari, Boolean onaydurumu, string aciklama,
        int teklifadi, string teklifadiolcusu, decimal birimsatisfiyati, decimal adet, int karyuzdesi, decimal toplamsatisfiyati, decimal maliyettekliftutari, DataTable detail)
        {
            init(type, userid, 0, 0, teklifid, cari, toplamtekliftutari, DateTime.Now, teklifkodu, onaydurumu, 0, aciklama, teklifmaliyetid, 0,
            teklifadi, teklifadiolcusu, birimsatisfiyati, adet, karyuzdesi, toplamsatisfiyati, maliyettekliftutari, false, string.Empty, string.Empty, 0, DateTime.Now, string.Empty, string.Empty, 0, detail);
        }

        /// <summary>
        /// Use this constructor with TeklifProc.SaveAllByTeklifId (TEKLIFI KAYDET ve KAPAT)
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="hat"></param>
        /// <param name="hatsaati"></param>
        /// <param name="hatsuresi"></param>  ///TeklifProcType.SaveAllByTekliflerId, user.ID, teklifid, cari, toplamtekliftutari, teklifkodu, onaydurumu, aciklama
        /// <param name="hatkm"></param>
        /// <param name="hesaplasmagunsuresi"></param>
        public TeklifProc(TeklifProcType type, int userid, int teklifid, int cari, decimal toplamtekliftutari, string teklifkodu, DateTime tekliftarihi, Boolean onaydurumu, string aciklama)
        {
            DataTable teklifmaliyetkalemleri = new TeklifMaliyetKalemleriTableType().Table;
            init(type, userid, 0, 0, teklifid, cari, toplamtekliftutari, tekliftarihi, teklifkodu, onaydurumu, 0, aciklama, 0, 0,
            0, string.Empty, 0, 0, 0, 0, 0, false, string.Empty, string.Empty, 0, DateTime.Now, string.Empty, string.Empty, 0, teklifmaliyetkalemleri);
        }

        /// <summary>
        /// Use this constructor with TeklifProc.Modify and TeklifProc.MaliyetModify
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        ///// <param name="hatkm"></param> 
        public TeklifProc(TeklifProcType type, int userid, int teklifid, int cari, string teklifkodu, decimal toplamtekliftutari, Boolean onaydurumu, string aciklama,
               int teklifadi, string teklifadiolcusu, decimal birimsatisfiyati, decimal adet, int karyuzdesi, decimal toplamsatisfiyati, decimal maliyettekliftutari, DataTable detail)
        {
            init(type, userid, 0, 0, teklifid, cari, toplamtekliftutari, DateTime.Now, teklifkodu, onaydurumu, 0, aciklama, 0, 0,
            teklifadi, teklifadiolcusu, birimsatisfiyati, adet, karyuzdesi, toplamsatisfiyati, maliyettekliftutari, false, string.Empty, string.Empty, 0, DateTime.Now, string.Empty, string.Empty, 0, detail);
        }
        /// <summary>
        /// Use this constructor with TeklifProc.TeklifOnayDurumu
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="teklifid"></param>
        public TeklifProc(TeklifProcType type, int userid, int teklifid, Boolean onaydurumu)
        {
            DataTable teklifmaliyetkalemleri = new TeklifMaliyetKalemleriTableType().Table;
            init(type, userid, 0, 0, teklifid, 0, 0, DateTime.Now, string.Empty, onaydurumu, 0, string.Empty, 0, 0, 0, string.Empty, 0, 0, 0, 0, 0,
            false, string.Empty, string.Empty, 0, DateTime.Now, string.Empty, string.Empty, 0, teklifmaliyetkalemleri);
        }  // user, data, out result, out cariid, out kullaniciid, out filtreonaydurumu, out baslangictarihi, out bitistarihi
        /// <summary>
        /// Use this constructor with TeklifProc.TEKLIF LISTE EKRANI RAPOR ALMA
        /// </summary>
        public TeklifProc(TeklifProcType type, int userid, int cariid, int kullaniciid, int filtreonaydurumu, string baslangictarihi, string bitistarihi)
        {
            DataTable teklifmaliyetkalemleri = new TeklifMaliyetKalemleriTableType().Table;
            init(type, userid, 0, 0, 0, cariid, 0, DateTime.Now, string.Empty, false, filtreonaydurumu, string.Empty, 0, 0, 0, string.Empty, 0, 0, 0, 0, 0,
            false, string.Empty, string.Empty, 0, DateTime.Now, baslangictarihi, bitistarihi, kullaniciid, teklifmaliyetkalemleri);
        }

        private void init(TeklifProcType type, int userid, int pagerowcount, int pageindex, int tekliflerid,
        int cariid, decimal toplamtekliftutari, DateTime tekliftarihi, string teklifkodu, Boolean? onaydurumu, int filtreonaydurumu, string teklifaciklama, 
        int teklifmaliyetleriid, int teklifid, int teklifadi, string teklifadiolcusu, decimal birimsatisfiyati, decimal adet,int karyuzdesi, 
        decimal toplamsatisfiyati, decimal maliyettekliftutari, Boolean maliyetonaydurumu, string maliyetaciklama, string maliyettipi, int onaylayanid, 
        DateTime onaylanmatarihi, string teklifbaslangictarihi, string teklifbitistarihi, int kullaniciid, DataTable teklifmaliyetkalemleri){
            Name = "TEKLIF_PROC";
            Params = new Dictionary<string, object>
            {
                { "@TYPE", (int)type },
                { "@ID", userid },
                { "@FILTER", string.Empty }, // string empty
                { "@PAGEROWCOUNT", pagerowcount },
                { "@PAGEINDEX", pageindex },
                { "@TEKLIFLERID", tekliflerid },
                { "@CARIID", cariid },
                { "@TOPLAMTEKLIFTUTARI", toplamtekliftutari }, 
                { "@TEKLIFTARIHI", tekliftarihi },
                { "@TEKLIFKODU", teklifkodu },
                { "@ONAYDURUMU", onaydurumu },
                { "@FILTREONAYDURUMU", filtreonaydurumu },
                { "@TEKLIFACIKLAMA", teklifaciklama },
                { "@TEKLIFMALIYETLERIID", teklifmaliyetleriid },
                { "@TEKLIFID", teklifid },
                { "@TEKLIFADI", teklifadi }, 
                { "@TEKLIFADIOLCUSU", teklifadiolcusu },
                { "@BIRIMSATISFIYATI", birimsatisfiyati },
                { "@ADET", adet },
                { "@KARYUZDESI", karyuzdesi },
                { "@TOPLAMSATISFIYATI", toplamsatisfiyati },
                { "@MALIYETTEKLIFTUTARI", maliyettekliftutari },
                { "@MALIYETONAYDURUMU", maliyetonaydurumu },
                { "@MALIYETACIKLAMA", maliyetaciklama },
                { "@MALIYETTIPI", maliyettipi },
                { "@ONAYLAYANID", onaylayanid },  
                { "@ONAYLANMATARIHI", onaylanmatarihi },
                { "@TEKLIFBASLANGICTARIHI", teklifbaslangictarihi },
                { "@TEKLIFBITISTARIHI", teklifbitistarihi },
                { "@KULLANICIID", kullaniciid },
                { "@TEKLIFMALIYETKALEMLERI", teklifmaliyetkalemleri }
            };
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using WebApp.datatype;

namespace WebApp.controls.database
{
    public class TeklifUrunAdiProc : Proc
    {
        /// <summary>
        /// Use this constructor with TeklifProc.GetAll ( ÜRÜN ADI GET ALL)
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="filterType"></param>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageRowCount"></param>
        public TeklifUrunAdiProc(TeklifProcType type, int userid, FilterType filterType, string filter, int pageIndex, int pageRowCount)
        { 
            init(type, userid, filterType, filter, pageRowCount, pageIndex, 0, string.Empty);
        }
        private void init(TeklifProcType type, int userid, FilterType filterType, string filter, int pagerowcount, int pageindex, int tekliflerid, string teklifurunadi)
        {
            Name = "TEKLIFURUNADI_PROC";
            Params = new Dictionary<string, object>
            {
                { "@TYPE", (int)type },
                { "@ID", userid },
                { "@FILTERTYPE", (int)filterType },
                { "@FILTER", filter },
                { "@PAGEROWCOUNT", pagerowcount },
                { "@PAGEINDEX", pageindex },
                { "@TEKLIFURUNLERID", tekliflerid },
                { "@TEKLIFURUNADI", teklifurunadi },
            };
        }
    }
}
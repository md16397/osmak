﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.datatype;

namespace WebApp.controls.database
{
    public class UserProc : Proc
    {
        /// <summary>
        /// Use this constructor with UserProcType.Login
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="language">preferred language by user</param>
        /// <param name="ip">Ip address</param>
        /// <param name="ipIsLocal">Determines ip address is intranet or internet ip. if true, it is an intranet ip else it is internet ip.</param>
        public UserProc(UserProcType type, string username, string password, LangType language, string ip, bool ipIsLocal)
        {
            int id = 0, pageRowCount = 0, pageIndex = 0, userid = 0, failLoginCount = 0;
            string filter = string.Empty, name = string.Empty, surname = string.Empty, startPage = "teklifler.aspx";
            bool active = false;
            UserType usertype = UserType.User;
            init(type, id, filter, pageRowCount, pageIndex, userid, active, failLoginCount, username, name, surname, password, language, usertype, ip, ipIsLocal, startPage);
        }

        /// <summary>
        /// Use this constructor with UserProcType.GetById
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="id">User ID of who is performing this process</param>
        public UserProc(UserProcType type, int id)
        {
            int pageRowCount = 0, pageIndex = 0, userid = 0, failLoginCount = 0;
            string filter = string.Empty, username = string.Empty, name = string.Empty, surname = string.Empty, password = string.Empty, ip = string.Empty, startPage = string.Empty;
            bool active = false, ipIsLocal = false;
            LangType language = LangType.tr;
            UserType usertype = UserType.User;
            init(type, id, filter, pageRowCount, pageIndex, userid, active, failLoginCount, username, name, surname, password, language, usertype, ip, ipIsLocal, startPage);
        }

        /// <summary>
        /// Use this constructor with UserProcType.GetAll
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="id">User ID of who is performing this process</param>
        /// <param name="filter">searches in text fields and brings what contains this filter</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageRowCount"></param>
        public UserProc(UserProcType type, int id, string filter, int pageIndex, int pageRowCount)
        {
            int userid = 0, failLoginCount = 0;
            string username = string.Empty, name = string.Empty, surname = string.Empty, password = string.Empty, ip = string.Empty, startPage = string.Empty;
            bool active = false, ipIsLocal = false;
            LangType language = LangType.tr;
            UserType usertype = UserType.User;
            init(type, id, filter, pageRowCount, pageIndex, userid, active, failLoginCount, username, name, surname, password, language, usertype, ip, ipIsLocal, startPage);
        }

        /// <summary>
        /// Use this constructor with UserProcType.Create
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="id">User ID of who is performing this process</param>
        /// <param name="active">Determines user activation on system</param>
        /// <param name="username">Username</param>
        /// <param name="name">Name</param>
        /// <param name="surname">Surname</param>
        /// <param name="password">Password</param>
        /// <param name="language">Language</param>
        /// <param name="usertype">User type</param>
        /// <param name="company">Company</param>
        /// <param name="startPage">Determines where to start after login for users</param>
        public UserProc(UserProcType type, int id, bool active, string username, string name, string surname, string password, LangType language, UserType usertype)
        {
            int failLoginCount = 0;
            string startpage = "teklifler.aspx";
            init(type, id, string.Empty, 0, 0, 0, active, failLoginCount, username, name, surname, password, language, usertype, string.Empty, false, startpage);
        }

        /// <summary>
        /// Use this constructor with UserProcType.Modify
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="id">User ID of who is performing this process</param>
        /// <param name="userid">User ID of who is modified</param>
        /// <param name="failLoginCount">User max. login attempt fail count</param>
        /// <param name="active">Determines user activation on system</param>
        /// <param name="username">Username</param>
        /// <param name="name">Name</param>
        /// <param name="surname">Surname</param>
        /// <param name="password">Password</param>
        /// <param name="language">Language</param>
        /// <param name="usertype">User type</param>
        /// <param name="company">Company</param>
        /// <param name="startPage">Determines where to start after login for users</param>
        public UserProc(UserProcType type, int id, int userid, bool active, int failLoginCount, string username, string name, string surname, string password, LangType language, UserType usertype)
        {
            int pageRowCount = 0, pageIndex = 0;
            string filter = string.Empty, ip = string.Empty, startpage = "teklifler.aspx";
            bool ipIsLocal = false;
            init(type, id, filter, pageRowCount, pageIndex, userid, active, failLoginCount, username, name, surname, password, language, usertype, ip, ipIsLocal, startpage);
        }

        /// <summary>
        /// Use this constructor with UserProcType.ProfileModify
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="id">User ID of who is performing this process</param>
        /// <param name="userid">User ID of who is modified</param>
        /// <param name="username">Username</param>
        /// <param name="name">Name</param>
        /// <param name="surname">Surname</param>
        /// <param name="password">Password</param>
        public UserProc(UserProcType type, int id, int userid, string username, string name, string surname, string password, LangType language)
        {
            int pageRowCount = 0, pageIndex = 0, failLoginCount = 0;
            string filter = string.Empty, ip = string.Empty, startPage = string.Empty;
            bool ipIsLocal = false;
            init(type, id, filter, pageRowCount, pageIndex, userid, false, failLoginCount, username, name, surname, password, language, UserType.Admin, ip, ipIsLocal, startPage);
        }

        /// <summary>
        /// Use this constructor with UserProcType.ChangeLanguage
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="id">User ID of who is performing this process</param>
        /// <param name="language">New Language value</param>
        public UserProc(UserProcType type, int id, LangType language)
        {
            string filter, username, name, surname, password, ip, startPage;
            int pageRowCount, pageIndex, userid, failLoginCount;
            bool active, ipIsLocal;
            UserType usertype = UserType.Admin;
            filter = username = name = surname = password = ip = startPage = string.Empty;
            pageRowCount = pageIndex = userid = failLoginCount = 0;
            active = ipIsLocal = false;
            init(type, id, filter, pageRowCount, pageIndex, userid, active, failLoginCount, username, name, surname, password, language, usertype, ip, ipIsLocal, startPage);
        }

        private void init(
            UserProcType type,
            int id,
            string filter,
            int pageRowCount,
            int pageIndex,
            int userid,
            bool active,
            int failLoginCount,
            string username,
            string name,
            string surname,
            string password,
            LangType language,
            UserType usertype,
            string ip,
            bool ipIsLocal,
            string startPage)
        {
            Name = "USER_PROC";
            Params = new Dictionary<string, object>
            {
                { "@TYPE", (int)type },
                { "@ID", id },
                { "@FILTER", filter },
                { "@PAGEROWCOUNT", pageRowCount },
                { "@PAGEINDEX", pageIndex },
                { "@USERID", userid },
                { "@ACTIVE", active },
                { "@MAXFAILLOGINCOUNT", SystemCredentials.MaxFailLoginCount },
                { "@FAILLOGINCOUNT", failLoginCount },
                { "@USERNAME", username },
                { "@NAME", name },
                { "@SURNAME", surname },
                { "@PASSWORD", password },
                { "@LANGUAGE", language.ToString() },
                { "@USERTYPE", (int)usertype },
                { "@IP", ip },
                { "@IPLOCAL", ipIsLocal },
                { "@STARTPAGE", startPage },
            };
        }
    }
}
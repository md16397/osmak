﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebApp.controls.database
{
    public class TeklifMaliyetKalemleriTableType : TableType
    {
        public TeklifMaliyetKalemleriTableType()
        {
            table = new DataTable("dbo.TEKLIFMALIYETKALEMLERI");
            this.addColumns();
        }
        private void addColumns()
        {
            table.Columns.Add("STATE", typeof(string));
            table.Columns.Add("ID", typeof(int));
            table.Columns.Add("TEKLIFMALIYETID", typeof(int));
            table.Columns.Add("HAMMADDEYARIMAMULID", typeof(int));
            table.Columns.Add("HAMMADDEYARIMAMUL", typeof(string));
            table.Columns.Add("MIKTAR", typeof(decimal));
            table.Columns.Add("KALEMADET", typeof(decimal));
            table.Columns.Add("OLCUBIRIMI", typeof(string));
            table.Columns.Add("BIRIMLISTEFIYATI", typeof(decimal));
            table.Columns.Add("GIRILENFIYAT", typeof(decimal));
            table.Columns.Add("TUTAR", typeof(decimal));
        }
    }
}
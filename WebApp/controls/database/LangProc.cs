﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.datatype;

namespace WebApp.controls.database
{
    public class LangProc : Proc
    {
        /// <summary>
        /// Use this constructor with LangProcType.GetAll
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="id">User ID of who is performing this process</param>
        /// <param name="filter">searches in text fields and brings what contains this filter</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageRowCount"></param>
        public LangProc(LangProcType type, int id, string filter, int pageIndex, int pageRowCount, bool onlyErrorRecords)
        {
            string langType = string.Empty,
                langSource = string.Empty,
                tr = string.Empty,
                ar = string.Empty,
                pageURL = string.Empty;
            int langid = 0;
            init(type, id, pageURL, filter, pageRowCount, pageIndex, onlyErrorRecords, langid, langType, langSource, tr, ar);
        }

        /// <summary>
        /// Use this constructor with LangProcType.Create
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="id">User ID of who is performing this process</param>
        /// <param name="langType">system type of language input</param>
        /// <param name="langSource">system source of language input</param>
        /// <param name="tr">Turkish system data</param>
        /// <param name="ar">Arabic system data</param>
        public LangProc(LangProcType type, int id, string langType, string langSource, string tr, string ar)
        {
            bool onlyErrorRecords = false;
            string filter = string.Empty, pageURL = string.Empty;
            int pageIndex = 0, pageRowCount = 0, langid = 0;
            init(type, id, pageURL, filter, pageRowCount, pageIndex, onlyErrorRecords, langid, langType, langSource, tr, ar);
        }

        /// <summary>
        /// Use this constructor with LangProcType.Modify
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="id">User ID of who is performing this process</param>
        /// <param name="langid">unique id of language record in database table</param>
        /// <param name="langType">system type of language input</param>
        /// <param name="langSource">system source of language input</param>
        /// <param name="tr">Turkish system data</param>
        /// <param name="ar">Arabic system data</param>
        public LangProc(LangProcType type, int id, int langid, string langType, string langSource, string tr, string ar)
        {
            bool onlyErrorRecords = false;
            string filter = string.Empty, pageURL = string.Empty;
            int pageIndex = 0, pageRowCount = 0;
            init(type, id, pageURL, filter, pageRowCount, pageIndex, onlyErrorRecords, langid, langType, langSource, tr, ar);
        }

        /// <summary>
        /// Use this constructor with LangProcType.GetByPageURL | LangProcType.GetPageJSData
        /// </summary>
        /// <param name="type">Procedure process type</param>
        /// <param name="id">User ID of who is performing this process</param>
        /// <param name="pageURL">page URL for filtering</param>
        public LangProc(LangProcType type, int id, string pageURL)
        {
            string filter = string.Empty, langType = string.Empty, langSource = string.Empty, inputTR = string.Empty, inputAR = string.Empty;
            int pageRowCount = 0, pageIndex = 0, langid = 0;
            bool onlyErrorRecords = false;
            init(type, id, pageURL, filter, pageRowCount, pageIndex, onlyErrorRecords, langid, langType, langSource, inputTR, inputAR);
        }

        private void init(
            LangProcType type,
            int id,
            string pageURL,
            string filter,
            int pageRowCount,
            int pageIndex,
            bool onlyErrorRecords,
            int langid,
            string langType,
            string langSource,
            string inputTR,
            string inputAR)
        {
            Name = "LANG_PROC";
            Params = new Dictionary<string, object>
            {
                { "@TYPE", (int)type },
                { "@ID", id },
                { "@PAGEURL", pageURL },
                { "@FILTER", filter },
                { "@PAGEROWCOUNT", pageRowCount },
                { "@PAGEINDEX", pageIndex },
                { "@ONLYERRORRECORDS", onlyErrorRecords },
                { "@LANGID", langid },
                { "@LANGTYPE", langType },
                { "@LANGSOURCE", langSource },
                { "@TR", inputTR },
                { "@AR", inputAR }
            };
        }
    }
}
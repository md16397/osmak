﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebApp.controls.database
{
    public class TableType
    {
        protected DataTable table;

        public DataTable Table
        {
            get { return table; }
            set { table = value; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using WebApp.datatype;

namespace WebApp.controls.database
{
    public class UrunProc : Proc
    {
        /// <summary>
        /// Use this constructor with UrunProc.GetById & Use this constructor with UrunProc.Delete
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="hatid"></param>
        public UrunProc(UrunProcType type, int userid, int urunid)
        {
            FilterType filterType = FilterType.Dropdown;
            init(type, userid, filterType, string.Empty, 0, 0, urunid, string.Empty, string.Empty, false, false, string.Empty);
        }
        /// <summary>
        /// Use this constructor with UrunProc.GetAll
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="filterType"></param>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageRowCount"></param>
        public UrunProc(UrunProcType type, int userid, FilterType filterType, string filter, int pageIndex, int pageRowCount)
        {
            init(type, userid, filterType, filter, pageRowCount, pageIndex, 0, string.Empty, string.Empty, false, false, string.Empty);
        }
        /// <summary>
        /// Use this constructor with UrunProc.Create
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="hat"></param>
        /// <param name="hatsaati"></param>
        /// <param name="hatsuresi"></param>
        /// <param name="hatkm"></param>
        /// <param name="hesaplasmagunsuresi"></param>
        public UrunProc(UrunProcType type, int userid, string urun, string urunkodu, Boolean adgirilsinmi, Boolean miktargirilsinmi, string aciklama)
        {
            FilterType filterType = FilterType.Dropdown;
            init(type, userid, filterType, string.Empty, 0, 0, 0, urun, urunkodu, adgirilsinmi, miktargirilsinmi, aciklama);
        }
        /// <summary>
        /// Use this constructor with UrunProc.Modify       
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userid"></param>
        /// <param name="hatid"></param>
        /// <param name="hat"></param>
        /// <param name="hatsaati"></param>
        /// <param name="donussaati"></param>
        /// <param name="hatsuresi"></param>
        /// <param name="hatkm"></param>
        /// <param name="hesaplasmagunsuresi"></param>
        public UrunProc(UrunProcType type, int userid, int urunid, string urunadi, string urunkodu, Boolean adgirilsinmi, Boolean miktargirilsinmi, string aciklama)
        {
            FilterType filterType = FilterType.Dropdown;
            init(type, userid, filterType, string.Empty, 0, 0, urunid, urunadi, urunkodu, adgirilsinmi, miktargirilsinmi, aciklama); 
        }
        private void init(UrunProcType type, int userid, FilterType filterType, string filter, int pagerowcount, int pageindex, int urunid, string urun, string urunkodu, Boolean adgirilsinmi, Boolean miktargirilsinmi, string aciklama)
        {
            Name = "HAMMADDEYARIMAMUL_PROC";
            Params = new Dictionary<string, object>
            {
                { "@TYPE", (int)type },
                { "@ID", userid },
                { "@FILTERTYPE", (int)filterType },
                { "@FILTER", filter },
                { "@PAGEROWCOUNT", pagerowcount },
                { "@PAGEINDEX", pageindex },
                { "@HAMMADDEYARIMAMULID", urunid },
                { "@ADI", urun },
                { "@KODU", urunkodu },
                { "@ADGIRILSINMI", adgirilsinmi },
                { "@MIKTARGIRILSINMI", miktargirilsinmi }, 
                { "@ACIKLAMA", aciklama },
            };
        }
    }
}
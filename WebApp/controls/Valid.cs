﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using WebApp.controls.database;
using WebApp.datatype;
using WebApp.models;

namespace WebApp.controls
{
    public class Valid
    {
        #region public methods
        #region generic methods
        public static bool ID(int input)
        {
            bool includeZero = false;
            return positiveNumber(input, includeZero);
        }
        public static bool Price(decimal input)
        {
            return positiveNumber(input, true);
        }

        public static bool Plate(string input)
        {
            return !string.IsNullOrWhiteSpace(input);
        }

        public static bool IP(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return false;
            string pattern = @"\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b";
            return regexCheck(pattern, input);
        }

        public static bool Name(string input)
        {
            return !string.IsNullOrWhiteSpace(input);
        }

        public static bool Comment(string input)
        {
            return input != null;
        }
        public static bool StartPage(UserType usertype, string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return false;
            return input == "teklifler.aspx";
        }
        public static bool Language(string language, out LangType result)
        {
            switch (language)
            {
                case "tr":
                    result = LangType.tr;
                    return true;
                default:
                    result = LangType.tr;
                    return false;
            }
        }
        public static bool FilterType(int input, out FilterType result)
        {
            switch (input)
            {
                case (int)datatype.FilterType.Dropdown:
                    result = datatype.FilterType.Dropdown;
                    return true;
                case (int)datatype.FilterType.List:
                    result = datatype.FilterType.List;
                    return true;
                default:
                    result = datatype.FilterType.List;
                    return false;
            }
        }
        #endregion generic methods
        #region user methods
        public static bool Captcha(User user, string input)
        {
            Cookie cookie = new Cookie(user.Context);
            if (!cookie.IsExist("captcha"))
                return false;

            string encryptedCode = cookie.Value("captcha");
            if (string.IsNullOrWhiteSpace(encryptedCode))
                return false;
            string decryptedCode = Crypto.Decrypt(encryptedCode, controls.Captcha.EncryptionKey);
            return decryptedCode == input;
        }

        public static bool FailLoginCount(int input)
        {
            bool includeZero = true;
            return Valid.positiveNumber(input, includeZero);// && input < SystemCredentials.MaxFailLoginCount;
        }

        public static bool Username(string input)
        {
            return !string.IsNullOrWhiteSpace(input);
        }

        public static bool Type(int type, out UserType result)
        {
            switch (type)
            {
                case (int)UserType.Admin:
                    result = UserType.Admin;
                    return true;
                case (int)UserType.User:
                    result = UserType.User;
                    return true;
                default:
                    result = UserType.User;
                    return false;
            }
        }

        public static bool Surname(string input)
        {
            return !string.IsNullOrWhiteSpace(input);
        }

        public static bool Password(string input)
        {
            return !string.IsNullOrWhiteSpace(input);
        }
        public static bool Password(string password, string passwordAgain)
        {
            return Password(password) && Password(passwordAgain) && password == passwordAgain;
        }

        public static bool User(User user, out string result)
        {
            if (!user.IsExist)
            {
                result = "{\"RESULT\":8}";
                return false;
            }
            if (!user.IsActive)
            {
                result = "{\"RESULT\":6}";
                return false;
            }
            result = string.Empty;
            return true;
        }

        public static bool UserGetAll(User selector, Dictionary<string, object> data,
            out string result,
            out string filter,
            out int pageIndex,
            out int pageRowCount)
        {
            return getAll(selector, data, out result, out filter, out pageIndex, out pageRowCount);
        }

        public static bool UserCreate(User creator, Dictionary<string, object> data,
            out string result,
            out string username,
            out UserType type,
            out string name,
            out string surname,
            out string password,
            out LangType language,
            out bool state)
        {
            result = string.Empty;
            username = data["username"].ToString();
            type = UserType.User;
            name = data["name"].ToString();
            surname = data["surname"].ToString();
            password = data["password"].ToString();
            language = LangType.tr;
            state = Convert.ToBoolean(data["state"]);

            if (!Valid.User(creator, out result))
                return false;
            if (creator.Type != UserType.Admin)
            {
                result = "{\"RESULT\":13}";
                return false;
            }
            if (!Valid.Username(username))
            {
                result = "{\"RESULT\":9}";
                return false;
            }
            if (!Valid.Type(Convert.ToInt16(data["type"]), out type))
            {
                result = "{\"RESULT\":16}";
                return false;
            }
            if (!Valid.Name(name))
            {
                result = "{\"RESULT\":10}";
                return false;
            }
            if (!Valid.Surname(surname))
            {
                result = "{\"RESULT\":11}";
                return false;
            }
            if (!Valid.Password(password, data["passwordAgain"].ToString()))
            {
                result = "{\"RESULT\":2}";
                return false;
            }
            return true;
        }

        public static bool UserModify(User modifier, Dictionary<string, object> data,
            out string result,
            out int id,
            out int failLoginCount,
            out string username,
            out UserType type,
            out string name,
            out string surname,
            out string password,
            out LangType language,
            out bool state)
        {
            result = string.Empty;
            id = Convert.ToInt32(data["id"]);
            failLoginCount = Convert.ToInt16(data["failLoginCount"]);
            username = data["username"].ToString();
            type = UserType.User;
            name = data["name"].ToString();
            surname = data["surname"].ToString();
            password = data["password"].ToString();
            language = LangType.tr;
            state = Convert.ToBoolean(data["state"]);

            if (!Valid.User(modifier, out result))
                return false;
            if (modifier.ID != id && modifier.Type != UserType.Admin)
            {
                result = "{\"RESULT\":17}";
                return false;
            }
            if (!Valid.Username(username))
            {
                result = "{\"RESULT\":9}";
                return false;
            }
            if (!Valid.Type(Convert.ToInt16(data["type"]), out type))
            {
                result = "{\"RESULT\":16}";
                return false;
            }
            if (!Valid.Name(name))
            {
                result = "{\"RESULT\":10}";
                return false;
            }
            if (!Valid.Surname(surname))
            {
                result = "{\"RESULT\":11}";
                return false;
            }
            if (!Valid.Password(password, data["passwordAgain"].ToString()))
            {
                result = "{\"RESULT\":2}";
                return false;
            }
            if (!Valid.FailLoginCount(failLoginCount))
            {
                result = "{\"RESULT\":74}";
                return false;
            }
            return true;
        }

        public static bool UserProfileModify(User modifier, Dictionary<string, object> data,
            out string result,
            out int id,
            out string username,
            out string name,
            out string surname,
            out string password,
            out LangType language)
        {
            result = string.Empty;
            id = Convert.ToInt32(data["id"]);
            username = data["username"].ToString();
            name = data["name"].ToString();
            surname = data["surname"].ToString();
            password = data["password"].ToString();
            language = LangType.tr;

            if (!Valid.User(modifier, out result))
                return false;
            if (!Valid.Username(username))
            {
                result = "{\"RESULT\":9}";
                return false;
            }
            if (!Valid.Name(name))
            {
                result = "{\"RESULT\":10}";
                return false;
            }
            if (!Valid.Surname(surname))
            {
                result = "{\"RESULT\":11}";
                return false;
            }
            if (!Valid.Password(password, password))
            {
                result = "{\"RESULT\":2}";
                return false;
            }
            return true;
        }
        #endregion user methods
        #region ip methods
        public static bool Address(string input)
        {
            return !string.IsNullOrWhiteSpace(input);
        }
        public static bool IpGetAll(User selector, Dictionary<string, object> data,
           out string result,
           out string filter,
           out int pageIndex,
           out int pageRowCount)
        {
            return getAll(selector, data, out result, out filter, out pageIndex, out pageRowCount);
        }
        public static bool IpCreate(User creator, Dictionary<string, object> data,
           out string result,
           out string address,
           out string port,
           out string protocol,
           out bool local,
           out bool state)
        {
            result = string.Empty;
            address = data["address"].ToString();
            port = data["port"].ToString();
            protocol = data["protocol"].ToString();
            local = Convert.ToBoolean(data["local"]);
            state = Convert.ToBoolean(data["state"]);
            if (!Valid.User(creator, out result))
                return false;
            if (!Valid.Address(address))
            {
                result = "{\"RESULT\":41}";
                return false;
            }
            return true;
        }
        public static bool IpModify(User modifier, Dictionary<string, object> data,
           out string result,
           out int id,
           out string address,
           out string port,
           out string protocol,
           out bool local,
           out bool state)
        {
            result = string.Empty;
            id = Convert.ToInt32(data["id"]);
            address = data["address"].ToString();
            port = data["port"].ToString();
            protocol = data["protocol"].ToString();
            local = Convert.ToBoolean(data["local"]);
            state = Convert.ToBoolean(data["state"]);
            if (!Valid.User(modifier, out result))
                return false;
            if (!Valid.Address(address))
            {
                result = "{\"RESULT\":41}";
                return false;
            }
            return true;
        }
        public static bool IpDelete(Dictionary<string, object> data,
            out string result,
            out int id)
        {
            result = string.Empty;
            id = Convert.ToInt32(data["id"]);
            return Valid.ID(id);
        }
        #endregion ip methods
        #region config methods
        public static bool ConfigType(int input, out datatype.ConfigType configType)
        {
            switch (input)
            {
                case (int)datatype.ConfigType.IP:
                    configType = datatype.ConfigType.IP;
                    return true;
                case (int)datatype.ConfigType.Print:
                    configType = datatype.ConfigType.Print;
                    return true;
                case (int)datatype.ConfigType.None:
                default:
                    configType = datatype.ConfigType.None;
                    return false;
            }
        }
        public static bool ConfigModify(User modifier, Dictionary<string, object> data,
            out string result,
            out int configid,
            out ConfigType configType,
            out string code,
            out string name,
            out bool boolean,
            out DateTime date,
            out int integer,
            out decimal deciml,
            out string string128,
            out string string256)
        {
            result = string.Empty;
            configid = Convert.ToInt32(data["id"]);
            configType = datatype.ConfigType.None;
            code = data["code"].ToString();
            name = data["name"].ToString();
            boolean = Convert.ToBoolean(data["boolean"]);
            date = Convert.ToDateTime(data["date"]);
            integer = Convert.ToInt32(data["integer"]);
            deciml = Convert.ToDecimal(data["deciml"]);
            string128 = data["string128"].ToString();
            string256 = data["string256"].ToString();
            if (!Valid.User(modifier, out result))
                return false;
            if (!Valid.ConfigType(Convert.ToInt16(data["type"]), out configType))
            {
                result = "{\"RESULT\":71}";
                return false;
            }
            if (!Valid.ID(configid))
            {
                result = "{\"RESULT\":72}";
                return false;
            }
            return true;
        }
        #endregion config methods
        #region urun methods
        public static bool UrunGetById(User creator, Dictionary<string, object> data,
           out string result,
           out int urunid)
        {
            result = string.Empty;
            urunid = Convert.ToInt32(data["urunid"]);
            if (!Valid.ID(urunid))
            {
                result = "{\"RESULT\":64}";
                return false;
            }
            if (!Valid.User(creator, out result))
                return false;
            return true;
        }
        public static bool UrunGetAll(User selector, Dictionary<string, object> data,
           out string result,
           out FilterType filterType,
           out string filter,
           out int pageIndex,
           out int pageRowCount)
        {
            filterType = datatype.FilterType.List;
            if (!getAll(selector, data, out result, out filter, out pageIndex, out pageRowCount))
                return false;
            if (!Valid.FilterType(Convert.ToInt16(data["filterType"]), out filterType))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            return true;
        }
        public static bool UrunCreate(User creator, Dictionary<string, object> data,
            out string result,
            out string urun,
            out string urunkodu,
            out Boolean adgirilsinmi,
            out Boolean miktargirilsinmi,
            out string aciklama)
        {
            result = string.Empty;
            urun = data["urun"].ToString();
            urunkodu = data["urunkodu"].ToString();
            adgirilsinmi = Convert.ToBoolean(data["adgirilsinmi"]);
            miktargirilsinmi = Convert.ToBoolean(data["miktargirilsinmi"]);
            aciklama = data["aciklama"].ToString();

            if (!Valid.User(creator, out result))
                return false;
            if (!Valid.EmptyString(urun))
            {
                result = "{\"RESULT\":18}";
                return false;
            }
            return true;
        }
        public static bool UrunModify(User modifier, Dictionary<string, object> data,
           out string result,
           out int id,
           out string urunadi,
           out string urunkodu,
           out Boolean adgirilsinmi,
           out Boolean miktargirilsinmi,
           out string aciklama)
        {
            result = string.Empty;
            id = Convert.ToInt32(data["id"]);
            urunadi = data["urun"].ToString();
            urunkodu = data["urunkodu"].ToString();
            adgirilsinmi = Convert.ToBoolean(data["adgirilsinmi"]);
            miktargirilsinmi = Convert.ToBoolean(data["miktargirilsinmi"]);
            aciklama = data["aciklama"].ToString();

            if (!Valid.User(modifier, out result))
                return false;
            if (!Valid.EmptyString(urunadi))
            {
                result = "{\"RESULT\":18}";
                return false;
            }
            if (!Valid.EmptyString(urunkodu))
            {
                result = "{\"RESULT\":19}";
                return false;
            }
            return true;
        }
        public static bool UrunDelete(User modifier, Dictionary<string, object> data,
           out string result,
           out int id)
        {
            result = string.Empty;
            id = Convert.ToInt32(data["id"]);
            if (!Valid.ID(id))
            {
                result = "{\"RESULT\":64}";
                return false;
            }
            if (!Valid.User(modifier, out result))
                return false;
            return true;
        }
        #endregion urun methods

        #region teklif methods
        public static bool TeklifState(string input)
        {
            return !string.IsNullOrWhiteSpace(input) && (input == "NEW" || input == "OLD" || input == "DEL" || input == "UPD");
        }
        public static bool TeklifDetail(string state, int teklifmaliyetid, object[] rows, out string result, out DataTable detail)
        {
            result = string.Empty;   
            detail = new TeklifMaliyetKalemleriTableType().Table;
            if (rows.Length <= 0)
            {
                result = "{\"RESULT\":43}";
                return false;
            }
            int i = 0;
            bool goon = true;
            do
            {
                Dictionary<string, object> row = (Dictionary<string, object>)rows[i];
                DataRow dr = detail.NewRow();
                //dr["STATE"] = state;
                //dr["ID"] = 0;  
                dr["TEKLIFMALIYETID"] = teklifmaliyetid;
                goon =
                    row.ContainsKey("STATE") &&
                    row.ContainsKey("ID") &&
                    row.ContainsKey("HAMMADDEYARIMAMULID") &&
                    row.ContainsKey("HAMMADDEYARIMAMUL") &&
                    row.ContainsKey("MIKTARVAL") &&
                    row.ContainsKey("KALEMADETVAL") &&
                    row.ContainsKey("OLCUBIRIMI") &&
                    row.ContainsKey("BIRIMLISTEFIYATIVAL") &&
                    row.ContainsKey("GIRILENFIYATVAL") &&
                    row.ContainsKey("TUTARVAL");
                if (!goon)
                {
                    result = "{\"RESULT\":-1}";
                    break;
                }
                dr["STATE"] = row["STATE"];
                if (!Valid.TeklifState(dr["STATE"].ToString()))
                {
                    goon = false;
                    result = "{\"RESULT\":-1}";
                    break;
                }
                dr["ID"] = Convert.ToInt32(row["ID"]);
                if (dr["STATE"].ToString() != "NEW" && !Valid.ID((int)dr["ID"]))
                {
                    goon = false;
                    result = "{\"RESULT\":-1}";
                    break;
                }
                dr["HAMMADDEYARIMAMULID"] = Convert.ToInt32(row["HAMMADDEYARIMAMULID"]);
                if (!Valid.ID((int)dr["HAMMADDEYARIMAMULID"]))
                {
                    goon = false;
                    result = "{\"RESULT\":40}";
                    break;
                }
                dr["HAMMADDEYARIMAMUL"] = row["HAMMADDEYARIMAMUL"].ToString();
            
                dr["OLCUBIRIMI"] = row["OLCUBIRIMI"].ToString();
                if (!Valid.EmptyString((string)dr["OLCUBIRIMI"]))
                {
                    goon = false;
                    result = "{\"RESULT\":41}";
                    break;
                }
                dr["MIKTAR"] = Convert.ToDecimal(row["MIKTARVAL"]);
             
                dr["KALEMADET"] = Convert.ToDecimal(row["KALEMADETVAL"]);
                if (!Valid.Price((decimal)dr["KALEMADET"]))
                {
                    goon = false;
                    result = "{\"RESULT\":41}";
                    break;
                }
                dr["BIRIMLISTEFIYATI"] = Convert.ToDecimal(row["BIRIMLISTEFIYATIVAL"]);
                if (!Valid.Price((decimal)dr["BIRIMLISTEFIYATI"]))
                {
                    goon = false;
                    result = "{\"RESULT\":41}";
                    break;
                }
                dr["GIRILENFIYAT"] = Convert.ToDecimal(row["GIRILENFIYATVAL"]);
                if (!Valid.Price((decimal)dr["GIRILENFIYAT"]))
                {
                    goon = false;
                    result = "{\"RESULT\":41}";
                    break;
                }
                dr["TUTAR"] = Convert.ToDecimal(row["TUTARVAL"]);
                if (!Valid.Price((decimal)dr["TUTAR"]))
                {
                    goon = false;
                    result = "{\"RESULT\":42}";
                    break;
                }
                detail.Rows.Add(dr);
                i++;
            } while (goon && i < rows.Length);
            return goon;
        }
        public static bool TeklifGetById(User creator, Dictionary<string, object> data,
           out string result,
           out int teklifid)
        {
            result = string.Empty;
            teklifid = Convert.ToInt32(data["teklifid"]);
            if (!Valid.ID(teklifid))
            {
                result = "{\"RESULT\":33}";
                return false;
            }
            if (!Valid.User(creator, out result))
                return false;
            return true;
        }
        public static bool TeklifUrunAdiGetAll(User selector, Dictionary<string, object> data,
           out string result,
           out FilterType filterType,
           out string filter,
           out int pageIndex,
           out int pageRowCount)
        {
            filterType = datatype.FilterType.List;
            if (!getAll(selector, data, out result, out filter, out pageIndex, out pageRowCount))
                return false;
            if (!Valid.FilterType(Convert.ToInt16(data["filterType"]), out filterType))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            return true;
        }
        public static bool GetAllTekliflerByFilter(User selector, Dictionary<string, object> data,
         out string result,
         out int cariid,
         out int kullaniciid,
         out int filtreonaydurumu,
         out string baslangictarihi,
         out string bitistarihi) 
        {
            result = string.Empty;
            cariid = Convert.ToInt32(data["cariid"]);
            kullaniciid = Convert.ToInt32(data["kullaniciid"]);
            filtreonaydurumu = Convert.ToInt32(data["filtreonaydurumu"]);
            baslangictarihi = data["baslangictarihi"].ToString();
            bitistarihi = data["bitistarihi"].ToString();
            if (data["baslangictarihi"].ToString() != "1970-01-01")
            {
                baslangictarihi = data["baslangictarihi"].ToString();
            }
            else
            {
                baslangictarihi = "";
            }
            if (data["bitistarihi"].ToString() != "1970-01-01")
            {
                bitistarihi = data["bitistarihi"].ToString();
            }
            else
            {
                bitistarihi = "";
            }
            if (!Valid.User(selector, out result)) // get All() fonksiyonunu sildik. özelleştirdik.
                return false;
            return true;
        }
        
        public static bool TeklifGetAll(User selector, Dictionary<string, object> data,
            out string result,
            out int pageIndex,
            out int pageRowCount,
            out int cariid,
            out int kullaniciid,
            out int filtreonaydurumu,
            out string baslangictarihi,
            out string bitistarihi)
        { 
            result = string.Empty;
            pageIndex = Convert.ToInt16(data["pageIndex"]);
            pageRowCount = Convert.ToInt16(data["pageRowCount"]);
            cariid = Convert.ToInt32(data["cariid"]);
            kullaniciid = Convert.ToInt32(data["kullaniciid"]);
            filtreonaydurumu = Convert.ToInt32(data["filtreonaydurumu"]);
            baslangictarihi = data["baslangictarihi"].ToString();
            bitistarihi = data["bitistarihi"].ToString();
            if (data["baslangictarihi"].ToString() != "1970-01-01")
            {
                baslangictarihi = data["baslangictarihi"].ToString();
            }
            else
            {
                baslangictarihi = "";
            }
            if (data["bitistarihi"].ToString() != "1970-01-01")
            {
                bitistarihi = data["bitistarihi"].ToString();
            }
            else
            {
                bitistarihi = "";
            }
            if (!Valid.User(selector, out result)) // get All() fonksiyonunu sildik. özelleştirdik.
                return false;
            if (pageIndex <= 0 || pageRowCount <= 0)
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            return true;
        }
        public static bool GetAllTeklifiOlusturanKullanicilar(User selector, Dictionary<string, object> data,
            out string result,
            out string filter,
            out int pageIndex,
            out int pageRowCount)
        {
            if (!getAll(selector, data, out result, out filter, out pageIndex, out pageRowCount))
                return false;
            return true;

        }
        public static bool TeklifCreate(User creator,  Dictionary<string, object> data,
         out string result,
         out int cari,
         out DateTime tekliftarihi,
         out string teklifkodu,
         out decimal toplamtekliftutari,
         out string aciklama,
         out int teklifadi,
         out string teklifadiolcusu,
         out decimal birimsatisfiyati,
         out decimal adet,
         out int karyuzdesi,
         out decimal toplamsatisfiyati,
         out decimal maliyettekliftutari,
         out DataTable detail){
            result = string.Empty;
            cari = Convert.ToInt32(data["cari"]);
            tekliftarihi = Convert.ToDateTime(data["tekliftarihi"]);
            teklifkodu = data["teklifkodu"].ToString();
            toplamtekliftutari = Convert.ToDecimal(data["toplamtekliftutari"]);
            aciklama = data["aciklama"].ToString();
            teklifadi = Convert.ToInt32(data["teklifadi"]);
            teklifadiolcusu = data["teklifadiolcusu"].ToString();
            birimsatisfiyati = Convert.ToDecimal(data["birimsatisfiyati"]);
            adet = Convert.ToDecimal(data["adet"]);
            karyuzdesi = Convert.ToInt32(data["karyuzdesi"]);
            toplamsatisfiyati = Convert.ToDecimal(data["toplamsatisfiyati"]);
            maliyettekliftutari = Convert.ToDecimal(data["maliyettekliftutari"]);
            detail = null;

            if (!Valid.User(creator, out result))
                return false;
            if (!Valid.positiveNumber(cari))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(toplamtekliftutari))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(teklifadi))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.EmptyString(teklifadiolcusu))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(birimsatisfiyati))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(adet))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.Price(karyuzdesi))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(toplamsatisfiyati))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(maliyettekliftutari))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.TeklifDetail("NEW", 0, (object[])data["detail"], out result, out detail))
                return false;
            return true;
        }
        public static bool TeklifModify(User modifier, Dictionary<string, object> data,
         out string result,
         out int teklifid,
         out int teklifmaliyetid,
         out int cari,
         out string teklifkodu,
         out decimal toplamtekliftutari,
         out Boolean onaydurumu,
         out string aciklama,
         out int teklifadi,
         out string teklifadiolcusu,
         out decimal birimsatisfiyati,
         out decimal adet,
         out int karyuzdesi,
         out decimal toplamsatisfiyati,
         out decimal maliyettekliftutari,
         out DataTable detail)
        {
            result = string.Empty;
            teklifid = Convert.ToInt32(data["teklifid"]);
            teklifmaliyetid = Convert.ToInt32(data["teklifmaliyetid"]);
            cari = Convert.ToInt32(data["cari"]);
            teklifkodu = data["teklifkodu"].ToString();
            toplamtekliftutari = Convert.ToDecimal(data["toplamtekliftutari"]);
            onaydurumu = Convert.ToBoolean(data["onaydurumu"]);
            aciklama = data["aciklama"].ToString();
            teklifadi = Convert.ToInt32(data["teklifadi"]);
            teklifadiolcusu = data["teklifadiolcusu"].ToString();
            birimsatisfiyati = Convert.ToDecimal(data["birimsatisfiyati"]);
            adet = Convert.ToDecimal(data["adet"]);
            karyuzdesi = Convert.ToInt32(data["karyuzdesi"]);// boş gelebilmesi için angulardan 0 gönder.
            toplamsatisfiyati = Convert.ToDecimal(data["toplamsatisfiyati"]);
            maliyettekliftutari = Convert.ToDecimal(data["maliyettekliftutari"]);
            detail = null;

            if (!Valid.User(modifier, out result))
            {
                return false;
            }
            if (!Valid.ID(teklifid))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.ID(teklifmaliyetid))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(cari))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.EmptyString(teklifkodu))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(toplamtekliftutari))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(teklifadi))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.EmptyString(teklifadiolcusu))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(birimsatisfiyati))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(adet))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(toplamsatisfiyati))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(maliyettekliftutari))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.TeklifDetail("UPD", teklifmaliyetid, (object[])data["detail"], out result, out detail))
                return false;
            return true;
        }
        public static bool TeklifDelete(User modifier, Dictionary<string, object> data,
        out string result,
        out int teklifid)
        {
            result = string.Empty;
            teklifid = Convert.ToInt32(data["teklifid"]);
            if (!Valid.ID(teklifid))
            {
                result = "{\"RESULT\":64}";
                return false;
            }//
            if (!Valid.User(modifier, out result))
                return false;
            return true;
        }
        public static bool MaliyetDelete(User modifier, Dictionary<string, object> data,
        out string result,
        out int teklifid,
        out int teklifmaliyetid)
           {
            result = string.Empty;
            teklifid = Convert.ToInt32(data["teklifid"]);
            teklifmaliyetid = Convert.ToInt32(data["teklifmaliyetid"]);
            if (!Valid.ID(teklifmaliyetid))
            {
                result = "{\"RESULT\":64}";
                return false;
            }//
            if (!Valid.User(modifier, out result))
                return false;
            return true;
        }
        public static bool TeklifGetByAll(User modifier, Dictionary<string, object> data,
        out string result,
        out int tekliflerid)
        {
            result = string.Empty;
            tekliflerid = Convert.ToInt32(data["tekliflerid"]);
            if (!Valid.ID(tekliflerid))
            {
                result = "{\"RESULT\":64}";
                return false;
            }
            if (!Valid.User(modifier, out result))
                return false;
            return true;
        } 
        public static bool SaveAllByTekliflerId(User modifier, Dictionary<string, object> data,
        out string result,
        out int teklifid,
        out int cari,
        out decimal toplamtekliftutari,
        out string teklifkodu,
        out DateTime tekliftarihi,
        out Boolean onaydurumu,
        out string aciklama)
        {
            result = string.Empty;
            teklifid = Convert.ToInt32(data["teklifid"]);
            cari = Convert.ToInt32(data["cari"]);
            teklifkodu = data["teklifkodu"].ToString();
            tekliftarihi = Convert.ToDateTime(data["tekliftarihi"].ToString());
            toplamtekliftutari = Convert.ToDecimal(data["toplamtekliftutari"]);
            onaydurumu = Convert.ToBoolean(data["onaydurumu"]);
            aciklama = data["aciklama"].ToString();
            if (!Valid.positiveNumber(cari))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.EmptyString(teklifkodu))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.ID(teklifid))
            {
                result = "{\"RESULT\":64}";
                return false;
            }
            if (!Valid.User(modifier, out result))
                return false;
            return true;
        }

       public static bool ModifyTeklif(User creator, Dictionary<string, object> data,
       out string result,
       out int teklifid,
       out int cari,
       out string teklifkodu,
       out decimal toplamtekliftutari,
       out Boolean onaydurumu,
       out string aciklama,
       out int teklifadi,
       out string teklifadiolcusu,
       out decimal birimsatisfiyati,
       out decimal adet,
       out int karyuzdesi,
       out decimal toplamsatisfiyati,
       out decimal maliyettekliftutari,
       out DataTable detail
      )
        {
            result = string.Empty;
            teklifid = Convert.ToInt32(data["teklifid"]);
            cari = Convert.ToInt32(data["cari"]);
            teklifkodu = data["teklifkodu"].ToString();
            toplamtekliftutari = Convert.ToDecimal(data["toplamtekliftutari"]);
            onaydurumu = Convert.ToBoolean(data["onaydurumu"]);
            aciklama = data["aciklama"].ToString();
            teklifadi = Convert.ToInt32(data["teklifadi"]);
            teklifadiolcusu = data["teklifadiolcusu"].ToString();
            birimsatisfiyati = Convert.ToDecimal(data["birimsatisfiyati"]);
            adet = Convert.ToDecimal(data["adet"]);
            karyuzdesi = Convert.ToInt32(data["karyuzdesi"]);
            toplamsatisfiyati = Convert.ToDecimal(data["toplamsatisfiyati"]);
            maliyettekliftutari = Convert.ToDecimal(data["maliyettekliftutari"]);
            detail = null;

            if (!Valid.User(creator, out result))
                return false;
            if (!Valid.ID(teklifid))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(cari))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(toplamtekliftutari))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(teklifadi))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.EmptyString(teklifadiolcusu))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(birimsatisfiyati))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(adet))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(toplamsatisfiyati))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.positiveNumber(maliyettekliftutari))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.TeklifDetail("NEW", 0, (object[])data["detail"], out result, out detail))
                return false;
            return true;
        }
      public static bool TeklifOnayDurumu(User modifier, Dictionary<string, object> data,
      out string result,
      out int tekliflerid,
      out Boolean onaydurumu)
        {
            result = string.Empty;
            tekliflerid = Convert.ToInt32(data["tekliflerid"]);
            onaydurumu = Convert.ToBoolean(data["onaydurumu"]);
            if (!Valid.ID(tekliflerid))
            {
                result = "{\"RESULT\":64}";
                return false;
            }
            if (!Valid.User(modifier, out result))
                return false;
            return true;
        }

        #endregion teklif methods

        #region cari methods
        public static bool CariGetById(User creator, Dictionary<string, object> data,
        out string result,
        out int cariid)
        {
            result = string.Empty;
            cariid = Convert.ToInt32(data["cariid"]);
            if (!Valid.ID(cariid))
            {
                result = "{\"RESULT\":64}";
                return false;
            }
            if (!Valid.User(creator, out result))
                return false;
            return true;
        }
        public static bool CariGetAll(User selector, Dictionary<string, object> data,
            out string result,
            out FilterType filterType,
            out string filter,
            out int pageIndex,
            out int pageRowCount)
        {
            filterType = datatype.FilterType.List;
            if (!getAll(selector, data, out result, out filter, out pageIndex, out pageRowCount))
                return false;
            if (!Valid.FilterType(Convert.ToInt16(data["filterType"]), out filterType))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            return true;
        }
        public static bool CariCreate(User creator, Dictionary<string, object> data,
         out string result,
         out string cariadi,
         out Boolean state,
         out string aciklama)
        {
            result = string.Empty;
            cariadi = data["cariadi"].ToString();
            state = Convert.ToBoolean(data["state"]);
            aciklama = data["aciklama"].ToString();

            if (!Valid.User(creator, out result))
                return false;
            if (!Valid.EmptyString(cariadi))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            return true;
        }
        public static bool CariModify(User modifier, Dictionary<string, object> data,
         out string result,
         out int id,
         out string cariadi,
         out Boolean aktif,
         out string aciklama)
        {
            result = string.Empty;
            id = Convert.ToInt32(data["id"]);
            cariadi = data["cariadi"].ToString();
            aktif = Convert.ToBoolean(data["aktif"]);
            aciklama = data["aciklama"].ToString();

            if (!Valid.User(modifier, out result))
                return false;
            if (!Valid.ID(id))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            if (!Valid.EmptyString(cariadi))
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            return true;
        }
        public static bool CariDelete(User modifier, Dictionary<string, object> data,
        out string result,
        out int id)
        {
            result = string.Empty;
            id = Convert.ToInt32(data["id"]);
            if (!Valid.ID(id))
            {
                result = "{\"RESULT\":64}";
                return false;
            }
            if (!Valid.User(modifier, out result))
                return false;
            return true;
        }
        #endregion cari methods

        #endregion public methods
        #region private methods
        private static bool regexCheck(string pattern, string input)
        {
            return new Regex(pattern).IsMatch(input);
        }
        public static bool EmptyString(string input)
        {
            return !string.IsNullOrWhiteSpace(input);
        }
        private static bool positiveNumber(int input, bool includeZero = true)
        {
            return includeZero ? input >= 0 : input > 0;
        }
        private static bool positiveNumber(decimal input, bool includeZero = true)
        {
            return includeZero ? input >= decimal.Zero : input > decimal.Zero;
        }
        private static bool datetimeCheck(DateTime input)
        {
            DateTime defaultvalue = new DateTime(1900, 1, 1);
            if (defaultvalue >= input)
            {
                return false;
            }
            return true;
        }
        private static bool getAll(User selector, Dictionary<string, object> data,
            out string result,
            out string filter,
            out int pageIndex,
            out int pageRowCount)
        {
            result = string.Empty;
            filter = data["filter"].ToString();
            pageIndex = Convert.ToInt16(data["pageIndex"]);
            pageRowCount = Convert.ToInt16(data["pageRowCount"]);
            if (!Valid.User(selector, out result))
                return false;
            if (filter == null || pageIndex <= 0 || pageRowCount <= 0)
            {
                result = "{\"RESULT\":-1}";
                return false;
            }
            return true;
        }
        #endregion private methods

    }
}
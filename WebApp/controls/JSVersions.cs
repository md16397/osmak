﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.datatype;

namespace WebApp.controls
{
    public class JSVersions
    {
        private Dictionary<LangType, JSVersion> import;
        public Dictionary<LangType, JSVersion> Import
        {
            get { return import; }
        }

        private Dictionary<LangType, JSVersion> export;
        public Dictionary<LangType, JSVersion> Export
        {
            get { return export; }
        }

        private Dictionary<LangType, JSVersion> checkin;
        public Dictionary<LangType, JSVersion> Checkin
        {
            get { return checkin; }
        }

        private Dictionary<LangType, JSVersion> checkout;
        public Dictionary<LangType, JSVersion> Checkout
        {
            get { return checkout; }
        }

        private Dictionary<LangType, JSVersion> passlist;
        public Dictionary<LangType, JSVersion> Passlist
        {
            get { return passlist; }
        }
        private Dictionary<LangType, JSVersion> permissions;
        public Dictionary<LangType, JSVersion> Permissions
        {
            get { return permissions; }
        }

        private Dictionary<LangType, JSVersion> product;
        public Dictionary<LangType, JSVersion> Product
        {
            get { return product; }
        }

        private Dictionary<LangType, JSVersion> document;
        public Dictionary<LangType, JSVersion> Document
        {
            get { return document; }
        }

        private Dictionary<LangType, JSVersion> firm;
        public Dictionary<LangType, JSVersion> Firm
        {
            get { return firm; }
        }

        private Dictionary<LangType, JSVersion> stock;
        public Dictionary<LangType, JSVersion> Stock
        {
            get { return stock; }
        }

        private Dictionary<LangType, JSVersion> currencylist;
        public Dictionary<LangType, JSVersion> Currencylist
        {
            get { return currencylist; }
        }

        private Dictionary<LangType, JSVersion> users;
        public Dictionary<LangType, JSVersion> Users
        {
            get { return users; }
        }

        private Dictionary<LangType, JSVersion> language;
        public Dictionary<LangType, JSVersion> Language
        {
            get { return language; }
        }

        private Dictionary<LangType, JSVersion> nationality;
        public Dictionary<LangType, JSVersion> Nationality
        {
            get { return nationality; }
        }

        private Dictionary<LangType, JSVersion> ip;
        public Dictionary<LangType, JSVersion> IP
        {
            get { return ip; }
        }

        private Dictionary<LangType, JSVersion> unit;
        public Dictionary<LangType, JSVersion> Unit
        {
            get { return unit; }
            set { unit = value; }
        }


        private JSVersion profile;
        public JSVersion Profile
        {
            get { return profile; }
        }

        private JSVersion login;
        public JSVersion Login
        {
            get { return login; }
        }

        private Dictionary<LangType, JSVersion> master;
        public Dictionary<LangType, JSVersion> Master
        {
            get { return master; }
        }

        private JSVersion config;
        public JSVersion Config
        {
            get { return config; }
        }


        public JSVersions()
        {
            import = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.import, "~/assets/js/locales/import.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.import, "~/assets/js/locales/import.ar.js") }
            };
            nationality = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.nationality, "~/assets/js/locales/nationality.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.nationality, "~/assets/js/locales/nationality.ar.js") }
            };
            export = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.export, "~/assets/js/locales/export.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.export, "~/assets/js/locales/export.ar.js") }
            };
            checkin = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.checkin, "~/assets/js/locales/checkin.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.checkin, "~/assets/js/locales/checkin.ar.js") }
            };
            checkout = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.checkout, "~/assets/js/locales/checkout.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.checkout, "~/assets/js/locales/checkout.ar.js") }
            };
            passlist = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.passlist, "~/assets/js/locales/passlist.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.passlist, "~/assets/js/locales/passlist.ar.js") }
            };
            product = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.product, "~/assets/js/locales/product.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.product, "~/assets/js/locales/product.ar.js") }
            };
            document = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.document, "~/assets/js/locales/document.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.document, "~/assets/js/locales/document.ar.js") }
            };
            firm = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.firm, "~/assets/js/locales/firm.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.firm, "~/assets/js/locales/firm.ar.js") }
            };
            stock = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.stock, "~/assets/js/locales/stock.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.stock, "~/assets/js/locales/stock.ar.js") }
            };
            currencylist = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.currencylist, "~/assets/js/locales/currencylist.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.currencylist, "~/assets/js/locales/currencylist.ar.js") }
            };
            users = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.users, "~/assets/js/locales/users.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.users, "~/assets/js/locales/users.ar.js") }
            };
            language = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.language, "~/assets/js/locales/language.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.language, "~/assets/js/locales/language.ar.js") }
            };
            ip = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.ip, "~/assets/js/locales/ip.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.ip, "~/assets/js/locales/ip.ar.js") }
            };
            unit = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.unit, "~/assets/js/locales/unit.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.unit, "~/assets/js/locales/unit.ar.js") }
            };
            permissions = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.permissions, "~/assets/js/locales/permissions.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.permissions, "~/assets/js/locales/permissions.ar.js") }
            };
            profile = new JSVersion(datatype.SystemFileType.profile, "~/assets/js/locales/profile.js");
            login = new JSVersion(datatype.SystemFileType.login, "~/assets/js/locales/login.js");
            master = new Dictionary<LangType, JSVersion> {
                { LangType.tr, new JSVersion(datatype.SystemFileType.master, "~/assets/js/locales/master.tr.js") },
                { LangType.ar, new JSVersion(datatype.SystemFileType.master, "~/assets/js/locales/master.ar.js") }
            };
            config = new JSVersion(datatype.SystemFileType.config, "~/assets/js/config.js");
        }

        public void Upgrade(SystemFileType page, LangType? langType)
        {
            LangType lang = langType == LangType.ar ? LangType.ar : LangType.tr;
            switch (page)
            {
                case SystemFileType.import:
                    import[lang].SetToCurrentTime();
                    break;
                case SystemFileType.permissions:
                    permissions[lang].SetToCurrentTime();
                    break;
                case SystemFileType.export:
                    export[lang].SetToCurrentTime();
                    break;
                case SystemFileType.checkin:
                    checkin[lang].SetToCurrentTime();
                    break;
                case SystemFileType.checkout:
                    checkout[lang].SetToCurrentTime();
                    break;
                case SystemFileType.passlist:
                    passlist[lang].SetToCurrentTime();
                    break;
                case SystemFileType.product:
                    product[lang].SetToCurrentTime();
                    break;
                case SystemFileType.document:
                    document[lang].SetToCurrentTime();
                    break;
                case SystemFileType.firm:
                    firm[lang].SetToCurrentTime();
                    break;
                case SystemFileType.stock:
                    stock[lang].SetToCurrentTime();
                    break;
                case SystemFileType.currencylist:
                    currencylist[lang].SetToCurrentTime();
                    break;
                case SystemFileType.nationality:
                    nationality[lang].SetToCurrentTime();
                    break;
                case SystemFileType.users:
                    users[lang].SetToCurrentTime();
                    break;
                case SystemFileType.language:
                    language[lang].SetToCurrentTime();
                    break;
                case SystemFileType.ip:
                    ip[lang].SetToCurrentTime();
                    break;
                case SystemFileType.unit:
                    unit[lang].SetToCurrentTime();
                    break;
                case SystemFileType.master:
                    master[lang].SetToCurrentTime();
                    break;
                case SystemFileType.profile:
                    login.SetToCurrentTime();
                    break;
                case SystemFileType.login:
                    profile.SetToCurrentTime();
                    break;
                case SystemFileType.config:
                    config.SetToCurrentTime();
                    break;
                default:
                    throw new ArgumentException();
            }
        }
    }
}
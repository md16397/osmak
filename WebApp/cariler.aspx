﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Osmak.Master" AutoEventWireup="true" CodeBehind="cariler.aspx.cs" Inherits="WebApp.cariler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" src="assets/js/cari.js?v=0.0.1"></script>
    <!-- Angular Components -->
    <script type="text/javascript" src="assets/js/component/breadcrumb.js"></script>
    <script type="text/javascript" src="assets/js/component/page-header.js"></script>
    <script type="text/javascript" src="assets/js/component/page-footer.js"></script>
    <script type="text/javascript" src="assets/js/component/cari-list.js"></script>
    <script type="text/javascript" src="assets/js/component/cari-create-modal.js?v=0.0.1"></script>
    <script type="text/javascript" src="assets/js/component/cari-create-btn.js?v=0.0.1"></script>
    <script type="text/javascript" src="assets/js/component/cari-modify-modal.js?v=0.0.1"></script>
    <script type="text/javascript" src="assets/js/component/cari-delete-modal.js?v=0.0.1"></script>
    <!-- /angular Components -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <page-header></page-header>
    <!-- Content -->
    <div class="content">
    <cari-create-btn ng-controller="cariCreateBtnCtrl"></cari-create-btn>
    <cari-list ng-controller="cariListCtrl"></cari-list>
    <page-footer></page-footer>
        
    <cari-create-modal></cari-create-modal>
    <cari-modify-modal></cari-modify-modal>
    <cari-delete-modal></cari-delete-modal>	
	</div>
    <!-- /content -->
</asp:Content>

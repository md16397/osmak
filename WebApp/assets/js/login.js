﻿window.onload = function () {
    //jQuery.fn.extend({
    //    switchClass: function (oldCls, newCls) {
    //        var oldClsArr = oldCls.split(' ');
    //        var newClsArr = newCls.split(' ');
    //        return this.each(function (index, elem) {
    //            oldClsArr.forEach(function (cls) {
    //                $(elem).removeClass(cls);
    //            });
    //            newClsArr.forEach(function (cls) {
    //                $(elem).addClass(cls);
    //            });
    //        });
    //    }
    //});
    //login.language.init();
    login.check();
    $('#login-form').on('submit', login.init);
    $('input[name="username"]').on('keyup', login.username.check);
    $('input[name="username"]').on('focus', login.username.focus);
    $('input[name="password"]').on('keyup', login.password.check);
    $('input[name="password"]').on('focus', login.password.focus);
    $('input[name="captcha"]').on('keyup', login.captcha.check);
    $('input[name="captcha"]').on('focus', login.captcha.focus);
    //login.username.check();
    //login.password.check();
};

var login = {
    check: function () {
        login.prepare();
        $.ajax({
            url: "hub/user.ashx?&t=1&d=" + new Date(),
            type: "GET",
            success: function (data, textStatus, jqXHR) {
                var response = jqXHR.responseJSON;
                if (response.hasOwnProperty('RESULT') && response.RESULT === 1 && response.hasOwnProperty('STARTPAGE')) {
                    window.location.href = response.STARTPAGE;
                }
                login.release();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                login.release();
            }
        });
    },
    init: function (e) {
        e.preventDefault(); //STOP default action
        if (!login.isReady()) {
            return false;
        }
        var postData = $(this).serializeArray();
        login.prepare();
        $.ajax({
            url: "hub/user.ashx?&t=0&d=" + new Date(),
            type: "POST",
            data: postData,
            success: function (data, textStatus, jqXHR) {
                var response = jqXHR.responseJSON;
                if (!response.hasOwnProperty('RESULT')) {
                    login.error('Bir şeyler ters gitti!');//Something went wrong!//'Bir şeyler ters gitti!'
                    return;
                }
                switch (response.RESULT) {
                    case 0://username & password is invalid
                        login.release();
                        login.username.error();
                        login.password.error();
                        break;
                    case 2://password is invalid
                        login.release();
                        login.password.error();
                        break;
                    case 1://success
                    case 3://local ip
                    case 7://different ip
                        window.location.href = response.STARTPAGE;
                        break;
                    case 4://passive ip
                    case 5://invalid ip
                        login.error('Sisteme giriş yetkiniz bulunmuyor!');//You have no authority to log in//'Sisteme giriş yetkiniz bulunmuyor!'
                        break;
                    case 6://passive user
                        login.error('Hesabınız pasifleştirilmiş!');//Your account is passive//'Hesabınız pasifleştirilmiş!'
                        break;
                    case 73://reached max. fail login count
                        login.error('Hesabınız erişime kapatılmıştır.');//Your account is closed to access//'Hesabınız erişime kapatılmıştır.'
                        break;
                    case 75://invalid captcha
                        login.release();
                        login.captcha.reload();
                        login.captcha.error();
                        break;
                    case 15://invalid language
                    default:
                        login.error('Bir şeyler ters gitti!');//Something went wrong!//'Bir şeyler ters gitti!'
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                login.error('Bir şeyler ters gitti!');//'Bir şeyler ters gitti!'
            }
        });
    },
    isReady: function () {
        if (!login.username.isValid) {
            login.username.error('* Kullanıcı Adı geçersiz');//Invalid username//'* Kullanıcı Adı geçersiz'
        }
        if (!login.password.isValid) {
            login.password.error('* Şifre geçersiz');//Invalid password//'* Şifre geçersiz'
        }
        if (!login.captcha.isValid) {
            login.captcha.error('* Güvenlik kodu geçersiz');//Invalid security code//'* Güvenlik kodu geçersiz'
        }
        return login.username.isValid && login.password.isValid && login.captcha.isValid;
    },
    prepare: function () {
        $("input").prop('disabled', true);
        $('button[type="submit"]').prop('disabled', true);
    },
    release: function () {
        $("input").prop('disabled', false);
        $('button[type="submit"]').prop('disabled', false);
    },
    username: {
        isValid: false,
        check: function () {
            if (this.value === '') {
                $(this).parent().addClass('has-error');
                $(this).parent().find('i').addClass('text-danger');
                login.username.isValid = false;
            } else if (!login.username.isValid) {
                $(this).parent().removeClass('has-error');
                $(this).parent().find('i').removeClass('text-danger');
                login.username.isValid = true;
            }
        },
        error: function (msg) {
            $('input[name="username"]').parent().find('span.help-block').text(msg);
            $('input[name="username"]').parent().addClass('has-error');
            $('input[name="username"]').parent().find('i').addClass('text-danger');
            login.username.isValid = false;
        },
        focus: function () {
            $(this).parent().find('span.help-block').text('');
        }
    },
    password: {
        isValid: false,
        check: function () {
            if (this.value === '') {
                $(this).parent().addClass('has-error');
                $(this).parent().find('i').addClass('text-danger');
                login.password.isValid = false;
            } else if (!login.password.isValid) {
                $(this).parent().removeClass('has-error');
                $(this).parent().find('i').removeClass('text-danger');
                login.password.isValid = true;
            }
        },
        error: function (msg) {
            $('input[name="password"]').val('');
            $('input[name="password"]').parent().find('span.help-block').text(msg);
            $('input[name="password"]').parent().addClass('has-error');
            $('input[name="password"]').parent().find('i').addClass('text-danger');
            login.password.isValid = false;
        },
        focus: function () {
            $(this).parent().find('span.help-block').text('');
        }
    },
    captcha: {
        isValid: false,
        check: function () {
            if (this.value === '') {
                $(this).parent().addClass('has-error');
                $(this).parent().find('i').addClass('text-danger');
                login.captcha.isValid = false;
            } else if (!login.captcha.isValid) {
                $(this).parent().removeClass('has-error');
                $(this).parent().find('i').removeClass('text-danger');
                login.captcha.isValid = true;
            }
        },
        error: function (msg) {
            $('input[name="captcha"]').val('');
            $('input[name="captcha"]').parent().find('span.help-block').text(msg);
            $('input[name="captcha"]').parent().addClass('has-error');
            $('input[name="captcha"]').parent().find('i').addClass('text-danger');
            login.captcha.isValid = false;
        },
        focus: function () {
            $(this).parent().find('span.help-block').text('');
        },
        url: 'hub/user.ashx?t=8',
        reload: function () {
            d = new Date();
            $('img.img-captcha:first').attr('src', login.captcha.url + '&d=' + d.getTime());
        }
    },
    error: function (msg) {
        swal({
            title: "Hay aksi...",
            text: msg,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Tamam",
            type: "error"
        }, function (isConfirm) {
            login.release();
        });
    }
};
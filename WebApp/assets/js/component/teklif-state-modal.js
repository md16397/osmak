﻿app.angular
    .component('teklifStateModal', {
        templateUrl: 'assets/template/teklif-state-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {

            $rootScope.showTeklifStateModal = function (_teklif) {
                console.log(_teklif);
                if (_teklif.TEKLIFONAYDURUMU == false) {
                    $scope.onaylanmis = true;
                    $scope.onaylanmamis = false;
                }
                else {
                    $scope.onaylanmamis = true;
                    $scope.onaylanmis = false;
                }
                $http({
                    method: 'POST',
                    url: 'hub/teklif.ashx?&t=6&d=' + new Date(),
                    data: {
                        tekliflerid: _teklif.TEKLIFLERID
                    }
                }).then(function (response) {
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error('Bir şeyler ters gitti...');
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                        window.location.href = 'login.aspx';
                        return;
                    }
                    console.log(response.data);
                    $scope.teklif.row = response.data;
                }, function (response) {
                    app.show.error('Bir şeyler ters gitti...');
                });
                $('.teklif-state-modal:first').modal({
                    backdrop: 'static',
                    keyboard: true
                });
            };
            $scope.teklif = {
                row: [],
                onay: true,
                red: false,
                onaydurumu: function () {
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=10&d=' + new Date(),
                        data: {
                            tekliflerid: $scope.teklif.row.TEKLIFLERID,
                            onaydurumu: $scope.teklif.onay
                        }
                    }).then(function (response) {
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                            window.location.href = 'login.aspx';
                            return;
                        }
                        $rootScope.onaystate = true;
                        $('.teklif-state-modal:first').modal('hide');
                        app.show.success('Teklif Bilgileri Onaylandı !');
                        $rootScope.loadList();
                    });
                },
                reddetdurumu: function () {
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=10&d=' + new Date(),
                        data: {
                            tekliflerid: $scope.teklif.row.TEKLIFLERID,
                            onaydurumu: $scope.teklif.red
                        }
                    }).then(function (response) {
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                            window.location.href = 'login.aspx';
                            return;
                        }
                        $rootScope.onaystate = false;
                        $('.teklif-state-modal:first').modal('hide');
                        app.show.success('Teklif Bilgileri Reddedildi !');
                        $rootScope.loadList();
                    });
                },
            };
        }]
    })
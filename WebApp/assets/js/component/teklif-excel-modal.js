﻿app.angular
    .component('teklifExcelModal', {
        templateUrl: 'assets/template/teklif-excel-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
            //window.addEventListener('keydown', function (event) {
            //    if (event.key === 'Escape') {
            //        $('.teklif-excel-modal:first').modal('hide');
            //    }
            //});
            $rootScope.showExcelModal = function (teklifexcel, teklifdetail, teklifkodu) {
                console.log(teklifexcel);
                console.log(teklifdetail);
                localStorage.removeItem('dolareuro');
                localStorage.removeItem('DOLARALISFIYATI');
                localStorage.removeItem('DOLARSATISFIYATI');
                localStorage.removeItem('EUROALISFIYATI');
                localStorage.removeItem('EUROSATISFIYATI');
                localStorage.removeItem('Tarih');
                $http({
                    method: "GET", //DOLAR ve EURO
                    url: 'hub/teklif.ashx?&t=13&d=' + new Date(), // dolar ve euro kurlarını excel'de göstermek için.
                    data: "{}",
                    contentType: "application/json",
                    dataType: "json",
                }).then(function (response) {
                    console.log(response.data);
                    localStorage.setItem("dolareuro", JSON.stringify(response.data));
                    localStorage.setItem("DOLARALISFIYATI", JSON.stringify(response.data.DOLARALISFIYATI));
                    localStorage.setItem("DOLARSATISFIYATI", JSON.stringify(response.data.DOLARSATISFIYATI));
                    localStorage.setItem("EUROALISFIYATI", JSON.stringify(response.data.EUROALISFIYATI));
                    localStorage.setItem("EUROSATISFIYATI", JSON.stringify(response.data.EUROSATISFIYATI));
                    localStorage.setItem("Tarih", JSON.stringify(response.data.Tarih));
                }, function (response) {
                    app.show.error('Bir şeyler ters gitti...');
                });
                $scope.teklif.teklifkod = teklifkodu;
                $scope.teklifdata = teklifexcel;
                $scope.teklifdetaildata = teklifdetail;
                //https://stackoverflow.com/questions/51506138/angularjs-group-objects-in-an-array-with-a-same-value-into-a-new-array
                //console.log("teklif-excel-modal", teklifexcel);
                //console.log("teklif-excel-modal", detailexcel);
                $('.teklif-excel-modal:first').modal({
                    backdrop: 'static',
                    keyboard: true
                });
            };
            $scope.teklif = {
                list: [],
                detailslist: [],
                teklifdata: [],
                teklifkod: null,
                init: function () {
                    window.location.pathname = 'teklifler.aspx';
                },
                excel: function () {
                    console.log($scope.teklifdata);
                    console.log($scope.teklifdetaildata);
                    var eventArray = $scope.teklifdetaildata, // gelen datayı TEKLIFMALIYETID ye göre gruplar.
                        result = Array.from(
                            eventArray.reduce((m, o) => m.set(o.TEKLIFMALIYETID, (m.get(o.TEKLIFMALIYETID) || []).concat(o)), new Map),
                            ([TEKLIFMALIYETID, events]) => ({ TEKLIFMALIYETID, events })
                        );
                    console.log(result);
                    [$scope.teklifdata] = [$scope.teklifdata];
                    $scope.teklif.list = [$scope.teklifdata];
                    $scope.teklif.detailslist = result;
                    console.log($scope.teklif.detailslist);
                    console.log($scope.teklif.list);
                    $scope.teklif.isExist = $scope.teklif.list.length > 0;
                    var teklifDate = new Date();
                    var yearTxt = teklifDate.getFullYear();
                    var montTxt = (teklifDate.getMonth() < 9 ? '0' : '') + (teklifDate.getMonth() + 1);
                    var dayTxt = (teklifDate.getDate() < 10 ? '0' : '') + teklifDate.getDate();
                    var filename = 'TeklifMaliyetReport_' + dayTxt + '-' + montTxt + '-' + yearTxt + '.xls';
                    $scope.teklif.tableToExcel('ReportTable', 'ExcelReportPage', filename);
                    $('.teklif-excel-modal:first').modal('hide');
                    app.show.success('Maliyetler Raporu Oluşturuldu!');
                    window.location.pathname = 'teklifler.aspx';
                },
                tableToExcel: (function () {
                    var uri = 'data:application/vnd.ms-excel;charset=UTF-8;base64,'
                        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

                    return function (table, name, filename) {
                        var ctx = { worksheet: name || 'Worksheet', table: $scope.teklif.dataToInnerTable() };//table.innerHTML } //değiştirdim
                        var blobData = new Blob([format(template, ctx)], { type: 'application/vnd.ms-excel' });//ekledim
                        //var blobdata = new Blob([table], { type: 'text/csv' });
                        document.getElementById("dlink").href = uri + base64(format(template, ctx));// uri + base64(format(template, ctx));//değiştirdim
                        document.getElementById("dlink").download = filename;
                        document.getElementById("dlink").click();
                    }
                })(),
                dataToInnerTable: function () {
                    var thead =
                        "<thead><tr><td><td><th style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 180px; background-color: #474866; color: white;\">" + "MÜŞTERİ ADI" +
                        "</th><th style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 170px; background-color: #474866; color: white;\">" + "TOPLAM TEKLİF TUTARI" +
                        "</th><th style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 150px; background-color: #474866; color: white;\">" + "TEKLİF KODU" +
                        "</th><th style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 110px; background-color: #474866; color: white;\">" + "TEKLİF TARİHİ" +
                        "</th><th style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 110px; background-color: #474866; color: white;\">" + "AÇIKLAMA" +
                        "</th></td></td></tr></thead>";
                    var tbody = "<tbody>";
                    var i = 0;
                    $scope.Osmaklogo = '<img src="http://osmaksistem.com/assets/images/logo_entry.png" width="190" height="75"';
                    for (var i = 0; i < $scope.teklif.list.length; i++) {
                        var tr =
                        "<tr><td>" + $scope.Osmaklogo +
                            "</td><td><td style=\"border: 1px solid black; text-align: center; font-size: 18px; color: #00173C;\"><b>" + $scope.teklif.list[i].CARI +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.list[i].TOPLAMTEKLIFTUTARITXT + " ₺" +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.list[i].TEKLIFKODU.replace(/,/g, "','") + "'" + // EXCEL teklif kodunu biçimlendiriyor. Bunu önlemek için  '  kullanıldı.
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.list[i].TEKLIFTARIHITXT +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.list[i].ACIKLAMA +
                            "</b></td></td></td></tr>";
                        tbody += tr;
                    }
                    var tr = "<tr></tr>"
                    tbody += tr;
                    $scope.Tarih = "* Bu kurlar Merkez Bankasının " + localStorage.getItem("Tarih") + " tarihli verilerinden alınmıştır.";
                    var tr =
                        "<tr><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "DOLAR ALIŞ" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "DOLAR SATIŞ" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "EURO ALIŞ" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "EURO SATIŞ" +
                        "</b></td><td style=\"font-size: 9px;\" rowspan='2'><b>" + $scope.Tarih +
                        "</b></td></td></td></tr>";
                    tbody += tr;
                  
                    $scope.dolareuro = localStorage.getItem("dolareuro");
                    $scope.DOLARALISFIYATI = parseFloat(localStorage.getItem("DOLARALISFIYATI")).toFixed(3);
                    $scope.DOLARSATISFIYATI = parseFloat(localStorage.getItem("DOLARSATISFIYATI")).toFixed(3);
                    $scope.EUROALISFIYATI = parseFloat(localStorage.getItem("EUROALISFIYATI")).toFixed(3);
                    $scope.EUROSATISFIYATI = parseFloat(localStorage.getItem("EUROSATISFIYATI")).toFixed(3);
                    $scope.Tarih = "* Bu kurlar Merkez Bankasının " + localStorage.getItem("Tarih") + " tarihli verilerinden alınmıştır.";

                    console.log($scope.Tarih);
                    if (typeof $scope.dolareuro !== 'undefined') {
                        console.log($scope.teklif.DOLARALISFIYATI);
                        var tr =
                            "<tr><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.DOLARALISFIYATI + " ₺" +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.DOLARSATISFIYATI + " ₺" +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.EUROALISFIYATI + " ₺" +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.EUROSATISFIYATI + " ₺" +
                            "</b></td></td></td></tr>";
                        tbody += tr;
                    }
                    var tr = "<tr></tr>"
                    tbody += tr;
                    do {
                        for (var i = 0; (typeof $scope.teklif.detailslist[i] !== 'undefined'); i++) {
                            var tr =
                                "<tr><td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 180px; background-color: #9d9eab;\"><b>" + "ÜRÜN ADI" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 150px; background-color: #9d9eab;\"><b>" + "ÜRÜN ÖLÇÜSÜ" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "BİRİM MALİYET FİYATI" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "BİRİM MALİYET ADETİ" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "KAR YÜZDESİ" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "BİRİM SATIŞ FİYATI" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 160px; background-color: #9d9eab;\" colspan='2'><b>" + "TOPLAM TEKLİF TUTARI" +
                                "</b></td></td></tr>";
                            tbody += tr;
                            if (typeof $scope.teklif.detailslist[i].events[0] !== 'undefined') {
                                var tr =
                                    "<tr><td><td style=\"border: 1px solid black; text-align: center; font-size: 19px; color: #00173C;\"><b>" + $scope.teklif.detailslist[i].events[0].TEKLIFMALIYETADI +
                                    "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 16px; color: #00173C;\"><b>" + $scope.teklif.detailslist[i].events[0].TEKLIFADIOLCUSU +
                                    "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist[i].events[0].BIRIMSATISFIYATITXT + " ₺" +
                                    "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist[i].events[0].ADETTXT +
                                    "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist[i].events[0].KARYUZDESI + " %" +
                                    "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist[i].events[0].TOPLAMSATISFIYATITXT + " ₺" +
                                    "</b></td></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\" colspan='2'><b>" + $scope.teklif.detailslist[i].events[0].MALIYETTEKLIFTUTARITXT + " ₺" + "</b></td></tr>"
                                tbody += tr;
                                }
                            var tr =
                                "<tr><td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "HAM M./YARI M. GRUBU" +
                                "</b></td><td style=\"border: 1px solid black; text-align: center; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "ADI" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "MİKTAR" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "ADET" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "ÖLÇÜ BİRİMİ" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "LİSTE BİRİM FİYATI" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; width: 130px; background-color: #e6e6eb;\"><b>" + "GİRİLEN BİRİM FİYAT" +
                                "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "TUTAR" +
                                "</b></td></td></tr>";
                            tbody += tr;
                            for (var j = 0; (typeof $scope.teklif.detailslist[i].events[j] !== 'undefined'); j++) {
                                var tr =
                                    "<tr><td><td style=\"border: 1px solid black; text-align: center; font-size: 12px; width: 95px;\">" + $scope.teklif.detailslist[i].events[j].HAMMADDEYARIMAMULADI +
                                    "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].HAMMADDEYARIMAMUL +
                                    "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].MIKTARTXT +
                                    "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].KALEMADETTXT +
                                    "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].OLCUBIRIMI +
                                    "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].BIRIMLISTEFIYATITXT + " ₺" +
                                    "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].GIRILENFIYATTXT + " ₺" +
                                    "</td></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].TUTARTXT + " ₺" + "</td>"
                                tbody += tr;
                            } "</tr>"
                            var tr = "<tr></tr>"
                            tbody += tr;
                            var tr = "<tr></tr>"
                            tbody += tr;
                        }
                    } while (typeof $scope.teklif.detailslist[i] !== 'undefined');
                    tbody += "</tbody>";

                    var tfoot = "<tfoot>";
                    for (var i = 0; i < $scope.teklif.list.length; i++) {
                        var tr =
                            "<tr><td><td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "OLUŞTURAN KULLANICI" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "OLUŞTURULMA TARİHİ" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "DÜZENLEYEN KULLANICI" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "DÜZENLENME TARİHİ" +
                            "</b></td></td></td></tr>";
                        tfoot += tr;
                        var tr =
                            "<tr><td><td><td style=\"border: 1px solid black; position: absolute; bottom: 0; text-align: center; font-size: 13px;\"><b>" + $scope.teklif.list[i].CREATEUSER +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 10px;\"><b>" + $scope.teklif.list[i].CREATEDATETXT +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 13px;\"><b>" + $scope.teklif.list[i].MODIFYUSER +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 10px;\"><b>" + $scope.teklif.list[i].MODIFYDATETXT +
                            "</b></td></td></td></tr>"
                        tfoot += tr;
                    }
                    var tr = "<tr></tr>"
                    tfoot += tr;
                    var tr =
                        "<tr><td><td><td><td style=\"position: absolute; bottom: 0; right: 0; font-size: 12px; text-decoration: underline; text-align: center; width: 120px;\"><b>" + "YETKİLİ KİŞİNİN İMZASI" +
                        "</b></td></td></td></td></tr>"
                    tfoot += tr;
                    "</tfoot>";
                    return thead + tbody + tfoot; // return thead + tbody + tfoot;
                },
            }
        }]
    })
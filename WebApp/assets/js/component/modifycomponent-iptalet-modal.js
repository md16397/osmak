﻿app.angular
    .component('modifycomponentIptaletModal', {
        templateUrl: 'assets/template/modifycomponent-iptalet-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
           
            $rootScope.showModifyIptaletModal = function (_teklif) {
                $scope.teklif.tekliflerid = _teklif;
                console.log("MODIFY COMPONENT IPTAL ET MODAL");
                $('.modifycomponent-iptalet-modal:first').modal({
                    backdrop: 'static',
                    keyboard: true
                });
            };
            $rootScope.modifyIptaletmodalTeklifid = false;
            $scope.teklif = { // TEKLIFLER DE DAHIL HEPSI SILINMELI.
                tekliflerid: 0,
                delete: function () {
                    if (typeof $scope.teklif.tekliflerid === 'undefined') {
                        $('.modifycomponent-iptalet-modal:first').modal('hide');
                        window.location.pathname = 'teklifler.aspx';
                        return;
                    }
                    app.show.spinner($('modifycomponent-iptalet-modal > div:first > div:first'));
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=5&d=' + new Date(),
                        data: {
                            teklifid: $scope.teklif.tekliflerid
                        }
                    }).then(function (response) {
                        app.hide.spinner($('modifycomponent-iptalet-modal > div:first > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 1) {
                            console.log(response.data);
                            $('.modifycomponent-iptalet-modal:first').modal('hide');
                            app.show.success('Teklif-Maliyet Bilgileri Silindi !');
                            localStorage.removeItem('tekliflist-id');
                            $rootScope.modifyIptaletmodalTeklifid = false;
                            Runopenmodify = $rootScope.runmodifydata;
                            Runopenmodify();
                            window.location.pathname = 'teklifler.aspx';
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                            window.location.href = 'login.aspx';
                            return;
                        }
                        app.show.error('Bir şeyler ters gitti...');
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                        app.hide.spinner($('modifycomponent-iptalet-modal > div:first > div:first'));
                    });
                }
            }
        }]
    })
﻿app.angular
    .component('cariCreateModal', {
        templateUrl: 'assets/template/cari-create-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
            //window.addEventListener('keydown', function (event) {// when press ESC, close the modal.
            //    if (event.key === 'Escape') {
            //        $('.cari-create-modal:first').modal('hide');
            //        $scope.cari.init();
            //    }
            //});
            $rootScope.showCreateModal = function () {
                $('.cari-create-modal:first').modal({
                    backdrop: 'static'
                });
            };
            $scope.cari = {
                cari: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.cari.cari.isValid = false;
                        $scope.cari.cari.value = '';
                        $scope.cari.cari.cls = '';
                        $scope.cari.cari.msg = '';
                        $scope.cari.cari.set('');
                    },
                    set: function (value) {
                        $scope.cari.cari.value = value;
                        $scope.cari.cari.isValid = true;
                        $scope.cari.cari.cls = '';
                        $scope.cari.cari.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.cari.cari.value);
                        if (applyCheck || $scope.cari.cari.isValid !== check) {
                            $scope.cari.cari.isValid = check;
                            if ($scope.cari.cari.isValid) {
                                $scope.cari.cari.success();
                            } else {
                                $scope.cari.cari.error('* Geçersiz Cari Adı!');
                            }
                        }
                        return $scope.cari.cari.isValid;
                    },
                    change: function () {
                        $scope.cari.cari.check();
                    },
                    success: function () {
                        $scope.cari.cari.cls = '';
                        $scope.cari.cari.msg = '';
                    },
                    error: function (msg) {
                        $scope.cari.cari.cls = 'has-error';
                        $scope.cari.cari.msg = msg;
                    }
                },
                state: {
                    name: 'Aktif',
                    value: true,
                    icon: {
                        cls: 'icon-check text-success'
                    },
                    init: function () {
                        $scope.cari.state.select($scope.cari.state.active);
                    },
                    select: function (state) {
                        $scope.cari.state.name = state.name;
                        $scope.cari.state.value = state.value;
                        $scope.cari.state.icon.cls = state.icon.cls.replace(/ pull-right/g, '');
                    },
                    set: function (value) {
                        $scope.cari.state.select(value ? $scope.cari.state.active : $scope.cari.state.passive);
                    },
                    active: {
                        name: 'Aktif',
                        value: true,
                        icon: {
                            cls: 'icon-check text-success'
                        },
                        anchor: {
                            cls: ''
                        }
                    },
                    passive: {
                        name: 'Pasif',
                        value: false,
                        icon: {
                            cls: 'icon-blocked text-danger'
                        },
                        anchor: {
                            cls: ''
                        }
                    }
                },
                aciklama: {
                    value: '',
                    set: function (aciklama) {
                        $scope.cari.aciklama.value = aciklama;
                    },
                    init: function () {
                        $scope.cari.aciklama.value = '';
                    }
                },
                init: function () {
                    $scope.cari.cari.init();
                    $scope.cari.state.init();
                    $scope.cari.aciklama.init();
                },
                check: function () {
                    var applyCheck = true;
                    var OK = {
                        cari: $scope.cari.cari.check(applyCheck)
                    };
                    return OK.cari;
                },
                save: function () {
                    if (!$scope.cari.check()) {
                        return;
                    }
                    app.show.spinner($('cari-create-modal > div:first > div:first'));
                    $http({
                        method: 'POST',
                        url: 'hub/cari.ashx?&t=3&d=' + new Date(),
                        data: {
                            cariadi: $scope.cari.cari.value,
                            state: $scope.cari.state.value,
                            aciklama: $scope.cari.aciklama.value
                        }
                    }).then(function (response) {
                        app.hide.spinner($('cari-create-modal > div:first > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 1) {//success
                            $scope.cari.init();
                            $rootScope.loadList();
                            app.show.success('Yeni Cari Bilgisi Oluşturuldu !');
                            $('.cari-create-modal:first').modal('hide');
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                            window.location.href = 'login.aspx';
                            return;
                        }
                        if (response.data['RESULT'] === 124) {
                            app.show.warning('UYARI', 'Bu cari bilgisi daha önce oluşturuldu.');
                            console.log(1);
                            return;
                        }
                        if (response.data['RESULT'] === 18) {
                            $scope.cari.cari.isValid = false;
                            $scope.cari.cari.error('*Geçersiz Cari Adı');
                            return;
                        }
                        //if running code come through this far then it means an unexpected error occurred
                        app.show.error('Bir şeyler ters gitti...');
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                        app.hide.spinner($('cari-create-modal > div:first > div:first'));
                    });
                }
            };
        }],
        bindings: {
            dateDisable: '<'
        }
    });

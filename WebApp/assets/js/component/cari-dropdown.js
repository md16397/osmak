﻿app.angular
    .component('cariDropdown', {
        templateUrl: 'assets/template/cari-dropdown.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
            var ctrl = this;
            $rootScope.disabled = function () {
                ctrl.caridisabled = true;
            };
            $scope.cari = {
                shouldInitOnInputChange: false,
                onfocus: false,
                focus: function ($event) {
                    $scope.cari.onfocus = true;
                    var length = typeof ctrl.inputValue === 'string' ? ctrl.inputValue.length : 0;
                    $event.target.setSelectionRange(0, length);
                    var width = angular.element($event.target).prop('offsetWidth');
                    $scope.cari.options.container.style.width = width + 'px';
                    $scope.cari.options.show();
                    if (ctrl.shouldLoadOptionsOnInputFocus) {
                        $scope.cari.options.load();
                    }
                },
                blur: function () {
                    $scope.cari.onfocus = false;
                    $scope.cari.options.hide();
                },
                filter: {
                    type: 0,
                    //value: '',
                    init: function () {
                        ctrl.inputValue = '';
                    },
                    change: function () {
                        $scope.cari.filter.timer.set();
                        if ($scope.cari.shouldInitOnInputChange && typeof ctrl.init === 'function') {
                            $scope.cari.shouldInitOnInputChange = false;
                            ctrl.init();
                        }
                    },
                    timer: {
                        value: null,
                        interval: 500,
                        set: function () {
                            if ($scope.cari.filter.timer.value) {
                                $timeout.cancel($scope.cari.filter.timer.value);
                                $scope.cari.filter.timer.value = null;
                            }
                            $scope.cari.filter.timer.value = $timeout(function () {
                                if ($scope.cari.options.isLoading) {
                                    return;
                                }
                                $scope.cari.options.page.index = 1;
                                $scope.cari.options.load();
                            }, $scope.cari.filter.timer.interval);
                        }
                    }
                },
                options: {
                    page: {
                        index: 1,
                        count: 1,
                        isLastPage: true
                    },
                    totalRow: {
                        value: 0,
                        set: function (value) {
                            $scope.cari.options.totalRow.value = value;
                        }
                    },
                    maxVisibleRow: {
                        value: app.pagination.maxVisible.row,
                        set: function (value) {
                            $scope.cari.options.maxVisibleRow.value = value;
                        }
                    },
                    isExist: false,
                    isLoading: false,
                    list: [],
                    container: {
                        style: {
                            width: '0px'
                        },
                        cls: ''
                    },
                    show: function () {
                        $scope.cari.options.container.cls = 'open';
                    },
                    hide: function () {
                        $timeout(function () {
                            if ($scope.cari.onfocus || $scope.cari.options.more.onfocus) {
                                return;
                            }
                            $scope.cari.options.container.cls = '';
                        }, 250);
                    },
                    load: function (extendList) {
                        $scope.cari.options.isLoading = true;
                        app.show.spinner($('cari-dropdown > .option-container'));
                        $http({
                            method: 'POST',
                            url: 'hub/cari.ashx?&t=2&d=' + new Date(),
                            data: {
                                pageIndex: $scope.cari.options.page.index,
                                filterType: $scope.cari.filter.type,
                                filter: ctrl.inputValue,//$scope.cari.filter.value,
                                pageRowCount: $scope.cari.options.maxVisibleRow.value,
                            }
                        }).then(function (response) {
                            $scope.cari.options.isLoading = false;
                            app.hide.spinner($('cari-dropdown > .option-container'));
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive
                                window.location.href = 'login.aspx';
                                return;
                            }
                            if (!extendList) {
                                $scope.cari.options.list = response.data.LIST;
                            } else {
                                $scope.cari.options.list.push.apply($scope.cari.options.list, response.data.LIST);
                            }
                            $scope.cari.options.isExist = $scope.cari.options.list.length > 0;
                            ctrl.shouldLoadOptionsOnInputFocus = !$scope.cari.options.isExist;
                            $scope.cari.options.totalRow.set(response.data.TOTALROW);
                            $scope.cari.options.paginate();
                        }, function (response) {
                            $scope.cari.options.isLoading = false;
                            console.log(response);
                            app.show.error('Bir şeyler ters gitti...');
                            app.hide.spinner($('cari-dropdown > .option-container'));
                        });
                    },
                    paginate: function () {
                        $scope.cari.options.page.count = $scope.cari.options.totalRow.value !== 0 ? Math.ceil($scope.cari.options.totalRow.value / $scope.cari.options.maxVisibleRow.value) : 1;
                        $scope.cari.options.page.isLastPage = $scope.cari.options.page.count <= $scope.cari.options.page.index;
                    },
                    click: function (cari) {
                        $scope.cari.shouldInitOnInputChange = true;
                        ctrl.shouldLoadOptionsOnInputFocus = true;
                        ctrl.inputValue = cari.ADI; // cari tablosu kolon adı 
                        if (typeof ctrl.onSelect === 'function') {
                            ctrl.onSelect({ cari: cari });
                        }
                    },
                    more: {
                        load: function () {
                            if ($scope.cari.options.page.isLastPage) {
                                return;
                            }
                            $scope.cari.options.page.index++;
                            var extendList = true;
                            $scope.cari.options.load(extendList);
                        },
                        onfocus: false,
                        focus: function () {
                            $scope.cari.options.more.onfocus = true;
                        },
                        blur: function () {
                            $scope.cari.options.more.onfocus = false;
                            $scope.cari.options.hide();
                        }
                    }
                }
            };
        }],
        bindings: {
            caridisabled: '=',
            inputLabel: '@',
            inputMessage: '@',
            componentClass: '@',
            onSelect: '&',
            init: '&',
            inputValue: '=',
            shouldLoadOptionsOnInputFocus: '=',
        }
    });
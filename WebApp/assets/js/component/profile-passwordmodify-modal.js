﻿app.angular
    .controller('profilePasswordModalCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
      
        $rootScope.showModifyModal = function (profileData) {
            $scope.profile.set(profileData);
            $('.profile-passwordmodify-modal:first').modal({
                backdrop: 'static'
            });
        };
        $rootScope.changeModifyModalLanguage = function (value) {
            $scope.profile.language.set(value);
        };
        $scope.profile = {
            rawdata: null,
            language: {
                value: 'tr',
                init: function () {
                    $scope.profile.language.set('tr');
                },
                set: function (value) {
                    $scope.profile.language.value = value;
                }
            },
            password: {
                value: '',
                init: function () {
                    $scope.profile.password.set('');
                },
                set: function (value) {
                    $scope.profile.password.value = value;
                }
            },
            oldpassword: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.profile.oldpassword.isValid = false;
                    $scope.profile.oldpassword.value = '';
                    $scope.profile.oldpassword.cls = '';
                    $scope.profile.oldpassword.msg = '';
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.profile.oldpassword.value);
                    if (applyCheck || $scope.profile.oldpassword.isValid !== check) {
                        $scope.profile.oldpassword.isValid = check;
                        if ($scope.profile.oldpassword.isValid) {
                            $scope.profile.oldpassword.success();
                        } else {
                            $scope.profile.oldpassword.error(systemMessages[725][document.documentElement.lang.toUpperCase()]);//'* Geçersiz Eski Şifre'
                        }
                    }
                    if ($scope.profile.oldpassword.isValid) {
                        if ($scope.profile.oldpassword.value !== $scope.profile.password.value) {
                            $scope.profile.oldpassword.isValid = false;
                            $scope.profile.oldpassword.error(systemMessages[726][document.documentElement.lang.toUpperCase()]);//'* Hatalı Eski Şifre'
                        } else if (!$scope.profile.oldpassword.isValid && $scope.profile.oldpassword.value === $scope.profile.password.value) {
                            $scope.profile.oldpassword.success();
                        }
                    }
                    return $scope.profile.oldpassword.isValid;
                },
                change: function () {
                    $scope.profile.oldpassword.check();
                },
                success: function () {
                    $scope.profile.oldpassword.cls = '';
                    $scope.profile.oldpassword.msg = '';
                },
                error: function (msg) {
                    $scope.profile.oldpassword.cls = 'has-error';
                    $scope.profile.oldpassword.msg = msg;
                }
            },
            newpassword: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.profile.newpassword.value = '';
                    $scope.profile.newpassword.isValid = false;
                    $scope.profile.newpassword.cls = '';
                    $scope.profile.newpassword.msg = '';
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.profile.newpassword.value);
                    console.log(check);
                    if (applyCheck || $scope.profile.newpassword.isValid !== check) {
                        $scope.profile.newpassword.isValid = check;
                        if ($scope.profile.newpassword.isValid) {
                            $scope.profile.newpassword.success();
                        } else {
                            $scope.profile.newpassword.error(systemMessages[727][document.documentElement.lang.toUpperCase()]);//'* Geçersiz Şifre'
                        }
                    }
                    if ($scope.profile.newpassword.isValid) {
                        if ($scope.profile.newpassword.again.value !== $scope.profile.newpassword.value) {
                            $scope.profile.newpassword.again.isValid = false;
                            $scope.profile.newpassword.again.error(systemMessages[728][document.documentElement.lang.toUpperCase()]);//'* Yeni Şifre alanı ile uyuşmuyor.'
                        } else if (!$scope.profile.newpassword.again.isValid && $scope.profile.newpassword.again.value === $scope.profile.newpassword.value) {
                            $scope.profile.newpassword.again.success();
                        }
                    }
                    return $scope.profile.newpassword.isValid;
                },
                change: function () {
                    $scope.profile.newpassword.check();
                },
                success: function () {
                    $scope.profile.newpassword.cls = '';
                    $scope.profile.newpassword.msg = '';
                },
                error: function (msg) {
                    $scope.profile.newpassword.cls = 'has-error';
                    $scope.profile.newpassword.msg = msg;
                },
                again: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.profile.newpassword.again.value = '';
                        $scope.profile.newpassword.again.isValid = false;
                        $scope.profile.newpassword.again.cls = '';
                        $scope.profile.newpassword.again.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.profile.newpassword.again.value);
                        if (applyCheck || $scope.profile.newpassword.again.isValid !== check) {
                            $scope.profile.newpassword.again.isValid = check;
                            if ($scope.profile.newpassword.again.isValid) {
                                $scope.profile.newpassword.again.success();
                            } else {
                                $scope.profile.newpassword.again.error(systemMessages[729][document.documentElement.lang.toUpperCase()]);//'* Geçersiz Şifre Tekrar'
                            }
                        }
                        if ($scope.profile.newpassword.again.isValid) {
                            if ($scope.profile.newpassword.again.value !== $scope.profile.newpassword.value) {
                                $scope.profile.newpassword.isValid = false;
                                $scope.profile.newpassword.error(systemMessages[730][document.documentElement.lang.toUpperCase()]);//'* Yeni Şifre Tekrar alanı ile uyuşmuyor.'
                            } else if (!$scope.profile.newpassword.isValid && $scope.profile.newpassword.again.value === $scope.profile.newpassword.value) {
                                $scope.profile.newpassword.success();
                            }
                        }
                        return $scope.profile.newpassword.again.isValid;
                    },
                    change: function () {
                        $scope.profile.newpassword.again.check();
                    },
                    success: function () {
                        $scope.profile.newpassword.again.cls = '';
                        $scope.profile.newpassword.again.msg = '';
                    },
                    error: function (msg) {
                        $scope.profile.newpassword.again.cls = 'has-error';
                        $scope.profile.newpassword.again.msg = msg;
                    }
                }
            },
            init: function () {
                $scope.profile.oldpassword.init();
                $scope.profile.newpassword.init();
                $scope.profile.newpassword.again.init();
            },
            check: function () {
                var applyCheck = true;
                var OK = {
                    oldpassword: $scope.profile.oldpassword.check(applyCheck),
                    newpassword: $scope.profile.newpassword.check(applyCheck),
                    againnewpassword: $scope.profile.newpassword.again.check(applyCheck)
                };
                return OK.oldpassword && OK.newpassword && OK.againnewpassword;
            },
            set: function (profileData) {
                $scope.profile.rawdata = profileData;
                $scope.profile.password.set(profileData.PASSWORD);
            },
            modify: function () {
                if (!$scope.profile.check()) {
                    return;
                }
                app.show.spinner($('profile-passwordmodify-modal > div:first > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/user.ashx?&t=6&d=' + new Date(),
                    data: {
                        id: $scope.profile.rawdata.ID,
                        username: $scope.profile.rawdata.USERNAME,
                        surname: $scope.profile.rawdata.SURNAME,
                        name: $scope.profile.rawdata.NAME,
                        password: $scope.profile.newpassword.value,
                        language: 'tr'
                    }
                }).then(function (response) {
                    app.hide.spinner($('profile-passwordmodify-modal > div:first > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error(systemMessages[731][document.documentElement.lang.toUpperCase()]);//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 1) {//success
                        $scope.profile.set(response.data);
                        //$rootScope.modifyRow(response.data);
                        $scope.profile.init();
                        app.show.success(systemMessages[732][document.documentElement.lang.toUpperCase()]);//'Yeni Şifre Kaydedildi!'
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                        window.location.href = 'login.aspx';
                        return;
                    }
                    if (response.data['RESULT'] === 2) {//Password is invalid
                        $scope.profile.newpassword.isValid = false;
                        $scope.profile.newpassword.error(systemMessages[727][document.documentElement.lang.toUpperCase()]);//'* Geçersiz Şifre'
                        return;
                    }

                    //if running code come through this far then it means an unexpected error occurred
                    app.show.error(systemMessages[731][document.documentElement.lang.toUpperCase()]);//'Bir şeyler ters gitti...'
                    /*if (response.data['RESULT'] === 9 || response.data['RESULT'] === 10 || response.data['RESULT'] === 11) {
                        app.show.error(systemMessages[731][document.documentElement.lang.toUpperCase()]);//'Bir şeyler ters gitti...'
                        return;
                    }*/
                }, function (response) {
                    console.log(response);
                    app.show.error(systemMessages[731][document.documentElement.lang.toUpperCase()]);//'Bir şeyler ters gitti...'
                    app.hide.spinner($('profile-passwordmodify-modal > div:first > div:first'));
                });
            },
            systemMessages: systemMessages
        };

    }])
    .directive('profilePasswordmodifyModal', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/profile-passwordmodify-modal.html'
        };
    });
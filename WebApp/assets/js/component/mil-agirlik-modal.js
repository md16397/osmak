﻿app.angular
    .component('milAgirlikModal', {
        templateUrl: 'assets/template/mil-agirlik-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', '$localStorage', function ($scope, $http, $timeout, $rootScope, $localStorage) {

            $rootScope.showMilModal = function (data) {
                $scope.teklif.get(data);
                localStorage.removeItem('boruagirlik');
                localStorage.removeItem('milagirlik');
                localStorage.removeItem('flansagirlik');
                $('.mil-agirlik-modal:first').appendTo("body");   // modalı öne getirir.
                $scope.teklif.init();
                $('.mil-agirlik-modal:first').modal({
                    backdrop: 'static'
                });
            };

            $scope.teklif = {
                milagirlik: '',
                cap: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.cap.isValid = false;
                        $scope.teklif.cap.value = '';
                        $scope.teklif.cap.text = '';
                        $scope.teklif.cap.cls = '';
                        $scope.teklif.cap.msg = '';
                        $scope.teklif.cap.set('');
                    },
                    set: function (cap) {
                        $scope.teklif.cap.value = cap.value;
                        $scope.teklif.cap.text = cap.text;
                        $scope.teklif.cap.isValid = true;
                        $scope.teklif.cap.cls = '';
                        $scope.teklif.cap.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.cap.value);
                        if (applyCheck || $scope.teklif.cap.isValid !== check) {
                            $scope.teklif.cap.isValid = check;
                            if ($scope.teklif.cap.isValid) {
                                $scope.teklif.cap.success();
                            } else {
                                $scope.teklif.cap.error('* Geçersiz Çap !');
                            }
                        }
                        return $scope.teklif.cap.isValid;
                    },
                    change: function () {
                        $scope.teklif.cap.check($scope.teklif.cap.value);
                    },
                    success: function () {
                        $scope.teklif.cap.cls = '';
                        $scope.teklif.cap.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.cap.cls = 'has-error';
                        $scope.teklif.cap.msg = msg;
                    }
                },
                boyu: {
                    isValid: false,
                    value: 0,
                    text: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.teklif.boyu.isValid = false;
                        $scope.teklif.boyu.value = '';
                        $scope.teklif.boyu.text = '';
                        $scope.teklif.boyu.cls = '';
                        $scope.teklif.boyu.msg = '';
                        $scope.teklif.boyu.set('');
                    },
                    set: function (boyu) {
                        $scope.teklif.boyu.value = boyu.value;
                        $scope.teklif.boyu.text = boyu.text;
                        $scope.teklif.boyu.isValid = true;
                        $scope.teklif.boyu.cls = '';
                        $scope.teklif.boyu.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.teklif.boyu.value);
                        if (applyCheck || $scope.teklif.boyu.isValid !== check) {
                            $scope.teklif.boyu.isValid = check;
                            if ($scope.teklif.boyu.isValid) {
                                $scope.teklif.boyu.success();
                            } else {
                                $scope.teklif.boyu.error('* Geçersiz Boy !');
                            }
                        }
                        return $scope.teklif.boyu.isValid;
                    },
                    change: function () {
                        $scope.teklif.boyu.check($scope.teklif.boyu.value);
                    },
                    success: function () {
                        $scope.teklif.boyu.cls = '';
                        $scope.teklif.boyu.msg = '';
                    },
                    error: function (msg) {
                        $scope.teklif.boyu.cls = 'has-error';
                        $scope.teklif.boyu.msg = msg;
                    }
                },
                init: function () {
                    $scope.teklif.cap.init();
                    $scope.teklif.boyu.init();
                },
                get: function (data) {
                    $scope.teklif.getdata = data;
                },
                check: function () {
                    var applyCheck = true;
                    var OK = {
                        cap: $scope.teklif.cap.check(applyCheck),
                        boyu: $scope.teklif.boyu.check(applyCheck)
                    };
                    return OK.cap && OK.boyu;
                },
                save: function () {
                    if (!$scope.teklif.check()) {
                        return;
                    }
                    app.show.spinner($('mil-agirlik-modal > div:first > div:first'));
                    // Mil Ağırlığı = + Çap * Çap * 0.617 * Boyu / 100000
                    $scope.teklif.milagirlik = + ($scope.teklif.cap.value * $scope.teklif.cap.value * 0.617 * $scope.teklif.boyu.value) / 100000;

                    var num = $scope.teklif.milagirlik;
                    var with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0] // ondalıklı sayıyı yuvarlamadan eşitle.
                    $scope.teklif.milagirlik = with2Decimals.replace(".", ",");

                    localStorage.setItem("milagirlik", JSON.stringify($scope.teklif.milagirlik));
                    console.log("$scope.teklif.milagirlik  --->", $scope.teklif.milagirlik);

                    $('.mil-agirlik-modal:first').modal('hide');

                    $rootScope.control = true;
                    if ($scope.teklif.getdata === 'create-boru') { //create-modal dan gelen data
                        $rootScope.boruagirlik();
                    }
                    else {
                        $rootScope.boruagirlik2();
                    }
                    $scope.teklif.init();
                },
            };
        }]
    })

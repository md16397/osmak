﻿app.angular
    .controller('profileComponentCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
        $scope.profile = {
            rawdata: null,
            list: [],
            isExist: false,
            id: {
                value: 0,
                init: function () {
                    $scope.profile.id.set(0);
                },
                set: function (value) {
                    $scope.profile.id.value = value;
                }
            },
            password: {
                value: '',
                init: function () {
                    $scope.profile.password.set('');
                },
                set: function (value) {
                    $scope.profile.password.value = value;
                }
            },
            username: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.profile.username.isValid = false;
                    $scope.profile.username.cls = '';
                    $scope.profile.username.msg = '';
                    $scope.profile.username.set('');
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.profile.username.value);
                    if (applyCheck || $scope.profile.username.isValid !== check) {
                        $scope.profile.username.isValid = check;
                        if ($scope.profile.username.isValid) {
                            $scope.profile.username.success();
                        } else {
                            $scope.profile.username.error(systemMessages[707][document.documentElement.lang.toUpperCase()]);//'* Geçersiz Kullanıcı Adı'
                        }
                    }
                    return $scope.profile.username.isValid;
                },
                change: function () {
                    $scope.profile.username.check();
                },
                success: function () {
                    $scope.profile.username.cls = '';
                    $scope.profile.username.msg = '';
                },
                error: function (msg) {
                    $scope.profile.username.cls = 'has-error';
                    $scope.profile.username.msg = msg;
                },
                set: function (value) {
                    $scope.profile.username.value = value;
                    $scope.profile.username.isValid = true;
                    $scope.profile.username.cls = '';
                    $scope.profile.username.msg = '';
                }
            },
            name: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.profile.name.isValid = false;
                    $scope.profile.name.cls = '';
                    $scope.profile.name.msg = '';
                    $scope.profile.name.set('');
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.profile.name.value);
                    if (applyCheck || $scope.profile.name.isValid !== check) {
                        $scope.profile.name.isValid = check;
                        if ($scope.profile.name.isValid) {
                            $scope.profile.name.success();
                        } else {
                            $scope.profile.name.error(systemMessages[708][document.documentElement.lang.toUpperCase()]);//'* Geçersiz Ad'
                        }
                    }
                    return $scope.profile.name.isValid;
                },
                change: function () {
                    $scope.profile.name.check();
                },
                success: function () {
                    $scope.profile.name.cls = '';
                    $scope.profile.name.msg = '';
                },
                error: function (msg) {
                    $scope.profile.name.cls = 'has-error';
                    $scope.profile.name.msg = msg;
                },
                set: function (value) {
                    $scope.profile.name.value = value;
                    $scope.profile.name.isValid = true;
                    $scope.profile.name.cls = '';
                    $scope.profile.name.msg = '';
                }
            },
            surname: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.profile.surname.isValid = false;
                    $scope.profile.surname.cls = '';
                    $scope.profile.surname.msg = '';
                    $scope.profile.surname.set('');
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.profile.surname.value);
                    if (applyCheck || $scope.profile.surname.isValid !== check) {
                        $scope.profile.surname.isValid = check;
                        if ($scope.profile.surname.isValid) {
                            $scope.profile.surname.success();
                        } else {
                            $scope.profile.surname.error(systemMessages[709][document.documentElement.lang.toUpperCase()]);//'* Geçersiz Soyad'
                        }
                    }
                    return $scope.profile.surname.isValid;
                },
                change: function () {
                    $scope.profile.surname.check();
                },
                success: function () {
                    $scope.profile.surname.cls = '';
                    $scope.profile.surname.msg = '';
                },
                error: function (msg) {
                    $scope.profile.surname.cls = 'has-error';
                    $scope.profile.surname.msg = msg;
                },
                set: function (value) {
                    $scope.profile.surname.value = value;
                    $scope.profile.surname.isValid = true;
                    $scope.profile.surname.cls = '';
                    $scope.profile.surname.msg = '';
                }
            },
            language: {
                selected: {
                    value: 'tr',
                    text: 'Türkçe',
                    flag: 'assets/images/flags/tr.png'
                },
                text: {
                    tr: 'Türkçe',
                    ar: 'عربى'
                },
                flag: {
                    tr: 'assets/images/flags/tr.png',
                    ar: 'assets/images/flags/sy.png'
                },
                init: function () {
                    $scope.profile.language.set(document.documentElement.lang);
                },
                set: function (value) {
                    if (value === 'tr' || value === 'ar') {
                        $scope.profile.language.selected.value = value;
                        $scope.profile.language.selected.text = $scope.profile.language.text[value];
                        $scope.profile.language.selected.flag = $scope.profile.language.flag[value];
                    }

                },
                change: function (value) {
                    if (value !== 'tr' && value !== 'ar') {
                        app.show.error(systemMessages[710][$scope.profile.language.selected.value.toUpperCase()]);//'Bir şeyler ters gitti!'
                        return;
                    }
                    document.documentElement.lang = value;
                    createCookie('lang', value, 30);
                    $scope.profile.language.set(value);
                    $rootScope.changeModifyModalLanguage(value);
                }
            },
            type: {
                value: '',
                init: function () {
                    $scope.profile.type.set('');
                },
                set: function (value) {
                    $scope.profile.type.value = value;
                }
            },
            startpage: {
                value: '',
                init: function () {
                    $scope.profile.startpage.set('');
                },
                set: function (value) {
                    $scope.profile.startpage.value = value;
                }
            },
            ipaddress: {
                value: '',
                init: function () {
                    $scope.profile.ipaddress.set('');
                },
                set: function (value) {
                    $scope.profile.ipaddress.value = value;
                }
            },
            createdate: {
                value: '',
                init: function () {
                    $scope.profile.createdate.set('');
                },
                set: function (value) {
                    $scope.profile.createdate.value = value;
                }
            },
            createuser: {
                value: '',
                init: function () {
                    $scope.profile.createuser.set('');
                },
                set: function (value) {
                    $scope.profile.createuser.value = value;
                }
            },
            modifydate: {
                value: '',
                init: function () {
                    $scope.profile.modifydate.set('');
                },
                set: function (value) {
                    $scope.profile.modifydate.value = value;
                }
            },
            modifyuser: {
                value: '',
                init: function () {
                    $scope.profile.modifyuser.set('');
                },
                set: function (value) {
                    $scope.profile.modifyuser.value = value;
                }
            },
            set: function (profileData) {
                console.log(profileData);
                $scope.profile.rawdata = profileData;
                $scope.profile.id.set(profileData.ID);
                $scope.profile.username.set(profileData.USERNAME);
                $scope.profile.name.set(profileData.NAME);
                $scope.profile.surname.set(profileData.SURNAME);
                $scope.profile.language.set(profileData.LANGUAGEVAL);
                $scope.profile.type.set(profileData.TYPETXT);
                $scope.profile.startpage.set(profileData.STARTPAGETXT);
                $scope.profile.ipaddress.set(profileData.IP);
                $scope.profile.password.set(profileData.PASSWORD);
                $scope.profile.createdate.set(profileData.CREATEDATETXT);
                $scope.profile.createuser.set(profileData.CREATEUSER);
                $scope.profile.modifydate.set(profileData.MODIFYDATETXT);
                $scope.profile.modifyuser.set(profileData.MODIFYUSER);
            },
            check: function () {
                var applyCheck = true;
                var OK = {
                    username: $scope.profile.username.check(applyCheck),
                    name: $scope.profile.name.check(applyCheck),
                    surname: $scope.profile.surname.check(applyCheck)
                };
                return OK.username && OK.name && OK.surname;
            },
            load: function () {
                app.show.spinner($('profile-component > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/user.ashx?&t=1&d=' + new Date(),
                    data: {

                    }
                }).then(function (response) {
                    app.hide.spinner($('profile-component > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error(systemMessages[710][document.documentElement.lang.toUpperCase()]);//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//unit is not exist or passive
                        window.location.href = 'login.aspx';
                        return;
                    }
                    $scope.profile.set(response.data);
                    //console.log(response.data);
                }, function (response) {
                    console.log(response);
                    app.show.error(systemMessages[710][document.documentElement.lang.toUpperCase()]);//'Bir şeyler ters gitti...'
                    app.hide.spinner($('profile-component > div:first'));
                });
            },
            show: {
                update: function (user) {
                    $rootScope.showModifyModal($scope.profile.rawdata);

                }
            },
            save: function () {
                if (!$scope.profile.check()) {
                    return;
                }
                app.show.spinner($('profile-component > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/user.ashx?&t=6&d=' + new Date(),
                    data: {
                        id: $scope.profile.id.value,
                        username: $scope.profile.username.value,
                        surname: $scope.profile.surname.value,
                        name: $scope.profile.name.value,
                        password: $scope.profile.password.value,
                        language: $scope.profile.language.selected.value
                    }
                }).then(function (response) {
                    app.hide.spinner($('profile-component > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error(systemMessages[710][document.documentElement.lang.toUpperCase()]);//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 1) {//success
                        $scope.profile.set(response.data);
                        //$rootScope.modifyRow(response.data);
                        //$scope.profile.init();
                        app.show.success(systemMessages[711][document.documentElement.lang.toUpperCase()]);//'Değişiklikler Kaydedildi!'
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                        window.location.href = 'login.aspx';
                        return;
                    }
                    if (response.data['RESULT'] === 9) {//Username is invalid
                        $scope.profile.username.set($scope.profile.rawdata.USERNAME);
                        $scope.profile.username.isValid = false;
                        $scope.profile.username.error(systemMessages[707][document.documentElement.lang.toUpperCase()]);//'* Geçersiz Kullanıcı Adı'
                        return;
                    }
                    if (response.data['RESULT'] === 10) {//Name is invalid
                        $scope.profile.name.set($scope.profile.rawdata.NAME);
                        $scope.profile.name.isValid = false;
                        $scope.profile.name.error(systemMessages[708][document.documentElement.lang.toUpperCase()]);//'* Geçersiz Ad'
                        return;
                    }
                    if (response.data['RESULT'] === 11) {//Surname is invalid
                        $scope.profile.surname.set($scope.profile.rawdata.SURNAME);
                        $scope.profile.surname.isValid = false;
                        $scope.profile.surname.error(systemMessages[709][document.documentElement.lang.toUpperCase()]);//'* Geçersiz Soyad'
                        return;
                    }
                    //if running code come through this far then it means an unexpected error occurred
                    app.show.error(systemMessages[710][document.documentElement.lang.toUpperCase()]);//'Bir şeyler ters gitti...'
                    /*if (response.data['RESULT'] === 2) {
                        app.show.error(systemMessages[710][document.documentElement.lang.toUpperCase()]);//'Bir şeyler ters gitti...'
                        return;
                    }*/
                }, function (response) {
                    console.log(response);
                    app.show.error(systemMessages[710][document.documentElement.lang.toUpperCase()]);//'Bir şeyler ters gitti...'
                    app.hide.spinner($('profile-component > div:first'));
                });
            },

            systemMessages: systemMessages
        };
        $scope.profile.load();
    }])
    .directive('profileComponent', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/profile-component.html'
        };
    });
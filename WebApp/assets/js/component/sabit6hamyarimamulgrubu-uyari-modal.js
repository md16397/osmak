﻿app.angular
    .component('sabit6hamyarimamulgrubuUyariModal', {
        templateUrl: 'assets/template/sabit6hamyarimamulgrubu-uyari-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
         
            $rootScope.showSabit6hammaddeyarimamulgrubuUyariModal = function (teklifcreatemodal, teklifmodifymodal) {
                $scope.savemimodifymi = teklifcreatemodal;
                $scope.teklifmodifymodal = teklifmodifymodal;
                console.log($scope.savemimodifymi);
                console.log($scope.teklifmodifymodal);
                $('.sabit6hamyarimamulgrubu-uyari-modal:first').modal({
                    backdrop: 'static',
                    keyboard: true
                }); 
            };
            $scope.teklif = {
                save: function () {
                    if ($scope.teklifmodifymodal == 'teklif-modify-modal-modifyfunction') {
                        console.log("teklif-modify-modaldan gelen modify function ise :");
                        app.show.spinner($('teklif-modify-modal > div:first > div:first'));
                        $http({
                            method: 'POST',
                            url: 'hub/teklif.ashx?&t=4&d=' + new Date(),
                            data: {
                                teklifid: JSON.parse(localStorage.getItem("teklifmodifymodal-id")), 
                                teklifmaliyetid: JSON.parse(localStorage.getItem("teklifmodifymodal-maliyetid")),
                                cari: JSON.parse(localStorage.getItem("teklifmodifymodal-cari")), 
                                teklifkodu: JSON.parse(localStorage.getItem("teklifmodifymodal-teklifkodu")),
                                toplamtekliftutari: JSON.parse(localStorage.getItem("teklif-modify-modal-toplamtekliftutari")),
                                onaydurumu: JSON.parse(localStorage.getItem("teklif-modify-modal-onaydurumu")), 
                                aciklama: JSON.parse(localStorage.getItem("teklif-modify-modal-aciklama")), 
                                teklifadi: JSON.parse(localStorage.getItem("teklif-modify-modal-teklifurunadi")),  
                                teklifadiolcusu: JSON.parse(localStorage.getItem("teklif-modify-modal-olcu")), 
                                birimsatisfiyati: JSON.parse(localStorage.getItem("teklif-modify-modal-birimsatisfiyati")),
                                adet: JSON.parse(localStorage.getItem("teklif-modify-modal-adet")), 
                                karyuzdesi: JSON.parse(localStorage.getItem("teklif-modify-modal-karyuzdesi")), 
                                toplamsatisfiyati: JSON.parse(localStorage.getItem("teklifmodifymodal-toplamsatisfiyati")),
                                maliyettekliftutari: JSON.parse(localStorage.getItem("teklif-modify-modal-toplamtekliftutari")),
                                detail: JSON.parse(localStorage.getItem("teklif-modify-modal-detail"))
                            }  
                        }).then(function (response) {
                            app.hide.spinner($('teklif-modify-modal > div:first > div:first'));
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');
                                return;
                            }
                            if (response.data['RESULT'] === 1) {//success
                                app.show.success('Teklif-Maliyet bilgileri Güncellendi!');
                                console.log(response.data);
                                localStorage.setItem("teklifdata", JSON.stringify(response.data)); 
                                Runopen3 = $rootScope.run3;
                                Runopen3();
                                $scope.id = response.data.TEKLIFLERID;
                                localStorage.removeItem('boruagirlik');  //
                                $('.sabit6hamyarimamulgrubu-uyari-modal:first').modal('hide');// modal kapanır.
                                $('.teklif-modify-modal:first').modal('hide'); // modal kapanır.
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                                window.location.href = 'login.aspx';
                                return;
                            }
                            //if running code come through this far then it means an unexpected error occurred
                            app.show.error('Bir şeyler ters gitti...');
                        }, function (response) {
                            app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                            app.hide.spinner($('teklif-modify-modal > div:first > div:first'));
                        });
                        return;
                    }
                    if ($scope.savemimodifymi == 'teklif-create-modal-save') { //teklif-create-modaldan gelen save function ise :
                        console.log("teklif-create-modaldan gelen save function ise :");
                        console.log("toplamtekliftutari :", JSON.parse(localStorage.getItem("teklif-create-modal-toplamtekliftutari")));
                        app.show.spinner($('sabit6hamyarimamulgrubu-uyari-modal > div:first > div:first'));
                        $http({
                            method: 'POST',
                            url: 'hub/teklif.ashx?&t=3&d=' + new Date(), // TYPE 3
                            data: {
                                cari: JSON.parse(localStorage.getItem("cari")),
                                tekliftarihi: JSON.parse(localStorage.getItem("tekliftarihi")),
                                teklifkodu: JSON.parse(localStorage.getItem("teklifkodu")),
                                toplamtekliftutari: JSON.parse(localStorage.getItem("teklif-create-modal-toplamtekliftutari")),
                                aciklama: JSON.parse(localStorage.getItem("aciklama")),
                                teklifadi: JSON.parse(localStorage.getItem("teklif-create-modal-teklifurunadi-TEKLIFURUNLERID")),
                                teklifadiolcusu: JSON.parse(localStorage.getItem("teklif-create-modal-teklifadiolcusu")),
                                birimsatisfiyati: JSON.parse(localStorage.getItem("teklif-create-modal-birimsatisfiyati")),
                                adet: JSON.parse(localStorage.getItem("teklif-create-modal-adet")),
                                karyuzdesi: JSON.parse(localStorage.getItem("teklif-create-modal-karyuzdesi")),
                                toplamsatisfiyati: JSON.parse(localStorage.getItem("teklif-create-modal-toplamsatisfiyati")),
                                maliyettekliftutari: JSON.parse(localStorage.getItem("teklif-create-modal-toplamtekliftutari")),
                                detail: JSON.parse(localStorage.getItem("teklif-create-modal-detail"))
                            }
                        }).then(function (response) {
                            app.hide.spinner($('sabit6hamyarimamulgrubu-uyari-modal > div:first > div:first'));
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');
                                return;
                            }
                            if (response.data['RESULT'] === 1) { // veritabanından gelen RESULT değeri 1 ise kaydedilmiştir.
                                $rootScope.teklifcreate = true;
                                console.log(response.data);
                                localStorage.setItem("sabit6hamyarimamul-uyari-modal-teklifkodu", JSON.stringify(response.data.TEKLIFKODU));
                                app.show.success('Yeni Teklif Oluşturuldu!'); // success modalı.
                                //localStorage.removeItem('teklifdata');
                                $rootScope.control = true;
                                localStorage.setItem("teklifdata", JSON.stringify(response.data));
                                Runopen3 = $rootScope.run3; // teklif-create-component.js de ki function burada çalıştırılır. Yeni eklenen kaydı teklif-create-componentte listelemek için.
                                Runopen3();
                                $rootScope.teklifidExist = response.data.TEKLIFLERID;
                                $scope.id = response.data.TEKLIFLERID;
                                $rootScope.teklifcreatemodalId = $scope.id;
                                console.log($rootScope.teklifcreatemodalId);
                                localStorage.setItem("sabit6hamyarimamul-uyari-modal-id", JSON.stringify($scope.id));
                                $rootScope.index = $scope.index;
                                $('.sabit6hamyarimamulgrubu-uyari-modal:first').modal('hide'); // modal kapanır.
                                $('.teklif-create-modal:first').modal('hide'); // modal kapanır.
                                //
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                                window.location.href = 'login.aspx';
                                return;
                            }
                            if (response.data['RESULT'] === 124) {
                                app.show.warning('UYARI', 'Bu Teklif daha önce oluşturuldu.');
                                return;
                            }
                            app.show.error('Bir şeyler ters gitti...');
                        }, function (response) {
                            app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                            app.hide.spinner($('sabit6hamyarimamulgrubu-uyari-modal > div:first > div:first'));
                        });
                        return;
                    }
                    else { //teklif-create-modaldan gelen modify function ise :
                        console.log("teklif-create-modaldan gelen modify function ise :");
                        if ($scope.id == null) {
                            $scope.id = $rootScope.teklifcreatemodalId;
                            console.log("$rootScope.teklifcreatemodalId ->", $rootScope.teklifcreatemodalId);
                        }
                        app.show.spinner($('sabit6hamyarimamulgrubu-uyari-modal > div:first > div:first'));
                        $http({
                            method: 'POST',
                            url: 'hub/teklif.ashx?&t=8&d=' + new Date(), // TYPE 8
                            data: { 
                                teklifid: $scope.id,
                                cari: JSON.parse(localStorage.getItem("cari")),
                                tekliftarihi: JSON.parse(localStorage.getItem("tekliftarihi")),
                                teklifkodu: JSON.parse(localStorage.getItem("teklif-create-component-teklifkodu")), 
                                toplamtekliftutari: JSON.parse(localStorage.getItem("teklif-create-modal-toplamtekliftutari")),
                                aciklama: JSON.parse(localStorage.getItem("aciklama")),
                                teklifadi: JSON.parse(localStorage.getItem("teklif-create-modal-teklifurunadi-TEKLIFURUNLERID")),
                                teklifadiolcusu: JSON.parse(localStorage.getItem("teklif-create-modal-teklifadiolcusu")),
                                birimsatisfiyati: JSON.parse(localStorage.getItem("teklif-create-modal-birimsatisfiyati")),
                                adet: JSON.parse(localStorage.getItem("teklif-create-modal-adet")),
                                karyuzdesi: JSON.parse(localStorage.getItem("teklif-create-modal-karyuzdesi")),
                                onaydurumu: false,
                                toplamsatisfiyati: JSON.parse(localStorage.getItem("teklif-create-modal-toplamsatisfiyati")),
                                maliyettekliftutari: JSON.parse(localStorage.getItem("teklif-create-modal-toplamtekliftutari")),
                                detail: JSON.parse(localStorage.getItem("teklif-create-modal-detail"))
                            }
                        }).then(function (response) {
                            app.hide.spinner($('sabit6hamyarimamulgrubu-uyari-modal > div:first > div:first'));
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');
                                return;
                            }
                            if (response.data['RESULT'] === 1) {
                                $rootScope.control = true;
                                localStorage.setItem("teklifdata", JSON.stringify(response.data));
                                console.log(response.data);
                                localStorage.setItem("sabit6hamyarimamul-uyari-modal-teklifkodu", JSON.stringify(response.data.TEKLIFKODU));
                                app.show.success('Teklif-Maliyet Bilgileri Eklendi!');
                                Runopen3 = $rootScope.run3;
                                Runopen3();
                                $rootScope.teklifidExist = response.data.TEKLIFLERID;
                                $scope.id = response.data.TEKLIFLERID;
                                $rootScope.teklifcreatemodalId = $scope.id;
                                console.log($rootScope.teklifcreatemodalId);
                                localStorage.setItem("sabit6hamyarimamul-uyari-modal-id", JSON.stringify($scope.id));
                                $('.sabit6hamyarimamulgrubu-uyari-modal:first').modal('hide');
                                $('.teklif-create-modal:first').modal('hide'); // modal kapanır.
                                //
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                                window.location.href = 'login.aspx';
                                return;
                            }
                            app.show.error('Bir şeyler ters gitti...');
                        }, function (response) {
                            app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                                app.hide.spinner($('sabit6hamyarimamulgrubu-uyari-modal > div:first > div:first'));
                        });
                        return;
                    }
                }
            }
        }]
    })
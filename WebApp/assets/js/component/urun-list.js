﻿app.angular
    .controller('urunListCtrl', ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
        $rootScope.modifyRow = function (_urun) {
            var i = 0;
            var goon = true;
            while (goon && $scope.urun.list.length > i) {
                if (_urun.HAMMADDEYARIMAMULID === $scope.urun.list[i].HAMMADDEYARIMAMULID) {
                    goon = false;
                    $scope.urun.list[i] = _urun;
                }
                i++;
            }
        };
        $rootScope.loadList = function () {
            $scope.urun.filter.init();
            $scope.urun.page.index = 1;
            $scope.urun.load();
        };
        $scope.urun = {
            page: {
                index: 1
            },
            filter: {
                type: 1, //this filter type is for page selecting = GET ALL LIST
                value: '',
                init: function () {
                    $scope.urun.filter.value = '';
                },
                change: function () {
                    $scope.urun.filter.timer.set();
                },
                timer: {
                    value: null,
                    interval: 500,
                    set: function () {
                        if ($scope.urun.filter.timer.value) {
                            $timeout.cancel($scope.urun.filter.timer.value);
                            $scope.urun.filter.timer.value = null;
                        }
                        $scope.urun.filter.timer.value = $timeout(function () {
                            if ($scope.urun.isLoading) {
                                return;
                            }
                            $scope.urun.page.index = 1;
                            $scope.urun.load();
                        }, $scope.urun.filter.timer.interval);
                    }
                }
            },
            totalRow: {
                value: 0,
                set: function (value) {
                    var paginate = $scope.urun.totalRow.value !== value;
                    $scope.urun.totalRow.value = value;
                    if (paginate) {
                        $scope.urun.paginate();
                    }
                }
            },
            maxVisibleRow: {
                value: app.pagination.maxVisible.row,
                set: function (value) {
                    var paginate = $scope.urun.maxVisibleRow.value !== value;
                    $scope.urun.maxVisibleRow.value = value;
                    if (paginate) {
                        $scope.urun.paginate();
                    }
                }
            },
            ngshow: {
                value: 0,
                set: function (value) {
                    $scope.urun.ngshow.value = value;
                }
            },
            list: [],
            isExist: false,
            load: function () {
                $scope.urun.isLoading = true;
                app.show.spinner($('urun-list > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/urun.ashx?&t=2&d=' + new Date(),
                    data: {
                        pageIndex: $scope.urun.page.index,
                        filterType: $scope.urun.filter.type,
                        filter: $scope.urun.filter.value,
                        pageRowCount: $scope.urun.maxVisibleRow.value
                    }
                }).then(function (response) {
                    $scope.urun.isLoading = false;
                    app.hide.spinner($('urun-list > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or user is passive
                        window.location.href = 'login.aspx';
                        return;
                    }
                    $scope.urun.list = response.data.LIST;
                    $scope.urun.isExist = $scope.urun.list.length > 0;
                    $scope.urun.totalRow.set(response.data.TOTALROW);
                }, function (response) {
                    $scope.urun.isLoading = false;
                    console.log(response);
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                    app.hide.spinner($('urun-list > div:first'));
                });
            },
            show: {
                update: function (_urun) {
                    console.log(_urun);
                    $rootScope.showModifyModal(_urun);
                },
                delete: function (_urun) {
                    console.log(_urun);
                    $rootScope.showDeleteModal(_urun);
                }
            },
            paginate: function () {
                var pageCount = $scope.urun.totalRow.value !== 0 ? Math.ceil($scope.urun.totalRow.value / $scope.urun.maxVisibleRow.value) : 1;
                $('.bootpag-flat').bootpag({
                    total: pageCount,
                    maxVisible: app.pagination.maxVisible.pageIndex,
                    page: $scope.urun.page.index,
                    leaps: false
                }).on("page", function (event, num) {
                    $scope.urun.page.index = num;
                    $scope.urun.load();
                }).children('.pagination').addClass('pagination-flat pagination-sm');
            }
        };
        $scope.urun.load();
    }])
    .directive('urunList', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/urun-list.html'
        };
    });
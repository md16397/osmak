﻿app.angular
    .controller('userModifyModalCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
     
        $rootScope.showModifyModal = function (userData) {
            $scope.user.set(userData);
            $('.user-modify-modal:first').modal({
                backdrop: 'static'
            });
        };
        $scope.user = {
            rawdata: null,
            id: {
                value: 0,
                init: function () {
                    $scope.user.id.set(0);
                },
                set: function (value) {
                    $scope.user.id.value = value;
                }
            },
            username: {
                value: '',
                init: function () {
                    $scope.user.username.set('');
                },
                set: function (value) {
                    $scope.user.username.value = value;
                }
            },
            type: {
                list: page.data.usertypes,
                isValid: false,
                selected: null,
                cls: '',
                msg: '',
                init: function () {
                    $scope.user.type.isValid = false;
                    $scope.user.type.selected = null;
                    $scope.user.type.cls = '';
                    $scope.user.type.msg = '';
                },
                select: function (item) {
                    $scope.user.type.isValid = true;
                    $scope.user.type.selected = item;
                    $scope.user.type.cls = '';
                    $scope.user.type.msg = '';
                },
                set: function (value) {
                    var goon = true;
                    var i = 0;
                    while (goon && i < $scope.user.type.list.length) {
                        if ($scope.user.type.list[i].value === value) {
                            $scope.user.type.select($scope.user.type.list[i]);
                            goon = false;
                        }
                        i++;
                    }
                    if (goon) {
                        $scope.user.type.init();
                    }
                },
                check: function (applyCheck) {
                    var check = $scope.user.type.selected && $scope.user.type.selected.hasOwnProperty('value') && typeof $scope.user.type.selected.value !== 'undefined' && $scope.user.type.selected.value != null ? true : false;
                    if (applyCheck || $scope.user.type.isValid !== check) {
                        $scope.user.type.isValid = check;
                        if ($scope.user.type.isValid) {
                            $scope.user.type.success();
                        } else {
                            $scope.user.type.error('* Geçersiz Kullanıcı Tipi');//'* Geçersiz Kullanıcı Tipi'
                        }
                    }
                    return $scope.user.type.isValid;
                },
                change: function () {
                    $scope.user.type.check();
                    $scope.user.startpage.filter();
                },
                success: function () {
                    $scope.user.type.cls = '';
                    $scope.user.type.msg = '';
                },
                error: function (msg) {
                    $scope.user.type.cls = 'has-error';
                    $scope.user.type.msg = msg;
                }
            },
            name: {
                value: '',
                init: function () {
                    $scope.user.name.set('');
                },
                set: function (value) {
                    $scope.user.name.value = value;
                }
            },
            surname: {
                value: '',
                init: function () {
                    $scope.user.surname.set('');
                },
                set: function (value) {
                    $scope.user.surname.value = value;
                }
            },
            startpage: {
                filter: function () {
                    $scope.user.startpage.list = page.fn.startpage.filter($scope.user.type.selected);
                    $scope.user.startpage.init();
                },
                list: [],
                isValid: false,
                selected: null,
                cls: '',
                msg: '',
                init: function () {
                    $scope.user.startpage.selected = null;
                    $scope.user.startpage.isValid = false;
                    $scope.user.startpage.cls = '';
                    $scope.user.startpage.msg = '';
                },
                select: function (item) {
                    $scope.user.startpage.isValid = true;
                    $scope.user.startpage.selected = item;
                    $scope.user.startpage.cls = '';
                    $scope.user.startpage.msg = '';
                },
                set: function (value) {
                    var goon = true;
                    var i = 0;
                    while (goon && i < $scope.user.startpage.list.length) {
                        if ($scope.user.startpage.list[i].value === value) {
                            $scope.user.startpage.select($scope.user.startpage.list[i]);
                            goon = false;
                        }
                        i++;
                    }
                    if (goon) {
                        $scope.user.startpage.init();
                    }
                },
                check: function (applyCheck) {
                    var check = $scope.user.startpage.selected && $scope.user.startpage.selected.hasOwnProperty('value') && typeof $scope.user.startpage.selected.value !== 'undefined' && $scope.user.startpage.selected.value != null ? true : false;
                    if (applyCheck || $scope.user.startpage.isValid !== check) {
                        $scope.user.startpage.isValid = check;
                        if ($scope.user.startpage.isValid) {
                            $scope.user.startpage.success();
                        } else {
                            $scope.user.startpage.error('* Geçersiz Başlangıç Sayfası');//'* Geçersiz Başlangıç Sayfası'
                        }
                    }
                    return $scope.user.startpage.isValid;
                },
                change: function () {
                    $scope.user.startpage.check();
                },
                success: function () {
                    $scope.user.startpage.cls = '';
                    $scope.user.startpage.msg = '';
                },
                error: function (msg) {
                    $scope.user.startpage.cls = 'has-error';
                    $scope.user.startpage.msg = msg;
                }
            },
            password: {
                value: '',
                init: function () {
                    $scope.user.password.set('');
                },
                set: function (value) {
                    $scope.user.password.value = value;
                }
            },
            failLoginCount: {
                isValid: false,
                value: 0,
                cls: '',
                msg: '',
                increase: function () {
                    if (!$scope.user.failLoginCount.isValid) {
                        $scope.user.failLoginCount.set(0);
                        return;
                    }
                    $scope.user.failLoginCount.value++;
                },
                decrease: function () {
                    if (!$scope.user.failLoginCount.isValid) {
                        $scope.user.failLoginCount.set(0);
                        return;
                    }
                    if ($scope.user.failLoginCount.value === 0) {
                        return;
                    }
                    $scope.user.failLoginCount.value--;
                },
                init: function () {
                    $scope.user.failLoginCount.isValid = false;
                    $scope.user.failLoginCount.value = 0;
                    $scope.user.failLoginCount.cls = '';
                    $scope.user.failLoginCount.msg = '';
                },
                set: function (value) {
                    $scope.user.failLoginCount.isValid = true;
                    $scope.user.failLoginCount.value = value;
                    $scope.user.failLoginCount.cls = '';
                    $scope.user.failLoginCount.msg = '';
                },
                check: function (applyCheck) {
                    var check = app.valid.positiveInteger($scope.user.failLoginCount.value);
                    if (applyCheck || $scope.user.failLoginCount.isValid !== check) {
                        $scope.user.failLoginCount.isValid = check;
                        if ($scope.user.failLoginCount.isValid) {
                            $scope.user.failLoginCount.success();
                        } else {
                            $scope.user.failLoginCount.error('* Geçersiz Hatalı Giriş Sayısı');//'* Geçersiz Hatalı Giriş Sayısı'
                        }
                    }
                    return $scope.user.failLoginCount.isValid;
                },
                change: function () {
                    $scope.user.failLoginCount.check();
                },
                success: function () {
                    $scope.user.failLoginCount.cls = '';
                    $scope.user.failLoginCount.msg = '';
                },
                error: function (msg) {
                    $scope.user.failLoginCount.cls = 'has-error';
                    $scope.user.failLoginCount.msg = msg;
                }
            },
            state: {
                name: 'Aktif',
                value: true,
                icon: {
                    cls: 'icon-user-check text-success'
                },
                init: function () {
                    $scope.user.state.select($scope.user.state.active);
                },
                select: function (state) {
                    $scope.user.state.name = state.name;
                    $scope.user.state.value = state.value;
                    $scope.user.state.icon.cls = state.icon.cls.replace(/ pull-right/g, '');
                },
                set: function (value) {
                    $scope.user.state.select(value ? $scope.user.state.active : $scope.user.state.passive);
                },
                active: {
                    name: 'Aktif',
                    value: true,
                    icon: {
                        cls: 'icon-user-check text-success'
                    },
                    anchor: {
                        cls: ''
                    }
                },
                passive: {
                    name: 'Pasif',
                    value: false,
                    icon: {
                        cls: 'icon-user-block text-danger'
                    },
                    anchor: {
                        cls: ''
                    }
                }
            },
            set: function (userData) {
                $scope.user.rawdata = userData;
                $scope.user.id.set(userData.ID);
                $scope.user.username.set(userData.USERNAME);
                $scope.user.type.set(userData.TYPEVAL);
                $scope.user.name.set(userData.NAME);
                $scope.user.surname.set(userData.SURNAME);
                $scope.user.startpage.filter();
                $scope.user.startpage.set(userData.STARTPAGEVAL);
                $scope.user.password.set(userData.PASSWORD);
                $scope.user.state.set(userData.ACTIVEVAL);
                $scope.user.failLoginCount.set(userData.FAILLOGINCOUNT);
            },
            init: function () {
                $scope.user.id.init();
                $scope.user.username.init();
                $scope.user.type.init();
                $scope.user.name.init();
                $scope.user.surname.init();
                $scope.user.startpage.init();
                $scope.user.password.init();
                $scope.user.state.init();
                $scope.user.failLoginCount.init();
                $scope.user.rawdata = null;
            },
            check: function () {
                var applyCheck = true;
                var OK = {
                    type: $scope.user.type.check(applyCheck),
                    failLoginCount: $scope.user.failLoginCount.check(applyCheck)
                };
                return OK.type && OK.failLoginCount;
            },
            modify: function () {
                if (!$scope.user.check()) {
                    return;
                }
                app.show.spinner($('user-modify-modal > div:first > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/user.ashx?&t=4&d=' + new Date(),
                    data: {
                        id: $scope.user.id.value,
                        username: $scope.user.username.value,
                        type: $scope.user.type.selected.value,
                        name: $scope.user.name.value,
                        surname: $scope.user.surname.value,
                        password: $scope.user.password.value,
                        passwordAgain: $scope.user.password.value,
                        state: $scope.user.state.value,
                        failLoginCount: $scope.user.failLoginCount.value
                    }
                }).then(function (response) {
                    app.hide.spinner($('user-modify-modal > div:first > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 1) {//success
                        $rootScope.modifyRow(response.data);
                        $scope.user.init();
                        $('.user-modify-modal:first').modal('hide');
                        app.show.success('Kullanıcı güncellendi!');//'Kullanıcı güncellendi!'
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6 || response.data['RESULT'] === 17) {//user is not exist or passive or user has no right to modify a user
                        window.location.href = 'login.aspx';
                        return;
                    }
                    //if running code come through this far then it means an unexpected error occurred
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                    /*if (response.data['RESULT'] === 9 || response.data['RESULT'] === 10 || response.data['RESULT'] === 11 || response.data['RESULT'] === 16 || response.data['RESULT'] === 2 || response.data['RESULT'] === 14 || response.data['RESULT'] === 12 || response.data['RESULT'] === 15) {//username or name or surname or type or password or company or startpage or language is invalid
                        app.show.error(systemMessages[351]);//'Bir şeyler ters gitti...'
                        return;
                    }*/
                }, function (response) {
                    console.log(response);
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                    app.hide.spinner($('user-modify-modal > div:first > div:first'));
                });
            }
        };
    }])
    .directive('userModifyModal', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/user-modify-modal.html'
        };
    });
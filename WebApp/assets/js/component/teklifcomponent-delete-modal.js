﻿app.angular
    .component('teklifcomponentDeleteModal', {
        templateUrl: 'assets/template/teklifcomponent-delete-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
         
            $rootScope.showDeleteModal = function (key, teklifid) {
                $scope.teklif.tekliflerid = teklifid;
                $scope.teklif.maliyetid = key;
                $('.teklifcomponent-delete-modal:first').modal({
                    backdrop: 'static',
                    keyboard: true
                });
            };
            $scope.teklif = {
                tekliflerid: 0,
                maliyetid: 0,
                delete: function () {
                    app.show.spinner($('teklifcomponent-delete-modal > div:first > div:first'));
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=11&d=' + new Date(), 
                        data: {
                            teklifid: $scope.teklif.tekliflerid,
                            teklifmaliyetid: $scope.teklif.maliyetid
                        }
                    }).then(function (response) {
                        app.hide.spinner($('teklifcomponent-delete-modal > div:first > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 1) {
                            $('.teklifcomponent-delete-modal:first').modal('hide');
                            app.show.success('Teklif-Maliyet Bilgileri Silindi !');
                            Runopen3 = $rootScope.run3;
                            Runopen3();
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                            window.location.href = 'login.aspx';
                            return;
                        }
                        //if running code come through this far then it means an unexpected error occurred
                        app.show.error('Bir şeyler ters gitti...');
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                        app.hide.spinner($('teklifcomponent-delete-modal > div:first > div:first'));
                    });
                }
            }
        }]
    })
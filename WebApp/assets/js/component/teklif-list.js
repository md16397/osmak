﻿app.angular
    .controller('teklifListCtrl', ['$scope', '$http', '$timeout', '$rootScope', '$localStorage', function ($scope, $http, $timeout, $rootScope, $localStorage) {
     // Admin giriş yaptıysa Teklif liste ekranında Onayla butonu aktif olmalı. Kullanıcı giriş yaptıysa Pasif olmalı.
            $http({
                method: 'POST',
                url: 'hub/user.ashx?&t=1&d=' + new Date(),
                data: {

                }
            }).then(function (response) {
                if (!response.data.hasOwnProperty('RESULT')) {
                    app.show.error('Bir şeyler ters gitti...');
                    return;
                }
                if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                    window.location.href = 'login.aspx';
                    return;
                }
                if (response.data.TYPEVAL === 0) {
                    $scope.teklif.usertype = true;
                }
            });
        $rootScope.modifyRow = function (_teklif) {
            var i = 0;
            var goon = true;
            while (goon && $scope.teklif.list.length > i) {
                if (_teklif.TEKLIFLERID === $scope.teklif.list[i].TEKLIFLERID) {
                    goon = false;
                    $scope.teklif.list[i] = _teklif;
                }
                i++;
            };
        };
        $rootScope.teklifs = {
            list2: [],
            list3: [],
            list4: []
        },
        $rootScope.loadList = function () {
            $scope.teklif.filter.init();
            $scope.teklif.page.index = 1;
            $scope.teklif.load();
         };
         $scope.image = [{
             src: 'assets/images/logo_entry.png',
         }];
        $scope.teklif = {
            list: [],
            tekliflist: [],
            detailslist: [],
            dovizlist: [],
            _teklifmaliyet: 0,
            dolareuro: [],
            usertype: false,
            tableToExcel: (function () {
                var uri = 'data:application/vnd.ms-excel;charset=UTF-8;base64,'
                    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                    , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                    , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

                return function (table, name, filename) {
                    var ctx = { worksheet: name || 'Worksheet', table: $scope.teklif.dataToInnerTable() };//table.innerHTML } //değiştirdim
                    var blobData = new Blob([format(template, ctx)], { type: 'application/vnd.ms-excel' });//ekledim
                    //var blobdata = new Blob([table], { type: 'text/csv' });
                    document.getElementById("dlink").href = uri + base64(format(template, ctx));// uri + base64(format(template, ctx));//değiştirdim
                    document.getElementById("dlink").download = filename;
                    document.getElementById("dlink").click();
                }
            })(),
            dataToInnerTable: function () {
                console.log($scope.teklif.tekliflist);
                var thead =
                    "<thead><tr><td><td><td><td><th style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 180px; background-color: #474866; color: white;\">" + "MÜŞTERİ ADI" +
                    "</th><th style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 170px;  background-color: #474866; color: white;\">" + "TOPLAM TEKLİF TUTARI" +
                    "</th><th style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 150px;  background-color: #474866; color: white;\">" + "TEKLİF KODU" +
                    "</th><th style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 110px;  background-color: #474866; color: white;\">" + "TEKLİF TARİHİ" +
                    "</th><th style=\"border: 1px solid black; text-align: center; font-size: 16px; width: 110px;  background-color: #474866; color: white;\">" + "AÇIKLAMA" +
                    "</th></td></td></td></td></tr></thead>";
                var tbody = "<tbody>";
                var i = 0;
                $scope.Osmaklogo = '<img src="http://osmaksistem.com/assets/images/logo_entry.png" width="190" height="75"';
                if (typeof $scope.teklif.tekliflist[i] !== 'undefined') {
                    var tr =
                        "<tr><td>" + $scope.Osmaklogo + 
                        "</td><td><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 18px; color: #00173C;\"><b>" + $scope.teklif.tekliflist[i].CARI +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.tekliflist[i].TOPLAMTEKLIFTUTARITXT + " ₺" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.tekliflist[i].TEKLIFKODU.replace(/,/g, "','") + "'" + // EXCEL teklif kodunu biçimlendiriyor. Bunu önlemek için  '  kullanıldı.
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.tekliflist[i].TEKLIFTARIHITXT +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + $scope.teklif.tekliflist[i].TEKLIFACIKLAMA +
                        "</b></td></td></td></td></td></tr>";
                    tbody += tr;
                }
                var tr = "<tr></tr>"
                tbody += tr;
                $scope.Tarih = "* Bu kurlar Merkez Bankasının " + localStorage.getItem("Tarih") + " tarihli verilerinden alınmıştır.";
                var tr =  // DOLAR ve EURO
                    "<tr><td><td><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "DOLAR ALIŞ" +
                    "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "DOLAR SATIŞ" +
                    "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "EURO ALIŞ" +
                    "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 14px;\"><b>" + "EURO SATIŞ" +
                    "</b></td><td style=\"font-size: 9px;\" rowspan='2'><b>" + $scope.Tarih +
                    "</b></td></td></td></td></td></tr>";
                tbody += tr;
                $scope.dolareuro = localStorage.getItem("dolareuro");
                $scope.DOLARALISFIYATI = parseFloat(localStorage.getItem("DOLARALISFIYATI")).toFixed(3);
                $scope.DOLARSATISFIYATI = parseFloat(localStorage.getItem("DOLARSATISFIYATI")).toFixed(3);
                $scope.EUROALISFIYATI = parseFloat(localStorage.getItem("EUROALISFIYATI")).toFixed(3);
                $scope.EUROSATISFIYATI = parseFloat(localStorage.getItem("EUROSATISFIYATI")).toFixed(3);
                $scope.Tarih = "* Bu kurlar Merkez Bankasının " + localStorage.getItem("Tarih") + " tarihli verilerinden alınmıştır.";

                console.log($scope.Tarih);
                if (typeof $scope.dolareuro !== 'undefined') {
                    console.log($scope.Tarih);
                    var tr =
                        "<tr><td><td><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.DOLARALISFIYATI + " ₺" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.DOLARSATISFIYATI + " ₺" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.EUROALISFIYATI + " ₺" +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 11px;\"><b>" + $scope.EUROSATISFIYATI + " ₺" +
                        "</b></td></td></td></td></td></tr>";
                    tbody += tr;
                }
                var tr = "<tr></tr>"
                tbody += tr;
                do {
                    for (var i = 0; (typeof $scope.teklif.detailslist[i] !== 'undefined'); i++) {
                        var tr =
                            "<tr><td><td><td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 180px; background-color: #9d9eab;\"><b>" + "ÜRÜN ADI" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 150px; background-color: #9d9eab;\"><b>" + "ÜRÜN ÖLÇÜSÜ" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "BİRİM MALİYET FİYATI" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "BİRİM MALİYET ADETİ" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "KAR YÜZDESİ" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 125px; background-color: #9d9eab;\"><b>" + "BİRİM SATIŞ FİYATI" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 14px; width: 160px; background-color: #9d9eab;\" colspan='2'><b>" + "TOPLAM TEKLİF TUTARI" +
                            "</b></td></td></td></td></tr>";
                        tbody += tr;
                        if (typeof $scope.teklif.detailslist[i].events[0] !== 'undefined') {
                            var tr =
                                "<tr><td><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 19px; color: #00173C;\"><b>" + $scope.teklif.detailslist[i].events[0].TEKLIFMALIYETADI +
                                "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 16px; color: #00173C;\"><b>" + $scope.teklif.detailslist[i].events[0].TEKLIFADIOLCUSU +
                                "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist[i].events[0].BIRIMSATISFIYATITXT + " ₺" +
                                "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist[i].events[0].ADETTXT +
                                "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist[i].events[0].KARYUZDESI + " %" +
                                "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\"><b>" + $scope.teklif.detailslist[i].events[0].TOPLAMSATISFIYATITXT + " ₺" +
                                "</b></td></td></td></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\" colspan='2'><b>" + $scope.teklif.detailslist[i].events[0].MALIYETTEKLIFTUTARITXT + " ₺" + "</b></td></tr>"
                            tbody += tr;
                        }
                        var tr =
                            "<tr><td><td><td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "HAM M./YARI M. GRUBU" +
                            "</b></td><td style=\"border: 1px solid black; text-align: center; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "ADI" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "MİKTAR" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "ADET" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "ÖLÇÜ BİRİMİ" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "LİSTE BİRİM FİYATI" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; width: 130px; background-color: #e6e6eb;\"><b>" + "GİRİLEN BİRİM FİYAT" +
                            "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px; background-color: #e6e6eb;\"><b>" + "TUTAR" +
                            "</b></td></td></td></td></tr>";
                        tbody += tr;
                        for (var j = 0; (typeof $scope.teklif.detailslist[i].events[j] !== 'undefined'); j++) {
                            var tr =
                                "<tr><td><td><td><td style=\"border: 1px solid black; text-align: center; font-size: 12px; width: 95px;\">" + $scope.teklif.detailslist[i].events[j].HAMMADDEYARIMAMULADI +
                                "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].HAMMADDEYARIMAMUL +
                                "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].MIKTARTXT +
                                "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].KALEMADETTXT +
                                "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].OLCUBIRIMI +
                                "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].BIRIMLISTEFIYATITXT + " ₺" +
                                "</td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].GIRILENFIYATTXT + " ₺" +
                                "</td></td></td></td><td style=\"border: 1px solid black; text-align: center; font-size: 12px;\">" + $scope.teklif.detailslist[i].events[j].TUTARTXT + " ₺" + "</td>"
                            tbody += tr;
                        } "</tr>"
                        var tr = "<tr></tr>"
                        tbody += tr;
                        var tr = "<tr></tr>"
                        tbody += tr;
                    }
                } while (typeof $scope.teklif.detailslist[i] !== 'undefined');
                tbody += "</tbody>";

                var tfoot = "<tfoot>";
                for (var i = 0; i < $scope.teklif.tekliflist.length; i++) {
                    var tr =
                        "<tr><td><td><td><td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "OLUŞTURAN KULLANICI" +
                        "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "OLUŞTURULMA TARİHİ" +
                        "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "DÜZENLEYEN KULLANICI" +
                        "</b></td><td style=\"border: 1px solid black; text-decoration: underline; text-align: center; font-size: 13px;\"><b>" + "DÜZENLENME TARİHİ" +
                        "</b></td></td></td></td></td></tr>";
                    tfoot += tr;
                    var tr =
                        "<tr><td><td><td><td><td style=\"border: 1px solid black; position: absolute; bottom: 0; text-align: center; font-size: 13px;\"><b>" + $scope.teklif.tekliflist[i].CREATEUSER +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 10px;\"><b>" + $scope.teklif.tekliflist[i].CREATEDATETXT +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 13px;\"><b>" + $scope.teklif.tekliflist[i].MODIFYUSER +
                        "</b></td><td style=\"border: 1px solid black; text-align: center; font-size: 10px;\"><b>" + $scope.teklif.tekliflist[i].MODIFYDATETXT +
                        "</b></td></td></td></td></td></tr>"
                    tfoot += tr;
                }
                var tr = "<tr></tr>"
                tfoot += tr;
                var tr =
                    "<tr><td><td><td><td><td><td style=\"position: absolute; bottom: 0; font-size: 12px; text-decoration: underline; text-align: center; width: 120px;\"><b>" + "YETKİLİ KİŞİNİN İMZASI" +
                    "</b></td></td></td></td></td></td></tr>"
                tfoot += tr;
                "</tfoot>";
                return thead + tbody + tfoot; // return thead + tbody + tfoot;
            },
            show: {
                update: function (_teklif) {
                    $rootScope.showModifyModal(_teklif);
                },
                delete: function (_teklif) {
                    $rootScope.showDeleteModal(_teklif);
                },
                detail: function (_teklif) {
                    localStorage.removeItem('modify-tekliftarihi');
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=6&d=' + new Date(),
                        data: {
                            tekliflerid: _teklif.TEKLIFLERID
                        }
                    }).then(function (response) {
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                            window.location.href = 'login.aspx';
                            return;
                        }
                        localStorage.setItem("tekliflist-modify", JSON.stringify(response.data)); // modifycomponent-create ve modifycomponent-modify modalda kullanıyorum
                        localStorage.setItem("tekliflist-id", JSON.stringify(response.data.TEKLIFLERID));
                        modify: true,
                        $rootScope.modify = false;
                        window.location.pathname = 'teklifler-detail.aspx';
                    });s
                },
                state: function (_teklif) {
                    $rootScope.showTeklifStateModal(_teklif);
                },
                excel: function (_teklif) {
                    localStorage.removeItem('dolareuro');
                    localStorage.removeItem('DOLARALISFIYATI');
                    localStorage.removeItem('DOLARSATISFIYATI');
                    localStorage.removeItem('EUROALISFIYATI');
                    localStorage.removeItem('EUROSATISFIYATI'); 
                    localStorage.removeItem('Tarih');
                    $http({ 
                        method: "GET", //DOLAR ve EURO
                        url: 'hub/teklif.ashx?&t=13&d=' + new Date(), // dolar ve euro kurlarını excel'de göstermek için.
                        data: "{}",
                        contentType: "application/json",
                        dataType: "json",
                    }).then(function (response) {
                        console.log(response.data);
                        localStorage.setItem("dolareuro", JSON.stringify(response.data));
                        localStorage.setItem("DOLARALISFIYATI", JSON.stringify(response.data.DOLARALISFIYATI));
                        localStorage.setItem("DOLARSATISFIYATI", JSON.stringify(response.data.DOLARSATISFIYATI));
                        localStorage.setItem("EUROALISFIYATI", JSON.stringify(response.data.EUROALISFIYATI));
                        localStorage.setItem("EUROSATISFIYATI", JSON.stringify(response.data.EUROSATISFIYATI));
                        localStorage.setItem("Tarih", JSON.stringify(response.data.Tarih));
                        $http({
                            method: 'POST',
                            url: 'hub/teklif.ashx?&t=6&d=' + new Date(),
                            data: {
                                tekliflerid: _teklif.TEKLIFLERID
                            },
                        }).then(function (response) {
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                                window.location.href = 'login.aspx';
                                return;
                            }
                            var teklifdata = response.data;
                            var teklifdetaildata = response.data.DETAILS;
                            console.log(teklifdata);
                            console.log(teklifdetaildata);
                            var eventArray = teklifdetaildata, // gelen datayı TEKLIFMALIYETID ye göre gruplar.
                                result = Array.from(
                                    eventArray.reduce((m, o) => m.set(o.TEKLIFMALIYETID, (m.get(o.TEKLIFMALIYETID) || []).concat(o)), new Map),
                                    ([TEKLIFMALIYETID, events]) => ({ TEKLIFMALIYETID, events })
                                );
                            [$scope.teklifdata] = [teklifdata];
                            $scope.teklif.tekliflist = [$scope.teklifdata];
                            $scope.teklif.detailslist = result;
                            console.log($scope.teklif.tekliflist);
                            console.log($scope.teklif.detailslist);
                            $scope.teklif.isExist = $scope.teklif.tekliflist.length > 0;
                            var teklifDate = new Date();
                            var yearTxt = teklifDate.getFullYear();
                            var montTxt = (teklifDate.getMonth() < 9 ? '0' : '') + (teklifDate.getMonth() + 1);
                            var dayTxt = (teklifDate.getDate() < 10 ? '0' : '') + teklifDate.getDate();
                            var filename = 'TeklifMaliyetReport_' + dayTxt + '-' + montTxt + '-' + yearTxt + '.xls';
                            $scope.teklif.tableToExcel('ReportTable', 'ExcelReportPage', filename);
                            app.show.success('Maliyetler Raporu Oluşturuldu!');
                            return;
                        }, function (response) {
                            app.show.error('Bir şeyler ters gitti...');
                        });
                    });
                },
                modifycomponent: function (_teklif) {
                    localStorage.removeItem('modify-tekliftarihi');
                        $http({
                            method: 'POST',
                            url: 'hub/teklif.ashx?&t=6&d=' + new Date(),
                            data: {
                                tekliflerid: _teklif.TEKLIFLERID
                            }
                        }).then(function (response) {
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                                window.location.href = 'login.aspx';
                                return;
                            }
                            localStorage.setItem("tekliflist-modify", JSON.stringify(response.data)); // modifycomponent-create ve modifycomponent-modify modalda kullanıyorum
                            localStorage.setItem("tekliflist-id", JSON.stringify(response.data.TEKLIFLERID));
                            modify: true,
                            $rootScope.modify = false;
                            window.location.pathname = 'teklifler-modify.aspx';
                        });
                },
            },
        
            page: {
                index: 1
            },
            init: function () {
                $scope.teklif.cari.init();
                $scope.teklif.kullanici.init();
                $scope.teklif.onaydurumu.init();
                $scope.teklif.baslangictarihi.init();
                $scope.teklif.bitistarihi.init();
                $scope.teklif.load();
            },
            filter: {
                type: 1, //this filter type is for page selecting = GET ALL LIST
                value: '',
                init: function () {
                    $scope.teklif.filter.value = '';
                },
                change: function () {
                    $scope.teklif.filter.timer.set();
                },
                timer: {
                    value: null,
                    interval: 500,
                    set: function () {
                        console.log($scope.teklif.filter.timer.value);
                        if ($scope.teklif.filter.timer.value) {
                            $timeout.cancel($scope.teklif.filter.timer.value);
                            $scope.teklif.filter.timer.value = null;
                        }
                        $scope.teklif.filter.timer.value = $timeout(function () {
                            if ($scope.teklif.isLoading) {
                                return;
                            }
                            $scope.teklif.page.index = 1;
                            $scope.teklif.load();
                        }, $scope.teklif.filter.timer.interval);
                    }
                }
            },
            cari: {
                isValid: false,
                selected: null,
                cls: '',
                msg: '',
                inputValue: '',
                shouldLoadOptionsOnInputFocus: true,
                init: function () {
                    $scope.teklif.cari.isValid = false;
                    $scope.teklif.cari.selected = null;
                    $scope.teklif.cari.cls = '';
                    $scope.teklif.cari.msg = '';
                    $scope.teklif.cari.inputValue = '';
                    $scope.teklif.cari.shouldLoadOptionsOnInputFocus = true;
                },
                select: function (cari) {
                    console.log(cari);
                    $scope.teklif.cari.selected = cari;
                },
                set: function (cari) {
                    console.log(cari);
                    $scope.teklif.cari.isValid = true;
                    $scope.teklif.cari.selected = cari;
                    $scope.teklif.cari.cls = '';
                    $scope.teklif.cari.msg = '';
                    $scope.teklif.cari.inputValue = cari.CARI; //select içerisinde console.log(cari); yaz gelen değerlere bak. ADI ve PARAMETRELERID geliyor
                    $scope.teklif.cari.shouldLoadOptionsOnInputFocus = true;
                }
            },
            kullanici: {
                isValid: false,
                selected: null,
                cls: '',
                msg: '',
                inputValue: '',
                shouldLoadOptionsOnInputFocus: true,
                init: function () {
                    $scope.teklif.kullanici.isValid = false;
                    $scope.teklif.kullanici.selected = null;
                    $scope.teklif.kullanici.cls = '';
                    $scope.teklif.kullanici.msg = '';
                    $scope.teklif.kullanici.inputValue = '';
                    $scope.teklif.kullanici.shouldLoadOptionsOnInputFocus = true;
                },
                select: function (kullanici) {
                    console.log(kullanici);
                    $scope.teklif.kullanici.selected = kullanici;
                },
                set: function (kullanici) {
                    console.log(kullanici);
                    $scope.teklif.kullanici.isValid = true;
                    $scope.teklif.kullanici.selected = kullanici;
                    $scope.teklif.kullanici.cls = '';
                    $scope.teklif.kullanici.msg = '';
                    $scope.teklif.kullanici.inputValue = kullanici.KULLANICI; //select içerisinde console.log(kullanici); yaz gelen değerlere bak. ADI ve PARAMETRELERID geliyor
                    $scope.teklif.kullanici.shouldLoadOptionsOnInputFocus = true;
                }
            },
            onaydurumu: {
                list: page.data.onaydurumu,
                isValid: false,
                selected: { name: '', value: 2 },
                cls: '',
                msg: '',
                init: function () {
                    $scope.teklif.onaydurumu.isValid = false;
                    $scope.teklif.onaydurumu.selected = { name: '', value: 2 };
                    $scope.teklif.onaydurumu.cls = '';
                    $scope.teklif.onaydurumu.msg = '';
                }
            },
            baslangictarihi: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.teklif.baslangictarihi.isValid = false;
                    $scope.teklif.baslangictarihi.value = '';
                    $scope.teklif.baslangictarihi.cls = '';
                    $scope.teklif.baslangictarihi.msg = '';
                }
            },
            bitistarihi: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.teklif.bitistarihi.isValid = false;
                    $scope.teklif.bitistarihi.value = '';
                    $scope.teklif.bitistarihi.cls = '';
                    $scope.teklif.bitistarihi.msg = '';
                }
            },
            totalRow: {
                value: 0,
                set: function (value) {
                    var paginate = $scope.teklif.totalRow.value !== value;
                    $scope.teklif.totalRow.value = value;
                    if (paginate) {
                        $scope.teklif.paginate();
                    }
                }
            },
            maxVisibleRow: {
                value: app.pagination.maxVisible.row,
                set: function (value) {
                    var paginate = $scope.teklif.maxVisibleRow.value !== value;
                    $scope.teklif.maxVisibleRow.value = value;
                    if (paginate) {
                        $scope.teklif.paginate();
                    }
                }
            },
            ngshow: {
                value: 0,
                set: function (value) {
                    $scope.teklif.ngshow.value = value;
                }
            },
            list: [],
            list2: [],
            list3: [],
            isExist: false,
            load: function () {
                console.log($scope.teklif.cari.selected);
                console.log($scope.teklif.kullanici.selected);
                console.log($scope.teklif.onaydurumu.selected.value);
                $scope.teklif.isLoading = true;
                app.show.spinner($('teklif-list > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/teklif.ashx?&t=2&d=' + new Date(),
                    data: {
                        pageIndex: $scope.teklif.page.index,
                        pageRowCount: $scope.teklif.maxVisibleRow.value,
                        cariid: $scope.teklif.cari.selected === null ? 0 : $scope.teklif.cari.selected.CARILERID,
                        kullaniciid: $scope.teklif.kullanici.selected === null ? 0 : $scope.teklif.kullanici.selected.ID,
                        filtreonaydurumu: $scope.teklif.onaydurumu.selected.value,
                       // onaydurumu: $scope.teklif.onaydurumu.selected.value === 2 ? null : $scope.teklif.onaydurumu.selected.value,
                        baslangictarihi: $scope.teklif.baslangictarihi.value === '' ? '' : new Date($scope.teklif.baslangictarihi.value).toLocaleDateString().split(".").reverse().join("-"),
                        bitistarihi: $scope.teklif.bitistarihi.value === '' ? '' : new Date($scope.teklif.bitistarihi.value).toLocaleDateString().split(".").reverse().join("-"),
                    }
                }).then(function (response) {
                    $scope.teklif.isLoading = false;
                    app.hide.spinner($('teklif-list > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                   //app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or user is passive
                        window.location.href = 'login.aspx';
                        return;
                    }
                    console.log(response.data);
                    $scope.teklif.list = response.data.LIST;
                    $scope.teklif.isExist = $scope.teklif.list.length > 0;
                    $scope.teklif.totalRow.set(response.data.TOTALROW);
                }, function (response) {
                    $scope.teklif.isLoading = false;
                   app.show.error('Bir şeyler ters gitti...');
                    app.hide.spinner($('teklif-list > div:first'));
                });
            },
            //show: {
            //    update: function (_teklif) {
            //        $rootScope.showModifyModal(_teklif);
            //    },
            //    delete: function (_teklif) {
            //        $rootScope.showDeleteModal(_teklif);
            //    },
            //    detail: function (_teklif) {
            //        $rootScope.showDetailModal(_teklif);
            //    }
            //},
            paginate: function () {
                var pageCount = $scope.teklif.totalRow.value !== 0 ? Math.ceil($scope.teklif.totalRow.value / $scope.teklif.maxVisibleRow.value) : 1;
                $('.bootpag-flat').bootpag({
                    total: pageCount,
                    maxVisible: app.pagination.maxVisible.pageIndex,
                    page: $scope.teklif.page.index,
                    leaps: false
                }).on("page", function (event, num) {
                    $scope.teklif.page.index = num;
                    $scope.teklif.load();
                }).children('.pagination').addClass('pagination-flat pagination-sm');
            }
        };
        $scope.teklif.load();
    }])
    .directive('teklifList', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/teklif-list.html'
        };
    });
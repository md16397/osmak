﻿app.angular
    .controller('teklifcreateComponentCtrl', ['$scope', '$http', '$rootScope', '$localStorage', function ($scope, $http, $rootScope, $localStorage) {
        localStorage.removeItem('aciklama'); //Storage de depolanan açıklama değeri, yeni kayıtlar ekleneceği için bu sayfa her açıldığında silinmeli. 
        localStorage.removeItem('teklifkodu');

        var Runopen3;
        $rootScope.run3 = function () { // teklif-create-modal ve teklif-modify-modal da kaydedilen değerler bu sayfada listelenecek.
            var _teklif = JSON.parse(localStorage.getItem("teklifdata")); // teklif-create-modal ve teklif-modify-modal da kaydedilen datalar teklifdata içerisinde depolanır.
           
            console.log(_teklif);
            if ($rootScope.control === true) {
                if (typeof _teklif.TEKLIFLERID !== 'undefined') {
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=6&d=' + new Date(), //ID ye göre listeleme yapabilmek için.
                        data: {
                            tekliflerid: _teklif.TEKLIFLERID
                        }
                    }).then(function (response) {
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {
                            window.location.href = 'login.aspx';
                            return;
                        }
                        $scope.teklif.rows = response.data.DETAILS; // teklife ait kalemler.
                        $scope.teklif.teklifdata = response.data;
                        console.log($scope.teklif.teklifdata);
                        console.log($scope.teklif.rows);
                        $scope.teklif.teklifkodu.set($scope.teklif.teklifdata.TEKLIFKODU);
                        localStorage.setItem("teklif-create-component-teklifkodu", JSON.stringify($scope.teklif.teklifdata.TEKLIFKODU)); 
                        if (typeof $scope.teklif.rows === 'undefined' && $scope.teklif.teklifdata.RESULT === 78) { // Tüm işlemler silindiyse
                            $rootScope.teklifidExist = 0;
                            $scope.teklif.init();
                            return;
                        }
                        var i = 0;
                        $scope.teklif.tekliftutarlarinitopla.value = 0; // component her açıldığında toplam teklif tutarı sıfırlanmalı. Maliyete ait bütün teklif tutarlarını toplamak için.
                        var eventArray = $scope.teklif.rows, // gelen datayı TEKLIFMALIYETID ye göre gruplar.
                            result = Array.from(
                                eventArray.reduce((m, o) => m.set(o.TEKLIFMALIYETID, (m.get(o.TEKLIFMALIYETID) || []).concat(o)), new Map),
                                ([TEKLIFMALIYETID, events]) => ({ TEKLIFMALIYETID, events })
                            );
                        console.log(result);
                        var i = 0;
                        do { // TEKLIFE AIT TOPLAM TEKLIF TUTARI HESAPLANIYOR... (teklif-list.js ekranında göstermek için.)
                            if (result[i] !== null) {
                                $scope.teklif.tekliftutarlarinitopla.value = $scope.teklif.tekliftutarlarinitopla.value + result[i].events[0].MALIYETTEKLIFTUTARIVAL;
                                $scope.teklif.toplamtekliftutari.set($scope.teklif.tekliftutarlarinitopla.value);
                                console.log($scope.teklif.tekliftutarlarinitopla.value);
                            }
                            i++;
                            console.log($scope.teklif.rows[i]);
                            console.log($scope.teklif.tekliftutarlarinitopla.value);
                        } while (typeof result[i] !== 'undefined');

                        $scope.teklif.toplamtekliftutari.set($scope.teklif.tekliftutarlarinitopla.value);
                        console.log($scope.teklif.tekliftutarlarinitopla.value);
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti...');
                    });
                }
            }
        };
        $rootScope.toplamsatis; 
        $rootScope.kontrol = {
            check: function () { // teklifcomponent-create.btn butonuna tıklayınca teklif-create-modal ın açılması için Cari adı ve teklif tarihi girilmiş olmalıdır.
                var applyCheck = true;
                var OK = {
                    cari: $scope.teklif.cari.check(applyCheck),
                    tekliftarihi: $scope.teklif.tekliftarihi.check(applyCheck)
                };
                return OK.cari && OK.tekliftarihi;
            },
        },
        $scope.teklif = { // 'Create modal' will close when new data is added. Data added to the table will come ...
            tables: [],
            isValid: false,
            isExist: false,
            count: 0,
            cls: '',
            msg: '',
            getdetails: '',
            rows: [],
            getteklifid: [],
            rows2: [],
            teklifdata: [],
            toplamsatis: [],
            modifydata: [],
            error: function (msg) {
                $scope.teklif.msg = msg;
            },// Teklife ait kalem yokken 'teklifi kaydet ve kapat' butonuna tıklanırsa uyarı vermeli.
            iptaletvecik: function () { // Teklifleri kaydedip Teklifler-liste ekranına yönlendirir.
                window.location.pathname = 'teklifler.aspx';
            },
            show: {
                update: function (key) {
                    $rootScope.showModifyModal(key); // teklif-modify-modal
                },
                delete: function (key, teklifid) { // teklifcomponent-delete-modal
                    $rootScope.showDeleteModal(key, teklifid);
                },
            },
            set: function (teklif_) {
                //$scope.teklif.cari.set({ CARILERID: teklif_.CARIID, ADI: teklif_.CARI });
                //$scope.teklif.teklifkodu.set(teklif_.TEKLIFKODU);
                //$scope.teklif.aciklama.set(teklif_.TEKLIFACIKLAMA);
                $scope.teklif.toplamtekliftutari.set(teklif_);
            },
            id: {
                value: 0,
                init: function () {
                    $scope.teklif.id.value = 0;
                },
                set: function (value) {
                    $scope.teklif.id.value = value;
                }
            },
            maliyetid: {
                value: 0,
                init: function () {
                    $scope.teklif.maliyetid.value = 0;
                },
                set: function (value) {
                    $scope.teklif.maliyetid.value = value;
                }
            },
            cari: {
                isValid: false,
                selected: null,
                cls: '',
                msg: '',
                inputValue: '',
                shouldLoadOptionsOnInputFocus: true,
                init: function () {
                    $scope.teklif.cari.isValid = false;
                    $scope.teklif.cari.selected = null;
                    $scope.teklif.cari.cls = '';
                    $scope.teklif.cari.msg = '';
                    $scope.teklif.cari.inputValue = '';
                    $scope.teklif.cari.shouldLoadOptionsOnInputFocus = true;
                },
                check: function (applyCheck) {
                    var check = $scope.teklif.cari.selected && $scope.teklif.cari.selected.hasOwnProperty('CARILERID') && typeof $scope.teklif.cari.selected.CARILERID !== 'undefined' && $scope.teklif.cari.selected.CARILERID != null ? true : false;
                    if (applyCheck || $scope.teklif.cari.isValid !== check) {
                        $scope.teklif.cari.isValid = check;
                        if ($scope.teklif.cari.isValid) {
                            $scope.teklif.cari.success();
                        } else {
                            $scope.teklif.cari.error('* Geçersiz Cari Adı');
                        }
                    }
                    return $scope.teklif.cari.isValid;
                },
                select: function (cari) {
                    $scope.teklif.cari.selected = cari;
                    $rootScope.cariselect = cari;
                    $scope.teklif.cari.check();
                    localStorage.setItem("cari", JSON.stringify($scope.teklif.cari.selected.CARILERID)); // create ve modify modallarına cari bilgisini göndermek için localstorage kullandım.
                },
                success: function () {
                    $scope.teklif.cari.cls = '';
                    $scope.teklif.cari.msg = '';
                },
                error: function (msg) {
                    $scope.teklif.cari.cls = 'has-error';
                    $scope.teklif.cari.msg = msg;
                }
            },
            teklifkodu: {
                isValid: false,
                value: '',
                text: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.teklif.teklifkodu.isValid = false;
                    $scope.teklif.teklifkodu.value = '';
                    $scope.teklif.teklifkodu.text = '';
                    $scope.teklif.teklifkodu.cls = '';
                    $scope.teklif.teklifkodu.msg = '';
                    $scope.teklif.teklifkodu.set('');
                },
                set: function (value) {
                    $scope.teklif.teklifkodu.value = value;
                    $scope.teklif.teklifkodu.text = value;
                    $scope.teklif.teklifkodu.isValid = true;
                    $scope.teklif.teklifkodu.cls = '';
                    $scope.teklif.teklifkodu.msg = '';
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.teklif.teklifkodu.value);
                    if (applyCheck || $scope.teklif.teklifkodu.isValid !== check) {
                        $scope.teklif.teklifkodu.isValid = check;
                        if ($scope.teklif.teklifkodu.isValid) {
                            $scope.teklif.teklifkodu.success();
                        } else {
                            $scope.teklif.teklifkodu.error('* Geçersiz Teklif Kodu !');
                        }
                    }
                    return $scope.teklif.teklifkodu.isValid;
                },
                change: function () {
                    $scope.teklif.teklifkodu.check($scope.teklif.teklifkodu.value);
                    $rootScope.teklifkodu = $scope.teklif.teklifkodu.value;
                    localStorage.setItem("teklifkodu", JSON.stringify($scope.teklif.teklifkodu.value));// create ve modify modallarına teklif kodu bilgisini göndermek için localstorage kullandım.
                },
                success: function () {
                    $scope.teklif.teklifkodu.cls = '';
                    $scope.teklif.teklifkodu.msg = '';
                },
                error: function (msg) {
                    $scope.teklif.teklifkodu.cls = 'has-error';
                    $scope.teklif.teklifkodu.msg = msg;
                }
            },
            tekliftarihi: {
                isValid: false,
                value: new Date(),
                cls: '',
                msg: '',
                init: function () {
                    $scope.teklif.tekliftarihi.isValid = false;
                    $scope.teklif.tekliftarihi.value = new Date();
                    $scope.teklif.tekliftarihi.cls = '';
                    $scope.teklif.tekliftarihi.msg = '';
                    $scope.teklif.tekliftarihi.set('');
                },
                set: function (value) {
                    $scope.teklif.tekliftarihi.value = new Date(value);
                    $scope.teklif.tekliftarihi.isValid = true;
                    $scope.teklif.tekliftarihi.cls = '';
                    $scope.teklif.tekliftarihi.msg = '';
          
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.teklif.tekliftarihi.value);
                    if (applyCheck || $scope.teklif.tekliftarihi.isValid !== check) {
                        $scope.teklif.tekliftarihi.isValid = check;
                        if ($scope.teklif.tekliftarihi.isValid) {
                            $scope.teklif.tekliftarihi.success();
                        } else {
                            $scope.teklif.tekliftarihi.error('* Geçersiz Teklif Tarihi!');
                        }
                    }
                    return $scope.teklif.tekliftarihi.isValid;
                },
                change: function () {
                    localStorage.setItem("tekliftarihi", JSON.stringify($scope.teklif.tekliftarihi.value));// create ve modify modallarına teklif tarihi bilgisini göndermek için localstorage kullandım.
                    $scope.teklif.tekliftarihi.check();
                },
                success: function () {
                    $scope.teklif.tekliftarihi.cls = '';
                    $scope.teklif.tekliftarihi.msg = '';
                },
                error: function (msg) {
                    $scope.teklif.tekliftarihi.cls = 'has-error';
                    $scope.teklif.tekliftarihi.msg = msg;
                }
            },
            aciklama: {
                value: '',
                init: function () {
                    $scope.teklif.aciklama.value = '';
                },
                change: function () {
                    localStorage.setItem("aciklama", JSON.stringify($scope.teklif.aciklama.value)); // create ve modify modallarına aciklama bilgisini göndermek için localstorage kullandım.
                },
                set: function (aciklama) {
                    $scope.teklif.aciklama.value = aciklama;
                },
            },
            toplamtekliftutari: {
                value: '',
                init: function () {
                    $scope.teklif.toplamtekliftutari.value = '';
                },
                set: function (toplamtekliftutari) {
                    console.log(toplamtekliftutari);
                    $scope.teklif.toplamtekliftutari.value = toplamtekliftutari;
                    $scope.teklif.toplamtekliftutari.text = toplamtekliftutari;
                   
                },
            },
            tekliftutarlarinitopla: {
                value: 0,
                text: '',
                init: function () {
                    $scope.teklif.tekliftutarlarinitopla.value = 0;
                    $scope.teklif.tekliftutarlarinitopla.text = '';
                },
                set: function (value) {
                    $scope.teklif.tekliftutarlarinitopla.value = value;
                    $scope.teklif.tekliftutarlarinitopla.text = value;
                }
            },
            init: function () { // inputların içi boş olur.
                $scope.teklif.cari.init();
                $scope.teklif.teklifkodu.init();
                $scope.teklif.aciklama.init();
            },
            check: function () { // cari ve teklif kodunun girilip girilmediğini kontrol eder. htmlde  ng-check olarak kullanılır.
                var applyCheck = true;
                var OK = {
                    cari: $scope.teklif.cari.check(applyCheck),
                    teklifkodu: $scope.teklif.teklifkodu.check(applyCheck)
                };
                return OK.cari && OK.teklifkodu;
            },
            list: [],
            detailslist: [],
            saveandclose: function () {
                var teklifexcel = $scope.teklif.teklifdata;
                var teklifdetail = $scope.teklif.rows;
                if (typeof $scope.teklif.rows === 'undefined' || $scope.teklif.rows.length === 0) { 
                 // $scope.teklif.error('* Lütfen Teklif kalemleri ekleyiniz.');
                    return;
                }
                if ($scope.teklif.cari.selected === null) {
                    $scope.teklif.error('* Lütfen Cari Adı seçiniz.');
                    return;
                } if ($scope.teklif.teklifkodu.value === '') {
                    return;
                }
                $http({
                    method: 'POST',
                    url: 'hub/teklif.ashx?&t=12&d=' + new Date(), // TYPE 12 ile ekrandaki bütün datayı kaydeder.
                    data: {
                        teklifid: $scope.teklif.teklifdata.TEKLIFLERID,
                        cari: $scope.teklif.cari.selected.CARILERID,
                        toplamtekliftutari: $scope.teklif.toplamtekliftutari.value,
                        teklifkodu: $scope.teklif.teklifkodu.value,
                        tekliftarihi: $scope.teklif.tekliftarihi.value,
                        onaydurumu: $scope.teklif.teklifdata.TEKLIFONAYDURUMU,
                        aciklama: $scope.teklif.aciklama.value
                    }
                }).then(function (response) {
                    app.hide.spinner($('teklif-modify-modal > div:first > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error('Bir şeyler ters gitti...');
                        return;
                    }
                    if (response.data['RESULT'] === 1) { // veritabanından RESULT değeri 1 dönerse kaydedilmiş olur.$rootScope.showExcelModal ile excel modalı açılır.
                        console.log(response.data);
                        $rootScope.showExcelModal(response.data, teklifdetail, response.data.TEKLIFKODU);// excel çıktısı almak için Modal açılmalı.
                       // app.show.success('Teklif-Maliyet Bilgileri Eklendi!');
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                        window.location.href = 'login.aspx';
                        return;
                    }
                    app.show.error('Bir şeyler ters gitti...');
                }, function (response) {
                    app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                    app.hide.spinner($('teklif-modify-modal > div:first > div:first'));
                });
            },
        };
        Runopen3 = $rootScope.run3; // 7.satırda yer alan $rootScope.run3 = function ()  function un çalışması için bu satırda tetiklenir.
        Runopen3();
}])
.directive('teklifCreateComponent', function () {
    return {
        restrict: 'E',
        templateUrl: 'assets/template/teklif-create-component.html' // teklif-create-component e ait html sayfasının yolu buraya yazılır. templateUrl
    };
});
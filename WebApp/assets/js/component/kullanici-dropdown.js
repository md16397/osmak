﻿app.angular
    .component('kullaniciDropdown', {
        templateUrl: 'assets/template/kullanici-dropdown.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
            var ctrl = this;
            $rootScope.disabled = function () {
                ctrl.kullanicidisabled = true;
            };
            $scope.kullanici = {
                shouldInitOnInputChange: false,
                onfocus: false,
                focus: function ($event) {
                    $scope.kullanici.onfocus = true;
                    var length = typeof ctrl.inputValue === 'string' ? ctrl.inputValue.length : 0;
                    $event.target.setSelectionRange(0, length);
                    var width = angular.element($event.target).prop('offsetWidth');
                    $scope.kullanici.options.container.style.width = width + 'px';
                    $scope.kullanici.options.show();
                    if (ctrl.shouldLoadOptionsOnInputFocus) {
                        $scope.kullanici.options.load();
                    }
                },
                blur: function () {
                    $scope.kullanici.onfocus = false;
                    $scope.kullanici.options.hide();
                },
                filter: {
                    type: 0,
                    //value: '',
                    init: function () {
                        ctrl.inputValue = '';
                    },
                    change: function () {
                        $scope.kullanici.filter.timer.set();
                        if ($scope.kullanici.shouldInitOnInputChange && typeof ctrl.init === 'function') {
                            $scope.kullanici.shouldInitOnInputChange = false;
                            ctrl.init();
                        }
                    },
                    timer: {
                        value: null,
                        interval: 500,
                        set: function () {
                            if ($scope.kullanici.filter.timer.value) {
                                $timeout.cancel($scope.kullanici.filter.timer.value);
                                $scope.kullanici.filter.timer.value = null;
                            }
                            $scope.kullanici.filter.timer.value = $timeout(function () {
                                if ($scope.kullanici.options.isLoading) {
                                    return;
                                }
                                $scope.kullanici.options.page.index = 1;
                                $scope.kullanici.options.load();
                            }, $scope.kullanici.filter.timer.interval);
                        }
                    }
                },
                options: {
                    page: {
                        index: 1,
                        count: 1,
                        isLastPage: true
                    },
                    totalRow: {
                        value: 0,
                        set: function (value) {
                            $scope.kullanici.options.totalRow.value = value;
                        }
                    },
                    maxVisibleRow: {
                        value: app.pagination.maxVisible.row,
                        set: function (value) {
                            $scope.kullanici.options.maxVisibleRow.value = value;
                        }
                    },
                    isExist: false,
                    isLoading: false,
                    list: [],
                    container: {
                        style: {
                            width: '0px'
                        },
                        cls: ''
                    },
                    show: function () {
                        $scope.kullanici.options.container.cls = 'open';
                    },
                    hide: function () {
                        $timeout(function () {
                            if ($scope.kullanici.onfocus || $scope.kullanici.options.more.onfocus) {
                                return;
                            }
                            $scope.kullanici.options.container.cls = '';
                        }, 250);
                    },
                    load: function (extendList) {
                        $scope.kullanici.options.isLoading = true;
                        app.show.spinner($('kullanici-dropdown > .option-container'));
                        $http({
                            method: 'POST',
                            url: 'hub/user.ashx?&t=2&d=' + new Date(),  // TEKLIF tablosundaki Kullanıcılar
                            data: {
                                pageIndex: $scope.kullanici.options.page.index,
                                filterType: $scope.kullanici.filter.type,
                                filter: ctrl.inputValue,//$scope.kullanici.filter.value,
                                pageRowCount: $scope.kullanici.options.maxVisibleRow.value,
                            }
                        }).then(function (response) {
                            $scope.kullanici.options.isLoading = false;
                            app.hide.spinner($('kullanici-dropdown > .option-container'));
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive
                                window.location.href = 'login.aspx';
                                return;
                            }
                            if (!extendList) {
                                $scope.kullanici.options.list = response.data.LIST;
                            } else {
                                $scope.kullanici.options.list.push.apply($scope.kullanici.options.list, response.data.LIST);
                            }
                            $scope.kullanici.options.isExist = $scope.kullanici.options.list.length > 0;
                            ctrl.shouldLoadOptionsOnInputFocus = !$scope.kullanici.options.isExist;
                            $scope.kullanici.options.totalRow.set(response.data.TOTALROW);
                            $scope.kullanici.options.paginate();
                        }, function (response) {
                            $scope.kullanici.options.isLoading = false;
                            console.log(response);
                            app.show.error('Bir şeyler ters gitti...');
                            app.hide.spinner($('kullanici-dropdown > .option-container'));
                        });
                    },
                    paginate: function () {
                        $scope.kullanici.options.page.count = $scope.kullanici.options.totalRow.value !== 0 ? Math.ceil($scope.kullanici.options.totalRow.value / $scope.kullanici.options.maxVisibleRow.value) : 1;
                        $scope.kullanici.options.page.isLastPage = $scope.kullanici.options.page.count <= $scope.kullanici.options.page.index;
                    },
                    click: function (kullanici) {
                        $scope.kullanici.shouldInitOnInputChange = true;
                        ctrl.shouldLoadOptionsOnInputFocus = true;
                        ctrl.inputValue = kullanici.USERNAME; // kullanici tablosu kolon adı 
                        if (typeof ctrl.onSelect === 'function') {
                            ctrl.onSelect({ kullanici: kullanici });
                        }
                    },
                    more: {
                        load: function () {
                            if ($scope.kullanici.options.page.isLastPage) {
                                return;
                            }
                            $scope.kullanici.options.page.index++;
                            var extendList = true;
                            $scope.kullanici.options.load(extendList);
                        },
                        onfocus: false,
                        focus: function () {
                            $scope.kullanici.options.more.onfocus = true;
                        },
                        blur: function () {
                            $scope.kullanici.options.more.onfocus = false;
                            $scope.kullanici.options.hide();
                        }
                    }
                }
            };
        }],
        bindings: {
            kullanicidisabled: '=',
            inputLabel: '@',
            inputMessage: '@',
            componentClass: '@',
            onSelect: '&',
            init: '&',
            inputValue: '=',
            shouldLoadOptionsOnInputFocus: '=',
        }
    });
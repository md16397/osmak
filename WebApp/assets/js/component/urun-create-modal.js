﻿app.angular
    .component('urunCreateModal', {
        templateUrl: 'assets/template/urun-create-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
          
            $rootScope.showCreateModal = function () {
                $('.urun-create-modal:first').modal({
                    backdrop: 'static'
                });
            };
            $scope.urun = { 
                urun: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.urun.urun.isValid = false;
                        $scope.urun.urun.value = '';
                        $scope.urun.urun.cls = '';
                        $scope.urun.urun.msg = '';
                        $scope.urun.urun.set('');
                    },
                    set: function (value) {
                        $scope.urun.urun.value = value;
                        $scope.urun.urun.isValid = true;
                        $scope.urun.urun.cls = '';
                        $scope.urun.urun.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.urun.urun.value);
                        if (applyCheck || $scope.urun.urun.isValid !== check) {
                            $scope.urun.urun.isValid = check;
                            if ($scope.urun.urun.isValid) {
                                $scope.urun.urun.success();
                                $scope.urun.urun.value = $scope.urun.urun.value.replaceAll(" ", "").toUpperCase();
                            } else {
                                $scope.urun.urun.error('* Geçersiz Ürün!');
                            }
                        }
                        return $scope.urun.urun.isValid;
                    },
                    change: function () {
                        $scope.urun.urun.check($scope.urun.urun.value.replaceAll(" ", "").toUpperCase());
                    },
                    success: function () {
                        $scope.urun.urun.cls = '';
                        $scope.urun.urun.msg = '';
                    },
                    error: function (msg) {
                        $scope.urun.urun.cls = 'has-error';
                        $scope.urun.urun.msg = msg;
                    }
                },
                urunkodu: {
                    isValid: false,
                    value: '',
                    cls: '',
                    msg: '',
                    init: function () {
                        $scope.urun.urunkodu.isValid = false;
                        $scope.urun.urunkodu.value = '';
                        $scope.urun.urunkodu.cls = '';
                        $scope.urun.urunkodu.msg = '';
                        $scope.urun.urunkodu.set('');
                    },
                    set: function (value) {
                        $scope.urun.urunkodu.value = value;
                        $scope.urun.urunkodu.isValid = true;
                        $scope.urun.urunkodu.cls = '';
                        $scope.urun.urunkodu.msg = '';
                    },
                    check: function (applyCheck) {
                        var check = app.valid.text($scope.urun.urunkodu.value);
                        if (applyCheck || $scope.urun.urunkodu.isValid !== check) {
                            $scope.urun.urunkodu.isValid = check;
                            if ($scope.urun.urunkodu.isValid) {
                                $scope.urun.urunkodu.success();
                                $scope.urun.urunkodu.value = $scope.urun.urunkodu.value.replaceAll(" ", "").toUpperCase();
                            } else {
                                $scope.urun.urunkodu.error('* Geçersiz Ürün Kodu !');
                            }
                        }
                        return $scope.urun.urunkodu.isValid;
                    },
                    change: function () {
                        $scope.urun.urunkodu.check($scope.urun.urunkodu.value);
                      //  $scope.urun.urunkodu.check($scope.urun.urunkodu.value.replaceAll(" ", "").toUpperCase());
                    },
                    success: function () {
                        $scope.urun.urunkodu.cls = '';
                        $scope.urun.urunkodu.msg = '';
                    },
                    error: function (msg) {
                        $scope.urun.urunkodu.cls = 'has-error';
                        $scope.urun.urunkodu.msg = msg;
                    }
                },
                state: { 
                    name: 'Aktif',
                    value: true,
                    icon: {
                        cls: 'icon-check text-success'
                    },
                    init: function () {
                        $scope.urun.state.select($scope.urun.state.active);
                    },
                    select: function (state) {
                        $scope.urun.state.name = state.name;
                        $scope.urun.state.value = state.value;
                        $scope.urun.state.icon.cls = state.icon.cls.replace(/ pull-right/g, '');
                    },
                    set: function (value) {
                        $scope.urun.state.select(value ? $scope.urun.state.active : $scope.urun.state.passive);
                    },
                    active: {
                        name: 'Aktif',
                        value: true,
                        icon: {
                            cls: 'icon-check text-success'
                        },
                        anchor: {
                            cls: ''
                        }
                    },
                    passive: {
                        name: 'Pasif',
                        value: false,
                        icon: {
                            cls: 'icon-blocked text-danger'
                        },
                        anchor: {
                            cls: ''
                        }
                    }
                },
                adgirilsinmi: {
                    isValid: false,
                    value: false,
                    init: function () {
                        $scope.urun.adgirilsinmi.isValid = false;
                        $scope.urun.adgirilsinmi.value = false;
                        $scope.urun.adgirilsinmi.cls = '';
                        $scope.urun.adgirilsinmi.msg = '';
                        $scope.urun.adgirilsinmi.set('');
                    },
                    set: function (value) {
                        $scope.urun.adgirilsinmi.value = value;
                        $scope.urun.adgirilsinmi.isValid = true;
                        $scope.urun.adgirilsinmi.cls = '';
                        $scope.urun.adgirilsinmi.msg = '';
                    },
                },
                miktargirilsinmi: {
                    isValid: false,
                    value: false,
                    init: function () {
                        $scope.urun.miktargirilsinmi.isValid = false;
                        $scope.urun.miktargirilsinmi.value = false;
                        $scope.urun.miktargirilsinmi.cls = '';
                        $scope.urun.miktargirilsinmi.msg = '';
                        $scope.urun.miktargirilsinmi.set('');
                    },
                    set: function (value) {
                        $scope.urun.miktargirilsinmi.value = value;
                        $scope.urun.miktargirilsinmi.isValid = true;
                        $scope.urun.miktargirilsinmi.cls = '';
                        $scope.urun.miktargirilsinmi.msg = '';
                    },
                },
                aciklama: {
                    value: '',
                    set: function (aciklama) {
                        $scope.urun.aciklama.value = aciklama;
                    },
                    init: function () {
                        $scope.urun.aciklama.value = '';
                    }
                },
                init: function () {
                    $scope.urun.urun.init();
                    $scope.urun.urunkodu.init();
                    $scope.urun.adgirilsinmi.init();
                    $scope.urun.miktargirilsinmi.init();
                    $scope.urun.state.init();
                    $scope.urun.aciklama.init();
                },
                check: function () {
                    var applyCheck = true;
                    var OK = {
                        urun: $scope.urun.urun.check(applyCheck)
                    };
                    return OK.urun;
                },
                save: function () {
                    console.log($scope.urun.urunkodu.value);
                    if ($scope.urun.urunkodu.value === '') {
                        $scope.urun.urunkodu.value = '';
                    }
                    if ($scope.urun.adgirilsinmi.value === '') {
                        $scope.urun.adgirilsinmi.value = false;
                    }
                    if ($scope.urun.miktargirilsinmi.value === '') {
                        $scope.urun.miktargirilsinmi.value = false;
                    }
                    if (!$scope.urun.check()) {
                        return;
                    }
                    app.show.spinner($('urun-create-modal > div:first > div:first'));

                    $http({
                        method: 'POST',
                        url: 'hub/urun.ashx?&t=3&d=' + new Date(),
                        data: {
                            urun: $scope.urun.urun.value.replaceAll(" ", "").toUpperCase(),
                            urunkodu: $scope.urun.urunkodu.value,
                            state: $scope.urun.state.value,
                            adgirilsinmi: $scope.urun.adgirilsinmi.value,
                            miktargirilsinmi: $scope.urun.miktargirilsinmi.value,
                            aciklama: $scope.urun.aciklama.value
                        }
                    }).then(function (response) {
                        app.hide.spinner($('urun-create-modal > div:first > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 1) {//success
                            $scope.urun.init();
                            $rootScope.loadList();
                            app.show.success('Yeni ürün oluşturuldu!');
                            $('.urun-create-modal:first').modal('hide');
                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                            window.location.href = 'login.aspx';
                            return;
                        }
                        if (response.data['RESULT'] === 124) {
                            app.show.warning('UYARI', 'Bu Ürün daha önce oluşturuldu.');
                            console.log(1);
                            return;
                        }
                        if (response.data['RESULT'] === 18) {
                            $scope.urun.urun.isValid = false;
                            console.log("response  -------->", response);
                            console.log("response  -------->", response.data);
                            $scope.urun.urun.error('*Geçersiz Ürünnnn');
                            return;
                        }
                        if (response.data['RESULT'] === 19) {
                            $scope.urun.urunkodu.isValid = false;
                            $scope.urun.urunkodu.error('* Geçersiz Ürün Kodu');
                            return;
                        }
                        //if running code come through this far then it means an unexpected error occurred
                        app.show.error('Bir şeyler ters gitti...');
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                        app.hide.spinner($('urun-create-modal > div:first > div:first'));
                    });
                }
            };
        }],
        bindings: {
            dateDisable: '<'
        }
    });

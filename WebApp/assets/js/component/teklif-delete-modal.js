﻿app.angular
    .component('teklifDeleteModal', {
        templateUrl: 'assets/template/teklif-delete-modal.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
         
            $rootScope.showDeleteModal = function (_teklif) {
                console.log("teklif-delete-modal", _teklif);
                $scope.teklif.teklifid = _teklif;
                $scope.teklif.cari = _teklif.CARI;
                $scope.teklif.toplamtekliffiyati = _teklif.TOPLAMTEKLIFTUTARITXT;
                $scope.teklif.tekliftarihi = _teklif.TEKLIFTARIHITXT;
                $scope.teklif.teklifkodu = _teklif.TEKLIFKODU;
                $('.teklif-delete-modal:first').modal({
                    backdrop: 'static',
                    keyboard: true
                });
            };
            $scope.teklif = {
                teklifid: 0,
                cari: null,
                toplamtekliffiyati: 0,
                tekliftarihi: null,
                teklifkodu: null,
                delete: function () {
                    app.show.spinner($('teklif-delete-modal > div:first > div:first'));
                    $http({
                        method: 'POST',
                        url: 'hub/teklif.ashx?&t=5&d=' + new Date(),
                        data: {
                            teklifid: $scope.teklif.teklifid.TEKLIFLERID
                        }
                    }).then(function (response) {
                        app.hide.spinner($('teklif-delete-modal > div:first > div:first'));
                        if (!response.data.hasOwnProperty('RESULT')) {
                            app.show.error('Bir şeyler ters gitti...');
                            return;
                        }
                        if (response.data['RESULT'] === 1) {//success
                            $rootScope.loadList();
                            $('.teklif-delete-modal:first').modal('hide');
                            app.show.success('Teklif-Maliyet Bilgileri Silindi !');

                            return;
                        }
                        if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                            window.location.href = 'login.aspx';
                            return;
                        }
                        //if running code come through this far then it means an unexpected error occurred
                        app.show.error('Bir şeyler ters gitti...');
                    }, function (response) {
                        app.show.error('Bir şeyler ters gitti...');//systemMessages[1934]
                        app.hide.spinner($('teklif-delete-modal > div:first > div:first'));
                    });
                }



            }

        }]
    })
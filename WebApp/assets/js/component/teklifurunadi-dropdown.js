﻿app.angular
    .component('teklifurunadiDropdown', {
        templateUrl: 'assets/template/teklifurunadi-dropdown.html',
        controller: ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
            var ctrl = this;
            $rootScope.disabled = function () {
                ctrl.teklifurunadidisabled = true;
            };
            $scope.teklifurunadi = {
                shouldInitOnInputChange: false,
                onfocus: false,
                focus: function ($event) {
                    $scope.teklifurunadi.onfocus = true;
                    var length = typeof ctrl.inputValue === 'string' ? ctrl.inputValue.length : 0;
                    $event.target.setSelectionRange(0, length);
                    var width = angular.element($event.target).prop('offsetWidth');
                    $scope.teklifurunadi.options.container.style.width = width + 'px';
                    $scope.teklifurunadi.options.show();
                    if (ctrl.shouldLoadOptionsOnInputFocus) {
                        $scope.teklifurunadi.options.load();
                    }
                },
                blur: function () {
                    $scope.teklifurunadi.onfocus = false;
                    $scope.teklifurunadi.options.hide();
                },
                filter: {
                    type: 0,
                    //value: '',
                    init: function () {
                        ctrl.inputValue = '';
                    },
                    change: function () {
                        $scope.teklifurunadi.filter.timer.set();
                        if ($scope.teklifurunadi.shouldInitOnInputChange && typeof ctrl.init === 'function') {
                            $scope.teklifurunadi.shouldInitOnInputChange = false;
                            ctrl.init();
                        }
                    },
                    timer: {
                        value: null,
                        interval: 500,
                        set: function () {
                            if ($scope.teklifurunadi.filter.timer.value) {
                                $timeout.cancel($scope.teklifurunadi.filter.timer.value);
                                $scope.teklifurunadi.filter.timer.value = null;
                            }
                            $scope.teklifurunadi.filter.timer.value = $timeout(function () {
                                if ($scope.teklifurunadi.options.isLoading) {
                                    return;
                                }
                                $scope.teklifurunadi.options.page.index = 1;
                                $scope.teklifurunadi.options.load();
                            }, $scope.teklifurunadi.filter.timer.interval);
                        }
                    }
                },
                options: {
                    page: {
                        index: 1,
                        count: 1,
                        isLastPage: true
                    },
                    totalRow: {
                        value: 0,
                        set: function (value) {
                            $scope.teklifurunadi.options.totalRow.value = value;
                        }
                    },
                    maxVisibleRow: {
                        value: app.pagination.maxVisible.row,
                        set: function (value) {
                            $scope.teklifurunadi.options.maxVisibleRow.value = value;
                        }
                    },
                    isExist: false,
                    isLoading: false,
                    list: [],
                    container: {
                        style: {
                            width: '0px'
                        },
                        cls: ''
                    },
                    show: function () {
                        $scope.teklifurunadi.options.container.cls = 'open';
                    },
                    hide: function () {
                        $timeout(function () {
                            if ($scope.teklifurunadi.onfocus || $scope.teklifurunadi.options.more.onfocus) {
                                return;
                            }
                            $scope.teklifurunadi.options.container.cls = '';
                        }, 250);
                    },
                    load: function (extendList) {
                        $scope.teklifurunadi.options.isLoading = true;
                        app.show.spinner($('teklifurunadi-dropdown > .option-container'));
                        $http({
                            method: 'POST',
                            url: 'hub/teklif.ashx?&t=14&d=' + new Date(), //[TEKLIFURUNADI_PROC]
                            data: {
                                pageIndex: $scope.teklifurunadi.options.page.index,
                                filterType: $scope.teklifurunadi.filter.type,
                                filter: ctrl.inputValue,
                                pageRowCount: $scope.teklifurunadi.options.maxVisibleRow.value,
                            }
                        }).then(function (response) {
                            $scope.teklifurunadi.options.isLoading = false;
                            app.hide.spinner($('teklifurunadi-dropdown > .option-container'));
                            if (!response.data.hasOwnProperty('RESULT')) {
                                app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                                return;
                            }
                            if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive
                                window.location.href = 'login.aspx';
                                return;
                            }
                            console.log(response.data);
                            console.log(response.data.LIST);
                            if (!extendList) {
                                $scope.teklifurunadi.options.list = response.data.LIST;
                            } else {
                                $scope.teklifurunadi.options.list.push.apply($scope.teklifurunadi.options.list, response.data.LIST);
                            }
                            $scope.teklifurunadi.options.isExist = $scope.teklifurunadi.options.list.length > 0;
                            ctrl.shouldLoadOptionsOnInputFocus = !$scope.teklifurunadi.options.isExist;
                            $scope.teklifurunadi.options.totalRow.set(response.data.TOTALROW);
                            $scope.teklifurunadi.options.paginate();
                        }, function (response) {
                            $scope.teklifurunadi.options.isLoading = false;
                            console.log(response);
                            app.show.error('Bir şeyler ters gitti...');
                            app.hide.spinner($('teklifurunadi-dropdown > .option-container'));
                        });
                    },
                    paginate: function () {
                        $scope.teklifurunadi.options.page.count = $scope.teklifurunadi.options.totalRow.value !== 0 ? Math.ceil($scope.teklifurunadi.options.totalRow.value / $scope.teklifurunadi.options.maxVisibleRow.value) : 1;
                        $scope.teklifurunadi.options.page.isLastPage = $scope.teklifurunadi.options.page.count <= $scope.teklifurunadi.options.page.index;
                    },
                    click: function (teklifurunadi) {
                        $scope.teklifurunadi.shouldInitOnInputChange = true;
                        ctrl.shouldLoadOptionsOnInputFocus = true;
                        ctrl.inputValue = teklifurunadi.TEKLIFURUNADI; // database kolon adı = ADI
                        if (typeof ctrl.onSelect === 'function') {
                            ctrl.onSelect({ teklifurunadi: teklifurunadi });
                        }
                    },
                    more: {
                        load: function () {
                            if ($scope.teklifurunadi.options.page.isLastPage) {
                                return;
                            }
                            $scope.teklifurunadi.options.page.index++;
                            var extendList = true;
                            $scope.teklifurunadi.options.load(extendList);
                        },
                        onfocus: false,
                        focus: function () {
                            $scope.teklifurunadi.options.more.onfocus = true;
                        },
                        blur: function () {
                            $scope.teklifurunadi.options.more.onfocus = false;
                            $scope.teklifurunadi.options.hide();
                        }
                    }
                }
            };
        }],
        bindings: {
            teklifurunadidisabled: '=',
            inputLabel: '@',
            inputMessage: '@',
            componentClass: '@',
            onSelect: '&',
            init: '&',
            inputValue: '=',
            shouldLoadOptionsOnInputFocus: '=',
        }
    });
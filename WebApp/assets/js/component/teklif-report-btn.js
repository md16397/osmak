﻿app.angular
    .controller('teklifReportBtnCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {
        window.addEventListener("keydown", function (event) {
            if (event.defaultPrevented) {
                console.log(0);
                return;
            }
            var handled = false;
            if (event.key !== undefined) {
                if (event.keyCode == 45) {
                    $rootScope.showReportModal();
                }
            } else if (event.keyCode !== undefined) {
                console.log(2);
            }
            if (handled) {
                console.log(3);
                event.preventDefault();
            }
        }, true);
        $scope.cls = 'fab-menu-bottom-right';
        $scope.show = {
            modal: function () {
                $rootScope.showReportModal();
            }
        };
    }])
    .directive('teklifReportBtn', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/teklif-report-btn.html'
        };
    });
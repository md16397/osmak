﻿app.angular
    .controller('ipCreateBtnCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {
        $scope.cls = 'fab-menu-bottom-right';
        $scope.visibility = systemConfig[1]['BOOLEAN'] ? false : true;
        $scope.show = {
            modal: function () {
                $('.ip-create-modal:first').modal({
                    backdrop: 'static'
                });
            }
        };

        $rootScope.setCreateBtnVisibility = function (visibility) {
            $scope.visibility = visibility;
        };
    }])
    .directive('ipCreateBtn', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/ip-create-btn.html'
        };
    });
﻿app.angular
.component('pageNavbar', {
    templateUrl: 'assets/template/page-navbar.html',
    controller: ['$scope', '$http', function ($scope, $http) {
        var ctrl = this;
        $scope.navbar = {
            logout: app.logout,
            toogleMenu:function () {
                $('body').toggleClass('sidebar-xs');
            },
            cls: {
                logo: {
                    a: '',
                    div: ''
                },
                menuBtn: '',
                dropdown: {
                    icon: '',
                    menu: 'dropdown-menu-right',
                    div: 'navbar-right'
                },
                set: function () {
                    $scope.navbar.cls.logo.a = '';
                    $scope.navbar.cls.logo.div = '';
                    $scope.navbar.cls.menuBtn = '';
                    $scope.navbar.cls.dropdown.icon = '';
                    $scope.navbar.cls.dropdown.menu = 'dropdown-menu-right';
                    $scope.navbar.cls.dropdown.div = 'navbar-right';

                }
            }
        };
        $scope.navbar.cls.set();
    }],
    bindings: {
        username: '@',
        profilePath: '@',
        profileName: '@',
        logoutText: '@',
    }
});
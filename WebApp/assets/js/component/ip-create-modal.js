﻿app.angular
    .controller('ipCreateModalCtrl', ['$scope', '$http', '$rootScope', function ($scope, $http, $rootScope) {
       
        $scope.ip = {
            address: {
                isValid: false,
                value: '',
                cls: '',
                msg: '',
                init: function () {
                    $scope.ip.address.isValid = false;
                    $scope.ip.address.value = '';
                    $scope.ip.address.cls = '';
                    $scope.ip.address.msg = '';
                },
                check: function (applyCheck) {
                    var check = app.valid.text($scope.ip.address.value);
                    if (applyCheck || $scope.ip.address.isValid !== check) {
                        $scope.ip.address.isValid = check;
                        if ($scope.ip.address.isValid) {
                            $scope.ip.address.success();
                        } else {
                            $scope.ip.address.error('* Geçersiz IP Adresi');//'* Geçersiz IP Adresi'
                        }
                    }
                    return $scope.ip.address.isValid;
                },
                change: function () {
                    $scope.ip.address.check();
                },
                success: function () {
                    $scope.ip.address.cls = '';
                    $scope.ip.address.msg = '';
                },
                error: function (msg) {
                    $scope.ip.address.cls = 'has-error';
                    $scope.ip.address.msg = msg;
                }
            },
            port: {
                value: '',
                init: function () {
                    $scope.ip.port.value = '';
                }
            },
            protocol: {
                value: '',
                init: function () {
                    $scope.ip.protocol.value = '';
                }
            },
            local: {
                name: 'Genel IP',
                value: false,
                init: function () {
                    $scope.ip.local.select($scope.ip.local.no);
                },
                select: function (local) {
                    $scope.ip.local.name = local.name;
                    $scope.ip.local.value = local.value;
                },
                yes: {
                    name: 'Lokal IP',
                    value: true,
                    anchor: {
                        cls: ''
                    }
                },
                no: {
                    name: 'Genel IP',
                    value: false,
                    anchor: {
                        cls: ''
                    }
                }
            },
            state: {
                name: 'Aktif',
                value: true,
                icon: {
                    cls: 'icon-checkmark-circle2 text-success'
                },
                init: function () {
                    $scope.ip.state.select($scope.ip.state.active);
                },
                select: function (state) {
                    $scope.ip.state.name = state.name;
                    $scope.ip.state.value = state.value;
                    $scope.ip.state.icon.cls = state.icon.cls.replace(/ pull-right/g, '');
                },
                active: {
                    name: 'Aktif',
                    value: true,
                    icon: {
                        cls: 'icon-checkmark-circle2 text-success'
                    },
                    anchor: {
                        cls: ''
                    }
                },
                passive: {
                    name: 'Pasif',
                    value: false,
                    icon: {
                        cls: 'icon-close2 text-danger'
                    },
                    anchor: {
                        cls: ''
                    }
                }
            },
            init: function () {
                $scope.ip.address.init();
                $scope.ip.port.init();
                $scope.ip.protocol.init();
                $scope.ip.local.init();
                $scope.ip.state.init();
            },
            check: function () {
                var applyCheck = true;
                var OK = {
                    address: $scope.ip.address.check(applyCheck)
                };
                return OK.address;
            },
            save: function () {
                if (!$scope.ip.check()) {
                    return;
                }
                app.show.spinner($('ip-create-modal > div:first > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/ip.ashx?&t=3&d=' + new Date(),
                    data: {
                        address: $scope.ip.address.value,
                        port: $scope.ip.port.value,
                        protocol: $scope.ip.protocol.value,
                        local: $scope.ip.local.value,
                        state: $scope.ip.state.value
                    }
                }).then(function (response) {
                    app.hide.spinner($('ip-create-modal > div:first > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 1) {//success
                        $scope.ip.init();
                        app.show.success('Yeni ip oluşturuldu!');//'Yeni ip oluşturuldu!'
                        $rootScope.loadList();
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive or user has no right to create a user
                        window.location.href = 'login.aspx';
                        return;
                    }
                    if (response.data['RESULT'] === 41) {//ip address is invalid
                        $scope.ip.address.isValid = false;
                        $scope.ip.address.error('* Geçersiz IP Adres');//'* Geçersiz IP Adres'
                        return;
                    }
                    //if running code come through this far then it means an unexpected error occurred
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'

                }, function (response) {
                    console.log(response);
                    app.show.error('Bir şeyler ters gitti...');//'Bir şeyler ters gitti...'
                    app.hide.spinner($('ip-create-modal > div:first > div:first'));
                });
            }
        };
    }])
    .directive('ipCreateModal', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/ip-create-modal.html'
        };
    });
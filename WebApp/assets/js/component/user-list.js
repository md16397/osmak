﻿app.angular
    .controller('userListCtrl', ['$scope', '$http', '$timeout', '$rootScope', function ($scope, $http, $timeout, $rootScope) {
        console.log("user-list ---------------->");
        $rootScope.modifyRow = function (user) {
            var i = 0;
            var goon = true;
            while (goon && $scope.user.list.length > i) {
                if (user.ID === $scope.user.list[i].ID) {
                    goon = false;
                    $scope.user.list[i] = user;
                }
                i++;
            }
        };
        $rootScope.loadList = function () {
            $scope.user.filter.init();
            $scope.user.page.index = 1;
            $scope.user.load();
        };
        $scope.user = {
            page: {
                index: 1
            },
            filter: {
                value: '',
                init: function () {
                    $scope.user.filter.value = '';
                },
                change: function () {
                    $scope.user.filter.timer.set();
                },
                timer: {
                    value: null,
                    interval: 500,
                    set: function () {
                        if ($scope.user.filter.timer.value) {
                            $timeout.cancel($scope.user.filter.timer.value);
                            $scope.user.filter.timer.value = null;
                        }
                        $scope.user.filter.timer.value = $timeout(function () {
                            if ($scope.user.isLoading) {
                                return;
                            }
                            $scope.user.page.index = 1;
                            $scope.user.load();
                        }, $scope.user.filter.timer.interval);
                    }
                }
            },
            totalRow: {
                value: 0,
                set: function (value) {
                    var paginate = $scope.user.totalRow.value !== value;
                    $scope.user.totalRow.value = value;
                    if (paginate) {
                        $scope.user.paginate();
                    }
                }
            },
            maxVisibleRow: {
                value: app.pagination.maxVisible.row,
                set: function (value) {
                    var paginate = $scope.user.maxVisibleRow.value !== value;
                    $scope.user.maxVisibleRow.value = value;
                    if (paginate) {
                        $scope.user.paginate();
                    }
                }
            },
            list: [],
            isExist: false,
            isLoading: false,
            load: function () {
                $scope.user.isLoading = true;
                app.show.spinner($('user-list > div:first'));
                $http({
                    method: 'POST',
                    url: 'hub/user.ashx?&t=2&d=' + new Date(),
                    data: {
                        pageIndex: $scope.user.page.index,
                        filter: $scope.user.filter.value,
                        pageRowCount: $scope.user.maxVisibleRow.value
                    }
                }).then(function (response) {
                    $scope.user.isLoading = false;
                    app.hide.spinner($('user-list > div:first'));
                    if (!response.data.hasOwnProperty('RESULT')) {
                        app.show.error(systemMessages[343]);//'Bir şeyler ters gitti...'
                        return;
                    }
                    if (response.data['RESULT'] === 8 || response.data['RESULT'] === 6) {//user is not exist or passive
                        window.location.href = 'login.aspx';
                        return;
                    }
                    $scope.user.list = response.data.LIST;
                    $scope.user.isExist = $scope.user.list.length > 0;
                    $scope.user.totalRow.set(response.data.TOTALROW);
                }, function (response) {
                    $scope.user.isLoading = false;
                    console.log(response);
                    app.show.error(systemMessages[343]);//'Bir şeyler ters gitti...'
                    app.hide.spinner($('user-list > div:first'));
                });
            },
            show: {
                update: function (user) {
                    $rootScope.showModifyModal(user);
                }
            },
            paginate: function () {
                var pageCount = $scope.user.totalRow.value !== 0 ? Math.ceil($scope.user.totalRow.value / $scope.user.maxVisibleRow.value) : 1;
                $('.bootpag-flat').bootpag({
                    total: pageCount,
                    maxVisible: app.pagination.maxVisible.pageIndex,
                    page: $scope.user.page.index,
                    leaps: false
                }).on("page", function (event, num) {
                    $scope.user.page.index = num;
                    $scope.user.load();
                }).children('.pagination').addClass('pagination-flat pagination-sm');
            }
        };
        $scope.user.load();
    }])
    .directive('userList', function () {
        return {
            restrict: 'E',
            templateUrl: 'assets/template/user-list.html'
        };
    });
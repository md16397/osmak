﻿var page = {
    data: {
        breadcrumbs: {
            home: {
                name: 'Anasayfa',
                href: '#'
            },
            links: [],
            active: {
                name: 'Teklif Güncelleme İşlemleri'
            }
        },
        header: {
            name: 'Teklifler',
            detail: 'Teklif Güncelleme İşlemleri'
        },
        olcubirimi: [{
            value: 'Metre',
            name: 'Metre'
        },
        {
            value: 'Kg',
            name: 'Kg'
        },
        {
            value: 'Adet',
            name: 'Adet'
        }],
        olcubirimi2: [{ // boru, mil, flanş ve kovan dışındaki hammade/yarı mamul grubu için Ölçü Birimi sadece 'Adet' listelenmelidir.
            value: 'Adet',
            name: 'Adet'
        }], 
        hammaddeyarimamul: [{
            name: 'HAM MADDE',
            value: 'HAM MADDE',
        }, {
            name: 'YARI MAMUL',
            value: 'YARI MAMUL',
            }],
         materyal: [{
            name: 'Demir (7.86)',
            value: '7.86',
        },
        //{
        //    name: 'DKP Sac',
        //    value: '8',
        //},
        {
            name: 'Paslanmaz Çelik (7.95)',
            value: '7.95',
        },
        //{
        //    name: 'Bakır',
        //    value: '8.96',
        //    },
        //{
        //    name: 'Pirinç',
        //    value: '8.55',
        //},
        {
            name: 'Döküm Çelik (7.2)',
            value: '7.2',
        },
        {
            name: 'Alüminyum (2.72)',
            value: '2.72',
        },
        //{
        //    name: '6061 Alüminyum (AlMg1SiCu)',
        //    value: '2.72',
        //},
        //{
        //    name: '7005 Alüminyum (AlZn4,5Mg1,5Mn)',
        //    value: '2.82',
        //},
        //{
        //    name: '7020 Alüminyum (AlZn4,5Mg1)',
        //    value: '2.82',
        //},
        //{
        //    name: '7075 Alüminyum (AlZn5,5MgCu)',
        //    value: '2.82',
        //},
        //{
        //    name: 'Bronz',
        //    value: '8.8',
        //},
        //{
        //    name: 'Teneke',
        //    value: '7.28',
        //},
        {
            name: 'Krom (7.1)',
            value: '7.1',
        },
        //{
        //    name: 'Altın',
        //    value: '19.36',
        //},
        //{
        //    name: 'Gümüş',
        //    value: '10.5',
        //},
        //{
        //    name: 'Civa',
        //    value: '13.6',
        //},
        //{
        //    name: 'iridyum',
        //    value: '22.6',
        //},
        //{
        //    name: 'Platin',
        //    value: '21.45',
        //},
        //{
        //    name: 'Tungsten',
        //    value: '19.22',
        //},
        //{
        //    name: 'Uranyum',
        //    value: '18.9',
        //},
        //{
        //    name: 'Kurşun',
        //    value: '11.37',
        //},
        //{
        //    name: 'Kobalt',
        //    value: '8.75',
        //},
        //{
        //    name: 'Kadmiyum',
        //    value: '8.64',
        //},
        //{
        //    name: 'Nikrom',
        //    value: '8.3',
        //},
        //{
        //    name: 'Alüminyum Bronzu',
        //    value: '7.7',
        //},
        //{
        //    name: 'Manganez',
        //    value: '7.42',
        //},
        //{
        //    name: 'Kalay',
        //    value: '7.3',
        //},
        //{
        //    name: 'Çinko',
        //    value: '7.1',
        //},
        //{
        //    name: 'Antimon',
        //    value: '6.69',
        //},
        //{
        //    name: 'Galyum',
        //    value: '5.91',
        //},
        //{
        //    name: 'Titanyum',
        //    value: '4.6',
        //},
        //{
        //    name: 'Baryum',
        //    value: '3.6',
        //},
        //{
        //    name: 'Skandiyum',
        //    value: '2.8',
        //},
        //{
        //    name: 'Duralümin',
        //    value: '2.8',
        //},
        //{
        //    name: 'Alüminyum Folyo',
        //    value: '2.73',
        //},
        //{
        //    name: 'Teflon',
        //    value: '2.25',
        //},
        //{
        //    name: 'Beton',
        //    value: '2.1',
        //},
        //{
        //    name: 'Grafit',
        //    value: '2.1',
        //},
        //{
        //    name: 'Paronit',
        //    value: '1.8',
        //},
        //{
        //    name: 'Magnezyum',
        //    value: '1.74',
        //},
        //{
        //    name: 'Nikel',
        //    value: '1.74',
        //},
        //{
        //    name: 'Karbon',
        //    value: '1.7',
        //},
        //{
        //    name: 'Kalsiyum',
        //    value: '1.54',
        //},
        //{
        //    name: 'Delrin',
        //    value: '1.42',
        //},
        //{
        //    name: 'Polioksimetilen',
        //    value: '1.41',
        //},
        //{
        //    name: 'Fiber',
        //    value: '1.4',
        //},
        //{
        //    name: 'Poliasetal-pom',
        //    value: '1.35',
        //},
        //{
        //    name: 'Tekstolit',
        //    value: '1.35',
        //},
        //{
        //    name: 'Kestamid',
        //    value: '1.2',
        //},
        {
            name: 'Poliyamid (1.2)',
            value: '1.2',
        },
            //{
            //    name: 'Polietilen',
            //    value: '1.2',
            //},
            //{
            //    name: 'Poliemit',
            //    value: '1.2',
            //},
            //{
            //    name: 'Kaprolon (PA 6)',
            //    value: '1.15',
            //},
            //{
            //    name: 'Alpolen',
            //    value: '0.96',
            //}

        ],
    }
};
//jQuery.ajaxPrefilter(function (options) { //  TCMB nin sitesindeki XML datasını alabilmek için CORS policy izin verir.
//    if (options.crossDomain && jQuery.support.cors) {
//        options.url = 'https://cors-anywhere.herokuapp.com/' + options.url;
//    }
//});
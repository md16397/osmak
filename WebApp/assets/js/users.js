﻿var page = {
    data: {
        breadcrumbs: {
            home: {
                name: 'Anasayfa',
                href: '#'
            },
            links: [],
            active: {
                name: 'Kullanıcılar'
            }
        },
        header: {
            name: 'Kullanıcılar',
            detail: 'Kullanıcı İşlemleri'
        },
        usertypes: [{
            name: 'Admin',
            value: 0
        }, {
            name: 'Kullanıcı',
            value: 1
        }],
    },
    fn: {
        startpage: {
            filter: function (usertype) {
                if (usertype === null) {
                    return [];
                }
                switch (usertype.value) {
                    case 0://Admin
                        return [{
                            name: 'Teklifler',
                            value: 'teklifler.aspx'
                        }, {
                            name: 'Ham Madde/Yarı Mamul',
                            value: 'urunler.aspx'
                        }, {
                            name: 'Cariler',
                            value: 'cariler.aspx'
                        }, {
                            name: 'Kullanıcılar',
                            value: 'users.aspx'
                        }, {
                            name: 'IP İşlemleri',
                            value: 'ip.aspx'
                        }, {
                            name: 'Profil',
                            value: 'profile.aspx'
                        }];
                    case 1://User
                        return [{
                            name: 'Teklifler',
                            value: 'teklifler.aspx'
                        }, {
                            name: 'Ham Madde/Yarı Mamul',
                            value: 'urunler.aspx'
                        }, {
                            name: 'Cariler',
                            value: 'cariler.aspx'
                        }, {
                            name: 'Profil',
                            value: 'profile.aspx'
                        }];
                    default: return [];
                }
            }
        }
    }
}
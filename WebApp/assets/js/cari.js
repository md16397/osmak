﻿var page = {
    data: {
        breadcrumbs: {
            home: {
                name: 'Anasayfa',
                href: '#'
            },
            links: [],
            active: {
                name: 'Cari İşlemleri'
            }
        },
        header: {
            name: 'Cariler',
            detail: 'Cari İşlemleri'
        }
    }
};
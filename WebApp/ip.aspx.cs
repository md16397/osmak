﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp.controls;

namespace WebApp
{
    public partial class ip : System.Web.UI.Page
    {
        public JSVersion IPJSVersion { get { return controls.SystemCredentials.Versions.IP[((WebApp.Osmak)Master).user.Language]; } }
        public JSVersion ConfigJSVersion { get { return controls.SystemCredentials.Versions.Config; } }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}